/*

  Munipack


  Copyright © 2009-2015 F.Hroch (hroch@physics.muni.cz)

  This file is part of Munipack.

  Munipack is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  
  Munipack is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public License
  along with Munipack.  If not, see <http://www.gnu.org/licenses/>.

*/

#define EMAIL "Filip Hroch <hroch@physics.muni.cz>"
#define COPYLEFT L"© 1997-2015 " EMAIL
#define XVERSION PACKAGE_VERSION
#define DVERSION "\n  DEVELOPMENT VERSION (build: " __DATE__ ", " __TIME__ ")"
#define INFOTEXT PACKAGE_NAME
#define INFOTEXTFULL "A general astronomical image processing software\n" DVERSION
#define HOMEPAGE "http://munipack.physics.muni.cz/"
#define BUGPAGE  "http://code.google.com/p/munipack/issues/list"
