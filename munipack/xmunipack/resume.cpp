/*

  xmunipack - resume of FITS file


  Copyright © 2011-3 F.Hroch (hroch@physics.muni.cz)

  This file is part of Munipack.

  Munipack is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  
  Munipack is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public License
  along with Munipack.  If not, see <http://www.gnu.org/licenses/>.

*/

#include "xmunipack.h"
#include <wx/wx.h>
#include <wx/fs_mem.h>
#include <wx/html/htmlwin.h>
#include <list>


using namespace std;


void MuniResume::OnLinkClicked(wxHtmlLinkEvent& event)
{
  wxString num(event.GetLinkInfo().GetHref());
  long n;
  if( num.ToLong(&n) ) {
    MuniHduEvent event(EVT_HDU,event.GetId());
    event.hdu = n > -1 ? n - 1 : -1;
    wxQueueEvent(GetParent(),event.Clone());
  }
}


MuniResume::MuniResume(wxWindow *w, const MuniConfig *c, const FitsMeta& meta): 
  wxHtmlWindow(w,wxID_ANY), config(c), filename("resume.html")
{
  CreatePage(meta);
  Bind(wxEVT_COMMAND_HTML_LINK_CLICKED,&MuniResume::OnLinkClicked,this);
}

void MuniResume::CreatePage(const FitsMeta& meta)
{
  wxFileSystem::AddHandler(new wxMemoryFSHandler);
  
  wxString page;

  page += "<html><body><h1><a href=\"-1\">" + meta.GetName() + "</a></h1>";

  for(size_t k = 0; k < meta.HduCount(); k++ ) {

    FitsMetaHdu hdu = meta.Hdu(k);
    wxString num;
    num.Printf("%d",int(k+1));
    wxString file("img_" + num + ".png");
    wxString extname(hdu.GetKey("EXTNAME"));
    if( extname.IsEmpty() )
      extname = hdu.Type_str();

    wxMemoryFSHandler::AddFile(file,hdu.GetIcon(),wxBITMAP_TYPE_PNG);
    files.push_back(file);

    page += "<h3><a href=\"" + num + "\">Hdu " + num + ": " + extname + "</a></h3>";
    page += "<table><tr>";
    page += "<td valign=\"top\"><a href=\"" + num + "\"><img src=\"memory:" + file + "\"></img></a></td>";
    page += "<td valign=\"top\"><table>";
    page += "<tr><td><b>Type:</b></td><td>" + hdu.Type_str() + "</td></tr>";

    if( hdu.Type() == HDU_IMAGE ) {

      wxString dim;
      dim.Printf("<td><b>Dimensions:</b></td><td>%d",int(hdu.Naxes(0)));
      for(size_t k = 1; k < hdu.Naxis(); k++) {
	wxString b;
	b.Printf(L"&nbsp;×&nbsp;%d",int(hdu.Naxes(k)));
	dim += b;
      }
      dim += "&nbsp;pixels</td>";
      page += "<tr>" + dim + "</tr>";
    }
    else if( hdu.Type() == HDU_TABLE ) {

      wxString b;
      b.Printf("%d",(int) hdu.Nrows());
      page += "<tr><td><b>Rows:</b></td><td>" + b + "</td></tr>";
      b.Printf("%d",(int) hdu.Ncols());
      page += "<tr><td><b>Columns:</b></td><td>" + b + "</td></tr>";
    }

    if( hdu.Type() == HDU_IMAGE ) {
       
      page += "<tr><td><b>Object:</b></td><td>" + 
	hdu.GetKey(config->fits_key_object) + "</td></tr>";
      FitsTime t(hdu.GetKey(config->fits_key_dateobs));
      page += "<tr><td><b>Date:</b></td><td>" + t.Date() + "</td></tr>";
      page += "<tr><td><b>Time:</b></td><td>" + t.Time() + "</td></tr>";
      page += "<tr><td><b>Spectral&nbsp;Band:</b></td><td>" + 
	hdu.GetKey(config->fits_key_filter) + "</td></tr>";
      page += "<tr><td><b>Exposure&nbsp;Time:</b></td><td>" + 
	hdu.Exposure_str(config->fits_key_exptime) + " sec</td></tr>";
    }
    page += "</td></table>";
  }
  page += "</td></tr></table>";

  page += "<hr>";
  page += "<p>Size: " + meta.GetHumanReadableSize();
  page += ",&nbsp;Location:&nbsp;" + meta.GetPath()  + "</p>";

  page += "</body></html>";

  wxMemoryFSHandler::AddFile(filename,page);
  LoadPage("memory:" + filename);
}

MuniResume::~MuniResume() {
  wxMemoryFSHandler::RemoveFile(filename);
  for(list<wxString>::const_iterator i = files.begin(); i != files.end(); ++i)
    wxMemoryFSHandler::RemoveFile(*i);
}
