/*

  xmunipack - config

  Copyright © 2012 F.Hroch (hroch@physics.muni.cz)

  This file is part of Munipack.

  Munipack is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  
  Munipack is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public License
  along with Munipack.  If not, see <http://www.gnu.org/licenses/>.

*/

#ifndef _XMUNIPACK_ICON_H_
#define _XMUNIPACK_ICON_H_
#endif

#ifndef _FITS_H_
#include "fits.h"
#endif


class MuniIcon
{
public:
  MuniIcon(const FitsFile&, const wxString&, const wxString&,
	   const wxImage&, const wxImage&, const wxImage&, int);
  virtual ~MuniIcon();

  wxImage GetIcon() const;
  std::vector<wxImage> GetList() const;
  wxImage GrayIcon(const FitsArray&, const FitsItt&) const;
  wxImage ColorIcon(const FitsArray&, const FitsItt&, const FitsColor&) const;
  wxImage ColorIcon(const FitsFile&, const FitsItt&, const FitsColor&) const;
  wxImage ImageIcon(const FitsArray&) const;
  wxImage MultiIcon(const std::vector<wxImage>&) const;

  static wxImage BrowserIcon(const wxImage&,int,int,
			     const wxString& =wxEmptyString, 
			     const wxColour& =*wxWHITE);
  static wxImage DefaultIcon(int,int);
  static wxImage SymbolIcon(const wxImage&,int,int);
  static wxImage ListIcon(const wxImage&,int,
			  const wxColour& =wxColour(255,255,255));
  static wxImage BulletIcon(const wxSize&, const wxColour&);

private:

  const FitsFile fits;
  wxImage icon;
  std::vector<wxImage> list;
  wxString display_colorspace,cdatafile;
  wxImage default_icon, table_icon, head_icon;
  int icon_size;

};

