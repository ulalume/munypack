/*

  XMunipack - derived events

  Copyright © 2009-2012 F.Hroch (hroch@physics.muni.cz)

  This file is part of Munipack.

  Munipack is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  
  Munipack is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public License
  along with Munipack.  If not, see <http://www.gnu.org/licenses/>.

*/


#define _XMUNIPACK_EVENT_H_

#ifndef _XMUNIPACK_TYPES_H_
#include "types.h"
#endif


/*

  Definition of new events. Adds some data structures to standard ones.

*/



wxDECLARE_EVENT(EVT_CONFIG_UPDATED, wxCommandEvent);
wxDECLARE_EVENT(EVT_FILELOAD, wxCommandEvent);
wxDECLARE_EVENT(EVT_FINISH_DIALOG, wxCommandEvent);



// -- Slew ---
// positions of zoomed areas

class MuniSlewEvent: public wxEvent
{
public:
  MuniSlewEvent(wxEventType eventType =wxEVT_NULL, int winid =wxID_ANY):
    wxEvent(winid, eventType),z_x(0),z_y(0),i_x(0),i_y(0) {}
  virtual wxEvent *Clone(void) const { return new MuniSlewEvent(*this); }
  int z_x, z_y, i_x, i_y;
  FitsBitmap picture;
};

wxDECLARE_EVENT(xEVT_SLEW,MuniSlewEvent);

#define MuniSlewEventHandler(func) (&func)

// -- Tune

class MuniTuneEvent: public wxCommandEvent
{
 public:
  MuniTuneEvent(WXTYPE e =wxEVT_NULL, int id =wxID_ANY): wxCommandEvent(e,id),x(0.0),index(0),fit(true) {}
  MuniTuneEvent(wxCommandEvent& e): wxCommandEvent(e),x(0.0),index(0),fit(true) {}
  wxEvent *Clone(void) const { return new MuniTuneEvent(*this); }
  double x;
  int index;
  bool fit;
};

wxDECLARE_EVENT(EVT_TUNE,MuniTuneEvent);

#define MuniTuneEventHandler(func) (&func)

// -- Astrometry

class MuniAstrometryEvent: public wxCommandEvent
{
 public:
  MuniAstrometryEvent(WXTYPE e =wxEVT_NULL, int id =wxID_ANY): wxCommandEvent(e,id),
    scale(0.0),angle(0.0),xcen(0.0),ycen(0.0),acen(0.0),dcen(0.0),astrometry(false) {}
  MuniAstrometryEvent(wxCommandEvent& e): wxCommandEvent(e),
    scale(0.0),angle(0.0),xcen(0.0),ycen(0.0),acen(0.0),dcen(0.0),astrometry(false) {}
  wxEvent *Clone(void) const { return new MuniAstrometryEvent(*this); }
  wxString proj;
  double scale,angle,xcen,ycen,acen,dcen;
  bool astrometry;
  MuniLayer layer;
};

wxDECLARE_EVENT(EVT_ASTROMETRY,MuniAstrometryEvent);

//#define MuniAstrometryEventHandler(func) (&func)


// -- Photometry

class MuniPhotometryEvent: public wxCommandEvent
{
 public:
 MuniPhotometryEvent(WXTYPE e =wxEVT_NULL, int id =wxID_ANY): wxCommandEvent(e,id),erase(false) {}
  MuniPhotometryEvent(wxCommandEvent& e): wxCommandEvent(e),erase(false) {}
  wxEvent *Clone(void) const { return new MuniPhotometryEvent(*this); }
  bool erase;
  MuniLayer layer;
};

wxDECLARE_EVENT(EVT_PHOTOMETRY,MuniPhotometryEvent);

// -- Hdu

class MuniHduEvent: public wxCommandEvent
{
 public:
  MuniHduEvent(WXTYPE e =wxEVT_NULL, int id =wxID_ANY): wxCommandEvent(e,id),hdu(-1) {}
  MuniHduEvent(wxCommandEvent& e): wxCommandEvent(e),hdu(-1) {}
  wxEvent *Clone(void) const { return new MuniHduEvent(*this); }
  int hdu;
};

wxDECLARE_EVENT(EVT_HDU,MuniHduEvent);


// -- Thread

// pssing of rendered image in thread

class MuniRenderEvent: public wxThreadEvent
{
 public:
  MuniRenderEvent(WXTYPE commandEventType =wxEVT_NULL, int id =wxID_ANY): 
    wxThreadEvent(commandEventType,id),x(0),y(0) {}
  MuniRenderEvent(wxThreadEvent& e): wxThreadEvent(e),x(0),y(0) {}
  wxEvent *Clone(void) const { return new MuniRenderEvent(*this); }
  FitsBitmap picture;
  FitsImage image;
  int x,y,x0,y0,width,height;
};

wxDECLARE_EVENT(EVT_RENDER,MuniRenderEvent);


// -- fits open

// passing of loaded Fits and Meta

class FitsOpenEvent: public wxThreadEvent
{
 public:
 FitsOpenEvent(WXTYPE commandEventType =wxEVT_NULL, int id =wxID_ANY): wxThreadEvent(commandEventType,id) {}
  FitsOpenEvent(wxThreadEvent& e): wxThreadEvent(e) {}
  wxEvent *Clone(void) const { return new FitsOpenEvent(*this); }
  wxString filename;
  FitsFile fits;
  FitsMeta meta;
};


wxDECLARE_EVENT(EVT_FITS_OPEN,FitsOpenEvent);

#define MuniFitsOpenEventHandler(func) (&func)


// -- meta open

// passing of loaded Meta

class MetaOpenEvent: public wxThreadEvent
{
public:
  MetaOpenEvent(WXTYPE commandEventType =wxEVT_NULL, int id =wxID_ANY): 
    wxThreadEvent(commandEventType,id) {}
  MetaOpenEvent(wxThreadEvent& e): wxThreadEvent(e) {}
  wxEvent *Clone(void) const { return new MetaOpenEvent(*this); }
  FitsMeta meta;
};

wxDECLARE_EVENT(EVT_META_OPEN,MetaOpenEvent);

#define MuniMetaOpenEventHandler(func) (&func)

// -- dialog

// close event of darkbat dialog

class MuniDialogEvent: public wxCommandEvent
{
 public:
 MuniDialogEvent(WXTYPE commandEventType =wxEVT_NULL, int id =wxID_ANY): 
  wxCommandEvent(commandEventType,id), return_code(0) {}
  MuniDialogEvent(wxCommandEvent& e): wxCommandEvent(e), return_code(0) {}
  wxEvent *Clone(void) const { return new MuniDialogEvent(*this); }
  wxArrayString commands,results,params,outputs;
  int return_code;
};

wxDECLARE_EVENT(EVT_DIALOG,MuniDialogEvent);

#define MuniDialogEventHandler(func) (&func)

