/*

  xmunipack - zoom

  Copyright © 2009-2012 F.Hroch (hroch@physics.muni.cz)

  This file is part of Munipack.

  Munipack is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  
  Munipack is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public License
  along with Munipack.  If not, see <http://www.gnu.org/licenses/>.

*/

#include "xmunipack.h"
#include "tune.h"
#include <wx/wx.h>
#include <wx/arrstr.h>


// --- FitsZoom

FitsZoom::FitsZoom(double z): zoom(z) {}

//const double FitsZoom::ratios[]={0.1,0.25,0.5,0.75,1.0,1.25,2.0,4.0,10.0,-1.0};
const double FitsZoom::ratios[]={0.01,0.05,0.1,0.2,0.3,0.4,0.5,0.6,0.7,0.8,0.9,1.0,
                                 1.1,1.2,1.3,1.4,1.5,1.6,1.7,1.8,1.9,2.0,
                                 2.1,2.2,2.3,2.4,2.5,2.6,2.7,2.8,2.9,3.0,
				 4.0,5.0,6.0,7.0,8.0,9.0,10.0,-1.0};


double FitsZoom::Zoom() const
{
  return zoom;
}

size_t FitsZoom::GetCount()
{
  int n = 0;
  for(int i = 0; ratios[i] > 0.0; i++)
    n++;
  return n;
}

double FitsZoom::FindGreater(double z)
{
  int n = GetCount();

  for(int i = 1; i < n; i++) {
    double x = 0.5*(ratios[i-1] + ratios[i]);
    if( z < x )
      return ratios[i];
  }
  return ratios[n-1];
}

double FitsZoom::FindSmaller(double z)
{
  int n = GetCount();

  for(int i = n-1; i > 1; i--) {
    double x = 0.5*(ratios[i-1] + ratios[i]);
    if( z > x )
      return ratios[i-1];
  }
  return ratios[0];
}
