
/*

  XMunipack - plotting


  Copyright © 2012 - 14  F.Hroch (hroch@physics.muni.cz)

  This file is part of Munipack.

  Munipack is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  
  Munipack is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public License
  along with Munipack.  If not, see <http://www.gnu.org/licenses/>.

*/

#ifndef _XMUNIPACK_PLOT_H_
#define _XMUNIPACK_PLOT_H_
#endif

#ifndef _FITS_H_
#include "fits.h"
#endif


#include "mathplot.h"
#include <wx/wx.h>
#include <vector>


/*
class MuniPlotTable
{
public:
  MuniPlotTable(const std::vector<wxRealPoint>&,const wxColour& =*wxBLACK);

  std::vector<wxRealPoint> points;
  wxColour colour;
  
};
*/

// class MuniPlot: public mpWindow
// 		//class MuniPlot: public wxPLplotwindow
// {
// public:
//   MuniPlot(wxWindow *);
//   void AddData(const MuniPlotTable&);

// private:
  
//   std::vector<MuniPlotTable> tables;
//   double xmin,xmax,ymin,ymax;

//   //  void OnSize(wxSizeEvent&);
//   void Draw();

// };


class MuniPlotHisto: public mpWindow
{
public:
  MuniPlotHisto(wxWindow *, bool =true);
  wxSize DoGetBestSize() const;

  void SetHisto(const std::vector<FitsHisto>&);
  void SetItt(const FitsItt&);
  void SetHisto(const FitsHisto&);

private:
  
  FitsItt itt;
  bool itt_plot, axes;
  std::vector<FitsHisto> hlist;
  int hmax;
  double xmin,xmax;
  wxSize bestsize;

};

/*
class PlotNite: public wxPLplotwindow
{
public:
  PlotNite(wxWindow *);
  wxSize DoGetBestSize() const;

  void SetColor(const FitsColor&);
  void SetXrange(double, double);

private:
  
  FitsColor color;
  float xmin,xmax;

  void Plot();
};
*/


/*
class MuniPlotUV: public wxPLplotwindow
{
public:
  MuniPlotUV(wxWindow *);
  virtual ~MuniPlotUV();
  wxSize DoGetBestSize() const;
  void DrawTri(const std::vector<double>&, const std::vector<double>&);
  void Clear();

private:

  int nuv;
  PLFLT *u,*v;
  void Draw();
  
};
*/
