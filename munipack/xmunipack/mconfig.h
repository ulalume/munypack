/*

  xmunipack - config

  Copyright © 2012-5 F.Hroch (hroch@physics.muni.cz)

  This file is part of Munipack.

  Munipack is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  
  Munipack is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public License
  along with Munipack.  If not, see <http://www.gnu.org/licenses/>.

*/

#ifndef _XMUNIPACK_CONFIG_H_
#define _XMUNIPACK_CONFIG_H_
#endif

#ifndef _XMUNIPACK_ENUM_H_
#include "enum.h"
#endif

#ifndef _FITS_H_
#include "fits.h"
#endif

#ifndef _XMUNIPACK_ICON_H_
#include "icon.h"
#endif

#include <wx/config.h>
#include <wx/animate.h>
#include <wx/artprov.h>



class MuniArtIcons 
{
public:
  MuniArtIcons(const wxArtClient& =wxART_OTHER, const wxSize& =wxDefaultSize);
  wxBitmap Icon(const wxArtID&) const;

private:
  const wxArtClient client;
  const wxSize size;
};


class MuniConfig: public wxConfig
{
public:
  MuniConfig();
  virtual ~MuniConfig();

  wxSize browser_size, view_size, help_size, console_size, colorization_size, header_size;
  int icon_size,icon_small,icon_zoom;
  int scroll_rate,detail_zoom,detail_scale;
  int display_itt,display_pal,display_coo,display_val;
  int browser_labeltype,browser_sorttype, browser_iconlist;
  bool astrometry_fullmatch;
  int astrometry_minmatch, astrometry_maxmatch;
  double astrometry_sig, astrometry_fsig;
  double photo_fwhm, photo_thresh, photo_satur, photo_readns, photo_phpadu;
  wxString astrometry_proj, astrometry_units;
  bool browser_reverse;
  int browser_sash;
  int icon_menu_width,icon_menu_height;
  double display_zoom,color_temperature,color_uwhitepoint,color_vwhitepoint;
  bool browser_collector, browser_tbar, view_tbar, display_nvision, console_wrap;
  bool display_grid, display_sources;
  wxString dirmask, rawmask, fits_key_object, fits_key_dateobs, fits_key_exptime,
    fits_key_filter, fits_key_observer, fits_key_gain, fits_key_area,
    browser_labelkey, browser_sortkey,display_colorspace,cdatafile,phsystemfile;
  wxString confdir, backup_suffix;
  wxIcon munipack_icon;
  wxImage default_icon, table_icon, head_icon;
  wxAnimation throbber;

  static wxString FindIconPath(const wxString& name);
  static wxIcon LoadIcon(const wxString&);
  static wxImage LoadImage(const wxString&);

};
