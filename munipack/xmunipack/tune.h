/*

  xmunipack - common for View & Tune


  Copyright © 1997-2009 F.Hroch (hroch@physics.muni.cz)

  This file is part of Munipack.

  Munipack is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  
  Munipack is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public License
  along with Munipack.  If not, see <http://www.gnu.org/licenses/>.

*/


#ifndef _XMUNIPACK_TUNE_H
#define _XMUNIPACK_TUNE_H
#endif

#ifndef _FITS_H_
#include "fits.h"
#endif

#include <wx/wx.h>


class MuniTuneAdjuster: public wxControl
{
public:
  MuniTuneAdjuster() {}
  MuniTuneAdjuster(wxWindow *,wxWindowID =wxID_ANY,double =0.0,double =0.0,
		   double =100.0, double =1.0, unsigned int =0,
		   const wxString& =wxEmptyString, const wxString& =wxEmptyString);
  MuniTuneAdjuster(const MuniTuneAdjuster&);
  MuniTuneAdjuster& operator = (const MuniTuneAdjuster&);

  void SetToolTip(const wxString&);
  void SetValue(double);

private:

  wxSlider *slider;
  wxSpinCtrlDouble *entry;
  wxString text;

  double xmin, xmax, x, step;
  int imin, imax, i;

  void InitRange();

  void Scroll(int);
  void Entry(const wxString&);
  void SetSlider(double);
  void OnScroll(wxScrollEvent&);
  void OnScrollFinish(wxScrollEvent&);
  void OnEntry(wxCommandEvent&);
  void OnEntryFinish(wxCommandEvent&);
  void OnSpinDouble(wxSpinDoubleEvent&);

};

class MuniTune: public wxFrame
{
public:
  MuniTune(wxWindow *, wxWindowID, const wxPoint&, const wxSize&, MuniConfig *,
	   const FitsArray&, const FitsItt&, const FitsPalette&);
  MuniTune(wxWindow *, wxWindowID, const wxPoint&, const wxSize&, MuniConfig *,
	   const FitsArray&, const FitsItt&, const FitsColor&);

private:

  MuniConfig *config;
  FitsArray array;
  FitsItt itt,itt_original;
  FitsPalette pal,pal_original;
  FitsColor color,color_original;
  FitsImage fimage;
  FitsDisplay display;
  wxWindow *mini;

  wxNotebookPage *chpanel;
  wxListView *chlist;
  wxChoice *type_itt;
  wxCheckBox *negcheck, *ncheck;
  MuniLUTus *lutus;
  wxChoice *lutch;
  wxStaticText *chlabel;
  MuniTuneAdjuster *adjzoom,*adjblack,*adjslope,*adjamount,*adjzero,
    *adjsatur,*adjnthresh,*adjmeso,*adjhuee,*adjwhiteu,*adjwhitev,
    *adjmean,*adjweight;
  MuniTuneEvent *etune;


  void Render();
  void OnIdle(wxIdleEvent&);
  void OnScroll(wxScrollEvent&);
  void OnScrollFinish(wxScrollEvent&);
  void OnChoiceItt(wxCommandEvent&);
  void OnChoicePal(wxCommandEvent&);
  void OnCheckNeg(wxCommandEvent&);
  void OnCheckNight(wxCommandEvent&);
  void OnAbsItt(wxCommandEvent&);
  void OnRelItt(wxCommandEvent&);
  void OnClose(wxCloseEvent&);
  void OnReset(wxCommandEvent&);
  void OnTuneFine(MuniTuneEvent&);

  void OnUpdateIttpar(wxUpdateUIEvent&);
  void OnUpdateNightadj(wxUpdateUIEvent&);
  void OnUpdateChannels(wxUpdateUIEvent&);
  void OnChangeChannel(wxListEvent&);

  MuniTuneAdjuster *CreateIttBlack(wxWindow *, const FitsItt&);
  MuniTuneAdjuster *CreateIttContrast(wxWindow *, const FitsItt&);
  MuniTuneAdjuster *CreateIttAmount(wxWindow *, const FitsItt&);
  MuniTuneAdjuster *CreateIttZero(wxWindow *, const FitsItt&);
  wxNotebookPage *CreateIttTab(wxNotebook *, const FitsItt&);

};




class FitsZoom
{
public:
  FitsZoom(double);
  
  double Zoom() const;
  static size_t GetCount();
  static double FindGreater(double);
  static double FindSmaller(double);

private:
  double zoom;
  static const double ratios[];
};


