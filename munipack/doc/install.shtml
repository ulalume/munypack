<!DOCTYPE HTML>
<html lang="en">
<head>
<!-- #include virtual="/meta.shtml" -->

<title>Munipack ‒ Installation</title>
</head>
<body>
<!-- #include virtual="/head.shtml" -->

<h1 class="noindent">Installation</h1>

<p class="abstract">
This page describes installation under recent
<a href="#ubuntu">Ubuntu</a>, <a href="#debian">Debian</a>
and <a href="#fedora">Fedora.</a>
</p>

<p>
For another operating system, try build yourself: bundle installation 
for <a href="debbuilder.html">DEB based distributions</a>,
<a href="rpmbuilder.html">RPM based distributions</a> and
<a href="SourceInstallation.html">Source Installation</a>.
</p>

<p>
The installation will issue very strong (☠) warning about unsigned archive
of Munipack. The warning can be safety ignored without worries.
</p>

<h2><a id="ubuntu">Ubuntu</a></h2>

<p>
<img class="symbol" src="ubuntu_icon.png" width="54" height="55" alt="ubuntu icon">
</p>

<p>
The installation under Ubuntu 14.04 and above (shipped with wxWidgets
3.0 and newest) is easy. Append following lines to your
<samp>/etc/apt/sources.list</samp>:</p>
<pre>
# Munipack repositories (binary and sources)
deb http://munipack.physics.muni.cz/ubuntu/ trusty/
deb-src http://munipack.physics.muni.cz/ubuntu/ trusty/
</pre>
<p>
and update the package database on your Ubuntu and install 
an appropriate part of Munipack (or simply both command-line 
and graphics clients):
</p>
<pre>
$ sudo apt-get update
$ sudo apt-get install munipack-cli munipack-gui
</pre>

<p>
For older versions of Ubuntu, modify the <samp>sources.list</samp>
according to your version name (see directories in 
<a href="http://munipack.physics.muni.cz/ubuntu">link</a> to get available ones)
and append wxWidgets by the instructions on CodeLite page 
<a href="http://codelite.org/LiteEditor/WxWidgets30Binaries">wxWidgets 3.0 
Packages and Repositories</a>.
</p>

<h2><a id="debian">Debian</a></h2>

<p>
<img class="symbol" src="debian_icon.png" width="64" height="64" alt="debian icon">
</p>

<p>
The installation under Debian requires wxWidgets in version 3.0 and
above because Munipack depends on its. Installation both the components
is straightforward. Append following lines to your
<samp>/etc/apt/sources.list</samp>:</p>
<pre>
# Munipack repositories (binary and sources)
deb http://repos.codelite.org/wx3.0.2/debian/ wheezy libs
deb http://munipack.physics.muni.cz/debian/ wheezy/
deb-src http://munipack.physics.muni.cz/debian/ wheezy/
</pre>
<p>
and update the package database on your Debian and install 
an appropriate part of Munipack
(or simply both command-line and graphics clients) as root:
</p>
<pre>
# apt-get update
# apt-get install munipack-cli munipack-gui
</pre>

<p>
For older versions of Debian, modify the <samp>sources.list</samp>
according to your version (see directories in 
<a href="http://munipack.physics.muni.cz/debian/">link</a> to get available ones).
</p>

<h2><a id="fedora">Fedora</a></h2>

<p>
<img class="symbol" src="fedora_icon.png" width="64" height="64" alt="fedora icon">
</p>

<p>
Recent versions of Fedora 21+ includes wxWidgets in
version 3.0 and above. The packages can be found at link:
</p>
<p class="download">
<a href="ftp://munipack.physics.muni.cz/pub/munipack/fedora/21">ftp://munipack.physics.muni.cz/pub/munipack/fedora/21</a>
</p>
<p>
Download all <samp>*.rpm</samp> for install:
</p>
<pre>
# rpm -i munipack-*.rpm
</pre>


<h2>See Also</h2>
<p>
<a href="download.html">Download</a>,
   <a href="version.html">Versioning</a> and <a href="guide.html">Guide</a>.
</p>

<!-- #include virtual="/foot.shtml" -->
</body>
</html>
