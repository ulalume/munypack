#!/bin/bash

# This script creates light curves and other related
# products by using Munipack data for 0716
#
# See detailed description in lctut.html

set -x

# data
mkdir ~/tmp
cd ~/tmp
wget ftp://munipack.physics.muni.cz/pub/munipack/munipack-data-0716.tar.gz
tar zxf munipack-data-0716.tar.gz
cd munipack-data-0716/

# preprocessing
munipack dark -o d30.fits d30_*.fits
munipack dark -o d120.fits d120_*.fits
munipack flat -o f_V.fits -dark d30.fits f30_*V.fits
munipack flat -o f_R.fits -dark d30.fits f30_*R.fits
munipack phcorr -dark d120.fits -flat f_V.fits 0716_*V.fits
munipack phcorr -dark d120.fits -flat f_R.fits 0716_*R.fits

# aperture photometry and star detection
munipack find -f 3 0716_*.fits
munipack aphot 0716_*.fits

# astrometry calibration
munipack cone -r 0.2 110.5 71.3
munipack astrometry -c cone.fits 0716_*.fits

# photometry calibration
wget http://munipack.physics.muni.cz/0716+71.lst
wget http://munipack.physics.muni.cz/ucac_jmuc.py
munipack fits --restore 0716+71.lst
python ucac_jmuc.py cone.fits cone_Johnson.fits

for FILTER in V R; do

  # manual
  for A in 0716_*${FILTER}.fits; do
    munipack phcal -C 1 --photsys-ref Johnson -f ${FILTER} \
        -O --mask '\1_mancal.\2' $A; 
  done

  # catalogue
  for A in 0716_*${FILTER}.fits; do
      munipack phcal \
        --photsys-ref Johnson --area 0.3 \
        -f ${FILTER} --col-mag ${FILTER} --col-magerr ${FILTER}ERR  \
        -c 0716+71.fits \
        -O --mask '\1_catcal.\2' \
        $A; 
  done

  # reference frame
  for A in 0716_*${FILTER}.fits; do
      munipack phcal \
        --photsys-ref Johnson --area 0.3 \
        -f ${FILTER}  \
        -r 0716_006${FILTER}_catcal.fits -O --mask '\1_refcal.\2' $A; 
  done

  # ucac
  for A in 0716_*${FILTER}.fits; do
      munipack phcal \
        --photsys-ref Johnson --area 0.3 --col-ra RAJ2000 --col-dec DEJ2000 \
        -f ${FILTER} --col-mag ${FILTER}mag --col-magerr e_${FILTER}mag  \
        -c cone_Johnson.fits \
        -O --mask '\1_ucacal.\2' \
        $A; 
  done

done

# light curves
for FILTER in V R; do
   for TYPE in man cat ref uca; do
      munipack timeseries 110.473,71.343 110.389,71.322 110.468,71.305 \
          -c MAG,MAGERR --stdout  0716_*${FILTER}_${TYPE}cal.fits > ${TYPE}cal_${FILTER}
   done
done

# plots
gnuplot <<EOF
set key bottom left
set title "0716+71"
set ylabel "magnitude in V"
set xlabel "JD - 2453409"
set yrange[14.5:13.7]
set term svg dynamic
set output 'lc0716_V.svg'
plot 'mancal_V' u (\$1-2453409):(\$2-4.5) t "instrumental - 4.5", 'catcal_V' u (\$1-2453409):(\$2-0.1) t "std.field - 0.1", 'refcal_V' u (\$1-2453409):2 t "frame", 'ucacal_V' u (\$1-2453409):(\$2+0.1) t "UCAC4 + 0.1"
EOF


gnuplot <<EOF
set key top left
set title "Calibration stars"
set ylabel "magnitude in V"
set xlabel "JD - 2453409"
set yrange[11.9:11.2]
set term svg dynamic
set output 'comp0716_V.svg'
plot 'mancal_V' u (\$1-2453409):(\$4-\$6+12.65) t "difference", 'catcal_V' u (\$1-2453409):(\$4-0.1) t "std.field - 0.1", 'refcal_V' u (\$1-2453409):4 t "frame", 'ucacal_V' u (\$1-2453409):(\$4+0.1) t "UCAC4 + 0.1"
EOF

