<!DOCTYPE HTML>
<html lang="en">
<head>
<!-- #include virtual="/meta.shtml" -->
<title>Munipack ‒ Astrometry</title>
</head>
<body>
<!-- #include virtual="/head.shtml" -->

<h1>Astrometry</h1>

<p class="abstract">
An astrometrical calibration of FITS frames.
</p>

<h2>Synopsis</h2>

<code>
munipack astrometry [.. parameters ..] file(s)[,result(s)]
</code>

<h2>Description</h2>

<p>
Astrometry action derives astrometry calibration of FITS frames 
(<a href="astoverview.html">Overview</a>).
</p>

<p>
Astrometry is naturally separated on two parts:
</p>
<ul>
<li>Matching where the correspondence between stars on frame
    and in catalogue is established. The matching can be done
    without knowledge of any transformation, when very general
    presumptions are supposed (the coordinates are Euclidean).
</li>
<li>
Astrometry transformation is computed by using of robust algorithms.
</li>
</ul>


<h2>Algorithm</h2>

<p>
Lets we denote the reference star coordinates as the catalogue and
coordinates of detected stars as the frame.
</p>

<p>
The following algorithm is used for matching:
</p>
<ul>
<li>Catalogue coordinates are projected to rectangular. The projection
    can be set with <samp>-p,--projection</samp> switch. As the center
    of projection centroid of all stars computed as arithmetical mean is selected.
</li>
<li>
Frame coordinates are prescaled to lie in interval 0 .. 1 and to have origin 
in center of frame (the projected and measured coordinates has similar
scale and origin after this transformation to improve numerical precision).
</li>
<li>
Twines of stars in catalogue and in frame are get as the starting step
of matching algorithm.
</li>
<li>
To get a next star in sequence, the following
function (in a meta-language) illustrates the principle:
<pre>
Function Sequence for next star
  For all unused stars in catalogue:
     Compute u1,v1
     For all unused stars on image:
        Compute u2,v1
        Is Acceptable and distance({u1,u2} and {v1,v2}) &lt; limit?
            Has the sequence required length?
              Got Solution!
            Call Sequence for next+1 star
            Did fail the Sequence?
              Skip the star 
End function Sequence
</pre>
Variables u1,v1 and u2,v2 are coordinates of in a triangle space
of a triplet of stars. The distance is distance in triangle space.
</li>
<li>
Implemented algorithm includes additional tests to be sequence acceptable:
<ul>
<li>the sequence length and limit for objects used to matching
     can be changed by <samp>--minmatch, --maxmatch</samp> parameters</li>
<li>The limit is set via <samp>--sig</samp> parameter which sets <i>σ</i>
    expected error in the coordinates of stars. The parameter is also
    used for estimation of tolerances of angles.</li>
<li>Angles computed between all stars in a real space must be inside
    tolerance.
</li>
<!--
<li>Ratios between counts and photons for all stars must be inside 
interval given by <samp>--fsig</samp>. The default value is set
to by acceptable for different filters is optical region.
</li>
-->

<li>the sequence must have first and last stars connected (all implemented
     tests must be passed).</li>
<li>To prevent use of degenerated triangles (for sides <i>a+b≈c</i>), 
    only triangles with the <i>v > 1 + σ - u</i> are used.
</ul>
</li>
<li>
The first sequence, which is successful found, stops matching. When 
<samp>--full-match</samp> is used, it is scan throughout
all possible twines. In this case, the sequence with the best
parameters is used. The full scan can take a lot of time.
</li>
</ul>

<p>
An alternative sequence can be constructed from known transformation
and the astrometry is just more precise.
</p>


<p>
The known sequence of stars can be used to determine astrometry transformation:
</p>
<ul>
<li>The coordinates of catalogue stars are corrected for its proper motion 
    (only when <samp>--col-pm-ra, --col-pm-dec</samp>) is set</li>
<li>Initial estimation of parameters is computed by minimizing of absolute
    deviations.</li>
<li>The corresponding stars in catalogue and frame are found by searching
    for stars in close neighborhood (practically, the <samp>--match nearly</samp>
    is performed.</li>
<li>The transformation is determined with all possibles stars by the robust
    minimization.</li>
</ul>

<p>
The matching needs at least of tree stars (to construct of a triangle. The
astrometry algorithm needs at least 5 stars (for 4 parameters) but
has lower-count alternative with minimum of two stars. Generally, 
the astrometry has recommended minimum of 7 stars, but ideal is over 20.
Any modern (like Munipack default UCAC4) catalogue has more reference
objects on medium crowded fields.
</p>

<h2>Prerequisites</h2>
<p>
The calibration needs both detected stars and instrumental photometry.
</p>

<h2>Input And Output</h2>

<p>
On input, list of frames containing the table with already detected stars 
and photometry is expected.
</p>

<p>
On output, the WCS calibration in the header of primary FITS is created 
(or updated) for all input images.
</p>

<h2>Parameters</h2>

<h3>Reference sources:</h3>
<dl>
<dt><samp>-c, --cat</samp></dt><dd>reference catalogue in FITS format</dd>
<dt><samp>-r, --ref</samp></dt><dd>reference frame (already calibrated frame)</dd>
<dt><samp>-R, --rel</samp></dt><dd>relative to the reference frame (no projection)</dd>
<dt><samp>--col-ra</samp></dt><dd>Right Ascension column in catalogue</dd>
<dt><samp>--col-dec</samp></dt><dd>Declination column in catalogue</dd>
<dt><samp>--col-pm-ra</samp></dt><dd>Proper motion in Right Ascension column in catalogue</dd>
<dt><samp>--col-pm-dec</samp></dt><dd>Proper motion in Declination column in catalogue</dd>
<dt><samp>--col-mag</samp></dt><dd>Magnitude-like column in catalogue</dd>
</dl>

<h3>Astrometry Parameters:</h3>
<dl>
<dt><samp>-p, --projection</samp></dt><dd>projection: none, gnomonic (default)</dd>
<dt><samp>--xcen</samp></dt><dd>center of projection on chip [pix] (default: width/2)</dd>
<dt><samp>--ycen</samp></dt><dd>center of projection on chip [pix] (default: height/2)</dd>
<dt><samp>--rcen</samp></dt><dd>center of projection in Right Ascension [deg]</dd>
<dt><samp>--dcen</samp></dt><dd>center of projection in Declination [deg]</dd>
<dt><samp>--scale</samp></dt><dd>scale [pix/deg]</dd>
<dt><samp>--angle</samp></dt><dd>angle of rotation [deg], clockwise positive</dd>
<dt><samp>--reflex</samp></dt><dd>set whatever the frame is reflected</dd>
</dl>

<h3>Parameters For Fit:</h3>
<dl>
<dt><samp>--fit</samp></dt><dd>method used for fitting of star positions: squares 
   (standard least-squares) or robust (by default)</dd>
<dt><samp>--match</samp></dt><dd>method used for initial matching: backtracking (default) 
   or nearly</dd>
</dl>

<h3>Parameters For Matching:</h3>
<dl>
<dt><samp>--sig</samp></dt><dd>typical uncertainity in coordinates of objects
                                on frames in pixels,default is 1 pixel</dd>
<dt><samp>--sigcat</samp></dt><dd>typical uncertainity in coordinates of objects
                                in catalogue in degres, default is 1 arcsec</dd>
<dt><samp>--fsig</samp></dt><dd>flux errors, default is 1
which fits common observation conditions (clouds, wrong filter). Very bad
bad observations may require larger values.
The parameter significantly affects matching speed.
</dd>
<dt><samp>--minmatch</samp></dt><dd>Sets count of objects in match sequence. 
Default is 5. Crowded fields will require increase the value on 7 or more. The extremely
sparse fields with a few stars only will enough 3-5. To use match algorithm minimal 
length is 3. Upper limit is given by <samp>--maxmatch</samp>.</dd>
<dt><samp>--maxmatch</samp></dt><dd>Set maximum count of objects for matching. 
 The default is 50 or count of objects in catalogue or in frame. There is no upper
 limit, but values over hundredth are probably unusable. The recommended value
 for crowded field is 50 - 100.</dd>
<dt><samp>--disable-flux-check</samp></dt><dd>To improve speed and reliability
of matching, fluxes are used as the additional independent quantity
for checking. This switch disables flux check completely. A possibility
of mismatching increases, especially on crowded fields.
</dd>
<dt><samp>--disable-rms-check</samp></dt><dd>Normally, RMS (in pixels) is smaller that 
xsig*dig (in pixels) for successful fit and one is on condition for the calibration. This
option disables the test and must be used very carefully. Useful in cases when 
statistical errors are insignificant to  systematical ones (due to an improper
projection).</dd>
<dt><samp>--full-match</samp></dt><dd>full matching combinations (normally finish when the first successful match has occurred)</dd>
</dl>

<h3>Additional:</h3>
<dl>
<dt><samp>--units</samp></dt><dd>output units: deg, arcmin, arcsec, mas, pix, auto (default)</dd>
<dt><samp>--disable-save</samp></dt><dd>disable save of calibration to header</dd>
<dt><samp>--remove</samp></dt><dd>remove calibration from FITS header</dd>
</dl>

<p>See <a href="man_com.html">Common options</a> for 
input/output filenames.</p>

<p>
For manual calibration, use <samp>--projection, --xcen, --ycen, --rcen,
--dcen, --scale, --angle</samp> and don't use <samp>-c, -r, -R</samp>.
The parameters are just interpreted in WCS framework and
stored to FITS header. 
</p>

<p>
If the header already contains astrometry, one is updated.
</p>

<h2>Caveats</h2>

<p>
Atmospheric refraction and various distortions are not implemented.
Only Gnomonic projection is available.
</p>


<h2>Examples</h2>

<h3>Sources Of Astrometrical Catalogues</h3>

<p>
Virtual Observatory (VO) is intended as the main source
of catalogues. The following example uses cone search
capability of VO to list part of 
<a href="http://vizier.u-strasbg.fr/viz-bin/VizieR?-source=I%2F322A">UCAC4</a> 
catalogue
</p>
<pre>
munipack cone -c UCAC4 -o 0716cat.fits -r 0.1 110.25 71.34
</pre>
<p>
UCAC4 is the recommended astrometric catalogue (default). We must provide
coordinates of center of frame (use coordinates of assumed object)
and the cone radius (else a catalogue default will be used).
Note parameter -s (sort by a column). The sorting requires
knowledge of catalogue structure but strongly affects speed and
success rate of matching.
</p>


<h2>Manual Calibration</h2>

<p>Manual calibration is designed with its properties:</p>
<ul>
<li>The proper calibration is known.</li>
<li>No catalogue and objects detection on images is required.</li>
</ul>

<p>
To save a calibration to FITS header, find the parameters by hand and
adjust the example
</p>
<pre>
$ munipack astrometry -p GNOMONIC --rcen=110.471 --dcen=71.351 
            --scale=5.64e-04 --angle=0.0 0716_1R.fits
</pre>

<p>
This is an example for blazar 
<a href="http://www.lsw.uni-heidelberg.de/projects/extragalactic/charts/0716+714.html">0716+71</a> 
which observation is in FITS file 0716_1R.fits included to
<a href="ftp://munipack.physics.muni.cz/pub/munipack/munipack-data-blazar.tar.gz">munipack-blazar.tar.gz</a>.
</p>

<p>
The calibration parameters are left untouched. There are are no limits and 
no any checks.
</p>

<table>
<caption>Parameters for manual calibration<sup><a href="#manual">[a]</a></sup></caption>
<tr><th>Parameter</th><th>Description</th><th>Units</th></tr>
<tr><td>-p</td><td>Type of spherical projection</td><td></td></tr>
<tr><td>--rcen</td><td>Right Ascension of center of the projection <i>α</i><sub>c</sub> (CRVAL1)<sup><a href="#tw">[b]</a></sup></td><td>degrees</td></tr>
<tr><td>--dcen</td><td>Declination of center of the projection <i>δ</i><sub>c</sub> (CRVAL2)<sup><a href="#tw">[b]</a></sup></td><td>degrees</td></tr>
<tr><td>--xcen</td><td>Reference point on a chip <i>x</i><sub>c</sub> (CRPIX1)<sup><a href="#tw">[b]</a></sup></td><td>pixels</td></tr>
<tr><td>--ycen</td><td>Reference point on a chip <i>y</i><sub>c</sub> (CRPIX2)<sup><a href="#tw">[b]</a></sup></td><td>pixels</td></tr>
<tr><td>--scale</td><td>Scale of projection <i>c</i></td><td>degrees/pixel</td></tr>
<tr><td>--angle</td><td>Angle of rotation around center <i>φ</i></td><td>degrees</td></tr>
<tr><td><samp>file</samp></td><td>a file to calibrate<sup><a href="#fl">[c]</a></sup></td><td></td></tr>
</table>

<div class="notes">
<p id="manual">
<sup><a href="#manual">[a]</a></sup>
The manual calibration is invoked when no catalogue or table
with reference star is specified.
</p>

<p id="tw">
<sup><a href="#tw">[b]</a></sup>
Twines of parameters <samp>--xcen, --ycen</samp> and <samp>--rcen, --dcen</samp>
must be specified together or both left unspecified.
</p>

<p id="fl">
<sup><a href="#fl">[c]</a></sup>
This manual calibration works only on a single file. Others types of calibrations
on a list of files.
</p>
</div>

<h2>Nearly And Matching Calibration</h2>

<p>Astrometrical calibration is designed with its properties:</p>
<ul>
<li>High precision as possible.</li>
<li>Minimum of manually provided parameters</li>
</ul>

<p>
At the start, astrometry calibration itself can be done, a star table (list)
of objects on an image must be prepared.
</p>

<p>
The object detection (as a side effect is the aperture photometry) 
is relative straightforward (detected objects are stored 
to another HDU with label FIND):
</p>
<pre>
$ munipack find -f 2 -t 5  0716_1R.fits
$ munipack aphot 0716_1R.fits
</pre>

<p>A catalogue with reference stars can be got with help of
<a href="vobs.html">Virtual Observatory</a>:
</p>
<pre>
$ munipack -o 0716_cat.fits -r 0.2 cone 110.25 71.34
</pre>
<p>
Selected astrometric stars in radius 0.2° around center
α = 110.25° and δ = 71.34° are saved to the FITS table 
0716_cat.fits. 
</p>

<p>
Note selection of catalogue UCAC4. The objects stored to the output file are
sorted by magnitude designed as 'f.mag'. You must known catalogue structure 
before
use of -s (simply get catalogue without -s option, look for structure and 
than use -s with right parameter). Sorting importantly increase probability 
of successful matching.
</p>

<p>
Invoking with --match NEARLY disables back-tracking and just only
make a transformation more precise the fitting procedure
</p>
<pre>
$ munipack astrometry --match NEARLY -c 0716_cat.fits 
           -p GNOMONIC --rcen=110.4743 --dcen=71.3508
           --scale=5.6616e-4 --angle=0 0716_1R.fits
</pre>
<p>
We are working on copy of that original
to prevent data loss in case of any fail.
</p>

<p>
In case of matching, when your are a lucky user, following command would
give you the excellent job:
</p>
<pre>
$ munipack astrometry -c 0716_cat.fits  0716_1R.fits 
</pre>

<table>
<caption>Parameters for astrometrical calibration</caption>
<tr><th>Parameter</th><th>Description</th></tr>
<tr><td>-p</td><td>Spherical projection</td></tr>
<tr><td>-c</td><td>Reference astrometry catalogue<sup><a href="#cm">[‡]</a></sup></td></tr>
<tr><td>-r</td><td>Reference already calibrated frame<sup><a href="#cm">[‡]</a></sup></td></tr>
<tr><td>-R</td><td>Reference frame for relative astrometry (no projection)<sup><a href="#cm">[‡]</a></sup></td></tr>
<tr><td>--col-ra</td><td>Label of Right Ascension column</td></tr>
<tr><td>--col-dec</td><td>Label of Declination column</td></tr>
<tr><td>--col-pm-ra</td><td>Label of proper motion of Right Ascension column</td></tr>
<tr><td>--col-pm-dec</td><td>Label of proper motion of Declination column</td></tr>
<tr><td>--minmatch</td><td>match sequence minimal length<sup><a href="#nm">[†]</a></sup></td></tr>
<tr><td>--maxmatch</td><td>match sequence length<sup><a href="#nm">[†]</a></sup></td></tr>
<tr><td><samp>file</samp></td><td>a file to calibrate</td></tr>
</table>

<div class="notes">
<p id="cm">
<sup><a href="#cm">[‡]</a></sup>
Parameters -c, -r and -R are exclusive mutual.
</p>

<p id="nm">
<sup><a href="#nm">[†]</a></sup>
Parameters --minmatch and --maxmatch affects both speed (greater is slower) 
and success (lower is worse) of matching. The recommended values
for --minmatch are 5 - 10 (default is 5) and --maxmatch 20 - 200 (default 30).  
</p>
</div>

<h2>Tips For Usage</h2>

<h3>Successful Matching</h3>

<p>
Matching needs at least <samp>--minmatch</samp> common stars
on frame and catalogue as selected <samp>--maxmatch</samp>. 
When matching is failed, check all center, radius and proper
reference catalogue.
</p>

<p>
As the best diagnostics tool is the astrometry in xmunipack 
(Tools->Astrometry).
</p>


<h3>Fine Tune Of Calibration</h3>

<p>
The matching of the reference catalog with detected stars is
extremely complicated procedure. Therefore, there are tunable 
parameters for both matching and fitting algorithms. 
In doubts, check ones:
</p>
<ul>
<li>The sorting of the table with astrometric stars.
Stars would be sorted from the most brighter to faintness ones 
(parameter -s of the cone search, 
tables of detected objects are sorted by default)</li>
<li>The search region is overlapping of the field of view of 
matched pictures.</li>
<li>Try
increase number of stars used for matching (--maxmatch)
and a match sequence length (--minmatch). Large values of both
parameters slow-downs matching.</li>
</ul>

<h2>See Also</h2>
<p><a href="astoverview.html">Astrometry Overview</a>,
<a href="dataform_astrometry.html">WCS Header</a>
</p>


<!-- #include virtual="/foot.shtml" -->
</body>
</html>

