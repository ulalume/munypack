<!DOCTYPE HTML>
<html lang="en">
<head>
<!-- #include virtual="/meta.shtml" -->
<title>Munipack ‒ Time Series</title>
</head>
<body>
<!-- #include virtual="/head.shtml" -->

<h1>Time Series</h1>

<p class="abstract">
This routine is general purpose utility to list of selected
elements from calibrated files.
</p>

<h2>Command</h2>

<code>
munipack timeserie [...] file(s)
</code>

<h2>Description</h2>

<p>
This utility is designed for listing of time series of a various
quantities from a set of already processed frames. 
Instrumental and calibrated data can be used. Full
calibrated data including astrometry and photometry calibration are preferred.
</p>

<p>
By listing of a set of processed files, this tool creates a time dependence of 
a required quantity. The quantity is selected as a FITS-table column name
and stored in a time series table.
The time series of a light-like quantity is referenced as a light curve (LC).
</p>

<p>
The utility can be used to derive various kinds of times-like quantities.
</p>


<h2>Prerequisites</h2>
<p>
Needs astrometry and photometry. To get calibrated fluxes or magnitudes, needs
photometry calibration.
</p>

<h2>Input And Output</h2>

<p>
On input, list of frames containing both astrometric and photometric information 
is expected.
</p>

<p>
On output, the FITS table representing the time series is created.
</p>


<h2>Parameters</h2>

<h3>Time:</h3>

<p>
The time can be specified for reference points:
</p>

<dl>
<dt>At Middle (--time-stamp=MID)</dt>
<dd>The time specifies exactly at middle of exposure duration. Computed
as begin time plus half of exposure. This is default.</dd>
<dt>At Begin (--time-stamp=BEGIN)</dt>
<dd>The time specifies exactly at begin of exposure.</dd>
<dt>At End (--time-stamp=END)</dt>
<dd>The time specifies exactly at end of exposure.</dd>
</dl>

<p>
Following types of time can be specified (<a href="http://en.wikipedia.org/wiki/Julian_day">Julian day on wiki</a>):
</p>

<dl>
<dt>Julian date (-T JD)</dt>
<dd>The time is specified in Julian dates in UT by default.</dd>
<dt>Modified Julian date (-T MJD)</dt>
<dd>The time is specified in modified Julian dates (JD − 2400000.5) in UT.</dd>
<dt>Heliocentric Julian date (-T HJD)</dt>
<dd>The time is specified in Julian dates at barycenter of solar system in UT.</dd>
<dt>Phase (-T PHASE)</dt>
<dd>The time is specified as phase in UT. The phase φ is computed from
a reference epoch <i>e<sub>0</sub></i> given by --epoch in Julian days and 
period <i>P</i> given by --period in days: <i>φ = {(t - e<sub>0</sub>)/P}</i>
(where <i>{.}</i> operator provides fractional part (see 
<a href="http://en.wikipedia.org/wiki/Floor_and_ceiling_functions">Floor and ceiling functions</a>).</dd>
</dl>

See also detailed description at 
<a href="dataform_tmseries.html#tmtable">description of output table</a>.

<h3>Listing:</h3>

<dl>
<dt><samp>-c, --column</samp></dt><dd>column(s) to output list (must exactly
          match names of column(s) in images on input)</dd>
<dt><samp>-C, --coocolumn</samp></dt><dd>column(s) of quantities used as
         coordinates</dd>
<dt><samp>-x, --extname</samp></dt><dd>An identifier of FITS-extension</dd>
<dt><samp>-T, --time-type</samp></dt><dd>JD − Julian date (default), MJD − modified JD, 
                                         HJD − heliocentric JD, PHASE</dd>
<dt><samp>--time-stamp</samp></dt><dd>reference time point: mid (default), begin, end</dd>
<dt><samp>--diffmag</samp></dt><dd>differential magnitudes (with respect to a first object)</dd>
<dt><samp>--epoch</samp></dt><dd>reference time point of light curve elements in JD</dd>
<dt><samp>--period</samp></dt><dd>period light curve elements in days</dd>
<dt><samp>--tol</samp></dt><dd>search radius for object identification in degrees</dd>
<dt><samp>--stdout</samp></dt><dd>results print also to standard output</dd>
<dt><samp>--cat</samp></dt><dd>catalogue of coordinates from the FITS file</dd>
</dl>

<p>
When <samp>-c</samp> or <samp>-C</samp> are not specified, 
the structure of the first file is discovered.
The coordinates are recognised automatically as rectangular
(equivalent to <samp>-C X,Y</samp>) or spherical (<samp>-C RA,DEC</samp>).
All other columns (except coordinates) are listed by default
(usually as <samp>-c COUNT,COUNTERR</samp>, <samp>-c PHOTON,PHOTONERR</samp>,
<samp>-c MAG,MARERR</samp>, etc.).
</p>



<h2>Stars Selection And Catalogue</h2>

<p>
By default, all stars on all frames are processed and stored to the output file.
To select stars, there are two ways:
</p>

<ul>
<li>specify coordinates on the command-line</li>
<li>use a catalogue</li>
</ul>

<p>
For a few stars request, the simpler way is specification of coordinates on the
command line. Use twices of equatorial coordinates (Right Ascension and Declination)
in degrees separated by commas (or semicolon).
For example:
</p>
<pre>
$ munipack timeseries 330.689,42.2765 330.667,42.2860  file.fits
</pre>

<p>More general way is use of a table with coordinates. Important
advantages over command line:
</p>
<ul>
<li>Very useful for passing of many stars and to use it repeatedly.</li>
<li>Proper motion can be specified. The file header can provide additional
    information (reference epoch) and units for all coordinates (complex
    info is crucial reason for use file).</li>
</ul>

<p>
The proper motion can be important for near and moving stars and should
by used for flying rocks.
</p>

<h2>Catalogue For Stars Selection</h2>

<p>
Format of the catalogue is very restrictive and must be carefully
followed. One is stored in FITS file with a just one table extension 
(EXTNAME doesn't matter). The header must contain keyword EPOCH which
denotes the reference time <i>t</i><sub>0</sub> in Julian days for object coordinates.
The current positions at <i>t</i> are computed from reference coordinates 
<i>α</i><sub>0</sub>, <i>δ</i><sub>0</sub>
and proper motions <i>μ</i><sub><i>α</i>0</sub>, <i>μ</i><sub><i>δ</i>0</sub> (in
degrees per century) as:<br>
&nbsp; α = <i>α</i><sub>0</sub> + <i>μ</i><sub><i>α</i>0</sub> /(<i>t</i> - <i>t</i><sub>0</sub>) / <i>T</i><br>
&nbsp; δ = <i>δ</i><sub>0</sub> + <i>μ</i><sub><i>δ</i>0</sub> /(<i>t</i> - <i>t</i><sub>0</sub>) / <i>T</i><br>
where <i>T</i> is the time unit given by TUNITS keywords in header.
One is one for deg/day and 365.25 for arcsec/year.
</p>

<p>
The most simple way how to create the catalogue, 
<a href="timeserie_cat.lst">timeserie_cat.lst</a> can be directly used
as example and edited. The FITS file timeserie_cat.fits is created as
</p>
<pre>
$ munipack fits --restore timeserie_cat.lst
</pre>


<table>
<caption>Table structure</caption>
<tr><th>Column</th><th>Description</th><th>Units</th></tr>
<tr><td>RA</td><td>Right ascension <i>α</i><sub>0</sub></td><td>degrees</td></tr>
<tr><td>DEC</td><td>Declination <i>δ</i><sub>0</sub></td><td>degrees</td></tr>
<tr><td>pmRA</td><td> proper motion in RA <i>μ</i><sub><i>α</i>0</sub></td><td>arcsec/year or deg/day<sup><a href="#pmu">[†]</a></sup></td></tr>
<tr><td>pmDEC</td><td>proper motion in DEC <i>μ</i><sub><i>δ</i>0</sub></td><td>arcsec/year or deg/day<sup><a href="#pmu">[†]</a></sup></td></tr>
</table>

<div class="notes">
<sup><a href="pmu">[†]</a></sup>
The string 'arcsec/year' or 'deg/day' must be present and specified exactly
via TUNIT3 and TUNIT3 keywords. Setting of proper motions to zeros will
usually satisfactory (except really fast moving objects).
</div>

<h2>Caveats</h2>

<p>
This utility is primary indented and designed for working with low
amount of data. The typical usage is listing of light curves
or positions of motion of objects during a night. Another example can be study 
of any instrumental quantity. This routine is generic analysing tool.
</p>

<p>
Use on large archives of observations is not recommended.
Spidering over a complicated directory structure would be really
slow. To work with a large data archive, use Munipack to create 
tables with photometry and astrometry data and keep the results in a database.
Much more better idea should be to import the data into some 
Virtual Observatory engine. Popular VO-engines are
<a href="http://ia2.oats.inaf.it/index.php/vodance">VO-Dance</a>,
<a href="http://saada.unistra.fr/saada/">Saada</a> or
<a href="http://docs.g-vo.org/DaCHS/">GAVO DaCHS</a>.
</p>



<h2>Examples</h2>

<p>
Timeseries for all stars:
</p>
<code>
$ munipack timeseries M67_Blue.fits
</code>

<p>
Light curve for stars at coordinates:
</p>
<code>
$ munipack timeseries 256,156 258,88 0716_*R.fits
</code>



<h2>See Also</h2>
<p>
<a href="dataform_tmseries.html">Data format for timeseries</a>,
<a href="lctut.html">Light Curve Tutorial</a>,
<a href="man_com.html">Common options</a>.</p>

<!-- #include virtual="/foot.shtml" -->
</body>
</html>

