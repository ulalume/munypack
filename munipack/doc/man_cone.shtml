<!DOCTYPE HTML>
<html lang="en">
<head>
<!-- #include virtual="/meta.shtml" -->
<title>Munipack ‒ Cone Search</title>
</head>
<body>
<!-- #include virtual="/head.shtml" -->

<h1>Cone Search</h1>

<p class="abstract">
Cone search for catalogues provided by Virtual Observatory. 
</p>

<h2>Command</h2>

<pre>
munipack cone [.. options ..] [--] RA DEC
munipack cone [--list-catalogues | --list-servers]
</pre>

<h2>Description</h2>

<p>
Search a selected catalogue provided by Virtual observatory
interface (VO-capable cone search) with following features:
</p>
<ul>
<li>List of available catalogues is prepared.</li>
<li>List of available servers is prepared.</li>
<li>All parameters are in degrees.</li>
<li>Output is stored to VOTable format (XML), 
FITS table or a plain text or any format supported by 
<a href="man_votable.html">votable</a>.</li>
</ul>
<p>
</p>

<h2>Negative Declinations</h2>

<p>
Negative coordinates starts with minus sign which can be recognised,
by the parsing routine, as an option or a switch. To prevent the confusion,
we must separate options and parameters by double-dash [--]. Than recognition
of the coordinates will work as expected.
</p>

<p>
A typical message when the problem is occurred:
<pre>
$ munipack cone 245.9 -26.5
Unknown option '26'
Usage: munipack cone ...
</pre>
<p>
And can be easy solved as
</p>
<pre>
$ munipack cone -- 245.9 -26.5
</pre>

<h2>Catalogue Servers</h2>

<p>
Central Virtual Observatory catalogue site (VizieR) is maintained by 
<a href="http://cdsweb.u-strasbg.fr/">Centre de Données astronomiques de Strasbourg</a>
and provided as a <a href="http://vizier.u-strasbg.fr/viz-bin/VizieR">VizieR</a> service.
The service is mirrored by other sites. The option <samp>--service</samp>
offers possibility to select an appropriate mirror.
</p>

<h2>Network Timeout</h2>

<p>
Sometimes connection can not be established on first attempt or a network
connection is down, in this case, the connection will be repeated for a while.
</p>


<h2>Input And Output</h2>

<p>
On input, no files are expected. Virtual Observatory is connected via network.
</p>

<p>
On output, results of the cone search are saved to a file.
</p>

<h2>Selection Constrains</h2>

<p>
Some servers supports additional parameters for search, output form and etc. 
For instance, there is a way how to limit magnitudes in V filter for stars 
brighter than eleven:
</p>
<pre>
munipack cone --par 'Vmag=<11' -- 245.9 -26.5
</pre>

<p>
Additional info can be found in <a href="http://vizier.u-strasbg.fr/vizier/doc/vizquery.htx">VizieR</a> query documentation.
</p>


<h2>Parameters</h2>

<dl>
<dt><samp>-c,--cat</samp></dt><dd>catalogue alias (see --list-catalogues)</dd>
<dt><samp>-r</samp></dt><dd>search radius in degrees (default: 0.1)</dd>
<dt><samp>-s</samp></dt><dd>sort by the column</dd>
<dt><samp>--url</samp></dt><dd>use the URL. Following options are ignored: 
                               -c, --cat, --par, -r, --server</dd>
<dt><samp>--id</samp></dt><dd>Full catalogue identifier</dd>
<dt><samp>--par</samp></dt><dd>additional optional parameters (by default maximum
                               verbosity is used)</dd>
<dt><samp>--type</samp></dt><dd>type of output file: fits,csv,txt,xml 
                (default by suffix of <samp>-o</samp> option)</dd>
<dt><samp>--vocat</samp></dt><dd>an alternative configuration file (normally, located in 
          <samp>[/opt]/usr/share/munipack/VOcat_conf.xml</samp>)</dd>
<dt><samp>--server</samp></dt><dd>use VizieR's mirror</dd>
<dt><samp>--list-catalogues</samp></dt><dd>list of predefined catalogues</dd>
<dt><samp>--list-servers</samp></dt><dd>list of available VizieR servers</dd>
<dt><samp>--</samp></dt><dd>Double-dash separates options and switches from
   coordinates. One is necessary for negative coordinates (else parsing can be confused
   by minus representing of an option).</dd>
</dl>

<p>
See <a href="man_com.html">Common options</a> for input/output filenames.
</p>

<h2>Using Another Catalogue</h2>
<p>
The list of available catalogues is limited. Another required catalogue can
be added by this way:
</p>
<ul>
<li>Go to <a href="http://vizier.u-strasbg.fr/viz-bin/VizieR">VizieR</a>
service and discover your catalogue. We will need ID of the catalogue like
"J/A+A/544/A31" (Ptolemaios catalogue).</li>
<li>
Invoke the cone as
<pre>munipack cone --id 'J/A+A/544/A31' -r 1 -- 104 -16</pre>
</li>
<li>
To use the data, apropriate parameters <samp>--col-ra, --col-dec,
--col-pm-ra, --col-pm-dec</samp> for <samp>astrometry, phcal,</samp> etc 
must be specified.
</li>
</ul>


<h2>Examples</h2>

<p>
Search GSC catalogue within Pleiades (α = 92.4°, δ = 24.1°) with cone radius
of 0.2°:
</p>
<code>
$ munipack cone -c GSC -r 0.2 92.4 24.1
</code>

<p>
Cone search for negative coordinates needs separate parameters and
options with double-dash:
</p>
<code>
$ munipack cone -c UCAC4 -r 0.2 -- 245.9 -26.5
</code>


<h2>See Also</h2>
<p><a href="vobs.html">Virtual Observatory</a>,
<a href="man_com.html">Common options</a>,
<a href="http://cdsweb.u-strasbg.fr/vizier/doc/vizquery.htx">vizquery documentation</a>.
</p>

<!-- #include virtual="/foot.shtml" -->
</body>
</html>

