#!/bin/sh
# generates include file for Make providing all images

#set -x

MFILE=image_list.mk

# thumbnails
rm -f Screenshot-*-mini.png;
for A in Screenshot-*.png; do
    B=${A%.png}-mini.png
    C=${A%-mini.png}.png
    if [ ! -f "$C" ]; then
	convert $A -resize 300x200 $B
    fi
done
convert big_logo.png -resize 100x100 title_logo.png
    

echo -n "image_list = " > $MFILE
for S in "*.png" "*.jpg" "*.svg" "javascript/*"; do
    for A in $S; do 
	echo "\\" >> $MFILE
	echo -n " $A " >> $MFILE
    done
done
echo >> $MFILE




