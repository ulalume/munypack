<!DOCTYPE HTML>
<html lang="en">
<head>
<!-- #include virtual="/meta.shtml" -->
<title>Munipack ‒ Manual Page</title>
</head>
<body>
<!-- #include virtual="/head.shtml" -->

<h1>Colour Frame Composition</h1>

<p class="abstract">
Merges a set of single filter exposures to a colour image.
</p>

<h2>Command</h2>

<code>
munipack coloring [-o file] [-c colorspace] file1[,ext1] ...
</code>

<h2>Description</h2>

<p>
Munipack implements a method which transforms images taken via
a set of standard photometric filters (Johnson BVR or equivalent) 
to <a href="http://en.wikipedia.org/wiki/CIE_1931_color_space">CIE XYZ</a> 
colour space (the space simulates the colour sensitivity of the human eye). 
Results has colours near of natural colours and simulates how the object 
would be seen by an extraordinary observer.
</p>

<p>
Main purpose of the utility is offer possibility to create images
in natural colours from instrument not equipped by CIE XYZ filters
(eg. any professional telescope).
</p>

<p>
Inputs are specified as twines: {file,ext}, where <samp>file</samp> is a name
of a FITS file, <samp>ext</samp> is the FITS extension. The extension is
optional and defaulted to a first image HDU.
</p>

<p>
The frames must be listed on command line from short to longer wavelengths 
(following conventional BVR order). This is exactly oppposite to RGB (XYZ).
</p>

<h2>Parameters</h2>

<dl>
<dt><samp>-c colour-space</samp></dt><dd>Colour-space of input images 
    (eg. Johnson BVR)</dd>
</dl>
<p>See <a href="man_com.html">Common options</a> for input/output filenames.</p>

<h2>Examples</h2>

<p>Create a new color FITS image <samp>m27.fits</samp> from a set of files
taken with filters <samp>m27_[B,V,R].fits</samp>:
</p>
<pre>
$ munipack coloring -o m27.fits -c 'Johnson BVR' m27_B.fits m27_V.fits m27_R.fits
</pre>

<figure>
<img class="figure" src="color-best.png" alt="color_best.png" title="M27 in Colours">
<figcaption>
Dumbbell nebula in colours
</figcaption>
</figure>

<h2>See Also</h2>
<p><a href="colorspace.html">Colour Processing</a>,
<a href="man_com.html">Common options</a></p>


<!-- #include virtual="/foot.shtml" -->
</body>
</html>

