<!DOCTYPE HTML>
<html lang="en">
<head>
<!-- #include virtual="/meta.shtml" -->
<title>Munipack ‒ Common Options</title>
</head>
<body>
<!-- #include virtual="/head.shtml" -->

<h1>Common Options</h1>


<p class="abstract">
Description of commonly used options and switches.
</p>


<h2>Verbose Output</h2>

<code> 
$ munipack --verbose ... action and so on ...
</code>

<p>
All executive actions can show detailed informations
about processing. The additional listing should be 
very usefull in doubts.
</p>

<p>
Munipack respects standard UNIX philosophy:
<b>Just only errors are reported. No logs means no errors.</b>
It is very usefull for processing of large datasets because
only important informations are delivered to user.
On the other side, the verbose output displays what exactly
the utilities doing so critical points can be potentialy localised.
</p>

<h2>Numerical Data Types</h2>

<pre>
-B [8|16|32|-32]
--bitpix[=8|16|32|-32]
</pre>
<p>
Select bits per pixel of output images. Possible values are: 8
(0-255),  16  (0-65535),  32  (0-4294967296) for integer non-negative numbers 
of ℕ set 
and -32 (-10<sup>-38</sup> .. 10<sup>38</sup>, 6 decimal digits) for real numbers 
of ℝ set, (values in braces indicates numerical ranges).
</p>

<p>
The parameter is set according to BITPIX in original frames for photometric
pre-corrections or set to -32 for derived images. 
Defaults are usually satisfactory. 
</p>

<p>
General guidelines: An optimal
bitpix  for raw (instrumental) data is 16 (default) which covers
a full range of digital  cameras.  The  representation  occupies
2 × width × height bytes.  Some  out  of  range  (rare) values will
be cut-off. 
</p>

<p>
The representing by real numbers (eg. -32)
is  recommended value for images intended for further processing
because saves numerical precision and their numerical range (but
occupies of twice more space with respect to 16).
</p>

<p>
8-bits reduces range (eg. suppress dynamical range) and  32  wastes  
a  lot  of storage place only. 
</p>


<h2>Input Filenames For A Single Output</h2>

<pre>
file(s).fits
@[file.lst]
-
</pre>

<p>
The list of files to process. Usually as a names with wildcards (* or ?).
Use @ (at) or - (dash) to read from a standard input. (@ character
is used in the same meaning in classical Iraf and Midas.)
</p>

<p>
For single output file (actions with <samp>-o|--output</samp> like
bias, dark, flat and timeseries), the input files can be passed as 
command line arguments or in a file. Arguments can be used by many ways:
</p>
<ul>
<li>Filenames: <samp>munipack ... one.fits more.fits red.fits nightmare.fits</samp></li>
<li>Wildcards: <samp>munipack ... *.fits</samp></li>
<li>Generated: <samp>munipack ... `find /dir -name '*.fits'`</samp></li>
</ul>

<p>
The input from file is initiated with @ character followed a file-name (file.lst). 
The file is a plain text file with single file per line. As example,
the content of the file.lst equivalent to the previous example:
</p>
<pre>
one.fits 
more.fits 
red.fits 
nightmare.fits
</pre>
<p>
The file can be prepared by hand or prepared by the command with using
of <a href="http://en.wikipedia.org/wiki/Redirection_(computing)">shell redirection</a>
and <a href="http://en.wikipedia.org/wiki/Find">find</a> utility:
</p>
<pre>
$ ls *.fits > file.lst                  # files in current directory
$ find /dir -name '*.fits' > file.lst   # all files in /dir, recursive
$ ls *.fits | munipack dark -           # filenames are piped from ls
</pre>


<h2>Input Filenames For Multiple Outputs</h2>


<code>
file(s)[,result(s)]
@[file.lst]
-
</code>

<p>
The list of files to process. Usually as a names with wildcards (* or ?).
The optional parameter <samp>result(s)</samp> can be used for a direct
setting of a new filename. Use @ (at) or - (dash) to read from a standard input.
<p>
 
<p>
For multiple output (actions without <samp>-o|--output</samp> like
find, aphot, astrometry and phcal), the input files 
are manipulated the same way as in previous example. Moreover,
the twines of files separated by a comma are recognized and result
files can be named differently.
</p>

<p>
Examples similar to previous ones:
</p>
<ul>
<li>Filenames: <samp>munipack ... one.fits,two.fits red.fits,green.fits</samp></li>
<li>Wildcards: <samp>munipack ... *.fits</samp></li>
<li>Generated: <samp>munipack ... `find /dir -name '*.fits'`</samp></li>
</ul>
<p>(No change for both wildcard and generated ones.)</p>

<pre>
one.fits,two.fits
red.fits,green.fits
</pre>
<pre>
$ for F in *.fits; do echo ${F},${F%.fits}_result.fits; done > file.lst
</pre>
<p>
Last example illustrates, how a lot of files can be easy renamed and used 
</p>


<p>
Note, that renaming can be modified by a powerful way as
describes <a href="#advanced">Advanced Output Filenames</a> section.
</p>



<h2>Simple Output Filenames</h2>

<pre>
-o name
--output name
</pre>
<p>
Specify an output file name for a single file. If the option is not 
presented, the output name
is derived from the particular action name.
<!--
Use dash '-' for  redirection  to
the  standard  output.  Precede  the  filename  with exclamation
point (!) to overwrite of an existing FITS file. 
In case that the name
is a directory, newly created file(s) are stored here.
-->
</p>

<h2>Target Directory</h2>

<pre>
-t directory
--target-directory=directory
</pre>

<p>
It would be useful to store output files in a specified directory. 
The most typical use is storing modified files in a working directory
when original files are untouched.
</p>

<h2>Backup Strategy</h2>

<p>Backup strategy for Munipack modifies traditional conventions. 
Backups are <b>switched-on</b> by default
(equivalent to use of -b option in every command). This is the important difference
to other FITS relates utilities. The observed data are too unique, valuable and once-in-a-lifetime
for ignoring backups.
The default behavior can be switched-off by using option <samp>--backup off</samp>.
</p>

<p>
For a comfortable use of routines, the backup method with just only once-time
copy is chooses. This may by potentially dangerous to data because older (original)
files are replaced.
</p>

<p>
The recommended way for processing is to use a working directory, different
to a directory with original data. Moreover, it is highly recommended to
store original data with permission flag set to read-only.
</p>

<p>
The parameter <samp>-t</samp> (<samp>--target-directory</samp>) can be used for this:
</p>

<pre>
$ munipack -t ~/work dark flat.fits *.fits
</pre>

<p>
Ones simply reads original data as <samp>sources.fits</samp> and store results in 
<samp>~/work/sources.fits</samp>.
</p>

<h2>Backup Options</h2>

<pre>
-b
--backup[=method] 
</pre>
<p>Backup options (default: on). Their syntax, environment variables and behavior 
exactly corresponds to GNU core util's 
<a href="http://www.gnu.org/software/coreutils/manual/html_node/Backup-options.html">Backup options</a>.
</p>

<pre>
-S suffix
--suffix=suffix
</pre>
<p>
Specify a suffix for backup files. Defaults set to tilde (~).
</p>

<p>
Certain characters (%,#,..) may interfere with bash and general regular expression
syntax which is used to recognize text patterns and ones are not recommended 
as suffixes.
</p>

<p>
Sometimes, perhaps to save a some disk space, backups can be just switch-off
with setting of the environment variable:
</p>
<pre>
$ export VERSION_CONTROL=off
</pre>


<h2 id="advanced">Advanced Output Filenames</h2>

<pre>
-O
--pattern pattern (default: (.+)\.(.+))
--mask mask (default: empty)
--format format (default: empty)
</pre>
<p>
Specify a regular expression or a format to describe of an output file name(s).
The <samp>-O</samp> switch-on the advanced functionality (else the simple backup
with suffix is used). The pattern is a regular expression used to matching and
on will usually include bracket expression for back-references. The back-references
can be used in mask with \number. To test a regular expression, use sed: 
<samp>sed s/pattern/mask/</samp>. <samp>--mask</samp> is used for newly created 
files whilst <samp>--backup</samp> for specify of backup files.
</p>

<p>
The default pattern splits filenames onto two parts name and extension separated
by a dot (\.). The pattern recognizing algorithm uses 
<a href="http://en.wikipedia.org/wiki/Regular_expression">Regular Expression</a> 
rules syntax.
The parts are accessible via \number operator. The \0 means original
filename, \1 name and \2 extension. 
</p>

<p>
The format is a standard format for output of
sequence images. To test a format, use <samp>printf "out%d.fits",666</samp>.
</p>

<p>
When just only <samp>-O</samp> is specified, backups are disabled.
</p>

<p>
When the advanced filename processing is set, the options <samp>-t,-S,-b</samp>
are ignored, because their functionality can be simply simulated.
</p>

<!--
<p>
When <samp>-O</samp> is specified but no other specificators (like masks, format)
are set, backups are disabled and the original file is modified in place.
</p>
-->


<p>Examples:</p>
<pre>
# store outputs in /tmp directory : -O --mask '/tmp/\0'
 barnard_0011R.fits -> /tmp/barnard_0011R.fits

# modify suffix: -O --pattern '(.+)\.fits' --mask '\1.fit'
 barnard_0011R.fits -> barnard_0011R.fit

# modify filename: -O --pattern '(.+)\.(.+)' --mask '\1_D.\2'
 barnard_0011R.fits -> barnard_0011R_D.fits

# alternate backups: -O --mask '\0.bak'
 barnard_0011R.fits -> barnard_0011R.fits.bak

# overwrite output: -O --mask '!\0'
 barnard_0011R.fits -> !barnard_0011R.fits

# list of numbered files: -O --format 'out_%02d.fits'
 barnard_0001R.fits -> out_01.fits

# disable backups: -O 
 barnard_0001R.fits -> barnard_0001R.fits
</pre>

<!--
# alternate backups: -O --backup '\0.bak'
 barnard_0011R.fits -> /tmp/barnard_0011R.fits.bak
-->



<p>See also: <a href="http://docs.wxwidgets.org/3.0/classwx_reg_ex.html">wxRegEx</a>,
<a href="http://docs.wxwidgets.org/3.0/classwx_string.html">wxString</a></p>


<!-- #include virtual="/foot.shtml" -->
</body>
</html>

