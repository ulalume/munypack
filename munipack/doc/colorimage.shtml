<!DOCTYPE HTML>
<html lang="en">
<head>
<!-- #include virtual="/meta.shtml" -->
<title>Munipack ‒ Colour Images Tutorial</title>
</head>
<body>
<!-- #include virtual="/head.shtml" -->

<h1>Colour Images</h1>

<p>
How to create of images in natural colours with standard photometric filters.
</p>

<h1>Dumbbell Nebula In Colours</h1>

<p>
Dumbbell nebula (M 27) is a planetary nebula in Saggita. Dumbbell is 
one from favourite objects for small telescopes, although its colours are
unseen for visual observers except very large equipment.
</p>


<h2>Sample Data</h2>

<p>
A sample data are available as 
<a href="ftp://munipack.physics.muni.cz/pub/munipack/munipack-data-m27.tar.gz">munipack-data-m27.tar.gz</a>.
Save it to arbitrary directory (for example <samp>/tmp/</samp>) like:
</p>
<pre>
$ cd /tmp
$ tar zxf munipack-data-m27.tar.gz
$ cd munipack-data-m27/
</pre>
<p>
The directory <samp>munipack-data-m27/</samp> contains exposures in the instrumental
colour system (nearly Johnson) in BVR filters and corresponding dark frames.
B filter exposures has duration 60 seconds. Both V and R filter exposures has 
40 seconds. Flat-fields are not available.
</p>

<h2>Data Processing</h2>

<p>
Follow these steps to get a colour image:
</p>

<ol>
<li>
Prepare correction frames and pre-correct object's frames:
<pre>
$ munipack dark -o d60.fits d60_*.fits
$ munipack phcorr -dark d60.fits m27_*B.fits
$ munipack dark -o d40.fits d40_*.fits
$ munipack phcorr -dark d40.fits m27_*[VR].fits
</pre>
</li>
<li>Stars detection and photometry
<pre>
$ munipack find m27_*.fits
$ munipack aphot m27_*.fits
</pre>
<p>To check detected stars and its preliminary photometry, run xmunipack:</p>
<pre>
$ xmunipack m27_01R.fits
</pre>
</li>
<li>Search the reference astrometry catalogue around Dumbbell's position
(required for astrometric calibration) in Virtual Observatory
and stores results of search in file <samp>cone.fits</samp>:
<pre>
$ munipack cone -r 0.1 299.87 22.71
</pre>
</li>
<li>Prepare the astrometric calibration of images (wait a while)
<pre>
$ munipack astrometry -c cone.fits m27_*.fits
</pre>
</li>
<li>
<div>
Specify centre of projection of the output image. A good choice is
a point near of centre of object, but also the centre of a selected frame
can be recommended. We are chooses <i>α</i>=299.9° and <i>δ</i>=22.72°.
The specification of the centre of projection (--rcen, --dcen)
is important for correct align of single frames.
</div>
<div>
We are merging all images in single filter to get more deeper exposure:
</div>
<pre>
$ munipack kombine -o m27_B.fits --rcen 299.9 --dcen 22.72 m27_*B.fits
$ munipack kombine -o m27_V.fits --rcen 299.9 --dcen 22.72 m27_*V.fits
$ munipack kombine -o m27_R.fits --rcen 299.9 --dcen 22.72 m27_*R.fits
</pre>
</li>
<li>
Result is the colour image
<pre>
$ munipack coloring -c 'Johnson BVR' -o m27.fits m27_B.fits m27_V.fits m27_R.fits 
</pre>
</li>
</ol>

<table>
<tr>
<td class="blank">
<figure>
<img src="color-best.png" alt="M27.png" title="Dumbell in Colours">
<figcaption>Dumbbell nebula in natural colours</figcaption>
</figure>
</td>
<td class="blank">
<figure>
<img src="color-scotopic.png" alt="M27.png" title="Dumbell in Night">
<figcaption>Dumbbell nebula as visible by a visual observer adapted to
low light conditions.</figcaption>
</figure>
</td>
</tr>
</table>

<p>
The output image nicely shows regions where radiation of forbidden lines dominates
(green) and Hα regions heated by shock-waves (red). 
</p>

<p>
Also try how this image would be visible by human vision at night, when colour vision 
receptors (rods) is not activated. The appearance will be similar to visual sights.
Use xmunipack and Tune->Color_menu or try helper utility:
</p>
<pre>
$ fitspng -fn 0,1 -o m27.png m27.fits
</pre>

<h2>See Also</h2>

<p>
Manuals:
<a href="man_coloring.html">Coloring</a>,
<a href="man_kombine.html">Kombine</a>,
<a href="man_astrometry.html">Astrometry</a>,
<a href="man_aphot.html">Aperture Photometry</a>,
<a href="man_phcorr.html">Photometric corrections</a>.
</p>

<p>
Tutorial: 
<a href="kombitut.html">Image Compositions</a>
</p>

<!-- #include virtual="/foot.shtml" -->
</body>
</html>
