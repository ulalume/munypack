#!/bin/sh
# generates RSS

#set -x

which python
if [ $? -ne 0 ]; then
    echo "Python not found. If you want generate RSS, please install it." > /dev/stderr
    exit 1
fi

# generate it
python munipack-atom.py > news_feed.xml

# important when previous command fails
exit 0
