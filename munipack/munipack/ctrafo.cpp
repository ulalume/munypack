/*

  Color transformations

  Copyright © 2010 - 2013 F.Hroch (hroch@physics.muni.cz)

  This file is part of Munipack.

  Munipack is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  
  Munipack is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public License
  along with Munipack.  If not, see <http://www.gnu.org/licenses/>.

*/

#include "munipack.h"
#include <wx/filename.h>


bool Munipack::ctrafo(MuniProcess *action, MuniCmdLineParser& cmd)
{
  CommonOutputSingle(action,cmd);
  CommonOptionsBitpix(action,cmd);

  wxString a;

  if( cmd.Found("ct",&a) )
    action->Write("CTABLE = '" + a + "'");
#ifdef MUNIPACK_DATA_DIR
  else {
    wxFileName fname(MUNIPACK_DATA_DIR,"ctable.dat");
    action->Write("CTABLE = '"+fname.GetFullPath()+"'");
  }
#endif

  if( cmd.Found("list") ) {
    action->Write("INFO = colorspaces");
    return true;
  }

  if( cmd.Found("c",&a) )
    action->Write("COLORSPACE = '" + a + "'");

  if( cmd.GetParamCount() < 2 ) { wxLogFatalError("More parameters required."); };

  action->Write("INPUT = '" + cmd.GetParam(0) + "'");

  action->Write("NBAND = %ld",long(cmd.GetParamCount() - 1));

  for(size_t i = 1; i < cmd.GetParamCount(); i++) {
    action->Write("%ld "+cmd.GetParam(i),long(i - 1));
  }

  return true;
}

