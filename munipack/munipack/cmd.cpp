/*

  Munipack - command line parser

  Copyright © 2010-15 F.Hroch (hroch@physics.muni.cz)

  This file is part of Munipack.

  Munipack is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  
  Munipack is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public License
  along with Munipack.  If not, see <http://www.gnu.org/licenses/>.


*/

#include "munipack.h"
#include <wx/wx.h>
#include <wx/app.h>
#include <wx/cmdline.h>
#include <wx/regex.h>

#define COMMON_OPTIONS_BACKUP \
  AddSwitch("b","","make backup (default: on)");  \
  AddOption("","backup","make and set backup method (default: existing)"); \
  AddOption("S","suffix","suffix for backup (default: ~)");

#define COMMON_OPTIONS_SINGLE \
  AddOption("o","output","output file, (-) for stdout");	\
  COMMON_OPTIONS_BACKUP

#define COMMON_OPTIONS_MULTIPLE \
  COMMON_OPTIONS_BACKUP \
  AddOption("t","target-directory","set target directory");		\
  AddSwitch("O",wxEmptyString,"switch-on advanced output filenames processing"); \
  AddOption(wxEmptyString,"pattern","matching pattern (for -O)");	\
  AddOption(wxEmptyString,"mask","mask for output file(s) (for -O)");	\
  AddOption(wxEmptyString,"format","format for output files (for -O)");

#define COMMON_OPTIONS_BITPIX \
  AddOption("B","bitpix","bitpix (-32,32,16,8)",wxCMD_LINE_VAL_NUMBER);

#define COMMON_OPTIONS_PHTOLLS \
  AddOption("","tol-temp",L"limit for nearby temperatures (1°C)",wxCMD_LINE_VAL_DOUBLE); \
  AddOption("","tol-exp",L"limit for nearby exposure times (1μsec)",wxCMD_LINE_VAL_DOUBLE);

#define COMMON_OPTIONS_ARITH \
  AddSwitch("a",wxEmptyString,"arithmetical instead of robust mean");

#define COMMON_OPTIONS_ADDKEY \
  AddOption("K","keys","key=value[,key=,..] add keyword(s) to header",wxCMD_LINE_VAL_STRING,wxCMD_LINE_PARAM_MULTIPLE);

#define COMMON_OPTIONS_PHQ \
    AddOption("q","quantity","select: PHRATE,FLUX,FNU,FLAM,MAG,ABMAG,STMAG");
//    AddOption("q","quantity","instrumental: COUNT,RATE,MAG\n\tcalibrated: PHOTON,PHOTRATE,PHOTNU,PHOTLAM,FLUX,FNU,FLAM,MAG,ABMAG,STMAG");


MuniCmdLineParser::MuniCmdLineParser(const wxString& action, int xargc, wchar_t *xargv[]):
  wxCmdLineParser(), argc(0), argv(0)
{
  if( xargc > 0 && action != "" ) {
    // filter command-line options:
    // * actions are not passed

    argc = xargc - 1;
    argv = new wxChar*[argc];
    int n = 0;
    for(int i = 0; i < xargc; i++) {
      if( xargv[i] != action ) {
	wxString a(xargv[i]);
	argv[n++] = wxStrdup(a.wc_str());
      }
    }

    SetCmdLine(argc,argv);
  }
  else {

    //    SetLogo(INFOTEXT " " XVERSION ", " COPYLEFT "\n" INFOTEXTFULL "\n");
    AddUsageText(
		 /*
                 "\nGeneral Actions:\n"
		 " calibrate\tcalibrate\n"
		 */
		 "\nActions:\n"
		 " phcorr\t\tApply photometric precorrections\n"
		 " bias\t\tAveraged bias frame\n"
		 " dark\t\tAveraged dark frame\n"
		 " flat\t\tAveraged flat-field frame\n"
		 " coloring\tCompose color frame\n"
		 " kombine\tCompose frames (deep exposures, mosaics)\n"
		 " ctrafo\t\tTransform color frames between color-spaces\n"
		 " cone\t\tCone search in a catalogue on Virtual Observatory\n"
		 " astrometry\tAstrometry calibration\n"
		 " find\t\tSearch for stars on frames\n"
		 " aphot\t\tAperture photometry\n"
		 " phcal\t\tPhotometry calibration\n"
		 " phfotran\tDetermine photometry system color transformation\n"
		 " timeseries\tList arbitrary quantity to time-series\n"
		 "\nLow Level Actions:\n"
		 " votable\tConversions of VOTables\n"
		 " fits\t\tOperations on FITS files\n"
		 " cross\t\tCross-match on FITS tables\n"
		 );

    AddSwitch("","version","print version and license");
    AddParam("action",wxCMD_LINE_VAL_STRING,wxCMD_LINE_PARAM_OPTIONAL);

    SetCmdLine(xargc,xargv);

    return;
  }

  // the text represents a brace for remove original Usage text
  AddUsageText("#");


  if( action == "bias" ) {

    AddUsageText("Usage: munipack "+action+" [options] file(s)");
    AddUsageText("* Averaged bias frame.");
    COMMON_OPTIONS_ARITH
    COMMON_OPTIONS_PHTOLLS
    COMMON_OPTIONS_BITPIX
    COMMON_OPTIONS_SINGLE
    AddParam("file(s)",wxCMD_LINE_VAL_STRING,wxCMD_LINE_PARAM_MULTIPLE);
  }
  else if( action == "dark" ) {

    AddUsageText("Usage: munipack "+action+" [options] file(s)");
    AddUsageText("* Averaged dark frame.");

    COMMON_OPTIONS_ARITH
    AddOption("bias",wxEmptyString,"bias frame");
    COMMON_OPTIONS_PHTOLLS
    COMMON_OPTIONS_BITPIX
    COMMON_OPTIONS_SINGLE
    AddParam("file(s)",wxCMD_LINE_VAL_STRING,wxCMD_LINE_PARAM_MULTIPLE);
  }

  else if( action == "flat" ) {

    AddUsageText("Usage: munipack "+action+" [options] file(s)");
    AddUsageText("* Averaged flat-field frame.");

    AddOption("bias",wxEmptyString,"bias frame");
    AddOption("dark",wxEmptyString,"dark frame");
    AddOption("xdark",wxEmptyString,"dark frame multiplicator factor",wxCMD_LINE_VAL_DOUBLE);
    AddOption("gain","","provide value of gain (rather than FITS keyword)",wxCMD_LINE_VAL_DOUBLE);
    AddSwitch("","gain-estimate","estimate gain");
    AddOption(wxEmptyString,"level","output mean level (default 1.0)",wxCMD_LINE_VAL_DOUBLE);
    AddOption(wxEmptyString,"saturate","saturation in ADU",wxCMD_LINE_VAL_DOUBLE);
    COMMON_OPTIONS_BITPIX
    COMMON_OPTIONS_SINGLE
    AddParam("file(s)",wxCMD_LINE_VAL_STRING,wxCMD_LINE_PARAM_MULTIPLE);
  }

  else if( action == "phcorr" ) {

    AddUsageText("Usage: munipack "+action+" [options] file(s)[,result(s)]");
    AddUsageText("* Apply photometric precorrections (gain, bias, dark, flat-field).");

    AddOption("flat",wxEmptyString,"flat-field frame");
    AddOption("bias",wxEmptyString,"bias frame");
    AddOption("dark",wxEmptyString,"dark frame");
    AddOption("xdark",wxEmptyString,"dark frame multiplicator factor",wxCMD_LINE_VAL_DOUBLE);
    AddOption("bitmask",wxEmptyString,"mask frame");
    AddOption("xbitmask",wxEmptyString,"mask frame as ZERO (default), MEAN (average)");
    AddOption("gain","","provide value of gain (rather than FITS keyword)",wxCMD_LINE_VAL_DOUBLE);
    AddSwitch("","gain-ignore","ignore gain transformation");
    AddSwitch(wxEmptyString,"normalise","Normalise on averaged flat-field level");
    AddSwitch(wxEmptyString,"force","apply to already corrected frames");
    COMMON_OPTIONS_PHTOLLS
    COMMON_OPTIONS_BITPIX
    COMMON_OPTIONS_MULTIPLE
    AddParam("file(s)[,result(s)]",wxCMD_LINE_VAL_STRING,wxCMD_LINE_PARAM_MULTIPLE);
  }

  else if( action == "coloring" ) {

    AddUsageText("Usage: munipack "+action+" [options] file(s)[,ext(s)]");
    AddUsageText("* Compose color frame.");

    AddOption("c",wxEmptyString,"colorspace");
    AddOption("ct",wxEmptyString,"color table");
    COMMON_OPTIONS_BITPIX
    COMMON_OPTIONS_SINGLE
    AddParam("files[,exts]",wxCMD_LINE_VAL_STRING,
  	     wxCMD_LINE_PARAM_OPTIONAL|wxCMD_LINE_PARAM_MULTIPLE);
  }

  else if( action == "ctrafo" ) {

    AddUsageText("Usage: munipack "+action+" [options] file black(s),weight(s)]");
    AddUsageText("* Transform color frames between various color-spaces.");

    AddSwitch(wxEmptyString,"list","known colospaces");
    AddOption("c",wxEmptyString,"colorspace");
    AddOption("ct",wxEmptyString,"color table");
    COMMON_OPTIONS_BITPIX
    COMMON_OPTIONS_SINGLE
    AddParam("file black,weight",wxCMD_LINE_VAL_STRING,
  	     wxCMD_LINE_PARAM_OPTIONAL|wxCMD_LINE_PARAM_MULTIPLE);
  }

  else if( action == "cone" ) {

    AddUsageText("Usage: munipack "+action+" [options] [--] RA DEC");
    AddUsageText("* Cone search in catalogues of Virtual Observatory.");

    AddOption("c","cat","catalogue (see --list-catalogues)");
    AddOption("r","radius","radius for cone search [deg] (default 0.1)",wxCMD_LINE_VAL_DOUBLE);
    AddOption("s","sort","sort by the column");
    AddOption("","par","add optional parameters");
    AddOption("","url","service URL");
    AddOption("","id","catalogue identifier");
    AddOption("","server","Virtual observatory server (see --list-servers)");
    AddSwitch("","list-catalogues","show available catalogues");
    AddSwitch("","list-servers","show available VO servers");
    AddOption("","vocat","configuration file");
    AddOption("","type","type of output file: fits,csv,txt,xml");
    COMMON_OPTIONS_SINGLE
    AddParam("ra",wxCMD_LINE_VAL_DOUBLE,wxCMD_LINE_PARAM_OPTIONAL);
    AddParam("dec",wxCMD_LINE_VAL_DOUBLE,wxCMD_LINE_PARAM_OPTIONAL);
  }

  else if( action == "votable" ) {

    AddUsageText("Usage: munipack "+action+" [options] file");
    AddUsageText("* Conversions of VOTables.");

    AddOption("","type","type of output file: fits,csv,txt,svg,xml");
    AddOption("s","","sort by the column");
    AddOption("pt","","select projection: gnomon");
    AddOption("pa","","Right Ascension of projection center",wxCMD_LINE_VAL_DOUBLE);
    AddOption("pd","","Declination of projection center",wxCMD_LINE_VAL_DOUBLE);
    AddOption("ps","","scale of projection",wxCMD_LINE_VAL_DOUBLE);
    AddOption("ml","","magnitude limit",wxCMD_LINE_VAL_DOUBLE);
    AddOption("","col-ra","Right Ascension column");
    AddOption("","col-dec","Declination column");
    AddOption("","col-mag","magnitude column");
    COMMON_OPTIONS_SINGLE
    AddParam("file",wxCMD_LINE_VAL_STRING,wxCMD_LINE_PARAM_MULTIPLE);
  }

  else if( action == "astrometry" ) {

    AddUsageText("Usage: munipack "+action+" [options] file(s)[,results(s)]");
    AddUsageText("* Astrometry calibration.");

    AddOption("c","cat","catalogue");
    AddOption("r","ref","reference frame");
    AddOption("R","rel","reference frame (relative)");
    AddOption("p","projection","projection type: none, gnomonic (default)");
    AddOption(wxEmptyString,"col-ra","Right Ascension column");
    AddOption(wxEmptyString,"col-dec","Declination column");
    AddOption(wxEmptyString,"col-pm-ra","Proper motion in Right Ascension column");
    AddOption(wxEmptyString,"col-pm-dec","Proper motion in Declination column");
    AddOption(wxEmptyString,"col-mag","Magnitude-like column");
    AddOption(wxEmptyString,"xcen","center of frame [pix] (default: width/2)",wxCMD_LINE_VAL_DOUBLE);
    AddOption(wxEmptyString,"ycen","center of frame [pix] (default: height/2)",wxCMD_LINE_VAL_DOUBLE);
    AddOption(wxEmptyString,"rcen","center of FOV in Right Ascension [deg]",wxCMD_LINE_VAL_DOUBLE);
    AddOption(wxEmptyString,"dcen","center of FOV in Declination [deg]",wxCMD_LINE_VAL_DOUBLE);
    AddOption(wxEmptyString,"scale","scale [pix/deg]",wxCMD_LINE_VAL_DOUBLE);
    AddOption(wxEmptyString,"angle","rotation angle [deg], clockwise positive",wxCMD_LINE_VAL_DOUBLE);
    AddSwitch(wxEmptyString,"reflex","set reflection");
    AddOption(wxEmptyString,"fit","robust (default) or squares");
    AddOption(wxEmptyString,"match","backtracking (default) or nearly");
    AddOption(wxEmptyString,"units","output units: deg,arcmin,arcsec,mas,uas,pix");
    AddOption(wxEmptyString,"rms","random errors in positions [deg]",wxCMD_LINE_VAL_DOUBLE);
    AddOption(wxEmptyString,"sig","errors of centroids on frame [pix],default=1pix",wxCMD_LINE_VAL_DOUBLE);
    AddOption(wxEmptyString,"sigcat","coordinate errors of cat/ref [deg],default=1\"",wxCMD_LINE_VAL_DOUBLE);
    AddOption(wxEmptyString,"fsig","error in fluxes, default=1",wxCMD_LINE_VAL_DOUBLE);
    AddOption(wxEmptyString,"minmatch","minimal lenght of match sequence, default = 5",wxCMD_LINE_VAL_NUMBER);
    AddOption(wxEmptyString,"maxmatch","maximum lenght of match sequence, default = 50",wxCMD_LINE_VAL_NUMBER);
    AddSwitch(wxEmptyString,"full-match","full matching");
    AddSwitch(wxEmptyString,"disable-flux-check","fluxes are not used during matching");
    AddSwitch(wxEmptyString,"disable-rms-check","disable RMS check");
    AddSwitch(wxEmptyString,"disable-save","don't save calibration to header");
    AddSwitch(wxEmptyString,"remove","remove complete astrometry calibration");
    COMMON_OPTIONS_MULTIPLE
    AddParam("file(s)[,results(s)]",wxCMD_LINE_VAL_STRING,wxCMD_LINE_PARAM_MULTIPLE);
  }

  else if( action == "find" ) {

    AddUsageText("Usage: munipack "+action+" [options] file(s)[,results(s)]");
    AddUsageText("* Search for stars on frames.");

    AddOption("f","fwhm","FWHM in pixels",wxCMD_LINE_VAL_DOUBLE);
    AddOption("th","threshold","threshold in sigmas above background",wxCMD_LINE_VAL_DOUBLE);
    AddOption(wxEmptyString,"saturate","saturation in ADU",wxCMD_LINE_VAL_DOUBLE);
    AddOption(wxEmptyString,"lothresh","lower for threshold in sigmas",wxCMD_LINE_VAL_DOUBLE);
    AddOption(wxEmptyString,"rndlo","lower for round",wxCMD_LINE_VAL_DOUBLE);
    AddOption(wxEmptyString,"rndhi","higher for round",wxCMD_LINE_VAL_DOUBLE);
    AddOption(wxEmptyString,"shrplo","lower for sharp",wxCMD_LINE_VAL_DOUBLE);
    AddOption(wxEmptyString,"shrphi","higher for sharp",wxCMD_LINE_VAL_DOUBLE);
    AddOption("","read-noise","read noise in ADU (default: frame header)",wxCMD_LINE_VAL_DOUBLE);
    AddSwitch(wxEmptyString,"remove","remove find table");
    COMMON_OPTIONS_MULTIPLE
    AddParam("file(s)[,results(s)]",wxCMD_LINE_VAL_STRING,wxCMD_LINE_PARAM_MULTIPLE);
  }

  else if( action == "aphot" ) {

    AddUsageText("Usage: munipack "+action+" [options] file(s)[,results(s)]");
    AddUsageText("* Aperture photometry");

    AddOption("s","saper","select aperture number",wxCMD_LINE_VAL_NUMBER);
    AddOption(wxEmptyString,"apertures","a1,a2,... define aperture radii in pixels ");
    AddOption(wxEmptyString,"ring","ri,ro inner and outer sky ring radius in pixels ");
    AddSwitch(wxEmptyString,"remove","remove aperture photometry table");
    COMMON_OPTIONS_MULTIPLE
    AddParam("file(s)[,results(s)]",wxCMD_LINE_VAL_STRING,wxCMD_LINE_PARAM_MULTIPLE);
  }    

    /*
  case ID_CALIBRATE:
    AddParam("calibrate",wxCMD_LINE_VAL_STRING);
    AddOption("r",wxEmptyString,"search radius in degrees");
    AddParam("ra,dec file(s)",wxCMD_LINE_VAL_STRING,
  	     wxCMD_LINE_PARAM_OPTIONAL|wxCMD_LINE_PARAM_MULTIPLE);
    break;
    */

  else if( action == "timeseries" ) {

    AddUsageText("Usage: munipack "+action+" [options] [RA,Dec] ... file(s)");
    AddUsageText("* List anything to time-series.");

    AddOption("c","column","column name(s) to list");
    AddOption("C","coocolumn","column name(s) of coordinates (object search)");
    AddOption("x","extname","FITS extension identificator");
    AddOption("T","time-type","JD (default), MJD, HJD, PHASE");
    AddOption(wxEmptyString,"time-stamp","MID (default), BEGIN, END");
    AddOption(wxEmptyString,"time-err","uncertainty in the time measurement [s]",wxCMD_LINE_VAL_DOUBLE);
    AddOption(wxEmptyString,"time-res","resolution in the time measurement [s]",wxCMD_LINE_VAL_DOUBLE);
    AddOption(wxEmptyString,"epoch","reference time for light curve elements [JD]",wxCMD_LINE_VAL_DOUBLE);
    AddOption(wxEmptyString,"period","period light curve elements [days]",wxCMD_LINE_VAL_DOUBLE);
    AddOption(wxEmptyString,"tol","search radius [deg]",wxCMD_LINE_VAL_DOUBLE);
    AddOption("","coo-type","DEG (default), RECT");
    AddOption(wxEmptyString,"cat","coordinates from the FITS file");
    AddSwitch(wxEmptyString,"stdout","print results also to standard output");
    COMMON_OPTIONS_SINGLE
    AddParam("[a,d]... file(s)",wxCMD_LINE_VAL_STRING,wxCMD_LINE_PARAM_MULTIPLE);
  }

  // case ID_LIST:
  //   AddParam("list",wxCMD_LINE_VAL_STRING);
  //   //    AddOption("c","create","cat (star catalogue), lc (light curve, default)");
  //   AddOption("T","time-type","JD-Julian day (default), MJD-modified JD, HJD-heliocentric JD (+), PHASE(*)");
  //   AddOption(wxEmptyString,"time-stamp","mid (default), begin, end");
  //   AddOption("C","coordinate-type","S-spherical (default), P-cartesian, pixels");
  //   //    AddSwitch("s","spherical","spherical coordinates (default)");
  //   //    AddSwitch("p","pixels","cartesian coordinates");
  //   AddSwitch(wxEmptyString,"index","specify index");
  //   AddOption(wxEmptyString,"col","column(s) to list");
  //   AddOption(wxEmptyString,"key","header keyword(s)");
  //   //    AddSwitch(wxEmptyString,"mag","output in instrumental magnitudes");
  //   //    AddSwitch(wxEmptyString,"full","full list");
  //   //    AddSwitch(wxEmptyString,"pos","position list");
  //   AddOption(wxEmptyString,"longitude","geographical longitude of station (-east)[deg]",wxCMD_LINE_VAL_DOUBLE);
  //   AddOption(wxEmptyString,"latitude","geographical latitude of station (+north)[deg]",wxCMD_LINE_VAL_DOUBLE);
  //   AddOption(wxEmptyString,"ra","Right Ascension [deg]",wxCMD_LINE_VAL_DOUBLE);
  //   AddOption(wxEmptyString,"dec","Declination [deg]",wxCMD_LINE_VAL_DOUBLE);
  //   AddOption(wxEmptyString,"minim","basic point of light curve elements in JD (*)",wxCMD_LINE_VAL_DOUBLE);
  //   AddOption(wxEmptyString,"period","period light curve elements in days (*)",wxCMD_LINE_VAL_DOUBLE);
  //   //    AddOption(wxEmptyString,"coo","coordinates: horizontal, pixels, equatorial");
  //   AddSwitch(wxEmptyString,"mag","output in magnitudes instead counts");
  //   AddSwitch(wxEmptyString,"flux","output in fluxes instead counts");
  //   AddSwitch(wxEmptyString,"calibr","calibrated quantities (for --mag or --flux)");
  //   AddSwitch(wxEmptyString,"diffmag","differential magnitudes (for --mag)");
  //   /*
  //   AddOption(wxEmptyString,"zeromag","zero magnitude (along with --mag, --calibr)",wxCMD_LINE_VAL_DOUBLE);
  //   AddOption(wxEmptyString,"photflux","flux of 1 ADU (count) source (--flux,--calibr)",wxCMD_LINE_VAL_DOUBLE);
  //   */
  //   AddOption(wxEmptyString,"zeromag","zero magnitude (along with -Q mag, --calibr)",wxCMD_LINE_VAL_DOUBLE);
  //   AddOption(wxEmptyString,"photflux","flux of 1 ADU (count) source (-Q flux,--calibr)",wxCMD_LINE_VAL_DOUBLE);
  //   AddOption(wxEmptyString,"epoch","reference Julian date (proper motion)",wxCMD_LINE_VAL_DOUBLE);
  //   AddOption(wxEmptyString,"naperture","aperture number",wxCMD_LINE_VAL_NUMBER);
  //   AddOption(wxEmptyString,"aperture","radius of aperture [pix]",wxCMD_LINE_VAL_DOUBLE);
  //   AddOption(wxEmptyString,"tol","search radius [deg]",wxCMD_LINE_VAL_DOUBLE);
  //   AddSwitch(wxEmptyString,"stdout","results print also to standard output");
  //   AddSwitch(wxEmptyString,"print-filename","print filenames");
  //   AddOption(wxEmptyString,"file","read coordinates from the file, (-) for stdin");
  //   COMMON_OPTIONS_SINGLE
  //   AddParam("[a,d[,mua,mud]]|[x,y[,mux,muy]]... file(s)",wxCMD_LINE_VAL_STRING,
  // 	     wxCMD_LINE_PARAM_MULTIPLE);
  //   break;

  else if( action == "fits" ) {

    AddUsageText("Usage: munipack "+action+" [options] file(s)[,result(s)]");
    AddUsageText("* Operations on FITS files.");

    AddSwitch("lh","header","list header");
    AddSwitch("lt","table","list table");
    AddSwitch("li","image","list image");
    AddOption("K","print-keys","key[,key,..] print selected header keyword(s)");
    AddOption(wxEmptyString,"remove-keys","key[,key,..] remove keyword(s)");
    AddOption(wxEmptyString,"update-key","key=value[,comment] add/update keyword");
    AddSwitch(wxEmptyString,"shell","shell-friendly format for print of keywords");
    AddSwitch(wxEmptyString,"dump","dump FITS to plain text");
    AddSwitch(wxEmptyString,"restore","restore FITS from plain text");
    COMMON_OPTIONS_MULTIPLE
    AddParam("file(s)[,result(s)]",wxCMD_LINE_VAL_STRING,wxCMD_LINE_PARAM_MULTIPLE);
  }

  else if( action == "cross" ) {

    AddUsageText("Usage: munipack "+action+" [options] file1 file2");
    AddUsageText("* Cross-match of FITS tables.");
    AddOption(wxEmptyString,"tol","search radius [deg]",wxCMD_LINE_VAL_DOUBLE);
    AddOption(wxEmptyString,"ftol","relative flux tolerance",wxCMD_LINE_VAL_DOUBLE);
    AddOption(wxEmptyString,"col-ra","Right Ascension columns (.,.)");
    AddOption(wxEmptyString,"col-dec","Declination columns (.,.)");
    AddOption(wxEmptyString,"col-pm-ra","Proper motion in Right Ascension columns (.,.)");
    AddOption(wxEmptyString,"col-pm-dec","Proper motion in Declination columns (.,.)");
    AddOption(wxEmptyString,"col-mag","magnitude columns (.,.)");
    COMMON_OPTIONS_SINGLE
    AddParam("file1 file2",wxCMD_LINE_VAL_STRING,wxCMD_LINE_PARAM_MULTIPLE);
  }

  else if( action == "kombine" ) {

    AddUsageText("Usage: munipack "+action+" [options] file(s)");
    AddUsageText("* Compose frames (deep exposures, mosaics).");

    AddOption("p","projection","projection: identity, gnomonic");
    AddOption(wxEmptyString,"xcen","center of frame [pix] (default: width/2)",wxCMD_LINE_VAL_DOUBLE);
    AddOption(wxEmptyString,"ycen","center of frame [pix] (default: height/2)",wxCMD_LINE_VAL_DOUBLE);
    AddOption(wxEmptyString,"rcen","center of FOV in Right Ascension [deg]",wxCMD_LINE_VAL_DOUBLE);
    AddOption(wxEmptyString,"dcen","center of FOV in Declination [deg]",wxCMD_LINE_VAL_DOUBLE);
    AddOption(wxEmptyString,"width","width of output [pix]",wxCMD_LINE_VAL_NUMBER);
    AddOption(wxEmptyString,"height","height of output [pix]",wxCMD_LINE_VAL_NUMBER);
    AddOption(wxEmptyString,"scale","scale [pix/deg]",wxCMD_LINE_VAL_DOUBLE);
    AddOption(wxEmptyString,"angle","position angle [deg]",wxCMD_LINE_VAL_DOUBLE);    
    AddOption("i","interpol","interpolation: none,bilinear,bicubic"); //,bspline
    AddSwitch("","adjust","adjust sky level");
    AddSwitch("","expomask","use exposure mask");
    COMMON_OPTIONS_BITPIX
    COMMON_OPTIONS_SINGLE
    AddParam("file(s)",wxCMD_LINE_VAL_STRING,wxCMD_LINE_PARAM_MULTIPLE);
  }

  else if( action == "phcal" ) {

    AddUsageText("Usage: munipack "+action+" [options] file(s)[,results(s)]");
    AddUsageText("* Photometry calibration.");

    AddOption("C","cal","specify the calibration ratio(s) by hand");
    AddOption("c","cat","reference photometry catalogue");
    AddOption("r","ref","reference frame");
    AddOption("f","filters","list of filters");
    AddOption(wxEmptyString,"col-ra","Right Ascension column");
    AddOption(wxEmptyString,"col-dec","Declination column");
    AddOption(wxEmptyString,"col-mag","magnitude column(s)");
    AddOption(wxEmptyString,"col-magerr","magnitude std. error column(s)");
    AddOption("","photsys-ref","reference photometry system (catalogue)");
    AddOption("","photsys-instr","instrumental photometry system (frames)");
    AddOption(wxEmptyString,"area","area of input aperture [m2]",wxCMD_LINE_VAL_DOUBLE);
    COMMON_OPTIONS_PHQ
    AddOption(wxEmptyString,"tratab","instrumental to reference photo-system table");
    AddOption(wxEmptyString,"phsystab","photometry systems definition table");
    AddSwitch(wxEmptyString,"advanced","advanced format (additional extensions included)");
    AddSwitch(wxEmptyString,"list","show available photometry systems");
    AddOption(wxEmptyString,"tol","search radius [deg]",wxCMD_LINE_VAL_DOUBLE);
    COMMON_OPTIONS_MULTIPLE
    AddParam("file(s)[,results(s)]",wxCMD_LINE_VAL_STRING,
  	     wxCMD_LINE_PARAM_MULTIPLE|wxCMD_LINE_PARAM_OPTIONAL);
  }

  else if( action == "phfotran" ) {

    AddUsageText("Usage: munipack "+action+" [options] file(s)");
    AddUsageText("* Determine photometry system color transformation.");

    AddOption("c","cat","reference photometry catalogue");
    AddOption("f","filters","list of filters");
    AddOption("C","cal","calibration ratios in the filters");
    AddOption(wxEmptyString,"col-ra","Right Ascension column");
    AddOption(wxEmptyString,"col-dec","Declination column");
    AddOption(wxEmptyString,"col-mag","magnitude column(s)");
    AddOption(wxEmptyString,"col-magerr","magnitude std. error column(s)");
    AddOption("","photsys-ref","reference photometry system (catalogue)");
    AddOption("","photsys-instr","instrumental photometry system (frames)");
    AddOption(wxEmptyString,"area","area of input aperture [m2]",wxCMD_LINE_VAL_DOUBLE);
    AddOption(wxEmptyString,"phsystab","photometry systems definition table");
    AddSwitch(wxEmptyString,"list","show available photometry systems");
    AddOption(wxEmptyString,"tol","search radius [deg]",wxCMD_LINE_VAL_DOUBLE);
    COMMON_OPTIONS_SINGLE
    AddParam("file(s)",wxCMD_LINE_VAL_STRING,
  	     wxCMD_LINE_PARAM_MULTIPLE|wxCMD_LINE_PARAM_OPTIONAL);
  }
}

MuniCmdLineParser::~MuniCmdLineParser()
{
  for(int i = 0; i < argc; i++)
    free(argv[i]);
  delete[] argv;
}


void MuniCmdLineParser::Usage() const
{
  wxString u(GetUsageString());

  // default Usage prints no actions, we are remove one and replace
  // with acceptable form.
  wxRegEx re("(Usage:.*[#]\n)(.*)");
  wxASSERT(re.IsValid());
  if( re.Matches(u) ) {
    wxString t = re.GetMatch(u);
    re.Replace(&t,"\\2");
    wxPrintf(t);
  }
  else
    wxPrintf(GetUsageString());

}
