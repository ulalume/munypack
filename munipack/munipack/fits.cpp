/*

  FITS manipulations

  Copyright © 2011-3 F.Hroch (hroch@physics.muni.cz)

  This file is part of Munipack.

  Munipack is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  
  Munipack is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public License
  along with Munipack.  If not, see <http://www.gnu.org/licenses/>.

  Note.

  Multiple use of AddOption is not supported. Therefore the

     fits -K NAXIS1 -K NAXIS2

  does not works and tokenizers(s) must be used on boths tails of pipes.

*/

#include "munipack.h"
#include <wx/wx.h>
#include <wx/tokenzr.h>
#include <wx/filename.h>


using namespace std;


bool Munipack::fits(MuniProcess *action, MuniCmdLineParser& cmd)
{
  wxString mode = "STRUCTURE";
  wxString a;

  if( cmd.Found("K",&a) || cmd.Found("print-keys",&a) ) {

    mode = "HEADER";

    wxStringTokenizer tokenizer(a, ",");
    while ( tokenizer.HasMoreTokens() )
      action->Write("KEYWORD = '" + tokenizer.GetNextToken() + "'");
  }

  else if( cmd.Found("remove-keys",&a) ) {

    mode = "HEADER";

    wxStringTokenizer tokenizer(a, ",");
    while ( tokenizer.HasMoreTokens() )
      action->Write("REMKEY = '" + tokenizer.GetNextToken() + "'");
  }

  else if( cmd.Found("update-key",&a) ) {

    mode = "HEADER";

    wxString line("UPDATEKEY =");
    wxStringTokenizer tokenizer(a, ",=");
    for(size_t i = 0; i < 3; i++) {
      if( tokenizer.HasMoreTokens() ) {
	wxString s(tokenizer.GetNextToken());
	s.Replace("'","''");
	line += " '"+s+"'";
      }
      else
	line += " ''";
    }
    action->Write(line);
  }

  else if( cmd.Found("lh") || cmd.Found("header") ) {

    mode = "HEADER";

  }

  else if( cmd.Found("lt") || cmd.Found("table") ) {

    mode = "TABLE";

  }

  else if( cmd.Found("li") || cmd.Found("image") ) {

    mode = "IMAGE";

  }

  else if( cmd.Found("dump") ) {

    mode = "DUMP";

  }
  else if( cmd.Found("restore") ) {

    mode = "RESTORE";

  }

  action->Write("MODE = '" + mode + "'");

  if( cmd.Found("shell") )
    action->Write("KEYLIST = 'SHELL'");

  CommonOutputMultiple(action,cmd);

  if( ! cmd.Found("O") && (mode == "RESTORE" || mode == "DUMP") ) {
    // In this modes, .lst are defaultly used

    SetAdvanced(true);
    if( mode == "RESTORE" ) {
      SetPattern("(.*).lst");
      SetMask("\\1.fits");
    }
    else if( mode == "DUMP" ) {
      SetPattern("(.*).fits");
      SetMask("\\1.lst");
    }
  }

  for(size_t i = 0; i < cmd.GetParamCount(); i++)
    WriteFiles(action,cmd.GetParam(i));

  return true;
}
