/*

  Aperture Photometry

  Copyright © 2010 - 2013 F.Hroch (hroch@physics.muni.cz)

  This file is part of Munipack.

  Munipack is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  
  Munipack is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public License
  along with Munipack.  If not, see <http://www.gnu.org/licenses/>.

*/

#include "munipack.h"
#include <wx/wx.h>
#include <wx/log.h>
#include <wx/filename.h>
#include <wx/tokenzr.h>

using namespace std;


bool Munipack::aphot(MuniProcess *action, MuniCmdLineParser& cmd)
{
  CommonOutputMultiple(action,cmd);

  if( cmd.Found("remove") )

    action->Write("REMOVE = T");

  else {

    wxString a;
    long n;

    if( cmd.Found("apertures",&a) ) {

      n = 0;
      wxStringTokenizer t(a,",");
      while ( t.HasMoreTokens() ) {
	t.GetNextToken();
	n++;
      }
      
      action->Write("NAPERTURES = %ld",n);
      action->Write("APERTURES = "+a);
    }

    if( cmd.Found("ring",&a) )
      action->Write("RING = "+a);

    if( cmd.Found("s",&n) || cmd.Found("saper",&n) )
      action->Write("SAPER = %ld",n);

  }

  for(size_t i = 0; i < cmd.GetParamCount(); i++)
    WriteFiles(action,cmd.GetParam(i));

  return true;
}
