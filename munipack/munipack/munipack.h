/*

  Munipack


  Copyright © 2009-2014 F.Hroch (hroch@physics.muni.cz)

  This file is part of Munipack.

  Munipack is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  
  Munipack is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public License
  along with Munipack.  If not, see <http://www.gnu.org/licenses/>.

*/

#include "version.h"
#include "config.h"
#include "mprocess.h"
#include <wx/wx.h>
#include <wx/app.h>
#include <wx/cmdline.h>
#include <wx/process.h>
#include <wx/event.h>
#include <queue>


class MuniCmdLineParser: public wxCmdLineParser
{
public:
  MuniCmdLineParser(const wxString&,int,wchar_t **);
  virtual ~MuniCmdLineParser();

  void Usage() const;

private:

  int argc;
  wchar_t **argv;

};


class Munipack: public wxAppConsole
{
public:

  bool OnInit();
  int OnRun();
  void OnFinish(wxProcessEvent&);

private:

  void CommonOptionsBitpix(MuniProcess *, const MuniCmdLineParser&);
  void CommonOptionsPhCorr(MuniProcess *, const MuniCmdLineParser&);
  void CommonOptionsArith(MuniProcess *, const MuniCmdLineParser&);
  void CommonOptionsPhq(MuniProcess *, const MuniCmdLineParser&);
  void CommonOptionsAddKey(MuniProcess *, const MuniCmdLineParser&);
  void CommonOutputMultiple(MuniProcess *, const MuniCmdLineParser&);
  void CommonOutputSingle(MuniProcess *, const MuniCmdLineParser&, const wxString& =wxEmptyString);

  bool bias(MuniProcess *,MuniCmdLineParser&);
  bool dark(MuniProcess *,MuniCmdLineParser&);
  bool flat(MuniProcess *,MuniCmdLineParser&);
  bool phcorr(MuniProcess *,MuniCmdLineParser&);
  bool coloring(MuniProcess *,MuniCmdLineParser&);
  bool ctrafo(MuniProcess *,MuniCmdLineParser&);
  bool cone(MuniProcess *,MuniCmdLineParser&);
  bool astrometry(MuniProcess *,MuniCmdLineParser&);
  bool phcal(MuniProcess *,MuniCmdLineParser&);
  bool find(MuniProcess *,MuniCmdLineParser&);
  bool aphot(MuniProcess *,MuniCmdLineParser&);
  bool votable(MuniProcess *,MuniCmdLineParser&);
  bool fits(MuniProcess *,MuniCmdLineParser&);
  //  void calibrate(MuniProcess *,MuniCmdLineParser&);
  //  void list(MuniProcess *,MuniCmdLineParser&);
  bool timeseries(MuniProcess *,MuniCmdLineParser&);
  bool kombine(MuniProcess *,MuniCmdLineParser&);
  bool phfotran(MuniProcess *,MuniCmdLineParser&);
  bool cross(MuniProcess *,MuniCmdLineParser&);

  void cone_lists(const MuniCmdLineParser&);

  void WriteFile(MuniProcess *, const wxString&);
  void WriteFiles(MuniProcess *, const wxString&);
  void WriteOutput(MuniProcess *, const wxString&);
  bool InputBy(MuniProcess *, const wxString&,bool);
  void apstr(const wxString&, long&, wxString&);

  void GetResult(const wxString&, wxString&, wxString&, wxString&);
  long FindBackup(const wxString&) const;
  wxString GetBackup(const wxString&) const;
  wxString GetOutput(const wxString&) const;

  void SetAdvanced(bool b) { advanced = b; }
  void SetPattern(const wxString& a) { pattern = a; }
  void SetMask(const wxString& a) { mask = a; }
  void SetFormat(const wxString& a) { format = a; }
  void SetSuffix(const wxString& a) { suffix = a; }
  void SetControl(const wxString& a) { control = a; }
  bool SetTargetDir(const wxString&);
  void EnableBackup(bool e =true) { control= e ? "existing" : "none"; }

  bool advanced;
  wxString pattern, mask, format, suffix, control, target;
  int ncount;

  MuniPipe pipe;
};

