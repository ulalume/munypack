/*

  timeseries (light curve)

  Copyright © 2012-3 F.Hroch (hroch@physics.muni.cz)

  This file is part of Munipack.

  Munipack is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  
  Munipack is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public License
  along with Munipack.  If not, see <http://www.gnu.org/licenses/>.


*/

#include "munipack.h"
#include <wx/wx.h>
#include <wx/tokenzr.h>
#include <wx/ffile.h>
#include <wx/wfstream.h>
#include <wx/txtstrm.h>
#include <wx/sstream.h>
#include <wx/intl.h>
#include <wx/regex.h>
#include <vector>

using namespace std;

bool Munipack::timeseries(MuniProcess *action, MuniCmdLineParser& cmd)
{
  wxString s;
  double x;
  vector<size_t> icoo;

  // determine character used as decimal point
  wxString decimal = wxLocale::GetInfo(wxLOCALE_DECIMAL_POINT,wxLOCALE_CAT_NUMBER);
  wxString sep = decimal == "." ? ",;" : ";";

  CommonOutputSingle(action,cmd);

  if( cmd.Found("x",&s) || cmd.Found("extname",&s) )
    action->Write("EXTNAME = '" + s + "'");

  if( cmd.Found("c",&s) || cmd.Found("column",&s) ) {

    wxString l;
    long n;
    apstr(s,n,l);
    action->Write("NCOL = %d",n);
    action->Write("COL = " + l);
  }  

  if( cmd.Found("C",&s) || cmd.Found("coocolumn",&s) ) {

    wxString l;
    long n;
    apstr(s,n,l);
    action->Write("NCOOCOL = %d",n);
    action->Write("COOCOL = " + l);
  }  

  if( cmd.Found("T",&s) || cmd.Found("time-type",&s) )
    action->Write("TIMETYPE = '" + s.MakeUpper() + "'");

  if( cmd.Found("time-stamp",&s) )
    action->Write("TIMESTAMP = '" + s.MakeUpper() + "'");

  if( cmd.Found("time-err",&x) )
    action->Write("TIMEERR = %e",x);

  if( cmd.Found("time-res",&x) )
    action->Write("TIMERES = %e",x);

  if( cmd.Found("cat",&s) )
    action->Write("CATALOGUE = '" + s + "'");

  if( cmd.Found("epoch",&x) )
    action->Write("EPOCH0 = %e",x);

  if( cmd.Found("period",&x) )
    action->Write("PERIOD = %e",x);

  if( cmd.Found("tol",&x) )
    action->Write("TOL = %e",x);

  if( cmd.Found("coo-type",&s) )
    action->Write("COOTYPE = '" + s.MakeUpper() + "'");

  if( cmd.Found("stdout") )
    action->Write("STDOUT = T");


  // recognizing numbers in twices as coordinates
  vector<double> a,d;
  vector<double> w;

  for(size_t i = 0; i < cmd.GetParamCount(); i++) {
    wxString l(cmd.GetParam(i));

    w.clear();
    wxStringTokenizer tokenizer(l,sep);
    while( tokenizer.HasMoreTokens() ){
      wxString token = tokenizer.GetNextToken();
      double q;
      if( token.ToDouble(&q) )
	w.push_back(q);
    }
    
    if( w.size() == 2 ) {

      a.push_back(w[0]);
      d.push_back(w[1]);
      icoo.push_back(i);

    }    
  }     

  if( a.size() > 0 ) {
    action->Write("NCOO = %d",a.size());
    for(size_t i = 0; i < a.size(); i++)
      action->Write("COO = %.15f %.15f",a[i],d[i]);
  }

  // files (skipping already detected numbers)
  vector<wxString> files;
  for(size_t i = 0; i < cmd.GetParamCount(); i++) {

    bool found = false;
    for(size_t j = 0; j < icoo.size(); j++)
      if( icoo[j] == i )
	found = true;

    if( ! found )
      files.push_back(cmd.GetParam(i));
  }

  if( ! files.empty() ) {
    action->Write("NFILES = %d",files.size());
    for(size_t i = 0; i < files.size(); i++)
      WriteFile(action,files[i]);
  }

  return true;
}
