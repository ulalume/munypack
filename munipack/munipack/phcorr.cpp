/*

  Photometric corrections

  Copyright © 2012-3 F.Hroch (hroch@physics.muni.cz)

  This file is part of Munipack.

  Munipack is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  
  Munipack is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public License
  along with Munipack.  If not, see <http://www.gnu.org/licenses/>.

*/

#include "munipack.h"


bool Munipack::phcorr(MuniProcess *action, MuniCmdLineParser& cmd)
{
  CommonOptionsBitpix(action,cmd);
  CommonOutputMultiple(action,cmd);
  CommonOptionsPhCorr(action,cmd);

  wxString a;
  if( cmd.Found("flat",&a) )
    action->Write("FLAT = '"+a+"'");

  if( cmd.Found("bias",&a) )
    action->Write("BIAS = '"+a+"'");

  if( cmd.Found("dark",&a) )
    action->Write("DARK = '"+a+"'");

  if( cmd.Found("bitmask",&a) )
    action->Write("MASK = '"+a+"'");

  if( cmd.Found("xbitmask",&a) )
    action->Write("XMASK = '"+a.Upper()+"'");

  double x;
  if( cmd.Found("xdark",&x) )
    action->Write("XDARK = %lf",x);

  if( cmd.Found("gain",&x) )
    action->Write("GAIN = %lf",x);

  if( cmd.Found("gain-ignore") )
    action->Write("GAIN_APPLY = F");

  if( cmd.Found("force") )
    action->Write("FORCEAPPLY = T");

  if( cmd.Found("normalise") )
    action->Write("NORMALISE = T");

  for(size_t i = 0; i < cmd.GetParamCount(); i++)
    WriteFiles(action,cmd.GetParam(i));

  return true;
}
