/*

  Implements FITS file name utilities (taylored by wxFileName):

  * GNU backup-file conventions

  * file-existing


  Copyright © 2013 F.Hroch (hroch@physics.muni.cz)

  This file is part of Munipack.

  Munipack is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  
  Munipack is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public License
  along with Munipack.  If not, see <http://www.gnu.org/licenses/>.

*/

#include <wx/wx.h>

class FITSFileName
{
public:
  FITSFileName(const wxString&);
  wxString GetBackup() const;
  wxString GetBaseName() const;
  bool FileExists() const;

  void SetSuffix(const wxString&);
  void SetControl(const wxString&);

  static bool ParseExt(const wxString&, wxString&,wxString&);

private:

  wxString filename,backup,version_control,simple_backup_suffix;

  wxString Get(const wxString&) const;
  long FindBackup(const wxString&) const;
  wxString NumberBackup(const wxString&,long) const;
};
