/* 

  VOTable parser


  Copyright © 2010 - 2013 F.Hroch (hroch@physics.muni.cz)

  This file is part of Munipack.

  Munipack is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  
  Munipack is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public License
  along with Munipack.  If not, see <http://www.gnu.org/licenses/>.


  Reference:

  http://www.ivoa.net/Documents/VOTable/20091130/REC-VOTable-1.2.html

*/

#include "votable.h"
#include <wx/wx.h>
#include <wx/app.h>
#include <wx/wfstream.h>
#include <wx/txtstrm.h>
#include <cstdio>

using namespace std;

class VOTab: public wxAppConsole
{
public:
  bool OnInit();
  int OnRun();
private:
  int vooper();
};

IMPLEMENT_APP_CONSOLE(VOTab)

bool VOTab::OnInit()
{
  wxLog::DisableTimestamp();
  return true;
}

int VOTab::OnRun()
{
  int exitcode = vooper();

  // Looks strangle? See: cone.cpp.

  fprintf(stderr,"STOP %d\n",exitcode);
  return exitcode;
}


int VOTab::vooper()
{
  wxString output, backup, type, file, sort;
  wxString pt, mk, ak, dk;
  double pa, pd, ps, ml;

  wxFFileInputStream pstream(stdin);
  wxTextInputStream input(pstream); 

  while(pstream.IsOk() && ! pstream.Eof()) {

    wxString line = input.ReadLine();

    if( line.StartsWith("VERBOSE") && GetBool(line) )
	wxLog::SetLogLevel(wxLOG_Debug);

    if( line.StartsWith("TYPE") )
      type = GetString(line);

    if( line.StartsWith("SORT") )
      sort = GetString(line);

    if( line.StartsWith("FILE") )
      file = GetString(line);

    if( line.StartsWith("PROJ TYPE") )
      pt = GetString(line);

    if( line.StartsWith("PROJ ALPHA") )
      pa = GetDouble(line);

    if( line.StartsWith("PROJ DELTA") )
      pd = GetDouble(line);

    if( line.StartsWith("PROJ SCALE") )
      ps = GetDouble(line);

    if( line.StartsWith("MAG LIMIT") )
      ml = GetDouble(line);

    if( line.StartsWith("COL_RA") )
      ak = GetString(line);

    if( line.StartsWith("COL_DEC") )
      dk = GetString(line);

    if( line.StartsWith("COL_MAG") )
      mk = GetString(line);

    if( line.StartsWith("OUTPUT") )
      GetOutput(line,output,backup);

  }


  wxFFileInputStream istream(file);
  VOTable vt(istream);

  if( ! sort.IsEmpty() )
    vt.Sort(sort);

  vt.SetProjection(pt);
  vt.SetProjectionCenter(pa,pd);
  vt.SetScale(ps);
  vt.SetMaglim(ml);
  vt.SetMagkey(mk);
  vt.SetAlphakey(ak);
  vt.SetDeltakey(dk);
  
  return vt.Save(output,backup,type) ? 0 : 1;
}

