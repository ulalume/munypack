
!
! gfortran -Wall teststat.f95 -L. -L../minpack -lrstat -lsort -llmin -lminpacks -lminpack -lm
!

program teststat

  use robustmean
  use rfun

  implicit none

  integer, parameter :: rp = selected_real_kind(15)

  real(rp) :: x(7), t(66666), z(7), w(16)
  real(rp) :: u,v,s
  integer :: i,n

!  goto 20

!!$  w = (/10035.000000000000,        10001.000000000000,        10030.000000000000 ,    &
!!$       10058.000000000000 ,       10072.000000000000 ,       10012.000000000000  , &
!!$       10043.000000000000  ,      10061.000000000000  ,      10013.000000000000  ,   &
!!$       10047.000000000000    ,    10031.000000000000   ,     10022.000000000000   , &
!!$       10010.000000000000   ,     10072.000000000000  ,      10014.000000000000     ,  &
!!$       10027.000000000000   ,     10033.052157716564 /)
!!$
!!$  w = (/  10049.000000000000   ,     10063.000000000000 ,       10059.000000000000 , &
!!$       9994.0000000000000  ,      10063.000000000000 ,       10016.000000000000 ,&
!!$       10040.000000000000  ,      10014.000000000000 ,       10058.000000000000 ,&
!!$       10012.000000000000  ,      10019.000000000000 ,       10016.000000000000 ,&
!!$       10064.000000000000  ,      10032.000000000000 ,       10070.000000000000 ,&
!!$       10034.000000000000  ,      10038.500000000040 /) 

  w = (/ 10109.000000000000 ,       10109.000000000000  ,      10109.000000000000 , &
       10109.000000000000 ,       10109.000000000000  ,      10109.000000000000  , &
       10109.000000000000 ,       10109.000000000000   ,     10109.000000000000  , &
       729415.00000000000 ,       10109.000000000000  ,      10109.000000000000 , &
       10109.000000000000 ,       10109.000000000000  ,      10109.000000000000 , &
       10109.000000000000 /)

  w(1:9) = (/ 0.00000000,       0.00000000,       0.00000000,       0.00000000, &
       0.00000000,       0.00000000,       109.288963,       18.7713432, &
       0.00000000 /)

  call rmean(w(1:9),u,v,s)
  write(*,*) "Rmean (patological):",u, v,s
!  stop
  do i = 0,20
     u = i/20.0
     v = invnorm(u)
!     write(*,*) u,v,(1+erf(v/sqrt(2.0)))/2-u
  end do
!  stop

  x = (/16, 12, 99,95,18,87,10 /)
  z = (/142, 141, 149, 149, 142, 148,149 /)

  call rmean(z,u,v,s)
  write(*,*) "Rmean (dark):",u, v,s

  u = sum(z)/size(z)
  v = sqrt(sum((z-u)**2)/(size(z)-1))
  write(*,*) "Amean: (dark)",u,v/sqrt(1.0*size(z)),v
!  stop

!  call medmad(x,u,v)
!  write(*,*) u,v

  20 continue

  open(1,file='s')
  do i = 1, size(t)
     t(i) = gdis(0.0_rp,1.0_rp)
     write(1,*) t(i)
  end do
  close(1)
  u = sum(t)/size(t)
  s = sqrt(sum((t-u)**2)/(size(t)-1.0))
  write(*,*) "Amean N(9,3): ",u, s, v/sqrt(real(size(t)))
!  stop
!  write(*,*) t
  call rmean(t,u,v,s)
  write(*,*) "Rmean N(9,3): ",u, s, v

  s = 0
  open(1,file='huber')
  do i = -50000,50000
     u = i/10000.0
!     write(1,*) u,ihuber(u)
!     s = s + exp(-ihuber(u))
     write(1,*) u,s*0.001/sqrt(2*3.14159)
!     write(1,*) u,exp(-ihuber(u))/sqrt(2*3.14159),exp(-u**2/2)/sqrt(2*3.14159)
!     if( i > 0 .and. i < 100) s = s + exp(-u**2/2)/sqrt(2*3.14159)
!     if( i > 0 .and. i < 100) s = s + exp(-ihuber(u))/sqrt(2*3.14159)
!     if( i > 0 .and. s < 0.25) s = s + 0.01*exp(-ihuber(u))/sqrt(2*3.14159)
     if( i > -100000000 ) then
        if( s < 10.25) then
!           s = s + 0.0001*exp(-u**2/2)/sqrt(2*3.14159)*u**2
!           s = s + 0.0001*exp(-ihuber(u))/sqrt(2*3.14159)*u**2
           s = s + 0.0001*exp(-abs(u))/2*u**2
        else
           write(*,*) u
           exit
        end if
     end if
  end do
  close(1)
!  write(*,*) 'quantil:',2*s
  write(*,*) 'quantil:',s


!  stop

  do i = 1, int(0.98*size(t))
     t(i) = gdis(9.0_rp,3.0_rp)
     n = i
  end do
!  n = 0
!  n = 999
  write(*,*)
  do i = n+1, size(t)
     t(i) = gdis(3.0_rp,1.0_rp)
  end do
  u = sum(t)/size(t)
  v = sqrt(sum((t-u)**2)/(size(t)-1.0))
  write(*,*) "Amean 0.9: N(9,3) + 0.1: N(3,3): ",u, v, v/sqrt(size(t)-0.0)


  call rinit(t,u,v)
  write(*,*) "Rinit:",u, v

  call rmean(t,u,v,s)
  write(*,*) "Rmean 0.9: N(9,3) + 0.1: N(3,3): ",u, v,s

  contains

    function gdis(mean, sig)

      ! generate random data with gauss distribution

      real(rp), intent(in) :: mean, sig
      real(rp) :: gdis, x

      call random_number(x)
!      gdis = invnorm(x)*sig + mean!)!*sig !+ mean !- sig !/ mean
!      gdis = invnorm(x)*sig + mean !- sig/mean
!     gdis = mean +sig - invdist(x)*sig  !- sig/mean
!      gdis = mean - sqrt(2.0)*sig*ierfc(2*x)   ! Numerical Recipes (6.14.3)
      gdis = mean - 1.618*sqrt(2.0)*sig*ierfc(2*x)   ! Numerical Recipes (6.14.3)

    end function gdis

    function ierfc(x)

      real(rp), intent(in) :: x
      real(rp) :: ierfc

      ierfc = ierf(x-1)

    end function ierfc

    function ierf(x)

      ! http://en.wikipedia.org/wiki/Error_function

      real(rp), intent(in) :: x
      real(rp) :: ierf, u, v, a, y, w
      
      a = 0.140012288686666
      u = log(1 - x**2)
      v = 2/3.14159265358979/a
      y = v + u/2

      w = sqrt(y**2 - u/a)
      ierf = sign(1.0d0,x)*sqrt(w - y)

    end function ierf

    function invdist(xx)

      real(rp), intent(in) :: xx
      real(rp) :: invdist

      ! inverzni fce k distribucni fci Gaussova rozdeleni
      ! s presnosti vetsi jak 0.00045

      real(rp) :: w,f,x
      logical :: interval

      x = xx
      if( x < 0.0 ) then
         invdist = 0.0
      elseif( x > 1.0 )then
         invdist = 1.0
      else
         interval = x < 0.5
         if( .not. interval ) x = 1.0 - x + epsilon(1.0)
         w = sqrt(-2.0*log(x));
         f = -w + (2.515517 + w*(0.802853 + w*0.010328))/ &
              (1.0 + w*(1.432788 + w*(0.189269 + w*0.001308)));
         if( interval ) then
            invdist = f
         else
            invdist = -f
         endif
      endif

    end function invdist

    ! excelent overwiew of algorithms !
    ! http://home.online.no/~pjacklam/notes/invnorm/

    function invnorm(p)

      !ren-raw chen, rutgers business school
      ! normal inverse
      ! translate from
      ! http://home.online.no/~pjacklam/notes/invnorm
      ! a routine written by john herrero
      real*8 invnorm
      real*8 p,p_low,p_high
      real*8 a1,a2,a3,a4,a5,a6
      real*8 b1,b2,b3,b4,b5
      real*8 c1,c2,c3,c4,c5,c6
      real*8 d1,d2,d3,d4
      real*8 z,q,r
      a1=-39.6968302866538
      a2=220.946098424521
      a3=-275.928510446969
      a4=138.357751867269
      a5=-30.6647980661472
      a6=2.50662827745924
      b1=-54.4760987982241
      b2=161.585836858041
      b3=-155.698979859887
      b4=66.8013118877197
      b5=-13.2806815528857
      c1=-0.00778489400243029
      c2=-0.322396458041136
      c3=-2.40075827716184
      c4=-2.54973253934373
      c5=4.37466414146497
      c6=2.93816398269878
      d1=0.00778469570904146
      d2=0.32246712907004
      d3=2.445134137143
      d4=3.75440866190742
      p_low=0.02425
      p_high=1-p_low
      if(p.lt.p_low) goto 201
      if(p.ge.p_low) goto 301
201   q=dsqrt(-2*dlog(p))
      z=(((((c1*q+c2)*q+c3)*q+c4)*q+c5)*q+c6)/((((d1*q+d2)*q+d3)*q+d4)*q+1)
      goto 204
301   if((p.ge.p_low).and.(p.le.p_high)) goto 202
      if(p.gt.p_high) goto 302
202   q=p-0.5
      r=q*q
      z=(((((a1*r+a2)*r+a3)*r+a4)*r+a5)*r+a6)*q/(((((b1*r+b2)*r+b3)*r+b4)*r+b5)*r+1)
      goto 204
302   if((p.gt.p_high).and.(p.lt.1)) goto 203
203   q=dsqrt(-2*dlog(1-p))
      z=-(((((c1*q+c2)*q+c3)*q+c4)*q+c5)*q+c6)/((((d1*q+d2)*q+d3)*q+d4)*q+1)
204   invnorm=z
      return

    end function invnorm


    function distgaus(t)

      real(rp), parameter :: sqrtpi2 = 2.50662827463100050242
      real(rp), intent(in) ::  t
      real(rp) ::  w,f, x, distgaus
      logical :: minus
      
      x = t
      minus = x < 0.0
      if( x > 6.0 )then
         distgaus = 1.0
      elseif( x <= -6.0 )then
         distgaus = 0.0
      else
         x = abs(x)
         w = 1.0/(1.0 + 2.316419 - x)
         f = 1.0 - exp(-x**2/2.0)/sqrtpi2*w* &
              (0.3193815+w*(-0.3565638+w*(1.781478+w*(-1.821256+w*1.330274))))
         if( minus )then 
            distgaus = 1.0 - f
         else
            distgaus = f
         endif
      endif
     
    end function distgaus

end program teststat
