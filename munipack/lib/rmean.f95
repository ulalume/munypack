!
!  Robust mean
!
!  Copyright © 2001 - 2015 F.Hroch (hroch@physics.muni.cz)
!
!  This file is part of Munipack.
!
!  Munipack is free software: you can redistribute it and/or modify
!  it under the terms of the GNU General Public License as published by
!  the Free Software Foundation, either version 3 of the License, or
!  (at your option) any later version.
!  
!  Munipack is distributed in the hope that it will be useful,
!  but WITHOUT ANY WARRANTY; without even the implied warranty of
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!  GNU General Public License for more details.
!  
!  You should have received a copy of the GNU General Public License
!  along with Munipack.  If not, see <http://www.gnu.org/licenses/>.
!
!

module robustmean

  implicit none

  ! Robust estimator of mean by:
  !  * Hogg in Launer, Wilkinson: Robustness in Statistics
  !  * Hubber: Robust Statistics
  !  * my experiences
  !
  ! 
  ! Subroutines Avalilable:
  !
  ! Estimates of robust mean, stat. deviation and dispersion:
  ! * rmean - uses simlutaneous fitting of scale and location, best results
  ! * rmean0 - basic robust estimator, just location is fitted, (fast method)
  ! * rmean1 - just working version, simlutaneous fitting of scale and location
  !            problems with convergence
  ! * rmean2 - joint robust eatimator of location and scale (best method)
  ! * qmean - estimate on base of quantiles (constructs CDF), for initials
  !
  ! Initial estimations:
  ! * rinit
  !
  ! Medians:
  ! * qmed - quick-median
  ! * xmed - median of sorted array


  ! print debug informations ?
  logical, parameter, private :: verbose = .false.
  logical, parameter, private :: debug = .false.

  ! estimate jacobian by differences or derivations ?
  logical, parameter, private :: analytic = .true.

  ! numerical precision of real numbers
  integer, parameter, private :: dbl = selected_real_kind(15)

  ! 50% quantil of N(0,1)
  real(dbl), parameter, private :: q50 = 0.6745

  ! limit to detect non-convergent series, reccomended values: >100
  real(dbl), parameter, private :: siglim = 1000.0

  real(dbl), dimension(:), allocatable, private :: xdata

  interface rmean
     module procedure rmean_double, rmean_single
  end interface rmean

  private :: funder, fundif, difjac, loglikely, rmean_double, rmean_single

contains

  ! rmean is an user's API

  subroutine rmean_double(x,t,dt,sig)

    ! simple interface for rmean

    real(dbl), dimension(:),intent(in) :: x
    real(dbl), intent(out) :: t,dt
    real(dbl), intent(out), optional :: sig
    real(dbl), parameter :: machtol = epsilon(t)
    real(dbl) :: xsig
    integer :: istat

    call rmean2(x,t,dt,xsig,machtol,istat)
    if( present(sig) ) sig = xsig
    
  end subroutine rmean_double

  subroutine rmean_single(x,t,dt,sig)

    ! this single version via double one is a little bit slower, 
    ! although easy to implement

    real, dimension(:),intent(in) :: x
    real, intent(out) :: t,dt
    real, intent(out), optional :: sig
    real(dbl), dimension(:), allocatable :: y
    real(dbl), parameter :: machtol = epsilon(t)
    real(dbl) :: d,dd,s
    integer :: istat

    allocate(y(size(x)))
    y = x
    call rmean2(y,d,dd,s,machtol,istat)
    deallocate(y)
    t = real(d)
    dt = real(dd)
    if( present(sig) ) sig = real(s)

  end subroutine rmean_single

  !-------------------------------------------------------------------

  subroutine rinit(x,t,s)

    ! initial estimate of parameters

    real(dbl), dimension(:),intent(in) :: x
    real(dbl), intent(out) :: t,s

    real(dbl) :: mad,med
    integer :: n,nmed

    n = size(x)

    if( n > 50 ) then

       ! This choice of the threshold used for median computation method
       ! assumes that odd and even elements in sequence are the same (within
       ! requested precision). Note that qmed is the fastest known algorithm 
       ! (~ 2*n) while xmed is simpler and slower (~ n*log(n)).

       nmed = min(n/2+1,n)
       ! this is right just only for odd-th elements of the sequence,
       ! we're ignore the right way, having huge dataset
       med = qmed(x,nmed)
       mad = qmed(abs(x - med),nmed)

       ! sigma for Normal distribution
       s = mad/q50
       t = med

    else if( n > 7 ) then
       
       ! correct way to compute median (two middle values)
       med = xmed(x)
       mad = xmed(abs(x - med))

       ! sigma for Normal distribution
       s = mad/q50
       t = med

    else if( n > 2 ) then

       ! compute parameters from empirical CDF by symetrical quantilies
       call qmean(x,t,s)

    else if( n == 2 ) then
       
       t = (x(1) + x(2))/2
       s = abs(x(1) - x(2))/2
       
    else if( n == 1 ) then
       
       t = x(1)
       s = 0

    else

       t = 0
       s = 0

    end if

    if( verbose ) write(*,*) "Rinit: t, s= ",t, s

  end subroutine rinit

  !-----------------------------------------------------------------------

  subroutine rmean1(x,t,dt,sig,xrel,xabs,maxit,istat)

    ! estimate mean and standard deviation by minimizing
    ! both mean and scale together, the equations are solved
    ! by combination of the iteraction and Newton's method
    ! as reccomends Huber (1980), p. 147 (6.7 the computation
    ! of M-estimates)
    !
    ! t   is solution on output
    ! dt  is standard error of t
    !
    !
    ! IMPORTANT
    !
    ! The procedure works well when covariance matrix is nearly
    ! diagonal because minimizes scale and mean separately in 
    ! theirs directions. By another words, the eigenvectors are
    ! orthogonal. The condition is sometimes violated when (perhaps) 
    ! data has a distribution unlike the Normal. In this case,
    ! the solution can converge to a false point. This is reason 
    ! for use rmean2 in any case! Second reason is the non-convergence
    ! when initial estimate is behind horizon of convergence.
    !
    ! Consider the implementation is being for study only.
    !

    use rfun

    real(dbl), dimension(:), intent(in) :: x
    real(dbl), intent(out) :: t,dt,sig
    real(dbl), intent(in) :: xrel, xabs
    integer, intent(in) :: maxit
    integer, intent(out) :: istat

    integer :: n,i,it
    real(dbl) :: d,t0,s,s0,sum1,sum2,sum3,absd,xm,var,kcorr
    real(dbl), dimension(:), allocatable :: r,p,dp

    istat = 0 
    n = size(x)

    ! initial values
    call rinit(x,t,s)

    ! we are stoping here for the reason:
    ! * identical data. eg. all data has the same value with machine precision,
    ! * less than 3 data-points are available
    if( abs(s) < xabs .or. n < 3 )then

       sig = s
       dt = 0
       if( n > 1 ) dt = sig/sqrt(real(n))

       if( n > 2 ) t = sum(x)/n
       ! the sum looks strangle, but minimizes rounding errors          
       
       istat = 2
       return
    endif
    ! we are suppose that n > 2 from here

    ! 99% Winsorisation
    allocate(xdata(n),r(n))
    r = (x - t)/s
    where( abs(r) < 3.0 )
       xdata = x
    elsewhere
       xdata = t + sign(3.0*s,r)
    end where

    ! save intial estimate
    t0 = t
    s0 = s

    ! iterations
    allocate(r(n),p(n),dp(n))
    it = 0
    do i = 1,maxit

       r = (x - t)/s
       call hubers(r,p)
       call dhubers(r,dp)
       sum1 = sum(p)
       sum2 = sum(dp)
       sum3 = sum(p**2)

       ! sum2 can be nearly zero or negative when the important part of data
       ! has large deviations (above 3.4 for Hampel's piecewise or 1.6 for Tukey's
       ! bi-weight), this si "re-descending M-estimate" function problem.
       ! In this case, and also when s*sum1 is smaller (which will indicate
       ! no convergence, perhaps), the computation is stoped and initial estimate
       ! of t,s is used.
       if( sum2 < xabs .or. sum2 > s*sum1 ) then
          istat = 3
          exit
       end if

       ! corrector for mean
       d = s*sum1/sum2
       t = t + d

       ! corrector for scale
       s = sqrt(sum3*s**2/(n-1))

       if( verbose ) write(*,'(a,3g15.5)') "mean, increment, scale: ",t,d,s

       ! exit of iterations:
       absd = abs(d)
       if( absd/abs(t) < xrel .or. absd < xabs ) exit
       ! the relative error must be at least |d|/|t| < xrel
       ! the absolute errot must be at least |d| < xabs

       it = it + 1
    enddo

    if( istat == 3 ) then
       ! fail of convergence
       t = t0
       s = s0
    end if

    ! estimation of standard deviation, K-factor is included (~1 + 1/n*var/dmean)
    if( sum2 > xabs ) then

       xm = sum2/n
       var = sum((dp - xm)**2)/n
       kcorr = (1 + var/xm**2/n)

       sig = s*sqrt(kcorr*sum3/sum2*n/(n-1))
       dt = sig / sqrt(sum2)

    else
       sig = s
       dt = sig/sqrt(real(n))
    end if

    deallocate(r,p,dp)

  end subroutine rmean1


  subroutine rmean2(x,t,dt,sig,tol,istat)

    ! minimizes parameters of a robust function with joint estimate of mean and scale
    !
    ! t   is estimation of mean
    ! dt  is standard error of t
    ! sig is standard deviation of sample
    !
    ! rmean1 is significantly faster
    
    use rfun
    use minpacks
    use neldermead

    real(dbl), dimension(:), intent(in) :: x
    real(dbl), intent(out) :: t,dt,sig
    real(dbl), intent(in) :: tol
    integer, intent(out) :: istat

    integer :: n,info,nprint,icount,numres,ifault
    real(dbl) :: s,sum2,sum3,smin,reqmin
    real(dbl), dimension(:), allocatable :: r,p,dp
    real(dbl), dimension(2) :: u,u0,du
    real(dbl), dimension(2,2) :: jac

    if( verbose ) then
       nprint = 1
    else
       nprint = 0
    end if

    istat = 0 
    n = size(x)

    ! initial values
    call rinit(x,t,s)

    ! we are stoping here for the reason:
    ! * identical data. eg. all data has the same value under machine precision
    ! * less than 3 data-points are available
    if( abs(s) < tol .or. n < 3 )then

       sig = s
       dt = 0
       if( n > 1 ) dt = sig/sqrt(real(n))

       istat = 2
       return
    endif
    ! we are suppose that n > 2 below this line

    ! winsorizing, outliers are replaced by 3-sigma values
    allocate(xdata(n),r(n))
    r = (x - t)/s
    where( abs(r) < winscut )
       xdata = x
    elsewhere
       xdata = t + sign(winscut*s,r)
    end where


    ! Location of proper minimum. The parameters are initiated by a non-M-estimate
    ! method and we are expecting a deviated estimate. The M-estimation
    ! is derived in two steps:
    !  1. approximate location of minimum of negative log(likelihood) function
    !     without use of derivates
    !  2. precise estimate by a gradient method
    !
    ! The gradient method can't be used directly because the estimation of scale
    ! frequently lies near (or above) horizon of convergence.
    
    ! Approximate location of -log(L), the function has one global minimum
    reqmin = s**2
    u = (/t,s/)
    u0 = u
    du = (/ s, s /)
    call nelmin(loglikely,size(u),u0,u,smin,reqmin,du,10,10000,icount,numres,ifault)
    if( verbose ) write(*,'(a,2i3,3g15.5)') 'Approximate solution: ',ifault,icount,u

    if( ifault /= 0 ) then
       ! no global convergence occured

       if( verbose ) write(*,*) "Finished prematurely without likelihood convergence."
       t = u0(1)
       sig = huber_sigcorr*u0(2)
       dt = sig/sqrt(real(n))
       goto 666

    end if
    u0 = u

    ! Precise solution, a gradient method leads to two minimums: right and infinity.
    ! We are suppose, that the previous estimation of u is very nearly to the right.
    if( analytic ) then
       call lmder2(funder,u,tol,jac,nprint,info)
    else
       call lmdif2(fundif,u,tol,jac,nprint,info)
    end if
       
    if( abs(u(2))/s < siglim .and. info /= 5 ) then
       ! the minimum localised successfully, update estimates
       t = u(1)
       s = u(2)

    else
       ! Non-convergence detected, so the initial values are used.
       ! The approach is less precise but works better for any unexpected data:
       !  * many same values and one outlier, we are in doubts what is the right result

       if( verbose ) write(*,*) "Finished prematurely without convergence in gradient."
       t = u0(1)
       sig = huber_sigcorr*u0(2)
       dt = sig/sqrt(real(n))
       goto 666
       
    end if


    ! Estimation of dispersion (variance) and uncertainty of mean
    ! With the extimations, two notes are related:
    !
    !  * the variance estimated by scale parameter of robust function is 
    !    usually smaller than standard deviations because the second
    !    moments of the functions are smaller. For Huber's function,
    !    the second moment is 1.18945 (computed as int(p(normal)*x**2))
    !    To get asymptotical estimation of dispersion from scale, we are
    !    appling the factor.
    !
    !  * The estimation of uncertainity is by Hubber(1981), formula (8.8)
    !    The sum2 has meaning: "which fraction of values lies inside -1.345...
    !    .. 1.345 interval" (in Huber's function). The sum3 is robust equivalent
    !    of mean of square deviations.


    allocate(p(n),dp(n))
    r = (x - t)/s
    call hubers(r,p)
    call dhubers(r,dp)
    sum2 = sum(dp)/n
    sum3 = sum(p**2) / (real(n-1)*real(n)) ! real() to prevent numerical overflow

    ! correction on taller second moment of Huber's distribution
    sig = huber_sigcorr*s

    if( sum2 > tol ) then
       ! Huber(1981), (8.8)
       dt = s*sqrt(sum3)/sum2
    else
       ! the alternative for the scale nearly to machine epsilon
       dt = sig/sqrt(real(n))
    end if

666 continue

    deallocate(xdata,r)
    if( allocated(p) ) deallocate(p,dp)

  end subroutine rmean2


  function loglikely(p)

    use rfun

    real(dbl), dimension(:), intent(in) :: p
    real(dbl) :: loglikely
    real(dbl), dimension(:), allocatable :: r,f
    real(dbl) ::t,s
    integer :: n

    n = size(xdata)
    allocate(r(n),f(n))

    t = p(1)
    s = p(2)

    r = (xdata - t)/s
    call ihubers(r,f)
!    f = r**2/2
    loglikely = sum(f) + n*log(s)
    
    deallocate(r,f)

  end function loglikely


  subroutine funder(m,np,p,fvec,fjac,ldfjac,iflag)
    
    use rfun

    integer, intent(in) :: m,np,ldfjac
    integer, intent(inout) :: iflag
    real(dbl), dimension(np), intent(in) :: p
    real(dbl), dimension(m), intent(out) :: fvec
    real(dbl), dimension(ldfjac,np), intent(out) :: fjac
    real(dbl), dimension(:), allocatable :: r,f,df,rd
    real(dbl), dimension(2) :: fv
    real(dbl), dimension(2,2) :: dfjac
    real(dbl) :: s
    integer :: n

    if( iflag == 0 ) then

       write(*,'(4g15.5)') p,fvec

       if( debug ) then
          write(*,*) 'djac:',fjac(1,:)
          write(*,*) 'djac:',fjac(2,:)

          call difjac(p(1),p(2),dfjac)
          write(*,*) 'djac:',dfjac(1,:)
          write(*,*) 'djac:',dfjac(2,:)
       end if

       return
    end if

    n = size(xdata)
    allocate(r(n),f(n))

    s = p(2)
    r = (xdata - p(1))/s
    call hubers(r,f)
!    f = r

    fv(1) = sum(f)
    fv(2) = sum(f*r) - n

    if( iflag == 1 ) then

       fvec = fv / s

    else if( iflag == 2 ) then

       allocate(df(n),rd(n))
       call dhubers(r,df)
!       df = 1

       rd = df*r

       fjac(1,1) = sum(df)
       fjac(1,2) = fv(1) + sum(rd)
       fjac(2,1) = fjac(1,2)
       rd = rd*r
       fjac(2,2) = 2*fv(2) + sum(rd) + n

       fjac = - fjac / s**2

       deallocate(rd,df)

    end if


    deallocate(r,f)

  end subroutine funder


  subroutine fundif(m,np,p,fvec,iflag)
    
    use rfun

    integer, intent(in) :: m,np
    integer, intent(in out) :: iflag
    real(dbl), dimension(np), intent(in) :: p
    real(dbl), dimension(m), intent(out) :: fvec
    real(dbl), dimension(:), allocatable :: r,f
    real(dbl), dimension(2,2) :: jac
    real(dbl), dimension(2) :: fv
    integer :: n
    real(dbl) :: s

    if( iflag == 0 ) then

       write(*,'(4g15.5)') p,fvec

       if( debug ) then
          n = 2
          call funder(2,2,p,fv,jac,2,n)
          write(*,*) ' jac:',jac(1,:)
          write(*,*) ' jac:',jac(2,:)
       end if

       return
    end if

    n = size(xdata)
    allocate(r(n),f(n))

    s = p(2)
    r = (xdata - p(1))/s
    call hubers(r,f)

    fvec(1) = sum(f)
    fvec(2) = sum(f*r) - n

    fvec = fvec / s

    deallocate(r,f)

  end subroutine fundif

  subroutine difjac(t,s,jac)

    real(dbl), intent(in) :: t,s
    real(dbl), dimension(:,:), intent(out) :: jac
    real(dbl), parameter :: d = 1e-4
    real(dbl), dimension(2) :: fv1,fv2
    integer :: iflag

    iflag = 1

    call fundif(2,2,(/t+d,s/),fv1,iflag)
    call fundif(2,2,(/t-d,s/),fv2,iflag)
    jac(1,:) = (fv1 - fv2)/(2*d)

    call fundif(2,2,(/t,s+d/),fv1,iflag)
    call fundif(2,2,(/t,s-d/),fv2,iflag)
    jac(2,:) = (fv1 - fv2)/(2*d)

  end subroutine difjac

  subroutine rmean0(x,t,dt,s,sig,tol,nprint,info)

    ! This was default robust estimator for a long time.
    ! The main difference from rmean2 is that both the 
    ! scale and the location is not estimated simultanously. 
    ! The scale has just its zero-initial estimate value
    ! and just location is iterated. In use, this estimator
    ! gives little-bit less precise results but is significantly
    ! faster.

    use rfun

    real(dbl), dimension(:), intent(in) :: x
    real(dbl), intent(in out) :: t
    real(dbl), intent(out) :: dt,sig
    real(dbl), intent(in) :: tol,s
    integer, intent(in out) :: info
    integer, intent(in) :: nprint

    integer, parameter :: maxit = 33
    integer :: n,it
    real(dbl) :: d,sum1,sum2,sum3,absd,xm,var,kcorr
    real(dbl), dimension(:), allocatable :: r,p,dp

    info = 0
    n = size(x)

    ! iterations
    allocate(r(n),p(n),dp(n))
    do it = 1,maxit

       r = (x - t)/s
       call hubers(r,p)
       call dhubers(r,dp)
       sum1 = sum(p)
       sum2 = sum(dp)

       ! sum2 can be nearly zero or negative when the important part of data
       ! has large deviations (above 3.4 for Hampel's piecewise or 1.6 for Tukey's
       ! bi-weight), this si "re-descending M-estimate" function problem.
       ! In this case, and also when s*sum1 is smaller (which will indicate
       ! no convergence, perhaps), the computation is stoped and initial estimate
       ! of t,s is used.
       if( sum2 < tol ) then
          info = 5
          exit
       end if

       ! corrector for mean
       d = s*sum1/sum2
       t = t + d

       if( nprint > 0 ) write(*,'(a,3g15.5)') "mean, increment, scale: ",t,d,s

       ! exit of iterations:
       absd = abs(d)
       if( absd/abs(t) < tol .or. absd < tol ) exit
       ! the relative error must be at least |d|/|t| < xrel
       ! the absolute errot must be at least |d| < xabs

       if( it == maxit ) info = 5
    enddo

    ! estimation of standard deviation, K-factor is included (~1 + 1/n*var/dmean)
    if( sum2 > tol .and. info /=5 ) then

       sum3 = sum(p**2)
       xm = sum2/n
       var = sum((dp - xm)**2)/n
       kcorr = (1 + var/xm**2/n)

       sig = s*sqrt(kcorr*sum3/sum2*n/(n-1))
       dt = sig / sqrt(sum2)

    else
       ! this point is reached when:
       !  * parameters are poorly estimated, all phi' are zeros
       !  * no convergence
       !
       !  As the consequence, results are perhaps incorrect!
       info = 5
       sig = s
       dt = sig/sqrt(real(n))
    end if

    deallocate(r,p,dp)

  end subroutine rmean0

  !-------------------------------------

  ! Quick median by Wirth: Algorith + data structures = programs
  ! fastest known median algorithm ~2*n

  function qmed(b,k) result(x)

    integer, intent(in) :: k
    real(dbl),dimension(:),intent(in) :: b
    real(dbl),dimension(:),allocatable :: a
    real(dbl) :: w,x
    integer :: n,l,r,i,j

    n = size(b)
    allocate(a(n))
    a = b

    l = 1
    r = n
    do while( l < r )
       x = a(k)
       i = l
       j = r
       do
          do while( a(i) < x )
             i = i + 1
          enddo
          do while( x < a(j) )
             j = j - 1
          enddo
          if( i <= j ) then
             w = a(i)
             a(i) = a(j)
             a(j) = w
             i = i + 1
             j = j - 1
          endif
          if( i > j ) exit
       enddo
       if( j < k ) l = i
       if( k < i ) r = j
    enddo
    deallocate(a)

  end function qmed


  ! computes median from pre-sorted array
  ! prefered for a few elements ~n*log(n)

  function xmed(b)

    use selectsort

    real(dbl),dimension(:),intent(in) :: b
    real(dbl) :: xmed
    real(dbl),dimension(:),allocatable :: a
    integer :: n

    n = size(b)

    if( n == 0 ) then
       xmed = 0.0_dbl
       return
    end if

    if( n == 1 ) then
       xmed = b(1)
       return
    end if

    allocate(a(n))
    a = b
    call ssort(a)

    xmed = (a((n+1)/2) + a(n/2+1))/2.0_dbl
    ! This trick is adopted from original DAOPHOT source code. The 
    ! integer-division has the property: for n odd is (n+1)/2  and 
    ! n/2+1 the same number whereas for the even n are different numbers.

    deallocate(a)

  end function xmed
  

  subroutine cdf(x,n,u,v)

    ! cumulative distribution function

    use quicksort

    real(dbl),dimension(:),intent(inout) :: x
    integer, intent(out) :: n
    real(dbl),dimension(:),intent(out) :: u,v
    real(dbl), parameter :: machtol = epsilon(x)
    integer :: i

    call qsort(x)

    n = 1
    u(1) = x(1)
    v(1) = 0
    do i = 2,size(x)
       if( abs(x(i) - x(i-1)) > machtol ) then
          ! one ignores identical values up to machine precision
          n = n + 1
          u(n) = x(i)
          v(n) = n
       end if
    end do
    v = v / real(n)

  end subroutine cdf

  subroutine quantile(q,x,y,t)

    ! estimates q-quantile

    real(dbl), intent(in) :: q
    real(dbl), dimension(:),intent(in) :: x,y
    real(dbl), intent(out) :: t
    integer :: i,n,low,high

    n = size(x)

    if( n == 0 ) then
       t = 0
       return
    else if( n == 1 ) then
       t = x(1)
       return
    end if

    if( q <= y(1) ) then
       t = x(1)
    else if( q >= y(n) ) then
       t = x(n)
    else

       ! this loop searches quantiles in a grid, 
       ! fail-back to outer values.
       low = 1
       high = n
       do i = 1,n-1
          if( y(i) <= q .and. q <= y(i+1) ) then
             low = i
             high = i+1
             exit
          end if
       end do

       t = (x(high) - x(low))/(y(high) - y(low))*(q - y(low)) + x(low)
    end if
!    write(*,*) high,low,x(high),x(low),y(high),y(low)
    

!    do i = 1,n
!       write(*,*) i,x(i),y(i)
!    end do
!    write(*,*) q,t

  end subroutine quantile

  subroutine qmean(xx,t,s)

    ! this function computes mean by using of quantiles

    real(dbl), dimension(3), parameter :: q = (/0.25, 0.5, 0.75/)
    real(dbl), dimension(:), intent(in) :: xx
    real(dbl), intent(out) :: t,s
    real(dbl), dimension(size(q)) :: y
    real(dbl), dimension(:), allocatable :: x,xcdf,ycdf
    integer :: i,n,nq

    nq = size(q)
    n = size(xx)

    allocate(x(n),xcdf(n),ycdf(n))

    x = xx
    call cdf(x,n,xcdf,ycdf)
    
    do i = 1, nq
       call quantile(q(i),xcdf(1:n),ycdf(1:n),y(i))
    end do

    ! we are trying to estimate parameters of Normal distribution
    t = sum(y)/nq
    s = (abs(y(3) - y(2)) + abs(y(1) - y(2))) / (2*q50)

!    if( verbose ) write(*,*) 'q1/4,q1/2,q3/4:',y
!    write(*,*) 'q1/4,q1/2,q3/4:',y
    deallocate(x,xcdf,ycdf)
    
  end subroutine qmean

  ! an interface for plotting of functions in minimum (not used for computations)
  subroutine graph(x,t,sig,type,fvec)

    real(dbl), dimension(:), target, intent(in) :: x
    real(dbl), intent(in) :: t,sig
    character(len=*), intent(in) :: type
    real(dbl), dimension(:) :: fvec
    integer :: iflag, n

    n = size(x)
    allocate(xdata(n))
    xdata = x

    if( type == "grad" ) then
       iflag = 1
       call fundif(2,2,(/t,sig/),fvec,iflag)
    else if( type == "like" ) then
       fvec(1) = loglikely((/t,sig/))
    end if

    deallocate(xdata)
    
  end subroutine graph


end module robustmean
