!
!  Robust estimate of parameters of line
!
!  Copyright © 2014 F.Hroch (hroch@physics.muni.cz)
!
!  This file is part of Munipack.
!
!  Munipack is free software: you can redistribute it and/or modify
!  it under the terms of the GNU General Public License as published by
!  the Free Software Foundation, either version 3 of the License, or
!  (at your option) any later version.
!  
!  Munipack is distributed in the hope that it will be useful,
!  but WITHOUT ANY WARRANTY; without even the implied warranty of
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!  GNU General Public License for more details.
!  
!  You should have received a copy of the GNU General Public License
!  along with Munipack.  If not, see <http://www.gnu.org/licenses/>.
!
!

module robustline

  use robustmean

  implicit none

  ! numerical precision of real numbers
  integer, parameter, private :: dbl = selected_real_kind(15)

  ! print debug informations ?
  logical, parameter, private :: verbose = .false.

  ! estimate jacobian by differences or derivations ?
  logical, parameter, private :: analytic = .false.
  ! currently, only differences are implemented

  ! limit to detect non-convergent series, reccomended values: >100
  real(dbl), parameter, private :: siglim = 1000.0

  real(dbl), dimension(:), allocatable, private :: xdata, ydata, xsig, ysig

  private :: line,med_aline,loglikely

contains

  subroutine rline2(x,y,dx,dy,a,b,da,db,sig)

    use rfun
    use minpacks
    use NelderMead

    real(dbl), dimension(:), intent(in) :: x,y,dx,dy
    real(dbl), intent(out) :: a,b,da,db,sig

    real(dbl), parameter :: tol = epsilon(tol)

    real(dbl), dimension(:), allocatable :: p,dp,r
    real(dbl), dimension(3) :: u,u0,du
    real(dbl), dimension(3,3) :: jac,hess
    integer :: info,nprint,icount,numres,ifault
    real(dbl) :: s,sum2,sum3,xm,var,kcorr,smin,reqmin,xcen,ycen
    
    integer :: i,n

    if( verbose ) then
       nprint = 1
    else
       nprint = 0
    end if

    if( size(x) /= size(y) ) stop 'Size of x,y data sets are different.'

    n = size(x)
    allocate(xdata(n),ydata(n),xsig(n),ysig(n))
    xdata = x
    ydata = y

    where( abs(dx) > epsilon(dx) )
       xsig = dx
       ysig = dy
    elsewhere
       xsig = epsilon(dx)
       ysig = epsilon(dy)
    end where
    
    call linit(a,b,s)
    if( verbose ) write(*,'(a,3g13.5)') 'Initial estimate:',a,b,s

    ! 99% Winsorisation, we are belive that the x-data are noisly
    ! but does not includes outliers
    allocate(r(n))
    if( verbose ) write(*,*) 'Winsorisation:'
    do i = 1,n
       r(i) = (line(x(i),a,b) - y(i))/s
       if( abs(r(i)) < 3.0 ) then
          xdata(i) = x(i)
          ydata(i) = y(i)
       else
          xdata(i) = x(i)
          ydata(i) = line(x(i),a,b) + sign(3.0*s,r(i))
          if( verbose ) write(*,'(4g15.5)') x(i),y(i),xdata(i),ydata(i)
       end if
    end do

    ! compute data centroids, useful for step size in Nelder-Mead algorithm
    xcen = qmed(x,min(n/2+1,n))
    ycen = qmed(y,min(n/2+1,n))
    
    ! locate minimum
    reqmin = s**2
    reqmin = epsilon(reqmin)
    u = (/a,b,1.0d0/)
    u0 = u
    du = (/ s*xcen, s*ycen, 0.1d0 /)
    call nelmin(loglikely,size(u),u0,u,smin,reqmin,du,1,10000,icount,numres,ifault)
    if( verbose ) write(*,'(a,i3,3g15.5)') 'Approximate solution: ',ifault,u
    
    if( ifault /= 0 ) then
       ! no global convergence occured

       if( verbose ) write(*,*) "Finished prematurely without likelihood convergence."
       a = u(1)
       b = u(2)
       sig = u(3)
       db = sig / sqrt(real(n))
       if( abs(b) > epsilon(b) )then
          da = db / b
       else
          da = 0
       end if
       goto 666

    end if
    u0 = u

    ! precise solution
    ! Precise solution, a gradient method leads to two minimums: right and infinity.
    ! We are suppose, that the previous estimation of u is very nearly to the right.
    if( analytic ) then
!       call lmder2(funder,u,tol,jac,nprint,info)
    else
       call lmdif2(fundif,u,tol,jac,nprint,info)
    end if
       
    if( abs(u(2))/s < siglim .and. info /= 5 ) then
       ! the minimum localised successfully, update estimates
       a = u(1)
       b = u(2)
       s = u(3)

    else
       ! Non-convergence detected, so the initial values are used.
       ! The approach is less precise but works better for any unexpected data:
       !  * many same values and one outlier, we are in doubts what is the right result

       if( verbose ) write(*,*) "Finished prematurely without convergence in gradient."
       a = u0(1)
       b = u0(2)
       sig = u0(3)
       db = sig/sqrt(real(n))
       if( abs(b) > epsilon(b) )then
          da = db / b
       else
          da = 0
       end if
       goto 666
       
    end if


    ! estimation of uncertainties
    allocate(p(n),dp(n))
    do i = 1,n
       r(i) = (ydata(i) - a - b*xdata(i))/sqrt(ysig(i)**2 + b**2*xsig(i)**2)/s
    end do
    call hubers(r,p)
    call dhubers(r,dp)
    sum2 = sum(dp)
    sum3 = sum(p**2)

    if( sum2 > tol ) then

       xm = sum2/n
       var = sum((dp - xm)**2)/n
       kcorr = (1 + var/xm**2/n)

       ! Huber (6.6)
       call qrinv(jac,hess)
       sig = s*sqrt(kcorr*sum3/sum2*n/(n-1))
       if( hess(1,1) > tol .and. hess(2,2) > tol  ) then
          da = sig * sqrt(abs(hess(1,1))) / s
          db = sig * sqrt(abs(hess(2,2))) / s
       else
          ! the alternative for the scale nearly to machine epsilon
          db = sig/sqrt(n-1.0)
          if( abs(b) > epsilon(b) )then
             da = db / b
          else
             da = 0
          end if
       end if

       if( verbose ) then
          write(*,*) 'jac:',real(jac(1,:))
          write(*,*) 'jac:',real(jac(2,:))
          write(*,*) 'jac:',real(jac(3,:))
          write(*,*) 'hess:',real(hess(1,:))
          write(*,*) 'hess:',real(hess(2,:))
          write(*,*) 'hess:',real(hess(3,:))
       end if

    else
       sig = s
       da = sig/sqrt(real(n))
       db = da
    end if

666 continue

    deallocate(xdata,ydata,xsig,ysig,r)
    if( allocated(p) ) deallocate(p,dp)

  end subroutine rline2

  subroutine linit(a,b,s)

    real(dbl), intent(out) :: a,b,s

    real(dbl), dimension(:), allocatable :: r
    real(dbl), dimension(2) :: t,dt
    real(dbl) :: mad
    integer :: i,n

    n = size(xdata)

    if( n < 2 ) then
       ! no data
       s = 0
       a = 0
       b = 0

    else if( n == 2 ) then
       ! interpolation
       s = 0
       a = ydata(1)
       b = (ydata(2) - ydata(1)) / (xdata(2) - xdata(1))

    else

       call aline(t,dt)

       allocate(r(size(xdata)))
       do i = 1, n
          r(i) = abs(line(xdata(i),t(1),t(2)) - ydata(i))
       end do
       a = t(1)
       b = t(2)

       if( n < 50 ) then
          mad = xmed(r)
       else
          mad = qmed(r,min(n/2+1,n))
       end if

       s = mad/0.6745

       deallocate(r)

    end if


  end subroutine linit


  function line(x,a,b)

    real(dbl), intent(in) :: x,a,b
    real(dbl) :: line
    
    line = a + b*x

  end function line

  subroutine aline(t,dt)

    use NelderMead

    real(dbl), dimension(:), intent(out) :: t,dt

    integer :: icount, numres, ifault
    real(dbl) :: reqmin,rms
    real(dbl), dimension(size(t)) :: t0

    t = 0
    dt = 1
    t0 = t
    reqmin = sqrt(epsilon(reqmin))
    call nelmin(med_aline,2,t0,t,rms,reqmin,dt,1,1000,icount,numres,ifault)

  end subroutine aline

  function med_aline(p) result(s)

    real(dbl), dimension(:), intent(in) :: p
    real(dbl) :: s

    s = sum(abs(p(1) + xdata*p(2) - ydata)/sqrt(ysig**2 + p(2)**2*xsig**2))

  end function med_aline

!!$  subroutine res(a,b,s,r)
!!$
!!$    real(dbl), intent(in) :: a,b,s
!!$    real(dbl), dimension(:), intent(out) :: r
!!$    real(dbl), dimension(size(r)), allocatable :: ds
!!$    integer :: i,n
!!$
!!$    n = size(xdata)
!!$    allocate(ds(n))
!!$    
!!$    ds = sqrt(ysig**2 + b**2*xsig**2)
!!$    do i = 1,n
!!$       r(i) = (line(xdata(i),a,b) - ydata(i))/ds(i)
!!$    end do    
!!$
!!$    deallocate(ds) 
!!$
!!$  end subroutine res

  function loglikely(p)

    use rfun

    real(dbl), dimension(:), intent(in) :: p
    real(dbl) :: loglikely
    real(dbl), dimension(:), allocatable :: r,f,ds
    real(dbl) :: a,b,s
    integer :: i,n

    n = size(xdata)
    a = p(1)
    b = p(2)
    s = p(3)

    if( s < epsilon(s) ) then
       loglikely = 100*n
       return
    end if

    allocate(r(n),f(n),ds(n))

    ds = sqrt(ysig**2 + b**2*xsig**2)
    do i = 1,n
       r(i) = (line(xdata(i),a,b) - ydata(i))/ds(i)/s
    end do
    call ihubers(r,f)
    
    loglikely = sum(f) + n*log(s)

    deallocate(r,f,ds)

  end function loglikely

!!$  subroutine funder(m,np,p,fvec,fjac,ldfjac,iflag)
!!$    
!!$    use rfun
!!$
!!$    integer, intent(in) :: m,np,ldfjac
!!$    integer, intent(inout) :: iflag
!!$    real(dbl), dimension(np), intent(in) :: p
!!$    real(dbl), dimension(m), intent(out) :: fvec
!!$    real(dbl), dimension(ldfjac,np), intent(out) :: fjac
!!$    real(dbl), dimension(:), allocatable :: r,f,df,rd
!!$    real(dbl), dimension(3) :: fv
!!$    real(dbl), dimension(3,3) :: dfjac
!!$    real(dbl) :: s
!!$    integer :: n
!!$
!!$    if( iflag == 0 ) then
!!$
!!$       write(*,'(4g15.5)') p,fvec
!!$
!!$       if( debug ) then
!!$          write(*,*) 'djac:',fjac(1,:)
!!$          write(*,*) 'djac:',fjac(2,:)
!!$
!!$          call difjac(p(1),p(2),dfjac)
!!$          write(*,*) 'djac:',dfjac(1,:)
!!$          write(*,*) 'djac:',dfjac(2,:)
!!$       end if
!!$
!!$       return
!!$    end if
!!$
!!$    n = size(xdata)
!!$    allocate(r(n),f(n))
!!$
!!$    a = p(1)
!!$    b = p(2)
!!$    s = p(3)
!!$
!!$!    call res(p(1),p(2),p(3),r)
!!$    
!!$    ds = sqrt(ysig**2 + b**2*xsig**2)
!!$    do i = 1,n
!!$       r(i) = (ydata(i) - a - b*xdata(i))
!!$    end do
!!$!    r = (ydata()/s
!!$    call hubers(r/ds/s,f)
!!$!    f = r
!!$
!!$    fv(1) = sum(f/xsig)
!!$    fv(2) = sum(f*(xdata/ds + r*b*xsig**2/ds**3)) + sum(b*xsig/ds**2)
!!$    fv(3) = sum(f*r/s/ds) + n
!!$
!!$    if( iflag == 1 ) then
!!$
!!$       fvec = fv / s
!!$
!!$    else if( iflag == 2 ) then
!!$
!!$       allocate(df(n),rd(n))
!!$       call dhubers(r,df)
!!$!       df = 1
!!$
!!$       rd = df*r
!!$
!!$       fjac(1,1) = sum(df)
!!$       fjac(1,2) = fv(1) + sum(rd)
!!$       fjac(2,1) = fjac(1,2)
!!$       rd = rd*r
!!$       fjac(2,2) = 2*fv(2) + sum(rd) + n
!!$
!!$       fjac = - fjac / s**2
!!$
!!$       deallocate(rd,df)
!!$
!!$    end if
!!$
!!$
!!$    deallocate(r,f)
!!$
!!$  end subroutine funder

  subroutine difjac(t,s,jac)

    real(dbl), intent(in) :: t,s
    real(dbl), dimension(:,:), intent(out) :: jac
    real(dbl), parameter :: d = 1e-4
    real(dbl), dimension(2) :: fv1,fv2
    integer :: iflag

    iflag = 1

    call fundif(2,2,(/t+d,s/),fv1,iflag)
    call fundif(2,2,(/t-d,s/),fv2,iflag)
    jac(1,:) = (fv1 - fv2)/(2*d)

    call fundif(2,2,(/t,s+d/),fv1,iflag)
    call fundif(2,2,(/t,s-d/),fv2,iflag)
    jac(2,:) = (fv1 - fv2)/(2*d)

  end subroutine difjac

  subroutine fundif(m,np,p,fvec,iflag)
    
    use rfun

    integer, intent(in) :: m,np
    integer, intent(in out) :: iflag
    real(dbl), dimension(np), intent(in) :: p
    real(dbl), dimension(m), intent(out) :: fvec
    real(dbl), dimension(:), allocatable :: r,f,ds
!    real(dbl), dimension(3,3) :: jac
    integer :: i,n
    real(dbl) :: a,b,s

    if( iflag == 0 ) then

       write(*,'(6g13.5)') p,fvec

!!$       if( debug ) then
!!$          n = 2
!!$          call funder(2,2,p,fv,jac,2,n)
!!$          write(*,*) ' jac:',jac(1,:)
!!$          write(*,*) ' jac:',jac(2,:)
!!$       end if

       return
    end if

    n = size(xdata)
    allocate(r(n),f(n),ds(n))

    a = p(1)
    b = p(2)
    s = p(3)

    ds = sqrt(ysig**2 + b**2*xsig**2)
    do i = 1,n
       r(i) = (ydata(i) - a - b*xdata(i)) / ds(i) /s
    end do
    call hubers(r,f)

    fvec(1) = sum(f/ds)
    fvec(2) = sum(f*(xdata/ds + r*b*xsig**2/ds**2))
    fvec(3) = sum(f*r/s/ds) - n

    fvec = - fvec / s

    deallocate(r,f,ds)

  end subroutine fundif

end module robustline


