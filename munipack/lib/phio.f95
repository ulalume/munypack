!
! common for read/write photometry-related FITSes 
!
!
! Copyright © 2013,2015 F.Hroch (hroch@physics.muni.cz)
!
! This file is part of Munipack.
!
! Munipack is free software: you can redistribute it and/or modify
! it under the terms of the GNU General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
! 
! Munipack is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.
! 
! You should have received a copy of the GNU General Public License
! along with Munipack.  If not, see <http://www.gnu.org/licenses/>.
!

module phio

  use fitsio
  use astrotrafo

  implicit none
  
  integer, parameter, private :: dbl = selected_real_kind(15)
  real(dbl), parameter, private :: rad = 57.295779513082322865_dbl

contains


  subroutine wcsget(un,t,status)

    integer, intent(in) :: un
    integer, intent(in out) :: status
    type(AstroTrafoProj), intent(out) :: t

    integer, parameter :: DIM = 2

    integer, dimension(DIM) :: naxes
    character(len=FLEN_CARD) :: buf
    character(len=FLEN_VALUE),dimension(2) :: ctype
    real(dbl), dimension(2,2) :: cd
    real(dbl), dimension(2) :: crval,crpix,crder

    ! read image dimensions
    call ftgisz(un,DIM,naxes,status)
    if( status /= 0 ) return

    ! read astrometric calibration
    call ftgkys(un,'CTYPE1',ctype(1),buf,status)
    call ftgkys(un,'CTYPE2',ctype(2),buf,status)
    call ftgkyd(un,'CRVAL1',crval(1),buf,status)
    call ftgkyd(un,'CRVAL2',crval(2),buf,status)
    call ftgkyd(un,'CRPIX1',crpix(1),buf,status)
    call ftgkyd(un,'CRPIX2',crpix(2),buf,status)
    call ftgkyd(un,'CD1_1',cd(1,1),buf,status)
    call ftgkyd(un,'CD1_2',cd(1,2),buf,status)
    call ftgkyd(un,'CD2_1',cd(2,1),buf,status)
    call ftgkyd(un,'CD2_2',cd(2,2),buf,status)

    if( status /= 0 ) return

    ! optional keywords
    call ftgkyd(un,'CRDER1',crder(1),buf,status)
    call ftgkyd(un,'CRDER2',crder(2),buf,status)
    if( status == KEYWORD_NOT_FOUND ) then
       status = 0
       crder = 0
    end if

    if( status /= 0 ) return

    ! interpret the calibration
    call trafo_fromwcs(t,ctype,crval,crpix,cd,crder)

  end subroutine wcsget

  subroutine trafo_fromwcs(t,ctype,crval,crpix,cd,crder)

    type(AstroTrafoProj), intent(out) :: t
    character(len=*), dimension(:), intent(in) :: ctype
    real(dbl), dimension(:),intent(in) :: crval,crpix,crder
    real(dbl), dimension(:,:),intent(in) :: cd
    character(len=FLEN_VALUE) :: type
    real(dbl) :: c,s,sc,rot,refl,err
    real(dbl), dimension(2,2) :: mi,m

    if( size(ctype) /= 2 ) stop 'Bad dimensions of CTYPE (no 2).'

    if( index(ctype(1),"-TAN") > 0 ) then
       type = 'GNOMONIC'
    else
       type = ' '
    end if

    ! un-flip spherical to rectangular coordinates
    mi(1,:) = (/-1.0_dbl, 0.0_dbl/)
    mi(2,:) = (/ 0.0_dbl, 1.0_dbl/)
    m = matmul(mi,cd)

    ! the reflection (flip, mirror) is determined from 
    ! diagonal elements with (non-)corresponding signs
    refl = sign(1.0_dbl,m(1,1)*m(2,2))

    ! transform-out reflex
    mi(1,:) = (/    refl, 0.0_dbl/)
    mi(2,:) = (/ 0.0_dbl, 1.0_dbl/)
    m = matmul(m,mi)

    c = (m(1,1) + m(2,2)) / 2.0_dbl
    s = (m(2,1) - m(1,2)) / 2.0_dbl
    sc = 1.0_dbl / sqrt(c**2 + s**2)   ! in pix per deg
    rot = rad*(atan2(-m(1,2),m(1,1)) + atan2(m(2,1),m(2,2)))/2.0_dbl

    if( sum(abs(crder)) > size(crder)*epsilon(1.0_dbl) ) then
       err = sqrt(sum(crder**2)/size(crder))
    else
       err = 1.0/sc
    end if

    call trafo_init(t,type,crval(1),crval(2),crpix(1),crpix(2), &
         scale=1/sc,rot=rot,refl=refl,err=err)

  end subroutine trafo_fromwcs


  function PhotCalibrated(filename)

    character(len=*), intent(in) :: filename
    logical :: PhotCalibrated

    integer, parameter :: extver = 0
    integer :: status, blocksize, ncols, n
    character(len=FLEN_VALUE) :: ttype,key
    character(len=FLEN_COMMENT) :: com

    PhotCalibrated = .false.

    ! input FITS file
    status = 0
    call ftopen(20,filename,0,blocksize,status)

    ! looking for photometry extension
    call ftmnhd(20,BINARY_TBL,PHOTOEXTNAME,extver,status)
    if( status == BAD_HDU_NUM ) goto 666

    call ftgncl(20,ncols,status)
    if( status /= 0 ) goto 666
    do n = 3,ncols
       call ftkeyn('TTYPE',n,key,status)
       call ftgkys(20,key,ttype,com,status)
       if( status /= 0 ) goto 666
       if( ttype == FITS_COL_PHOTON ) then
          PhotCalibrated = .true.
          exit
       end if
    end do

666 continue
    call ftclos(20,status)
!    call ftrprt('STDERR',status)

!    if( status == BAD_HDU_NUM ) stop 'Photometry extension not found.'

  end function PhotCalibrated

end module phio

