

! gfortran -fcheck=all  -Wall testrline.f95 -L. -L../minpack -lrstat -lsort -llmin -lminpacks -lminpack -lm

program testrline

  use robustline

  implicit none

  integer, parameter :: dbl = selected_real_kind(15)
  integer, parameter :: nmax = 100
  real(dbl), dimension(:),allocatable :: x,y,dx,dy
  real(dbl) :: a,b,da,db,s,t
  integer :: i,n

  allocate(x(nmax),y(nmax),dx(nmax),dy(nmax))
  do i = 1,size(x)

     call random_number(t)
     x(i) = t
     y(i) = 1 + 1*x(i) + gdis(0.0_dbl,0.01_dbl)

  end do
  i = size(x)/3
  y(i) = 1 + 1*x(i) + gdis(0.0_dbl,0.1_dbl)
  i = size(x)/2
  y(i) = 1 + 1*x(i) + gdis(0.0_dbl,0.1_dbl)

  i = size(x)/4
  y(i) = 1 + 1*x(i) + gdis(0.0_dbl,0.1_dbl)

  dx = 0.01 * 0.707
  dy = 0.01 * 0.707

  deallocate(x,y,dx,dy)
  read(*,*) n
  allocate(x(n),y(n),dx(n),dy(n))
  do n = 1,size(x)
     read(*,*) x(n),y(n),dx(n),dy(n)
  enddo

  call rline2(x,y,dx,dy,a,b,da,db,s)

  write(*,*) 'a=',a,'+-',da
  write(*,*) 'b=',b,'+-',db
  write(*,*) 's=',s

  open(1,file='rline2')
  do i = 1, size(x)
     write(1,*) x(i),y(i),y(i) - (a + b*x(i)) 
  end do
  close(1)

  deallocate(x,y,dx,dy)

contains

    function gdis(mean, sig)

      ! generate random data with gauss distribution

      real(dbl), intent(in) :: mean, sig
      real(dbl) :: gdis, x

      call random_number(x)
      gdis = mean - sqrt(2.0)*sig*ierfc(2*x)   ! Numerical Recipes (6.14.3)
      

    end function gdis
    function ierfc(x)

      real(dbl), intent(in) :: x
      real(dbl) :: ierfc

      ierfc = ierf(x-1)

    end function ierfc

    function ierf(x)

      ! http://en.wikipedia.org/wiki/Error_function

      real(dbl), intent(in) :: x
      real(dbl) :: ierf, u, v, a, y, w
      
      a = 0.140012288686666
      u = log(1 - x**2)
      v = 2/3.14159265358979/a
      y = v + u/2

      w = sqrt(y**2 - u/a)
      ierf = sign(1.0d0,x)*sqrt(w - v)

    end function ierf


end program testrline

