!
!  Fortran 95+ module implementing the least square method by using Minpack
!
!  Copyright © 2013 F.Hroch (hroch@physics.muni.cz)
!
!  This file is part of Munipack.
!
!  Munipack is free software: you can redistribute it and/or modify
!  it under the terms of the GNU General Public License as published by
!  the Free Software Foundation, either version 3 of the License, or
!  (at your option) any later version.
!  
!  Munipack is distributed in the hope that it will be useful,
!  but WITHOUT ANY WARRANTY; without even the implied warranty of
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!  GNU General Public License for more details.
!  
!  You should have received a copy of the GNU General Public License
!  along with Munipack.  If not, see <http://www.gnu.org/licenses/>.
!
!
! 
!  This module implements more simplified versions of original functions
!  of Minpack. One is primary intended to provide statistical routines.
!
!  The simplification:
!
!  * Power of modern Fortran is used (dimensions of arrays can't be passed,
!    working arrays are created automatically on base of dimensions of input
!    arrays). There is the size limitation of input arrays which are on stack.
!
!  * Tolerances are set for maximal precision (should be potentially slower).
!
!  * Only a few parameters are available for users.
! 
!
! Also these important points needs to be understand:
!
!  * The function covar is not available in all distribution packages
!    (perhaps, due to some mess). Therefore, the final linking can fail.
!
!  * The covar function is not directly used. The original idea, to estimate
!    of covariance matrix via the function, gives imprecise results. Therefore,
!    every function computes precise jacobian in minimum and qrinv function
!    is provided for computation of covariance matrix. One could fail
!    for ill-conditioned problems.
!

module minpacks

  implicit none

  ! precision for double real
  integer, parameter, private :: dbl = selected_real_kind(15)

contains


  subroutine lmdif2(fcn,x,tol,jac,nprint,info)

    integer, intent(out) :: nprint
    integer, intent(out) :: info
    real(dbl), intent(in) :: tol
    real(dbl), dimension(:), intent(in out) :: x
    real(dbl), dimension(:,:), intent(out) :: jac

    interface
       subroutine fcn(m,n,x,fvec,iflag)
         integer, parameter :: dbl = selected_real_kind(15)
         integer, intent(in) :: n, m
         integer, intent(in out) :: iflag
         real(dbl), dimension(m), intent(in) :: x
         real(dbl), dimension(n), intent(out) :: fvec
       end subroutine fcn
    end interface

    real(dbl), dimension(size(x)) :: fvec,qtf,wa1,wa2,wa3,wa4,diag
    real(dbl), dimension(size(x),size(x)) :: fjac
    integer, dimension(size(x)) :: ipvt
    real(dbl) :: xtol,ftol, gtol
    integer :: npar,nfev,maxfev

    npar = size(x)
    ftol = tol
    xtol = tol
    gtol = epsilon(gtol)
    maxfev = 200*(npar+1)

    call lmdif(fcn,npar,npar,x,fvec,ftol,xtol,gtol,maxfev,epsilon(0.0_dbl), &
         diag,1,100.0_dbl,nprint,info,nfev,fjac,npar,ipvt,qtf,wa1,wa2,wa3,wa4)
    call fdjac2(fcn,npar,npar,x,fvec,jac,npar,2,0.0_dbl,wa4)

  end subroutine lmdif2

  subroutine lmder2(fcn,x,tol,jac,nprint,info)

    integer, intent(out) :: nprint
    integer, intent(out) :: info 
    real(dbl), intent(in) :: tol
    real(dbl), dimension(:), intent(in out) :: x
    real(dbl), dimension(:,:), intent(out) :: jac

    interface
       subroutine fcn(m,n,x,fvec,fjac,ldfjac,iflag)
         integer, parameter :: dbl = selected_real_kind(15)
         integer, intent(in) :: n, m, ldfjac
         integer, intent(in out) :: iflag
         real(dbl), dimension(n), intent(in) :: x
         real(dbl), dimension(m), intent(out) :: fvec
         real(dbl), dimension(ldfjac,n), intent(out) :: fjac
       end subroutine fcn
    end interface

    real(dbl), dimension(size(x)) :: fvec,qtf,wa1,wa2,wa3,wa4,diag
    real(dbl), dimension(size(x),size(x)) :: fjac
    integer, dimension(size(x)) :: ipvt
    real(dbl) :: xtol,ftol, gtol
    integer :: npar,nfev,njev,maxfev,iflag

    npar = size(x)
    ftol = tol
    xtol = tol
    gtol = epsilon(gtol)
    maxfev = 200*(npar+1)

    call lmder(fcn,npar,npar,x,fvec,fjac,npar,ftol,xtol,gtol,maxfev, &
         diag,1,100.0_dbl,nprint,info,nfev,njev,ipvt,qtf,wa1,wa2,wa3,wa4)
    iflag = 2
    call fcn(npar,npar,x,fvec,jac,npar,iflag)

  end subroutine lmder2


  subroutine hybrd2(fcn,x,jac,info)

    integer, intent(out) :: info
    real(dbl), dimension(:), intent(in out) :: x
    real(dbl), dimension(:,:), intent(out) :: jac

    interface
       subroutine fcn(n,x,fvec,iflag)
         integer, parameter :: dbl = selected_real_kind(15)
         integer, intent(in) :: n
         integer, intent(in out) :: iflag
         real(dbl), dimension(n), intent(in) :: x
         real(dbl), dimension(n), intent(out) :: fvec
       end subroutine fcn
    end interface

    real(dbl), dimension(size(x)) :: fvec,qtf,wa1,wa2,wa3,wa4,diag
    real(dbl), dimension(size(x),size(x)) :: fjac,r
    real(dbl), dimension(size(x)**2) :: xr
    real(dbl) :: xtol
    integer :: npar,nfev,maxfev,i,j,n

    npar = size(x)
    xtol = epsilon(x)
    maxfev = 200*(npar+1)

    call hybrd(fcn,npar,x,fvec,xtol,maxfev,npar-1,npar-1,0.0_dbl, &
         diag,1,100.0_dbl,1,info,nfev,fjac,npar,xr,size(xr),qtf,wa1,wa2,wa3,wa4)

    r = 0.0_dbl
    n = 0
    do i = 1,size(r,1)
       do j = i,size(r,2)
          n = n + 1
          r(i,j) = xr(n)
       end do
    end do
    jac = matmul(fjac,r)

  end subroutine hybrd2


  subroutine hybrj2(fcn,x,jac,info)

    integer, intent(out) :: info
    real(dbl), dimension(:), intent(in out) :: x
    real(dbl), dimension(:,:), intent(out) :: jac

    interface
       subroutine fcn(n,x,fvec,fjac,ldfjac,iflag)
         integer, parameter :: dbl = selected_real_kind(15)
         integer, intent(in) :: n, ldfjac
         integer, intent(in out) :: iflag
         real(dbl), dimension(n), intent(in) :: x
         real(dbl), dimension(n), intent(out) :: fvec
         real(dbl), dimension(ldfjac,n), intent(out) :: fjac
       end subroutine fcn
    end interface

    real(dbl), dimension(size(x)) :: fvec,qtf,wa1,wa2,wa3,wa4,diag
    real(dbl), dimension(size(x),size(x)) :: fjac,r
    real(dbl), dimension((size(x)*(size(x)+1))/2) :: xr
    real(dbl) :: xtol
    integer :: npar,nfev,njev,maxfev,i,j,n

    npar = size(x)
    xtol = epsilon(x)
    maxfev = 200*(npar+1)

    call hybrj(fcn,npar,x,fvec,fjac,npar,xtol,maxfev, &
         diag,1,100.0_dbl,1,info,nfev,njev,xr,size(xr),qtf,wa1,wa2,wa3,wa4)

    r = 0.0_dbl
    n = 0
    do i = 1,size(r,1)
       do j = i,size(r,2)
          n = n + 1
          r(i,j) = xr(n)
       end do
    end do
    jac = matmul(fjac,r)

  end subroutine hybrj2



! ----------------------------------------------------------------------
!
! ADD-ONS
!

  subroutine qrinv(a,ainv)

    ! Compute inverse matrix in least-quare sense by the use
    ! of the QR factorisation

    ! This implementation is correct, but speed is no matter.
    ! Any tests for non-square and ill-conditioned matrixes has not been performed.

    implicit none

    real(dbl), dimension(:,:), intent(in) :: a
    real(dbl), dimension(:,:), intent(out) :: ainv

    integer :: m,n,lda, i,j
    real(dbl), dimension(size(a,1),size(a,2)) :: r, q, qtb
    integer, dimension(size(a,1)) :: ipvt
    real(dbl), dimension(size(a,1)) :: diag,rdiag,acnorm,wa,x,b,sdiag

    lda = size(a,1)
    m = lda
    n = size(a,2)
    diag = 0 ! needs to be set for least-square solution in for singular matrix
    
    ! form the r matrix, r is upper trinagle (without diagonal)
    ! of the factorized a, diagonal is presented in rdiag 
    q = a
    call qrfac(m,n,q,lda,.true.,ipvt,n,rdiag,acnorm,wa)


!    write(*,*) 'qrfac:',q,rdiag,ipvt

    r = q
    forall(i = 1:n)
       r(i,i) = rdiag(i)
    end forall

!!$    r = 0.0_dbl
!!$    do i = 1,n
!!$       do j = i,n
!!$          if( i == j ) then
!!$             r(i,j) = rdiag(i)
!!$          else
!!$             r(i,j) = q(i,j)
!!$          end if
!!$       end do
!!$    end do

    ! form the q matrix 
    call qform(m,n,q,lda,wa)

!    do i = 1,n
!       write(*,'(a,2f15.7)') 'q:',q(i,:)
!    end do
!    do i = 1,n
!       write(*,'(a,2f15.7)') 'r:',r(i,:)
!    end do

    ! determine the inverse matrix
    qtb = transpose(q)
    do i = 1, n
       b = 0.0_dbl
       b(ipvt(i)) = 1.0_dbl
       b = matmul(q,b)
!       write(*,*) b
       call qrsolv(n,r,m,ipvt,diag,b,x,sdiag,wa)
       ainv(:,ipvt(i)) = x
!       ainv(:,ipvt(i)) = matmul(q,x)
    enddo

!    write(*,*) 'ainv:',ainv

  end subroutine qrinv


end module minpacks

