/* 

  FITS related utility


  Copyright © 2011-3 F.Hroch (hroch@physics.muni.cz)

  This file is part of Munipack.

  Munipack is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  
  Munipack is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public License
  along with Munipack.  If not, see <http://www.gnu.org/licenses/>.


  Plan:

  * implement adding and removing COMMENT,HISTORY records


*/

#include "fitsut.h"
#include "mfitsio.h"
#include "fortranio.h"
#include <string>
#include <vector>
#include <iostream>
#include <fstream>
#include <cassert>

using namespace std;

int main()
{
  int mode = ID_NULL;
  int hmode = ID_MODE_NULL;
  string filename, key, val, com, output, backup;
  vector<string> keywords;
  int keylist = ID_FULL;

  while( cin.good() ) {

    string line;
    getline(cin,line);

    size_t eq = line.find('=');

    string value;
    if( eq != string::npos ) 
      value = line.substr(eq+1);

    if( line.find("MODE") != string::npos && eq != string::npos ) {
    
      if( value.find("STRUCTURE") != string::npos )
	mode = ID_STRUCTURE;
      else if ( value.find("HEADER") != string::npos )
	mode = ID_HEADER;
      else if ( value.find("TABLE") != string::npos )
	mode = ID_TABLE;
      else if ( value.find("IMAGE") != string::npos )
	mode = ID_IMAGE;
      else if ( value.find("DUMP") != string::npos )
	mode = ID_DUMP;
      else if ( value.find("RESTORE") != string::npos )
	mode = ID_RESTORE;

    }

    if( line.find("KEYWORD") != string::npos && eq != string::npos ) {

      hmode = ID_MODE_PRINT;
      size_t i = value.find('\'');
      size_t j = value.rfind('\'');
      size_t n = j - i - 1;
      keywords.push_back(value.substr(i+1,n));

    }

    if( line.find("REMKEY") != string::npos && eq != string::npos ) {

      hmode = ID_MODE_REMOVE;
      size_t i = value.find('\'');
      size_t j = value.rfind('\'');
      size_t n = j - i - 1;
      keywords.push_back(value.substr(i+1,n));

    }

    if( line.find("UPDATEKEY") != string::npos && eq != string::npos ) {

      hmode = ID_MODE_UPDATE;
      //      cerr << value << endl;
      vector<string> items(strftok(value));
      assert(items.size() == 3);
      key = items[0];
      val = items[1];
      com = items[2];
    }

    if( line.find("KEYLIST") != string::npos && eq != string::npos )
      keylist = ID_SHELL;

    if( line.find("OUTPUT") != string::npos && eq != string::npos ) {
      vector<string> items(strftok(value));
      assert(items.size() == 2);
      output = items[0];
      backup = items[1];
    }
      

    if( line.find("FILE") != string::npos && eq != string::npos ) {

      vector<string> items(strftok(value));
      assert(items.size() == 3);

      string filename(items[0]);
      string backup(items[1]);
      string newname(items[2]);

      // begin of process file
      int ret;

      if( mode == ID_STRUCTURE )
	ret = structure(filename);

      else if( mode == ID_HEADER ) {
	if( hmode == ID_MODE_PRINT || hmode == ID_MODE_NULL )
	  ret = header_print(filename,keywords,keylist);
	else if( hmode == ID_MODE_REMOVE || hmode == ID_MODE_UPDATE ) {
	  string name;
	  fitsback(filename,backup,newname,false,name);
	  if( hmode == ID_MODE_REMOVE )
	    ret = header_remove(name,keywords);
	  else if( hmode == ID_MODE_UPDATE ) 
	    ret = header_update(name,key,val,com);
	}
      }

      else if( mode == ID_TABLE )
	ret = table(filename);
      
      else if( mode == ID_IMAGE )
	ret =  image(filename);

      else if( mode == ID_DUMP )
	ret =  dump(filename,newname);

      else if( mode == ID_RESTORE )
	ret =  restore(filename,newname);

      if( ret != 0 )
	cerr << "Processing of `" << filename << "' failed with code: " << ret << endl;

      // end of process file

    }

  }

  fprintf(stderr,"STOP %d\n",0);
  return 0;
}


