!
!  FITS I/O for photometry calibration
!
!  Copyright © 2014-5 F.Hroch (hroch@physics.muni.cz)
!
!  This file is part of Munipack.
!
!  Munipack is free software: you can redistribute it and/or modify
!  it under the terms of the GNU General Public License as published by
!  the Free Software Foundation, either version 3 of the License, or
!  (at your option) any later version.
!  
!  Munipack is distributed in the hope that it will be useful,
!  but WITHOUT ANY WARRANTY; without even the implied warranty of
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!  GNU General Public License for more details.
!  
!  You should have received a copy of the GNU General Public License
!  along with Munipack.  If not, see <http://www.gnu.org/licenses/>.

module sfits

  use fitsio
  use iso_fortran_env

  implicit none

  integer, parameter, private :: dbl = selected_real_kind(15)
  real(dbl), parameter, private :: rad = 57.295779513082322865_dbl
  logical, private :: verbose = .true.

  type qFITS
     character(len=FLEN_FILENAME) :: filename
     real, dimension(:,:), allocatable :: frame
     integer :: naxis, nrows, saper
     integer, dimension(2) :: naxes
     real(dbl), dimension(:),allocatable :: ra,dec,cts,dcts,sky,dsky,& 
          ph,dph,phsky,dphsky
     real(dbl) :: exptime,coo_err,aper,area,scale
     real(dbl), dimension(2) :: annuls
     character(len=FLEN_VALUE) :: filter
     logical :: status
  end type qFITS

  private :: readfits, qfits_init, qfits_deallocate, qfits_setph

contains



  subroutine writecal(filenames,backups,newnames,keys,advanced,phsystable,filters, &
       catid,photsys,area,init_area,tra,ctph,dctph,quantities,phres)

    use phsysfits
    use jamming
    use calibre
    use fotran

    character(len=*), dimension(:), intent(in) :: filenames,backups,newnames,keys, &
         quantities, filters
    character(len=*), intent(in) :: photsys, catid, phsystable
    real(dbl), dimension(:,:), intent(in) :: tra
    real(dbl), dimension(:), intent(in) :: ctph,dctph
    type(photores), dimension(:), intent(in), optional :: phres
    real(dbl), intent(in) :: area
    logical, intent(in) :: init_area, advanced
    
    real(dbl), parameter :: nullval = 0
    integer, parameter :: dim2 = 2
    integer, dimension(:), allocatable :: ndat
    integer, dimension(:,:), allocatable :: idx,pairs
    type(qFITS), dimension(:), allocatable :: fitses
    type(type_phsys) :: phsyscal
    real(dbl), dimension(:), allocatable :: atol
    real(dbl), dimension(:,:), allocatable :: ras,decs,ct,dct,sky,dsky,ph,dph,phsky,dphsky
    real, dimension(:,:,:), allocatable :: cube,tcube
    integer :: i,j,k,n,status,nrows_max, width, height, nq, ncat, nfiles, nrows


    nfiles = size(filenames)
    nq = size(quantities)

    status = 0
    allocate(fitses(nfiles))

    nrows_max = 0
    ! read frames
    do n = 1, nfiles
       call readfits(filenames(n),keys,fitses(n),status)
       if( status /= 0 ) stop 'FITS read failed.'
       if( fitses(n)%nrows > nrows_max ) nrows_max = fitses(n)%nrows
       if( init_area ) fitses(n)%area = area
    end do

    ! init photometry parameters
    call phselect(phsystable,photsys,phsyscal)
    call phsyspairs(phsyscal,filters,pairs)

    ! convert frames from counts to photons
    if( nfiles == 1 ) then
       fitses(1)%frame = real(ctph(1)*fitses(1)%frame)
    else
       ! uncorrect handling for multiple frames with small mutual offsets !
       ! this code supposes all the fames with no mutual offset and the same dimensions
       width = fitses(1)%naxes(1)
       height = fitses(1)%naxes(2)
       allocate(cube(width,height,nfiles),tcube(width,height,nfiles))
       do n = 1,nfiles
          cube(:,:,n) = fitses(n)%frame
       end do

       do i = 1,width
          do j = 1,height
             call tra_frame(tra,pairs,ctph,cube(i,j,:),tcube(i,j,:))
          end do
       end do

       do n = 1,nfiles
          fitses(n)%frame = tcube(:,:,n)
       end do

       deallocate(cube,tcube)
    end if


    ! match both frames to be able to determine of colour indexes
    if( nfiles > 1 ) then

       allocate(ras(nrows_max,nfiles),decs(nrows_max,nfiles),atol(nfiles), &
            ndat(nfiles),idx(nrows_max,nfiles))
       idx = 0
       ras = 0
       decs = 0
       do n = 1,nfiles
          nrows = fitses(n)%nrows
          ndat(n) = nrows
          ras(1:nrows,n) = fitses(n)%ra
          decs(1:nrows,n) = fitses(n)%dec
       end do

       atol = 5.0*fitses%coo_err

       call jamframes(atol, ndat, ras, decs, idx)

       deallocate(ras,decs,atol,ndat)

    else ! nfiles == 1
       allocate(idx(nrows_max,nfiles))
       forall(i=1:size(idx,1))
          idx(i,1) = i
       end forall
    end if

    ncat = 0
    do i = 1,size(idx,1)
       if( all(idx(i,:) > 0) ) ncat = ncat + 1
    end do

    allocate(ct(ncat,nfiles),dct(ncat,nfiles),sky(ncat,nfiles),dsky(ncat,nfiles),&
         ras(ncat,nfiles),decs(ncat,nfiles))


    ncat = 0
    do i = 1,size(idx,1)
       if( all(idx(i,:) > 0) )then
          ncat = ncat  + 1
          do j = 1,nfiles
             k = idx(i,j)
             ras(ncat,j) = fitses(j)%ra(k)
             decs(ncat,j) = fitses(j)%dec(k)
             ct(ncat,j) = fitses(j)%cts(k)
             dct(ncat,j) = fitses(j)%dcts(k)
             sky(ncat,j) = fitses(j)%sky(k)
             dsky(ncat,j) = fitses(j)%dsky(k)
          end do
       end if
    end do
    deallocate(idx)

    do n = 1,nfiles
       deallocate(fitses(n)%ra,fitses(n)%dec)
       allocate(fitses(n)%ra(ncat),fitses(n)%dec(ncat))
       fitses(n)%ra = ras(:,n)
       fitses(n)%dec= decs(:,n)
    end do
    deallocate(ras,decs)


    allocate(ph(ncat,nfiles),dph(ncat,nfiles),phsky(ncat,nfiles),dphsky(ncat,nfiles))
    ph = -1
    dph = -1
    phsky = -1
    dphsky = -1
       
    do i = 1,ncat
       if( nfiles == 1 )then
          ph(i,:) = ctph(1)*ct(i,:)
          dph(i,:) = ph(i,:)*(dct(i,:)/ct(i,:))
          phsky(i,:) = ctph(1)*sky(i,:)
          dphsky(i,:) = phsky(i,:) * (dsky(i,:)/sky(i,:))
       else
          call fotra(tra,pairs,ctph,ct(i,:),dct(i,:),ph(i,:),dph(i,:))
          call fotra(tra,pairs,ctph,sky(i,:),dsky(i,:),phsky(i,:),dphsky(i,:))
       end if
    end do

    do n = 1,size(fitses)
       call qfits_setph(fitses(n),ph(:,n),dph(:,n),phsky(:,n),dphsky(:,n))
    end do
   
    
    do n = 1,size(fitses)
       if( present(phres) ) then          
          call savefits(filenames(n),backups(n),newnames(n),fitses(n),advanced,photsys, &
               phsyscal,area,init_area,catid,quantities,ctph(n),dctph(n),status,phres(n))
       else
          call savefits(filenames(n),backups(n),newnames(n),fitses(n),advanced,photsys, &
               phsyscal,area,init_area,catid,quantities,ctph(n),dctph(n),status)
       end if
       call qfits_deallocate(fitses(n))
    end do

    call deallocate_phsyscal(phsyscal)
    deallocate(ct,dct,sky,dsky,ph,dph,phsky,dphsky,pairs)

  end subroutine writecal

  subroutine readfits(filename,keys,fits,status)

    use astrotrafo
    use phio

    character(len=*), intent(in) :: filename
    character(len=*), dimension(:), intent(in) :: keys
    type(qFITS), intent(out) :: fits
    integer, intent(in out) :: status

    real, parameter :: nullval = -huge(1.0)
    integer, parameter :: group = 1, extver = 0, frow = 1, felem = 1    
    real(dbl), dimension(:), allocatable :: xcen, ycen
    type(AstroTrafoProj) :: tastr
    logical :: anyf
    character(len=FLEN_CARD) :: buf,key
    integer :: w,h,i,ccol,dcol,xcol,ycol,scol,ecol,nrows,saper

    call qfits_init(fits)

    fits%filename = filename

    call ftiopn(25,filename,0,status)
    call ftgisz(25,size(fits%naxes),fits%naxes,status)
    if( status /= 0 ) goto 666

    ! WCS
    call wcsget(25,tastr,status)
    if( status == KEYWORD_NOT_FOUND ) then
       write(error_unit,*) 'Error: Astrometry keywords not found in header.'
       goto 666
    end if
    fits%scale = tastr%scale

    call ftgkyd(25,keys(1),fits%exptime,buf,status)
    if( status == KEYWORD_NOT_FOUND ) then
       write(error_unit,*) "Warning: Exposure time identified by FITS keyword `", &
            trim(keys(1)),"' not found (default to 1 sec)."
       fits%exptime = 1
       status = 0
    end if

    call ftgkyd(25,keys(2),fits%area,buf,status)
    if( status == KEYWORD_NOT_FOUND ) then
!       write(error_unit,*) "Warning: Area identified by FITS keyword `",trim(keys(2)), &
!            "' not found (default to 1 [m2])."
       fits%area = 1
       status = 0
    end if

    call ftgkys(25,keys(5),fits%filter,buf,status)
    if( status == KEYWORD_NOT_FOUND ) then
       write(error_unit,*) "Warning: Filter identified by FITS keyword `",trim(keys(5)), &
            "' not found (default to empty `')."
       fits%filter = ''
       status = 0
    end if

    ! frame
    w = fits%naxes(1)
    h = fits%naxes(2)
    allocate(fits%frame(w,h))
    call ftg2de(25,1,nullval,w,w,h,fits%frame,anyf,status)

    ! table
    call ftmnhd(25,BINARY_TBL,APEREXTNAME,0,status)
    if( status == BAD_HDU_NUM ) then
       write(error_unit,*) &
            "Failed to find a photometry extension in ",trim(filename)
       goto 666
    end if

    call ftgkyd(25,FITS_KEY_APER,fits%aper,buf,status)
    call ftgkyj(25,FITS_KEY_SAPER,saper,buf,status)

    ! sky annuls
    do i = 1, 2
       call ftkeyn(FITS_KEY_ANNULUS,i,key,status)
       call ftgkyd(25,key,fits%annuls(i),buf,status)
    end do
    if( status == KEYWORD_NOT_FOUND ) status = 0
    if( status /= 0 ) goto 666

    fits%saper = saper
    fits%aper = fits%aper / tastr%scale
    fits%annuls = fits%annuls / tastr%scale

    call ftgnrw(25,nrows,status)

    call ftgcno(25,.false.,FITS_COL_X,xcol,status)
    call ftgcno(25,.false.,FITS_COL_Y,ycol,status)
    call ftgcno(25,.false.,FITS_COL_SKY,scol,status)
    call ftgcno(25,.false.,FITS_COL_SKYERR,ecol,status)

    write(key,'(a,i0)') FITS_COL_APCOUNT,saper
    call ftgcno(25,.false.,key,ccol,status)
    write(key,'(a,i0)') FITS_COL_APCOUNTERR,saper
    call ftgcno(25,.false.,key,dcol,status)

    allocate(xcen(nrows),ycen(nrows))
    allocate(fits%sky(nrows),fits%dsky(nrows), &
         fits%cts(nrows),fits%dcts(nrows))

    call ftgcvd(25,xcol,frow,felem,nrows,nullval,xcen,anyf,status)
    call ftgcvd(25,ycol,frow,felem,nrows,nullval,ycen,anyf,status)
    call ftgcvd(25,scol,frow,felem,nrows,nullval,fits%sky,status)
    call ftgcvd(25,ecol,frow,felem,nrows,nullval,fits%dsky,status)
    call ftgcvd(25,ccol,frow,felem,nrows,nullval,fits%cts,status)
    call ftgcvd(25,dcol,frow,felem,nrows,nullval,fits%dcts,status)

    call ftclos(25,status)
    if( status /= 0 ) goto 666

    !  coordinates conversions
    allocate(fits%ra(nrows),fits%dec(nrows))
    call invtrafo(tastr,xcen,ycen,fits%ra,fits%dec)

    fits%nrows = nrows
    fits%coo_err = tastr%err

    deallocate(xcen,ycen)

666 continue
    call ftrprt('STDERR',status)

  end subroutine readfits


  subroutine savefits(filename,backup,newname,fits,advanced,photsys,phsyscal, &
       area,init_area,catid, qlabels,ctph, dctph, status, phres)

    use phsysfits
    use photoconv
    use robustmean
    use calibre

    character(len=*), intent(in) :: filename,backup,newname,photsys,catid
    character(len=*), dimension(:), intent(in) :: qlabels
    type(qFITS), intent(in) :: fits
    type(type_phsys), intent(in) :: phsyscal
    real(dbl), intent(in) :: area, ctph, dctph
    logical, intent(in) :: init_area, advanced
    type(photores), intent(in), optional :: phres
    integer, intent(in out) :: status

    integer, parameter :: group = 1, extver = 0, frow = 1, felem = 1    
    character(len=*), parameter :: bunit = units(3)
    character(len=FLEN_FILENAME) :: output
    character(len=FLEN_VALUE), dimension(:), allocatable :: ttype, tform,tunit
    character(len=FLEN_CARD), dimension(2) :: com
    real(dbl), dimension(:,:), allocatable :: q,dq
    character(len=FLEN_CARD) :: buf,key
    real(dbl) :: w,rsky,rskyerr,rskysig,photflam,photzpt,photplam,photbw
    integer :: n, i, width, height, hdutype, nq, nrows, ncols, chdu, nhdu

    width = fits%naxes(1)
    height = fits%naxes(2)
    nq = size(qlabels)

    call fitsback(filename,backup,newname,.false.,output,status)
    call ftiopn(25,filename,0,status)
    call ftinit(26,output,0,status)
    call ftcphd(25,26,status)

    if( status /= 0 ) goto 666

    call ftghdn(25,chdu)    

    call ftukys(26,FITS_KEY_PHOTSYS,photsys,'photometry filter system',status)
    call ftukys(26,FITS_KEY_BUNIT,bunit,'Physical units of array values',status)

    if( init_area ) then
       call ftukyd(26,FITS_KEY_AREA,area,-5,'[m2] Area of input aperture',status)
    end if

    if( photsys /= '' ) then
       ! compatibility with HST calibration
       call phstkeys(phsyscal,fits%filter,photflam,photzpt,photplam,photbw)
       call ftukyd(26,FITS_KEY_PHOTFLAM,photflam,5,'[erg/s/cm2/A] flux for 1 photon/s/cm2',status)
       call ftukyd(26,FITS_KEY_PHOTZPT,photzpt,-6,'mag zero-point for fluxes in [erg/s/cm2/A]',status)
       call ftukyd(26,FITS_KEY_PHOTPLAM,photplam,-6,'[A] effective wavelength',status)
       call ftukyd(26,FITS_KEY_PHOTBW,photbw,-6,'[A] passband FWHM',status)
    end if

    call ftp2de(26,1,width,width,height,fits%frame,status)
    if( status == 412 ) status = 0

    if( advanced ) then
       ! copy all others extensions
       call ftthdu(25,nhdu,status)
       do n = 1,nhdu
          if( n /= chdu ) then
             call ftmahd(25,n,hdutype,status)
             call ftcopy(25,26,0,status)
             if( status /= 0 ) goto 666
          end if
       end do
    end if

    ! prepare HDU with photometry
    ncols = 6 + 2*nq
    allocate(ttype(ncols),tform(ncols),tunit(ncols))

    tform = '1D'
    tunit(1:2) = 'deg'
    tunit(3:) = bunit

    ttype(1) = FITS_COL_RA
    ttype(2) = FITS_COL_DEC
    ttype(3) = FITS_COL_SKY
    ttype(4) = FITS_COL_SKYERR
    ttype(5) = FITS_COL_PHOTON
    ttype(6) = FITS_COL_PHOTONERR

    do n = 1, nq
       i = 2*n + 5
       ttype(i) = qlabels(n)
       ttype(i+1) = trim(qlabels(n))//'ERR'
       call quantity(qlabels(n),tunit(i))
       tunit(i+1) = tunit(i)
    end do

    call ftibin(26,0,size(ttype),ttype,tform,tunit,PHOTOEXTNAME,0,status)
    call ftukyd(26,FITS_KEY_CTPH,ctph,13,'counts per photons, calibration',status)
    call ftukyd(26,FITS_KEY_CTPHERR,dctph,1,'std. error of CTPH ',status)
    call ftukyd(26,FITS_KEY_APER,fits%aper,3,'[deg] calibration aperture radius',status)
    com(1) = '[deg] inner sky annulus radius'
    com(2) = '[deg] outer sky annulus radius'
    do i = 1, 2
       call ftkeyn(FITS_KEY_ANNULUS,i,key,status)
       call ftukyd(26,key,fits%annuls(i),3,com(i),status)
    end do
    call ftukys(26,FITS_KEY_PHOTSYS,photsys,'photometry filter system',status)
    call ftukys(26,FITS_KEY_FILTER,fits%filter,'a filter in the system',status)

    if( status /= 0 ) goto 666


    ! estimate mean background
    call rmean(fits%phsky,rsky,rskyerr,rskysig)
    w = (3600/fits%scale)**2 
    rsky = rsky / w
    rskyerr = rskyerr / w
    rskysig = rskysig / w
    call ftukyd(26,FITS_KEY_SKYMEAN,rsky,-5,'[ph/arcsec2] mean sky level',status)
    call ftukyd(26,FITS_KEY_SKYSTD,rskyerr,-2, &
         '[ph/arcsec2] standard deviation of mean sky',status)
    call ftukyd(26,FITS_KEY_SKYSIG,rskysig,-2, &
         '[ph/arcsec2] standard deviation of sky',status)

    call ftukys(26,FITS_KEY_CREATOR,FITS_VALUE_CREATOR,FITS_COM_CREATOR,status)
    if( status /= 0 ) goto 666

    call ftpcom(26,BEGIN_PHOTOCAL,status)
    if( catid /= '' ) then
       call ftpcom(26,"Reference photometric sequence: "//trim(catid),status)
    end if
    write(buf,'(a,g0.5,a,es8.1)') "Counts per photons = ",ctph," +- ",dctph
    call ftpcom(26,buf,status)

    if( present(phres) ) then

       write(buf,'(a,i0)') "Objects used = ",phres%ndat
       call ftpcom(26,buf,status)
       call ftpcom(26,'   Catalogue RA,DEC [deg]        Photons         Counts        rel.dev.',status)
       do i = 1, phres%ndat
          write(buf,'(2f13.8,2en15.3,f15.5)') &
               phres%ra(i),phres%dec(i),phres%pht(i),phres%cts(i),phres%res(i)
          call ftpcom(26,buf,status)
       end do
    end if

    call ftpcom(26,MUNIPACK_VERSION,status)
    call ftpcom(26,'Description: http://munipack.physics.muni.cz/dataform_photometry.html',status)
    call ftpcom(26,END_PHOTOCAL,status)

    nrows = size(fits%ra)

    call ftpcld(26,1,frow,felem,nrows,fits%ra,status)
    call ftpcld(26,2,frow,felem,nrows,fits%dec,status)
    call ftpcld(26,3,frow,felem,nrows,fits%phsky,status)
    call ftpcld(26,4,frow,felem,nrows,fits%dphsky,status)
    call ftpcld(26,5,frow,felem,nrows,fits%ph,status)
    call ftpcld(26,6,frow,felem,nrows,fits%dph,status)

    allocate(q(nrows,nq),dq(nrows,nq))
    do n = 1, nq
       call phsysconv(qlabels(n),fits%filter,phsyscal,fits%area,fits%exptime,1.0_dbl,&
            fits%ph,fits%dph,q(:,n),dq(:,n))
    end do

    do n = 1,nq
       i = 2*n + 5
       call ftpcld(26,i,frow,felem,nrows,q(:,n),status)
       call ftpcld(26,i+1,frow,felem,nrows,dq(:,n),status)
    end do
    deallocate(ttype,tform,tunit,q,dq)

    ! add residuals to new extension
    if( advanced .and. present(phres) ) then

       ncols = 5
       allocate(ttype(ncols),tform(ncols),tunit(ncols))

       tform = '1D'
       tunit(1:2) = 'deg'
       tunit(3:) = ''

       ttype(1) = FITS_COL_RA
       ttype(2) = FITS_COL_DEC
       ttype(3) = FITS_COL_PHOTON
       ttype(4) = FITS_COL_COUNT
       ttype(5) = 'RESIDUALS'

       call ftibin(26,0,size(ttype),ttype,tform,tunit,EXT_PHRES,0,status)
       call ftukyd(26,FITS_KEY_CTPH,ctph,13,'counts per photons, calibration',status)
       call ftukyd(26,FITS_KEY_CTPHERR,dctph,1,'std. error of CTPH ',status)

       call ftpcom(26,'This table contains relative residuals of photometry',status)
       call ftpcom(26,'calibration in '//trim(PHOTOEXTNAME)//' extension.',status)
       write(buf,'(a,g0.5,a,es8.1)') "Counts per photons = ",ctph," +- ",dctph
       call ftpcom(26,buf,status)

       call ftpcld(26,1,frow,felem,phres%ndat,phres%ra,status)
       call ftpcld(26,2,frow,felem,phres%ndat,phres%dec,status)
       call ftpcld(26,3,frow,felem,phres%ndat,phres%pht,status)
       call ftpcld(26,4,frow,felem,phres%ndat,phres%cts,status)
       call ftpcld(26,5,frow,felem,phres%ndat,phres%res,status)
       deallocate(ttype,tform,tunit)

    end if

    call ftclos(26,status)
    call ftclos(25,status)


666 continue
    call ftrprt('STDERR',status)

  end subroutine savefits


  subroutine tra_frame(tra,pairs,ctph,xcts,xpht)

    use fotran

    integer, dimension(:,:), intent(in) :: pairs
    real(dbl), dimension(:,:), intent(in) :: tra
    real(dbl), dimension(:), intent(in) :: ctph
    real, dimension(:), intent(in) :: xcts
    real, dimension(:), intent(out) :: xpht

    real(dbl), dimension(:), allocatable :: cts,dcts,pht,dpht
    integer :: n

    n = size(xcts)
    allocate(cts(n),dcts(n),pht(n),dpht(n))
    cts = xcts
    dcts = 0
    dpht = 0
    call fotra(tra,pairs,ctph,cts,dcts,pht,dpht)
    xpht = real(pht)
    deallocate(cts,dcts,pht,dpht)

  end subroutine tra_frame
    

  subroutine qfits_init(fits)
    
    type(qFITS), intent(out) :: fits
    fits%status = .false.
    fits%filename = ''
    fits%nrows = 0
    fits%naxes = 0
    fits%aper = 0
    fits%area = 1
    fits%exptime = 1
    fits%annuls = 0
    fits%filter = ''

  end subroutine qfits_init

  subroutine qfits_deallocate(fits)

    type(qFITS), intent(in out) :: fits

    if( allocated(fits%frame) ) deallocate(fits%frame)
    if( allocated(fits%ra) ) deallocate(fits%ra)
    if( allocated(fits%dec) ) deallocate(fits%dec)
    if( allocated(fits%cts) ) deallocate(fits%cts)
    if( allocated(fits%dcts) ) deallocate(fits%dcts)
    if( allocated(fits%sky) ) deallocate(fits%sky)
    if( allocated(fits%dsky) ) deallocate(fits%dsky)
    if( allocated(fits%ph) ) deallocate(fits%ph)
    if( allocated(fits%dph) ) deallocate(fits%dph)
    if( allocated(fits%phsky) ) deallocate(fits%phsky)
    if( allocated(fits%dphsky) ) deallocate(fits%dphsky)

  end subroutine qfits_deallocate

  subroutine qfits_setph(fits,ph,dph,phsky,dphsky)
    
    type(qFITS), intent(in out) :: fits
    real(dbl), dimension(:), intent(in) :: ph,dph,phsky,dphsky
    integer :: ncat

    ncat = size(ph)
    allocate(fits%ph(ncat),fits%dph(ncat),fits%phsky(ncat),fits%dphsky(ncat))
    fits%ph = ph
    fits%dph = dph
    fits%phsky = phsky
    fits%dphsky = dphsky

  end subroutine qfits_setph

end module sfits
