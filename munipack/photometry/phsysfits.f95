!
!  Photometric systems table
!
!  Copyright © 2013 - 2014 F.Hroch (hroch@physics.muni.cz)
!
!  This file is part of Munipack.
!
!  Munipack is free software: you can redistribute it and/or modify
!  it under the terms of the GNU General Public License as published by
!  the Free Software Foundation, either version 3 of the License, or
!  (at your option) any later version.
!  
!  Munipack is distributed in the hope that it will be useful,
!  but WITHOUT ANY WARRANTY; without even the implied warranty of
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!  GNU General Public License for more details.
!  
!  You should have received a copy of the GNU General Public License
!  along with Munipack.  If not, see <http://www.gnu.org/licenses/>.


module phsysfits

  use fitsio
  use iso_fortran_env

  implicit none

  integer, parameter, private :: dbl = selected_real_kind(15)

  type type_phsys
     character(len=FLEN_VALUE) :: name, filter_ref
     character(len=FLEN_VALUE), dimension(:), allocatable :: filter
     real(dbl), dimension(:), allocatable :: lam_eff,lam_fwhm,nu_eff,nu_fwhm, &
          fnu_ref,flam_ref
  end type type_phsys

  interface deallocate_phsyscal
     module procedure deallocate_phsyscal_single,deallocate_phsyscal_multiple
  end interface deallocate_phsyscal

contains


  subroutine phsysread(filename,phsys,status)

    character(len=*), intent(in) :: filename
    type(type_phsys), dimension(:), allocatable, intent(out) :: phsys
    integer, intent(out) :: status

    integer, parameter :: frow = 1, felem = 1
    real(dbl), parameter :: nullval = 0.0_dbl

    integer :: nrows, ncols, hdutype, nhdu, blocksize,i, n
    integer, parameter :: maxcols = 7
    integer, dimension(maxcols) :: col
    character(len=FLEN_VALUE), dimension(maxcols) :: cols
    character(len=FLEN_VALUE) :: photosys
    character(len=FLEN_CARD) :: buf
    logical :: anyf

    cols(1) = FITS_COL_FILTER
    cols(2) = FITS_COL_LAMEFF
    cols(3) = FITS_COL_LAMFWHM
    cols(4) = FITS_COL_NUEFF
    cols(5) = FITS_COL_NUFWHM
    cols(6) = FITS_COL_FLAMREF
    cols(7) = FITS_COL_FNUREF

    status = 0

    ! open get number of HDU
    call ftopen(15,filename,0,blocksize,status)
    call ftthdu(15,nhdu,status)
    if( status /= 0 ) goto 666

    ! checkpoint
    call ftgkys(15,'HDUNAME',photosys,buf,status)
    if( status == KEYWORD_NOT_FOUND .or. photosys /= FHDUNAME ) then
       status = 0
       write(error_unit,*) "Warning: Photometry system file `",trim(filename), &
            "' has wrong identificator."
    end if
    if( status /= 0 ) goto 666

    allocate(phsys(nhdu-1))

    do n = 1, nhdu-1

       call ftmahd(15,n+1,hdutype,status)
       if( hdutype /= BINARY_TBL .or. status /= 0 ) goto 666

       call ftgkys(15,'EXTNAME',phsys(n)%name,buf,status)
       call ftgkys(15,FITS_KEY_FILTREF,phsys(n)%filter_ref,buf,status)
       if( status == KEYWORD_NOT_FOUND .or. status /= 0 ) goto 666
       
       call ftgncl(15,ncols,status)
       call ftgnrw(15,nrows,status)

       ! find columns by cols
       do i = 1, size(cols)
          call ftgcno(15,.false.,cols(i),col(i),status)
       end do
       if( status /= 0 ) goto 666
       if( ncols /= size(cols) ) stop 'ncols /= size(cols)'

       allocate(phsys(n)%filter(nrows),phsys(n)%lam_eff(nrows),phsys(n)%lam_fwhm(nrows),&
            phsys(n)%nu_eff(nrows),phsys(n)%nu_fwhm(nrows),phsys(n)%flam_ref(nrows),&
            phsys(n)%fnu_ref(nrows))

       call ftgcvs(15,col(1),frow,felem,nrows,'',phsys(n)%filter,anyf,status)
       call ftgcvd(15,col(2),frow,felem,nrows,nullval,phsys(n)%lam_eff,anyf,status)
       call ftgcvd(15,col(3),frow,felem,nrows,nullval,phsys(n)%lam_fwhm,anyf,status)
       call ftgcvd(15,col(4),frow,felem,nrows,nullval,phsys(n)%nu_eff,anyf,status)
       call ftgcvd(15,col(5),frow,felem,nrows,nullval,phsys(n)%nu_fwhm,anyf,status)
       call ftgcvd(15,col(7),frow,felem,nrows,nullval,phsys(n)%fnu_ref,anyf,status)
       call ftgcvd(15,col(6),frow,felem,nrows,nullval,phsys(n)%flam_ref,anyf,status)

       if( status /= 0 ) goto 666

!       do i = 1,nrows
!          write(*,*) i,trim(phsys(n)%filter(i)),phsys(n)%lam_eff(i), &
!               phsys(n)%lam_fwhm(i),phsys(n)%fnu_ref(i)
!       end do

    end do

    
    call ftclos(15,status)
    if( status /= 0 ) goto 666

    return

666 continue

    if( allocated(phsys) ) then
       do i = 1, size(phsys)
          if( allocated(phsys(i)%filter) ) deallocate(phsys(i)%filter)
          if( allocated(phsys(i)%lam_eff) ) deallocate(phsys(i)%lam_eff)
          if( allocated(phsys(i)%lam_fwhm) ) deallocate(phsys(i)%lam_fwhm)
          if( allocated(phsys(i)%nu_eff) ) deallocate(phsys(i)%nu_eff)
          if( allocated(phsys(i)%nu_fwhm) ) deallocate(phsys(i)%nu_fwhm)
          if( allocated(phsys(i)%fnu_ref) ) deallocate(phsys(i)%fnu_ref)
          if( allocated(phsys(i)%flam_ref) ) deallocate(phsys(i)%flam_ref)
       end do
    end if

    call ftclos(15,status)
    call ftrprt('STDERR',status)

  end subroutine phsysread

  subroutine listphsys(phsystable)

    character(len=*), intent(in) :: phsystable
    type(type_phsys), dimension(:), allocatable :: phsyscal    
    integer :: status,i,j

    call phsysread(phsystable,phsyscal,status)
    if( status /= 0 ) stop 'Failed to read photometry system table.'

    write(*,*)
    do i = 1,size(phsyscal)
       write(*,'(2a)',advance="no") trim(phsyscal(i)%name),": "
       do j = 1, size(phsyscal(i)%filter)
          write(*,'(a)',advance="no") trim(phsyscal(i)%filter(j))
          if( j == size(phsyscal(i)%filter) ) then
             write(*,*)
          else
             write(*,'(a)',advance="no") ","
          end if
       end do
    end do

    call deallocate_phsyscal(phsyscal)

  end subroutine listphsys

  subroutine deallocate_phsyscal_multiple(phsyscal)

    type(type_phsys), dimension(:), allocatable :: phsyscal
    integer :: i

    if( allocated(phsyscal) ) then
       do i = 1,size(phsyscal)
          call deallocate_phsyscal_single(phsyscal(i))
       end do
       deallocate(phsyscal)
    end if
    
  end subroutine deallocate_phsyscal_multiple

  subroutine deallocate_phsyscal_single(phsyscal)

    type(type_phsys) :: phsyscal

    if( allocated(phsyscal%filter) ) then
       deallocate(phsyscal%filter,phsyscal%lam_eff,phsyscal%lam_fwhm, &
            phsyscal%nu_eff,phsyscal%nu_fwhm,phsyscal%fnu_ref,phsyscal%flam_ref)
    end if
    
  end subroutine deallocate_phsyscal_single

  subroutine phselect(phsystable,phsystem,phsys)

    character(len=*), intent(in) :: phsystable,phsystem
    type(type_phsys), intent(out) :: phsys
    type(type_phsys), dimension(:), allocatable :: phsyscal

    integer :: i,n,status

    if( phsystem == '' ) stop 'Photometry system undefined.'

    call phsysread(phsystable,phsyscal,status)
    if( status /= 0 ) stop 'Failed to read photometry system table.'

    do i = 1,size(phsyscal)
       if( phsyscal(i)%name == phsystem ) then
          n = size(phsyscal(i)%filter)
          phsys%name = phsyscal(i)%name
          phsys%filter_ref = phsyscal(i)%filter_ref
          allocate(phsys%filter(n),phsys%lam_eff(n),phsys%lam_fwhm(n),&
            phsys%nu_eff(n),phsys%nu_fwhm(n),phsys%flam_ref(n),phsys%fnu_ref(n))
          phsys%filter = phsyscal(i)%filter
          phsys%lam_eff = phsyscal(i)%lam_eff
          phsys%lam_fwhm = phsyscal(i)%lam_fwhm
          phsys%nu_eff = phsyscal(i)%nu_eff
          phsys%nu_fwhm = phsyscal(i)%nu_fwhm
          phsys%flam_ref = phsyscal(i)%flam_ref
          phsys%fnu_ref = phsyscal(i)%fnu_ref
       end if
    end do

    call deallocate_phsyscal(phsyscal)    

  end subroutine phselect


  subroutine selphsystem(phsyscal,phsystem,filter,leff,lfwhm,feff,ffwhm,flam_ref, &
       fnu_ref,status)

    type(type_phsys), dimension(:), intent(in) :: phsyscal
    character(len=*), intent(in) :: phsystem,filter
    real(dbl), intent(out) :: leff,lfwhm,feff,ffwhm,flam_ref,fnu_ref
    integer, intent(out) :: status
    
    integer :: i,j

    status = -1

    do i = 1,size(phsyscal)
!       write(*,*) trim(phsyscal(i)%name)
       if( phsyscal(i)%name == phsystem ) then
          do j = 1, size(phsyscal(i)%filter) 
!             write(*,*) trim(phsyscal(i)%filter(j)), trim(filter)
             if( phsyscal(i)%filter(j) == filter ) then
                status = 0
                feff = phsyscal(i)%nu_eff(j)
                ffwhm = phsyscal(i)%nu_fwhm(j)
                leff = phsyscal(i)%lam_eff(j)
                lfwhm = phsyscal(i)%lam_fwhm(j)
                flam_ref = phsyscal(i)%flam_ref(j)
                fnu_ref = phsyscal(i)%fnu_ref(j)
                return
             end if
          end do
       end if
    end do

  end subroutine selphsystem


  subroutine phsysmagph(phsys,filter,mag,dmag,ph,dph)

    use photoconv

    type(type_phsys), intent(in) :: phsys
    character(len=*), dimension(:), intent(in) :: filter
    real(dbl), dimension(:,:), intent(in) :: mag,dmag
    real(dbl), dimension(:,:), intent(out) :: ph,dph

    real(dbl), dimension(size(filter)) :: feff,ffwhm,leff,lfwhm,flamref,fnuref
    integer :: n,j,status
    real(dbl), dimension(:,:), allocatable :: ph0,dph0,ph3,dph3,mag0,dmag0, &
         magx,dmagx,mag3,dmag3

    if( size(mag,2) /= size(filter) ) stop 'Counts of filters and magnitudes different.'

    j = 1
    do n = 1, size(mag,2)
       call selphsystem((/phsys/),phsys%name,filter(n), &
            leff(n),lfwhm(n),feff(n),ffwhm(n),flamref(n),fnuref(n),status)

       if( status /= 0 ) then
          write(error_unit,*) 'Filter `',trim(filter(n)),"' not found in system `",&
               trim(phsys%name),"'."
          stop 'Filter not found.'
       end if
    end do

!    allocate(ph0(size(ph,1),size(ph,2)),dph0(size(ph,1),size(ph,2)))

    do j = 1,size(mag,2)
       call mag2ph0(leff(j),lfwhm(j),flamref(j),mag(:,j),dmag(:,j),ph(:,j),dph(:,j))
    end do

!    do n = 1,size(mag,1)
!       call mag2ph3(leff,lfwhm,flamref,mag(n,:),dmag(n,:),ph(n,:),dph(n,:))
!    end do
    
!    do j = 1,size(mag,1)
!       write(*,*) j,mag(j,3),dmag(j,3),ph(j,3),dph(j,3)
!    end do

!    allocate(mag0(size(ph,1),size(ph,2)),dmag0(size(ph,1),size(ph,2)))

!    call ph2mag0(leff(1),lfwhm(1),flamref(1),ph(:,1),dph(:,1),mag0(:,1),dmag0(:,1))
!    do j = 1,size(mag,1)
!       write(*,'(5g15.5)') real(mag(j,3)),real(ph(j,3)),real(ph0(j,3)),real(ph0(j,2)/ph0(j,3)),&
!            real(ph0(j,3)/ph(j,3))
!    end do

!    stop

!    return

    allocate(ph0(size(ph,1),size(ph,2)),dph0(size(ph,1),size(ph,2)), &
         ph3(size(ph,1),size(ph,2)),dph3(size(ph,1),size(ph,2)), &
         mag3(size(ph,1),size(ph,2)),dmag3(size(ph,1),size(ph,2)),&
         mag0(size(ph,1),size(ph,2)),dmag0(size(ph,1),size(ph,2)),&
         magx(size(ph,1),size(ph,2)),dmagx(size(ph,1),size(ph,2)),&
)

    goto 666
    

!    do n = 1,size(mag,1)
!       call mag2ph(leff,lfwhm,flamref,mag(n,:),dmag(n,:),ph(n,:),dph(n,:))
!       call mag2ph0(leff,flamref,mag(n,:),dmag(n,:),ph0(n,:),dph0(n,:))
!       write(*,*) real(mag(n,:)),real(ph(n,:))
!    end do

    do n = 1,size(mag,1)
!       call mag2ph(leff,lfwhm,flamref,mag(n,:),dmag(n,:),ph(n,:),dph(n,:))
!       call mag2ph3(leff,lfwhm,flamref,mag(n,:),dmag(n,:),ph3(n,:),dph3(n,:))
!       write(*,*) real(mag(n,:)),real(ph(n,:))
    end do

!    do n = 1,size(mag,1)
!       call mag2ph(leff,lfwhm,flamref,mag(n,:),dmag(n,:),ph(n,:),dph(n,:))
!       call mag2ph(leff,lfwhm,flamref,mag(n,:),dmag(n,:),ph(n,:),dph(n,:))
!       write(*,*) real(mag(n,:)),real(ph(n,:))
!    end do

!    do n = 1,size(mag,1)
!       write(*,'(2f10.3,3g15.5)') mag(n,3),mag(n,2)-mag(n,3),ph0(n,3),ph(n,3),ph3(n,3)
!    end do

    do j = 1,size(mag,2)
!       call mag2ph0(leff(j),flamref(j),mag(:,j),dmag(:,j),ph0(:,j),dph0(:,j))
!       call ph2mag0(leff(j),flamref(j),ph0(:,j),dph0(:,j),mag0(:,j),dmag0(:,j))
    end do
    do n = 1,size(mag,1)
       call mag2ph3(leff,lfwhm,flamref,mag(n,:),dmag(n,:),ph3(n,:),dph3(n,:))
!       call ph2mag3(leff,lfwhm,flamref,ph3(n,:),dph3(n,:),mag3(n,:),dmag3(n,:))
    end do

    do j = 1,size(mag,2)-2
       write(*,*) trim(filter(j))
       do n = 1,size(mag,1)
          write(*,'(2f10.3,3g15.5)') mag(n,j),mag(n,j)-mag(n,j+1),ph0(n,j),ph3(n,j)
       end do
       do n = 1,size(mag,1)
          write(*,'(5f10.3)') mag(n,j),mag(n,j)-mag(n,j+1),mag0(n,j),mag3(n,j)
       end do
    end do
    do j = size(mag,2)-1,size(mag,2)
       write(*,*) trim(filter(j))
!       do n = 1,size(mag,1)
!          call mag2ph3(leff,lfwhm,flamref,mag(j,:),dmag(j,:),ph3(j,:),dph3(j,:))
!          call ph2mag3(leff,lfwhm,flamref,ph3(j,:),dph3(j,:),mag3(j,:),dmag3(j,:))
!       end do
       do n = 1,size(mag,1)
          write(*,'(2f10.3,3g15.5)') mag(n,j),mag(n,j-1)-mag(n,j),ph0(n,j),ph3(n,j)
       end do
       do n = 1,size(mag,1)
          write(*,'(5f10.3)') mag(n,j),mag(n,j-1)-mag(n,j),mag0(n,j),mag3(n,j)
       end do
    end do

    do n = 1,size(mag,1)
!       call ph2mag3(leff,lfwhm,flamref,ph(n,:),dph(n,:),magx(n,:),dmagx(n,:))
!       call ph2mag0(lfwhm(3),flamref(3),ph0(:,3),dph0(:,3),mag0(:,3),dmag0(:,3))
!       call ph2mag0(leff(3),lfwhm(3),flamref(3),ph(:,3),dph(:,3),mag0(:,3),dmag0(:,3))
!       call ph2mag3(leff,lfwhm,flamref,ph3(n,:),dph3(n,:),mag3(n,:),dmag3(n,:))
    end do


!!$    do j = 1,size(mag,2)-2
!!$       write(*,*) trim(filter(j))
!!$       do n = 1,size(mag,1)
!!$          write(*,'(5f10.3)') mag(n,j),mag(n,j)-mag(n,j+1),mag0(n,j),mag3(n,j)
!!$       end do
!!$    end do
!!$    do j = size(mag,2)-1,size(mag,2)
!!$       write(*,*) trim(filter(j))
!!$       do n = 1,size(mag,1)
!!$          write(*,'(5f10.3)') mag(n,j),mag(n,j-1)-mag(n,j),mag0(n,j),mag3(n,j)
!!$       end do
!!$    end do

!    stop

!    do n = 1,size(mag,1)
!       call mag2ph(leff,lfwhm,flamref,mag(n,:),dmag(n,:),ph(n,:),dph(n,:))
!       call mag2ph3(leff,lfwhm,flamref,mag(n,:),dmag(n,:),ph(n,:),dph(n,:))
!       call mag2ph3(leff,lfwhm,flamref,mag(n,:),dmag(n,:),ph(n,:),dph(n,:))
!       write(*,*) real(mag(n,:)),real(ph(n,:))
!    end do

    ph = ph0
    dph = dph0


    666 continue

    ! when magnitude error is unavailable, we are use the most optimistics
    ! variant with non-photon noise negligible
    where( dmag > 9 .or. dmag < epsilon(dmag) )
       dph = sqrt(ph)
    end where

    deallocate(ph0,dph0,ph3,dph3,mag0,dmag0,magx,dmagx,mag3,dmag3)

  end subroutine phsysmagph

  subroutine phsysphmag(phsys,filter,ph,dph,mag,dmag)

    use photoconv

    type(type_phsys), intent(in) :: phsys
    character(len=*), dimension(:), intent(in) :: filter
    real(dbl), dimension(:,:), intent(in) :: ph,dph
    real(dbl), dimension(:,:), intent(out) :: mag,dmag

    real(dbl), dimension(size(filter)) :: feff,ffwhm,leff,lfwhm,flamref,fnuref
    integer :: n,j,status

    if( size(mag,2) /= size(filter) ) stop 'Counts of filters and magnitudes different.'

    j = 1
    do n = 1, size(mag,2)
       call selphsystem((/phsys/),phsys%name,filter(n), &
            leff(n),lfwhm(n),feff(n),ffwhm(n),flamref(n),fnuref(n),status)

       if( status /= 0 ) then
          write(error_unit,*) 'Filter `',trim(filter(n)),"' not found in system `",&
               trim(phsys%name),"'."
          stop 'Filter not found.'
       end if
    end do

!    do n = 1,size(ph,1)
    do j = 1,size(ph,1)
!       call ph2mag3(leff,lfwhm,flamref,ph(j,:),dph(j,:),mag(j,:),dmag(j,:))
    end do
    do n = 1,size(ph,2)
       call ph2mag0(leff(n),lfwhm(n),flamref(n),ph(:,n),dph(:,n),mag(:,n),dmag(:,n))
    end do

!    write(*,*) real(ph(1:3,2:3)),real(mag(1:3,2:3))
!    write(*,*) real(mag(1:5,3))
!    end do

!    do n = 1,size(ph,1)
!       call ph2mag0(lfwhm(n),flamref,ph(n,:),dph(n,:),mag(n,:),dmag(n,:))
!       write(*,'(5f15.5)') real(mag(n,:))
!    end do

!    where( dmag > 9.0 )
!       dph = sqrt(ph)
!    end where

  end subroutine phsysphmag

  subroutine phsysmagflux(phsys,filter,mag,dmag,flux,dflux)

    use photoconv

    type(type_phsys), intent(in) :: phsys
    character(len=*), dimension(:), intent(in) :: filter
    real(dbl), dimension(:,:), intent(in) :: mag,dmag
    real(dbl), dimension(:,:), intent(out) :: flux,dflux

    real(dbl), dimension(size(filter)) :: feff,ffwhm,leff,lfwhm,flamref,fnuref
    integer :: n,j,status

    if( size(mag,2) /= size(filter) ) stop 'Counts of filters and magnitudes different.'

    j = 1
    do n = 1, size(mag,2)
       call selphsystem((/phsys/),phsys%name,filter(n), &
            leff(n),lfwhm(n),feff(n),ffwhm(n),flamref(n),fnuref(n),status)

       if( status /= 0 ) then
          write(error_unit,*) 'Filter `',trim(filter(n)),"' not found in system `",&
               trim(phsys%name),"'."
          stop 'Filter not found.'
       end if
    end do

    do n = 1,size(mag,1)
       call mag2flux0(lfwhm,flamref,mag(n,:),dmag(n,:),flux(n,:),dflux(n,:))
    end do

  end subroutine phsysmagflux


  subroutine phsysconv(key,filter,phsyscal,area,exptime,arcscale,ph,dph,q,dq)

    use photoconv

    character(len=*), intent(in) :: key, filter
    type(type_phsys), intent(in) :: phsyscal
    real(dbl), intent(in) :: exptime,area,arcscale
    real(dbl), dimension(:), intent(in) :: ph,dph
    real(dbl), dimension(:), intent(out) :: q,dq
    real(dbl), dimension(:), allocatable :: r,dr
    real(dbl) :: feff,ffwhm,leff,lfwhm,fnuref,flamref
    integer :: i
    logical :: defined

    defined = .false.
    do i = 1,size(phsyscal%filter)
       if( filter == phsyscal%filter(i) ) then
          feff = phsyscal%nu_eff(i)
          ffwhm = phsyscal%nu_fwhm(i)
          leff = phsyscal%lam_eff(i)
          lfwhm = phsyscal%lam_fwhm(i)
          fnuref = phsyscal%fnu_ref(i)
          flamref = phsyscal%flam_ref(i)
          defined = .true.
       end if
    end do

    if( .not. defined ) then
       write(error_unit,*) "Filter `",trim(filter),"' not defined."
       stop 'Undefined filter of photometry system.'
    end if

    allocate(r(size(q)),dr(size(dq)))

    call phrate(area,exptime,arcscale,ph,dph,r,dr)

    if( key == 'FNU' ) then
       call ph2fnu(feff,ffwhm,r,dr,q,dq)
    else if( key == 'FLAM' ) then
       call ph2flam0(leff,lfwhm,r,dr,q,dq)
    else if( key == 'PHOTNU' ) then
       call ph2photnu(ffwhm,r,dr,q,dq)
    else if( key == 'PHOTLAM' ) then
       call ph2photlam(lfwhm,r,dr,q,dq)
    else if( key == 'ABMAG' ) then
       call ph2abmag(feff,ffwhm,r,dr,q,dq)
    else if( key == 'STMAG' ) then
       call ph2stmag(leff,lfwhm,r,dr,q,dq)
    else if( key == 'MAG' ) then
!       call ph2mag(fnuref,feff,ffwhm,r,dr,q,dq)
       call ph2mag0(leff,lfwhm,flamref,r,dr,q,dq)
!       write(*,'(f15.5)') q(1:3)
!       do i = 1,size(r)
!          call ph2mag(flamref,leff,lfwhm,r,dr,q,dq)
!          write(*,'(f15.5)') q(1)
!       end do


    else if( key == 'FLUX' ) then
       call ph2flux(feff,r,dr,q,dq)
    else if( key == 'PHOTRATE' ) then
       q = r
       dq = dr
    else if( key(1:6) == 'PHOTON' ) then
       q = ph
       dq = dph
    else
       stop 'An unknown quantity for calibrated data requested.'
    end if

    deallocate(r,dr)

  end subroutine phsysconv

  subroutine phstkeys(phsyscal,filter,photflam,photzpt,photplam,photbw)

    use photoconv

    character(len=*), intent(in) :: filter
    type(type_phsys), intent(in) :: phsyscal
    real(dbl), intent(out) :: photflam,photzpt,photplam,photbw
    integer :: i

    do i = 1,size(phsyscal%filter)
       if( filter == phsyscal%filter(i) ) then

          ! unit erg/.. <-> J/.. conversion
          photflam = 1e6 * STspflux * (planck*c) / phsyscal%lam_eff(i)**2
          ! the aperture correction 0.1 has been ommited
          photzpt = -21.0 
          photplam = phsyscal%lam_eff(i) / 1e-10
          photbw = phsyscal%lam_fwhm(i) / 1e-10

       end if
    end do
       
  end subroutine phstkeys

  subroutine phsyspairs(phsys,filters,pairs)

    ! arrange filters for photometry transformation

    type(type_phsys), intent(in) :: phsys
    character(len=*), dimension(:), intent(in) :: filters
    integer, dimension(:,:), allocatable :: pairs

    integer :: i,j,l,m,n,npairs

    if( size(filters) == 1 ) then
       allocate(pairs(1,2))
       pairs = 1
       return
    end if

    n = 0
    do j = 1,size(filters)
       if( filters(j) == phsys%filter_ref ) n = j
    end do
    if( n == 0 ) stop 'Reference filter not included in filter set.'

    m = 0
    do j = 1, size(phsys%filter)
       if( phsys%filter(j) == phsys%filter_ref ) m = j
    end do
    if( m == 0 ) stop 'Reference filter not found in standard set.'

    allocate(pairs(size(filters),2))
    npairs = 1
    pairs(1,:) = n

    ! we are supposes that the filters are sorted by wavelength order
    n = pairs(1,1)
    do j = m-1,1,-1
       l = 0
       do i = 1,size(filters)
          if( phsys%filter(j) == filters(i) ) l = i
       end do
       if( l /= 0 ) then
          npairs = npairs + 1
          pairs(npairs,:) = (/l,n/)
          n = l
       end if
    end do
    n = pairs(1,1)
    do j = m+1, size(phsys%filter)
       l = 0
       do i = 1,size(filters)
          if( phsys%filter(j) == filters(i) ) l = i
       end do
       if( l /= 0 ) then
          npairs = npairs + 1
          pairs(npairs,:) = (/n,l/)
          n = l
       end if
    end do

  end subroutine phsyspairs

end module phsysfits

