!
!  photometric calibration
!
!  Copyright © 2012-4 F.Hroch (hroch@physics.muni.cz)
!
!  This file is part of Munipack.
!
!  Munipack is free software: you can redistribute it and/or modify
!  it under the terms of the GNU General Public License as published by
!  the Free Software Foundation, either version 3 of the License, or
!  (at your option) any later version.
!  
!  Munipack is distributed in the hope that it will be useful,
!  but WITHOUT ANY WARRANTY; without even the implied warranty of
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!  GNU General Public License for more details.
!  
!  You should have received a copy of the GNU General Public License
!  along with Munipack.  If not, see <http://www.gnu.org/licenses/>.


program phcal

  use fitsio
  use phsysfits
  use mfits
  
  implicit none

  integer, parameter :: dbl = selected_real_kind(15)
  character(len=4*FLEN_FILENAME) :: line, key, val
  character(len=FLEN_FILENAME), dimension(:), allocatable :: filename, backup, newname
  character(len=FLEN_FILENAME) :: ref = '', cat = '', tratable = '', &
       phsystable = 'photosystems.fits'
  character(len=FLEN_VALUE), dimension(:), allocatable :: qlabels
  character(len=FLEN_VALUE) :: col_ra = FITS_COL_RA, col_dec = FITS_COL_DEC, &
       photsys_ref = '', photsys_instr = ''
  character(len=FLEN_VALUE), dimension(:), allocatable :: col_mag, col_magerr, filters
  logical :: verbose = .false., plog = .false., list = .false.
  logical :: init_area = .false. , init_photsys = .false.
  logical :: cal_manual = .false.
  logical :: advanced = .false.
  real(dbl) :: utol = -1.0/3600.0
  real(dbl) :: area
  real(dbl), dimension(:), allocatable :: ctph_manual,dctph_manual
  integer :: eq,nfile,n,nmag,nmagerr,nfilters,nctph
  character(len=FLEN_KEYWORD), dimension(5) :: keys
  integer :: nqlabels

  keys(1) = FITS_KEY_EXPTIME
  keys(2) = FITS_KEY_AREA
  keys(3) = FITS_KEY_AIRMASS
  keys(4) = FITS_KEY_PHOTSYS
  keys(5) = FITS_KEY_FILTER
  nqlabels = 7
  qlabels = (/'PHOTRATE','FLUX    ','FNU     ','FLAM    ','MAG     ', &
       'ABMAG   ','STMAG   '/)

  allocate(col_mag(0), col_magerr(0), filters(0))
  allocate(ctph_manual(0), dctph_manual(0))
  allocate(filename(0), backup(0), newname(0))
  nfilters = 0
  nmag = 0
  nmagerr = 0
  nfile = 0
  area = 1

  do
     read(*,'(a)',end=20) line

     eq = index(line,'=')
     if( eq == 0 ) stop 'Improper control data.'

     key = line(:eq-1)
     val = line(eq+1:)

     if( key == 'VERBOSE' ) then

        read(val,*) verbose

     else if( key == 'PIPELOG' ) then

        read(val,*) plog

     else if( key == 'ADVANCED' ) then

        read(val,*) advanced

     else if( key == 'FITS_KEY_EXPTIME' ) then

        read(val,*) keys(1)

     else if( key == 'FITS_KEY_AREA' ) then

        read(val,*) keys(2)

     else if( key == 'FITS_KEY_GAIN' ) then

        read(val,*) keys(3)

     else if( key == 'FITS_KEY_PHOTOSYS' ) then ! remove?

        read(val,*) keys(4)

     else if( key == 'FITS_KEY_FILTER' ) then

        read(val,*) keys(5)

     else if( key == 'NFILTERS' ) then

        read(val,*) nfilters

        deallocate(filters)
        allocate(filters(nfilters))
        filters = ''

     else if( key == 'FILTERS' ) then

        ! a set of filters
        read(val,*) filters

     else if( key == 'COL_RA' ) then

        read(val,*) col_ra

     else if( key == 'COL_DEC' ) then

        read(val,*) col_dec

     else if( key == 'COL_NMAG' ) then

        read(val,*) nmag

        deallocate(col_mag)
        allocate(col_mag(nmag))
        col_mag = ''
        
     else if( key == 'COL_MAG' ) then

        ! a set columns
        read(val,*) col_mag

     else if( key == 'COL_NMAGERR' ) then

        read(val,*) nmagerr

        deallocate(col_magerr)
        allocate(col_magerr(nmagerr))
        col_magerr= ''

     else if( key == 'COL_MAGERR' ) then

        read(val,*) col_magerr

     else if( key == 'TOL' ) then

        read(val,*) utol

     else if( key == 'AREA' ) then

        read(val,*) area
        init_area = .true.

     else if( key == 'NQUANTITIES' ) then
 
        read(val,*) nqlabels

        deallocate(qlabels)
        allocate(qlabels(nqlabels))
        qlabels = ''

     else if( key == 'QUANTITIES' ) then
 
        read(val,*) qlabels

     else if( key == 'PHOTSYS_REF' ) then

        read(val,*) photsys_ref

     else if( key == 'PHOTSYS_INSTR' ) then

        read(val,*) photsys_instr
        init_photsys = .true.

     else if( key == 'PHSYSTABLE' ) then

        read(val,*) phsystable

     else if( key == 'TRATABLE' ) then

        read(val,*) tratable

     else if( key == 'LIST' ) then

        read(val,*) list

     else if( key == 'NCTPH' ) then

        read(val,*) nctph
        deallocate(ctph_manual,dctph_manual)
        allocate(ctph_manual(nctph),dctph_manual(nctph))
        ctph_manual = 1
        dctph_manual= 0

     else if( key == 'CTPH' ) then

        ! manual calibration
        read(val,*) ctph_manual
        dctph_manual = 0
        cal_manual = .true.

     else if( key == 'CAT' ) then
        
        ! calibration agains to the catalogue
        read(val,*) cat

     else if( key == 'REF' ) then

        ! calibration agains to the already calibrated frame
        read(val,*) ref

     else if( line(1:5) == 'NFILE' .and. eq > 0) then

        read(val,*) nfile

        deallocate(filename,backup,newname)
        allocate(filename(nfile),backup(nfile),newname(nfile))
        filename = ''
        backup = ''
        newname = ''
        n = 0

     else if( key == 'FILE' ) then

        n = n + 1
        if( n > size(filename) ) stop 'Too many files.'

        read(line(eq+1:),*) filename(n), backup(n), newname(n)

     end if

  end do

20 continue

  if( size(filename) == 0 ) stop 'No frames to process.'

  if( list ) then

     call listphsys(phsystable)

  else

     if( cat /= '' ) then
        call calibrate   ! catcal ?
     else if( ref /= '' ) then
        call framecal
     else if( cal_manual ) then
        call mancal
     else
        stop 'Calibration type unknown.'
     end if

  end if

  deallocate(filename,backup,newname,col_mag,col_magerr,filters,ctph_manual, &
       dctph_manual)

  stop 0


contains

  ! calibration
  subroutine calibrate

    use phsysfits
    use calibre
    use minpacks
    use photoconv
    use jamming
    use sfits

    character(len=FLEN_VALUE) :: catid,photosys_instr,photsys1,photsys2
    character(len=FLEN_VALUE), dimension(:), allocatable :: filter_frames
    character(len=FLEN_VALUE), dimension(:,:), allocatable :: fpairs,fp
    real(dbl), allocatable, dimension(:) :: refra, refdec, ctph0,ctph,dctph, &
         exptime, areas, tol,aper
    real(dbl), allocatable, dimension(:,:) :: tratab,refph,drefph, &
         cts,dcts,dn,ddn,ra,dec,mag,dmag,annuls
    real(dbl) :: q
    type(type_phsys) :: phsyscal
    type(photores), dimension(:), allocatable :: phres
    integer, dimension(:,:), allocatable :: pairs
    integer :: status,nfiles,ncat,i

    if( photsys_ref == '' ) stop 'No identificator for reference photometry system.'

    if( nmag == 0 .or. nmag /= nmagerr ) &
         stop 'Bad specification of the magnitude or the errors column.'

    nfiles = size(filename)

    call readcat(cat,(/col_ra,col_dec/),col_mag,col_magerr, &
         refra,refdec,mag,dmag,catid,status)
    ! filters as results of readcat (?!)
    if( status /= 0 ) stop 'Failed to read a catalogue.'
    if( .not. allocated(mag) ) stop 'Empty catalogue.'

    ! reference system
    call phselect(phsystable,photsys_ref,phsyscal)
    call phsyspairs(phsyscal,filters,pairs)

    call readframes(filename,keys,phsyscal%filter,photosys_instr,ra,dec,dn,ddn, &
         utol,exptime,areas,filter_frames,aper,annuls,tol,init_area,status)
    if( status /= 0 ) stop 'Failed to read frames.'
    if( init_area ) areas = area

    call jamcat(tol, refra, refdec, mag, dmag, ra,dec,dn,ddn,cts,dcts)

    ncat = size(cts,1)
    if( ncat == 0 ) stop 'Joint stars not found.'
    allocate(refph(ncat,size(mag,2)),drefph(ncat,size(mag,2)))

    if( photsys_ref /= '' ) then
       call phsysmagph(phsyscal,filters,mag(1:ncat,:),dmag(1:ncat,:),refph,drefph)
       if( verbose ) then
          write(*,*) 'Filter pairs:'
          do i = 1,size(pairs,1)
             write(*,*) i,trim(filters(pairs(i,1))),trim(filters(pairs(i,2))),pairs(i,:)
          end do
       end if
    else
       ! undefined photosystem, relative rates
       call relmagph(mag(1:ncat,:),dmag(1:ncat,:),refph,drefph)
    end if


    if( tratable/= '' ) then
       allocate(fp(size(pairs,1),2))
       do i = 1,size(fp,1)
          fp(i,:) = filters(pairs(i,:))
       end do
       call tra_load(tratable,photsys2,photsys1,fpairs,tratab,fp,status)
       deallocate(fp)
       if( photsys2 /= photsys_ref ) stop 'Reference and conversion table system differs.'
    else
       allocate(tratab(size(refph,2),2))
       tratab = 0
       tratab(1,1) = 1
       tratab(:,2) = 1
    end if

    do n = 1,nfiles
       q = exptime(n)*areas(n)
       refph(:,n) = q*refph(:,n)
       drefph(:,n) = q*drefph(:,n)
       ! we're in doubts that our exposure time is the same as 
       ! the catalogue, else sqrt(dmag_our/dmag_cat) must be applied
    end do

    allocate(phres(nfiles),ctph(nfiles),dctph(nfiles))
    do n = 1, nfiles
       call photores_init(phres(n),ncat)
       phres(n)%ra = refra(1:ncat)
       phres(n)%dec = refdec(1:ncat)
    enddo

    if( nfiles == 1 ) then
       call calibr(refph,drefph,cts(1:ncat,:),dcts(1:ncat,:),ctph,dctph,phres,verbose)
    else
       allocate(ctph0(size(ctph)))
       ctph = 1
       call caliber(pairs,filters,tratab,refph,drefph,cts(1:ncat,:),dcts(1:ncat,:),ctph,dctph,phres,verbose)
       ctph0 = ctph
       call caliber(pairs,filters,tratab,refph,drefph,cts(1:ncat,:),dcts(1:ncat,:),ctph,dctph,phres,verbose)
       ctph = ctph0 / ctph
       deallocate(ctph0)
    end if

    if( verbose ) then

       write(*,*) 'Final solutions:'
       do i = 1,size(ctph)
          write(*,'(3a,g15.5,a,1pg8.1)') 'ctph(',trim(filters(i)),') =', &
               ctph(i),'+-',dctph(i)
       end do
    end if

    call writecal(filename,backup,newname,keys,advanced,phsystable,filters, &
         catid,photsys_ref,area,init_area,tratab,ctph,dctph,qlabels,phres)

    do i = 1,size(phres)
       call photores_destroy(phres(i))
    end do
    deallocate(phres)

    if( allocated(pairs) ) deallocate(pairs)
    call deallocate_phsyscal(phsyscal)

    deallocate(refra,refdec,refph,drefph,filter_frames,exptime,areas, &
         tol,tratab,cts,dcts,dn,ddn,ra,dec,mag,dmag,ctph,dctph, &
         aper,annuls)

  end subroutine calibrate

  subroutine tra_load(tratable,photsys2,photsys1,fpairs,tratab,pairs,status)

    use fits_fotran

    character(len=FLEN_VALUE), intent(in) :: tratable
    real(dbl), allocatable, dimension(:,:), intent(out) :: tratab
    character(len=FLEN_VALUE), intent(out) :: photsys2,photsys1
    character(len=FLEN_VALUE), dimension(:,:), allocatable, intent(out) :: fpairs
    character(len=FLEN_VALUE), dimension(:,:), intent(in) :: pairs
    
    character(len=FLEN_VALUE), dimension(:,:), allocatable :: fpairs1
    real(dbl), allocatable, dimension(:,:) :: tratab1
    integer, intent(in out) :: status

    integer :: i,j,idx

    call traload(tratable,photsys2,photsys1,fpairs,tratab,status)
    if( status /= 0 ) stop 'Instrumental to standard conversion table not found.'
    
    if( size(tratab,1) > size(pairs,1) ) then
          
       n = size(pairs,1)
       allocate(fpairs1(n,2),tratab1(n,size(tratab,2)))
       do i = 1,n
          idx = 0
          do j = 1,size(fpairs,1)
             if( all(fpairs(j,:) == pairs(i,:)) ) idx = j
          end do
          if( idx > 0 ) then
             fpairs1(i,:) = fpairs(idx,:)
             tratab1(i,:) = tratab(idx,:)
          end if
       end do
       deallocate(tratab,fpairs)
       allocate(fpairs(n,2),tratab(n,size(tratab1,2)))
       fpairs = fpairs1
       tratab = tratab1
       deallocate(tratab1,fpairs1)

    else if( size(tratab,1) < size(fpairs,1) ) then
       stop "Tratab has insufficient dimension for all frame filters."
    endif
    
  end subroutine tra_load


  subroutine relmagph(mag,dmag,ph,dph)

    use photoconv

    real(dbl), dimension(:,:), intent(in) :: mag,dmag
    real(dbl), dimension(:,:), intent(out) :: ph,dph

    do n = 1, size(mag,2)
       call mag2rate(mag(:,n),dmag(:,n),ph(:,n),dph(:,n))
!       dph(:,n) = sqrt(ph(:,n))
    end do
    

  end subroutine relmagph


  subroutine framecal

    ! framecal suppose calibration in single filter only

    use jamming
    use calibre
    use sfits

    character(len=FLEN_VALUE) :: catid,photosys_frames
    character(len=FLEN_VALUE), dimension(:), allocatable :: filter_ref,filters_fram
    real(dbl), allocatable, dimension(:) :: refra, refdec, ctph,dctph, &
         exptime, areas, tol, aper
    real(dbl), allocatable, dimension(:,:) :: refph,drefph, &
         cts,dcts,ra,dec,annuls
    real(dbl), dimension(1,1), parameter :: tratab = real(1,dbl)
    type(photores), dimension(:), allocatable :: phres
    type(type_phsys) :: phsyscal
    integer :: status,nfiles,ncat,i

    nfiles = size(filename)
    status = 0

    nfilters = 1
    allocate(filters_fram(1),filter_ref(1))

    ! refrence system
    call phselect(phsystable,photsys_ref,phsyscal)
    
    call readref(ref,refra,refdec,refph,drefph,filter_ref(1),status)
    if( status /= 0 ) stop 'Failed to read a reference frame.'

    call readframes(filename,keys,phsyscal%filter,photosys_frames,ra,dec,cts,dcts,&
         utol,exptime,areas,filters_fram,aper,annuls,tol,init_area,status)
    if( status /= 0 ) stop 'Failed to read frames.'
    if( init_area ) areas = area

    call jamref(tol, refra, refdec, refph, drefph, ra,dec,cts,dcts)

    ncat = size(cts,1)

    allocate(phres(nfiles),ctph(nfiles),dctph(nfiles))
    do n = 1, nfiles
       call photores_init(phres(n),ncat)
       phres(n)%ra = refra(1:ncat)
       phres(n)%dec = refdec(1:ncat)
    enddo

    call calibr(refph(1:ncat,:),drefph(1:ncat,:),cts(1:ncat,:),dcts(1:ncat,:),&
         ctph,dctph,phres,verbose)

    call writecal(filename,backup,newname,keys,advanced,phsystable,filters_fram,&
         catid,photsys_ref,area,init_area,tratab,ctph,dctph,qlabels,phres)

    deallocate(refra,refdec,refph,drefph,filter_ref,filters_fram,exptime,areas, &
         tol,cts,dcts,ra,dec,ctph,dctph,aper,annuls)

    do i = 1,size(phres)
       call photores_destroy(phres(i))
    end do

    deallocate(phres)
    call deallocate_phsyscal(phsyscal)

  end subroutine framecal

  ! manual calibration
  subroutine mancal

    use sfits
    use fits_fotran

    character(len=FLEN_VALUE) :: catid,photsys1,photsys2
    character(len=FLEN_VALUE), dimension(:,:), allocatable :: fpairs
    real(dbl), allocatable, dimension(:,:) :: tratab
    integer :: status

    if( tratable/= '' ) then
       call traload(tratable,photsys2,photsys1,fpairs, &
            tratab,status)
       if( status /= 0 ) stop 'Instrumental to standard conversion table not found.'
       if( size(tratab,1) /= size(filters) ) &
            stop "Dimensions of transformation table and filters doesn't corresponds."
       deallocate(fpairs)
    else
       
       allocate(tratab(size(filename),2))
       tratab = 0
       tratab(1,1) = 1
       tratab(:,2) = 1
    end if

    catid = ''

    call writecal(filename,backup,newname,keys,advanced,phsystable,filters, &
         catid,photsys_ref,area,init_area,tratab,ctph_manual,dctph_manual,qlabels)

    deallocate(tratab)
    
  end subroutine mancal


  subroutine readref(ref,refra,refdec,refph,drefph,filter,status)

    character(len=*), intent(in) :: ref
    real(dbl), dimension(:), allocatable, intent(out) :: refra,refdec
    real(dbl), dimension(:,:), allocatable, intent(out) :: refph,drefph
    character(len=*), intent(out) :: filter
    integer, intent(in out) :: status

    character(len=FLEN_COMMENT) :: com
    integer, parameter :: frow = 1, felem = 1
    real(dbl), parameter :: nullval = 0
    logical :: anyf
    integer :: nrows,rcol,dcol,pcol,ecol

    if( status /= 0 ) return

    ! open and move to a table extension
    call ftiopn(15,ref,0,status)

    call ftgkys(15,keys(5),filter,com,status)
    if( status == KEYWORD_NOT_FOUND ) &
         stop 'Filter not found in reference frame. Detected an internal inconsistency.'

    call ftmnhd(15,BINARY_TBL,PHOTOEXTNAME,0,status)
    if( status == BAD_HDU_NUM ) &
         stop 'Failed to find a photometry extension in reference frame.'

    call ftgnrw(15,nrows,status)
    if( status /= 0 ) goto 666

    call ftgcno(15,.false.,FITS_COL_RA,rcol,status)
    call ftgcno(15,.false.,FITS_COL_DEC,dcol,status)
    call ftgcno(15,.false.,FITS_COL_PHOTON,pcol,status)
    call ftgcno(15,.false.,FITS_COL_PHOTONERR,ecol,status)

    allocate(refra(nrows),refdec(nrows),refph(nrows,1),drefph(nrows,1))
    call ftgcvd(15,rcol,frow,felem,nrows,nullval,refra,anyf,status)
    call ftgcvd(15,dcol,frow,felem,nrows,nullval,refdec,anyf,status)
    call ftgcvd(15,pcol,frow,felem,nrows,nullval,refph(:,1),anyf,status)
    call ftgcvd(15,ecol,frow,felem,nrows,nullval,drefph(:,1),anyf,status)

    call ftclos(15,status)
    if( status /= 0 ) goto 666

    return 

666 continue

    call ftclos(15,status)
    call ftrprt('STDERR',status)
    
    if( allocated(refra) ) deallocate(refra,refdec,refph,drefph)


  end subroutine readref

end program phcal
