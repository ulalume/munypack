!
!  transformation of an instrumental to standard photometry system
!
!  Copyright © 2013-4 F.Hroch (hroch@physics.muni.cz)
!
!  This file is part of Munipack.
!
!  Munipack is free software: you can redistribute it and/or modify
!  it under the terms of the GNU General Public License as published by
!  the Free Software Foundation, either version 3 of the License, or
!  (at your option) any later version.
!  
!  Munipack is distributed in the hope that it will be useful,
!  but WITHOUT ANY WARRANTY; without even the implied warranty of
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!  GNU General Public License for more details.
!  
!  You should have received a copy of the GNU General Public License
!  along with Munipack.  If not, see <http://www.gnu.org/licenses/>.


program phfotran

  use fitsio
  use mfits
  use phsysfits

  implicit none

  integer, parameter :: dbl = selected_real_kind(15)
  character(len=FLEN_KEYWORD), dimension(5) :: keys
  character(len=4*FLEN_FILENAME) :: record, key, val, output, backup
  logical :: verbose = .false., plog = .false., list = .false.
  character(len=FLEN_FILENAME), dimension(:), allocatable :: filenames
  character(len=FLEN_VALUE), dimension(:), allocatable :: col_mag, col_magerr, filters
  character(len=FLEN_FILENAME) :: phsystable = 'photosystems.fits', cat = ''
  character(len=FLEN_VALUE) :: col_ra = FITS_COL_RA, col_dec = FITS_COL_DEC, &
       photsys_instr = '', photsys_ref = ''
  real(dbl) :: utol = -1.0/3600.0
  real(dbl), dimension(:), allocatable :: ctph
  logical :: init_area = .false.
  real(dbl) :: area
  integer :: nfile,n,eq,nmag,nmagerr,nfilters,nctph

  output = '!phfotran.fits'
  backup = ''
  area = 1

  keys(1) = FITS_KEY_EXPTIME
  keys(2) = FITS_KEY_AREA
  keys(3) = FITS_KEY_AIRMASS
  keys(4) = FITS_KEY_PHOTSYS
  keys(5) = FITS_KEY_FILTER


  allocate(filenames(0))
  nfile = 0
  allocate(col_mag(0), col_magerr(0), filters(0), ctph(0))
  nfilters = 0
  nmag = 0
  nmagerr = 0
  nctph = 0
  ctph = 0

  do
     read(*,'(a)',end=20) record

     eq = index(record,'=')
     if( eq == 0 ) stop 'Improper control data on input.'

     key = record(:eq-1)
     val = record(eq+1:)

     if( key == 'VERBOSE' ) then

        read(val,*) verbose

     else if( key == 'PIPELOG' ) then

        read(val,*) plog

     else if( key == 'FITS_KEY_EXPTIME' ) then

        read(val,*) keys(1)

     else if( key == 'FITS_KEY_PHOTOSYS' ) then ! remove

        read(val,*) keys(4)

     else if( key == 'FITS_KEY_FILTER' ) then

        read(val,*) keys(5)

     else if( key == 'COL_RA' ) then

        read(val,*) col_ra

     else if( key == 'COL_DEC' ) then

        read(val,*) col_dec

     else if( key == 'COL_NMAG' ) then

        read(val,*) nmag

        deallocate(col_mag)
        allocate(col_mag(nmag))

     else if( key == 'COL_MAG' ) then

        ! a set of columns of filters
        read(val,*) col_mag

     else if( key == 'COL_NMAGERR' ) then

        read(val,*) nmagerr

        deallocate(col_magerr)
        allocate(col_magerr(nmagerr))

     else if( key == 'COL_MAGERR' ) then

        read(val,*) col_magerr

     else if( key == 'NFILTERS' ) then

        read(val,*) nfilters

        deallocate(filters)
        allocate(filters(nfilters))

     else if( key == 'FILTERS' ) then

        ! a set of filters
        read(val,*) filters

     else if( key == 'NCTPH' ) then

        read(val,*) nctph

        deallocate(ctph)
        allocate(ctph(nctph))

     else if( key == 'CTPH' ) then

        ! efficiency (+extinction)
        read(val,*) ctph

     else if( key == 'PHOTSYS_INSTR' ) then

        read(val,*) photsys_instr

     else if( key == 'PHOTSYS_REF' ) then

        read(val,*) photsys_ref

     else if( key == 'PHSYSTABLE' ) then

        read(val,*) phsystable

     else if( key == 'LIST' ) then

        read(val,*) list

     else if( key == 'TOL' ) then

        read(val,*) utol

     else if( key == 'AREA' ) then

        read(val,*) area
        init_area = .true.

     else if( key == 'CAT' ) then
        
        read(val,*) cat

     else if( key == 'OUTPUT' ) then

        read(val,*) output, backup

     else if( key == 'NFILE' ) then

        read(val,*) nfile
        deallocate(filenames)
        allocate(filenames(nfile))
        n = 0

     else if( key == 'FILE' ) then

        n = n + 1
        if( n > size(filenames) ) stop 'Too many files.'

        read(val,*) filenames(n)

     end if

  end do
20 continue

  if( list ) then

     call listphsys(phsystable)

  else

     if( nctph == 0 .and. nfilters > 0 ) then
        nctph = nfilters
        deallocate(ctph)
        allocate(ctph(nctph))
        ctph = 1
     end if

     call fotrans

  end if

  deallocate(filenames,col_mag,col_magerr,filters,ctph)

  stop 0

contains


  ! transformation
  subroutine fotrans

    use fits_fotran
    use fotran
    use jamming
    
    character(len=FLEN_VALUE) :: catid
    character(len=FLEN_KEYWORD) :: photsys_instr
    character(len=FLEN_VALUE), dimension(:), allocatable :: filter
    real(dbl), allocatable, dimension(:) :: ra, dec, airmass
    real(dbl), allocatable, dimension(:,:) :: tratab,tratab_e,ph,dph,cts,dcts,flux,dflux
    integer, dimension(:,:), allocatable :: pairs

    if( size(filters) == 0 ) stop 'Filters undefined. Use -f option.'

    if( size(filters) > 0 .and. size(col_mag) == 0 ) then
       deallocate(col_mag)
       allocate(col_mag(size(filters)))
       col_mag = filters
    end if

    call jamcatx(cat,phsystable,keys,col_ra,col_dec,col_mag,col_magerr,&
       utol, area, init_area, filters, filenames, catid, filter, ra, dec, airmass, &
       ph,dph, flux,dflux,cts,dcts, photsys_ref, photsys_instr, pairs)

    if( size(ra) < 5 ) &
         stop 'Determination of parameters of line requires five or more points.'

!    call tratri(ph,dph,cts,dcts,tratab,verbose)
    call stdtra(pairs,ctph,ph,dph,cts,dcts,tratab,tratab_e,verbose)

    call trawrite(output,backup,filenames,photsys_ref,photsys_instr,filters,filters,&
         ra,dec,airmass,ctph,ph,dph,flux,dflux,cts,dcts,tratab,tratab_e,pairs)

    deallocate(ra,dec,airmass,cts,dcts,ph,dph,flux,dflux,filter,tratab)

  end subroutine fotrans

end program phfotran

