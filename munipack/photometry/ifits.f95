!
!  ifits    I/O FITS interface 
!
!  Copyright © 2010-13 F.Hroch (hroch@physics.muni.cz)
!
!  This file is part of Munipack.
!
!  Munipack is free software: you can redistribute it and/or modify
!  it under the terms of the GNU General Public License as published by
!  the Free Software Foundation, either version 3 of the License, or
!  (at your option) any later version.
!  
!  Munipack is distributed in the hope that it will be useful,
!  but WITHOUT ANY WARRANTY; without even the implied warranty of
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!  GNU General Public License for more details.
!  
!  You should have received a copy of the GNU General Public License
!  along with Munipack.  If not, see <http://www.gnu.org/licenses/>.


module ifits

  use fitsio

  implicit none

contains

subroutine fremove(filename,extnames)

  character(len=*),intent(in) :: filename
  character(len=*),dimension(:),intent(in) :: extnames

  integer, parameter :: extver = 0
  integer :: status, blocksize, hdutype, i

  status = 0
  call ftopen(15,filename,1,blocksize,status)
  
  do i = 1, size(extnames)
     call ftmahd(15,1,hdutype,status)
     do 
        call ftmnhd(15,BINARY_TBL,extnames(i),extver,status)
        if( status == BAD_HDU_NUM ) then
           status = 0
           exit
        end if
        call ftdhdu(15,hdutype,status)
     end do
  end do

  call ftclos(15,status)
  call ftrprt('STDERR',status)  

end subroutine fremove


end module ifits
