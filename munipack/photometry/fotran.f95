!
!  transformation of an instrumental to standard photometry system
!
!  Copyright © 2013 - 14 F.Hroch (hroch@physics.muni.cz)
!
!  This file is part of Munipack.
!
!  Munipack is free software: you can redistribute it and/or modify
!  it under the terms of the GNU General Public License as published by
!  the Free Software Foundation, either version 3 of the License, or
!  (at your option) any later version.
!  
!  Munipack is distributed in the hope that it will be useful,
!  but WITHOUT ANY WARRANTY; without even the implied warranty of
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!  GNU General Public License for more details.
!  
!  You should have received a copy of the GNU General Public License
!  along with Munipack.  If not, see <http://www.gnu.org/licenses/>.
!
!
! Additional work:
!
!  * implement Pending function in analytical derivations, derfcn
!  * solve problem of error estimation
!  * provide calibration informations (star identifications,...)
!  * try Augmented Lagrangian method
!  * try non-triagonal matrix
!  * generalize on non-square transformation matrix
!

module fotran

  implicit none 

  integer, parameter, private :: dbl = selected_real_kind(15)
!  real(dbl), dimension(:,:), pointer, private :: cts,dcts
  real(dbl), dimension(:,:), allocatable, private :: cts,dcts
  real(dbl), dimension(:,:), allocatable, private :: pht,dpht
  real(dbl), dimension(:), allocatable, private :: mad
  real(dbl), dimension(:), allocatable, private :: xdata,ydata
  real(dbl), private :: lambda
  integer, private :: ncts, npht, ndat
  logical, private :: verbose

  private :: res,relerr,robfcn,medtri,derfcn

contains

  subroutine stdtra(pairs,ctph,xpht,xdpht,xcts,xdcts,tratab,tratab_e,verb)

    ! we are following the way of photometry calibration by chapter
    !   Hardie in Astronomical Techniques handbook.

    use NelderMead
    use robustmean
    use minpack
    use minpacks
    use rfun
    use robustline
    
    integer, dimension(:,:), intent(in) :: pairs
    real(dbl), dimension(:), intent(in) :: ctph
    real(dbl), dimension(:,:), target, intent(in) :: xpht,xcts,xdpht,xdcts
    real(dbl), dimension(:,:), allocatable, intent(out) :: tratab,tratab_e
    logical, intent(in) :: verb

    real(dbl),dimension(2) :: p,dp
    real(dbl),dimension(:,:), allocatable :: phc,ctc,dctc,dphc
    real(dbl),dimension(:), allocatable :: r,dr
    real(dbl) :: s0,sig
    integer :: i, k,l
    logical :: down

!    cts => xcts
!    dcts => xdcts
    npht = size(xpht,2)
    ncts = size(xcts,2)
    verbose = verb

    if( size(xpht,1) /= size(xcts,1) ) stop 'Dimensions pht /= cts.'
    if( size(xdpht,1) /= size(xpht,1) ) stop 'Dimensions pht /= dpht.'
    if( size(xdcts,1) /= size(xcts,1) ) stop 'Dimensions cts /= dcts.'
    if( size(ctph) /= size(xpht,2) ) stop 'Dimensions ctph /= pht(2)'

    ndat = size(xpht,1)

    if( .not. (ndat > 0) ) stop 'Data missing.'

    allocate(phc(ndat,npht-1),ctc(ndat,ncts-1),dctc(ndat,ncts-1), &
         dphc(ndat,ncts-1),r(ndat),dr(ndat),pht(ndat,npht),dpht(ndat,npht), &
         cts(ndat,npht),dcts(ndat,npht))

    do k = 1,npht
       if( ctph(k) < epsilon(ctph) ) stop 'Non-positive ratio.'
       pht(:,k) = xpht(:,k) !/ ctph(k)
       dpht(:,k) = xdpht(:,k) !/ ctph(k)
       cts(:,k) = xcts(:,k) * ctph(k)
       dcts(:,k) = xdcts(:,k) * ctph(k)
    end do


    ! color index
    down = .true.
    do i = 1,npht-1
       if( pairs(i+1,1) == pairs(1,1) ) down = .false.
       if( down ) then
          k = pairs(i+1,1)
          l = pairs(i+1,2)
       else
          k = pairs(i+1,1)
          l = pairs(i+1,2)          
       end if
!       write(*,*) k,l,pht(1,k)/pht(1,l)
       call ratio(pht(:,k),pht(:,l),dpht(:,k),dpht(:,l),phc(:,i),dphc(:,i))
       call ratio(cts(:,k),cts(:,l),dcts(:,k),dcts(:,l),ctc(:,i),dctc(:,i))
!       phc(:,i) = ctph(k) * phc(:,i)

!!$       where( abs(pht(:,l)) > epsilon(pht) )
!!$          phc(:,i) = pht(:,k) / pht(:,l)
!!$          dphc(:,i) = pht(:,k)/pht(:,l)*sqrt((dpht(:,k)/pht(:,k))**2+(dpht(:,l)/pht(:,l))**2)
!!$       elsewhere
!!$          phc(:,i) = 0
!!$          dphc(:,i) = huge(dphc)
!!$       end where
!!$       where( abs(cts(:,l)) > epsilon(cts) )
!!$          ctc(:,i) = cts(:,k) / cts(:,l)
!!$          dctc(:,i) = cts(:,k)/cts(:,l)*sqrt((dcts(:,k)/cts(:,k))**2+(dcts(:,l)/cts(:,l))**2)
!!$       elsewhere
!!$          ctc(:,i) = 0
!!$          dctc(:,i) = huge(dctc)
!!$       end where
    end do

!    s0 = sum(pht(:,2)/cts(:,2))/ndat
!    write(*,*) s0

!    do i = 1,ndat
!       write(*,*) phc(i,1),ctc(i,1),pht(i,:),cts(i,:),cts(i,:)/pht(i,:),cts(i,:)/pht(i,:)*s0,cts(i,:)/pht(i,:)*(s0/1.07)
!       write(*,*) phc(i,1),ctc(i,1),pht(i,:),cts(i,:),cts(i,:)/pht(i,:)
!    end do

!    stop
    

    ! reference filter
!    do i = 1,ndat
!       do j = i+1,ndat
!          write(*,*) pht(i,2)/pht(j,2),ctc(i,1)-ctc(j,1)

!       end do
!    end do

    allocate(tratab(npht,2),tratab_e(npht,2))

    ! reference
    k = pairs(1,1)
    l = pairs(1,2)
!    call aline(ctc(:,1),pht(:,k)/cts(:,l),p,dp)
!    r = pht(:,k)/cts(:,l)
!    dr = pht(:,k)/cts(:,l)*sqrt((dpht(:,k)/pht(:,k))**2+(dcts(:,l)/cts(:,l))**2)
    call ratio(pht(:,k),cts(:,l),dpht(:,k),dcts(:,l),r,dr)
!    call rline2(ctc(:,1),pht(:,k)/cts(:,l),dctc(:,1),dr, &
!    r = r*exp(-kext(k)*air(k))
    call rline2(ctc(:,1),r,dctc(:,1),dr,p(1),p(2),dp(1),dp(2),sig)
!    s0 = 0
    if( verbose ) then
       do i = 1,ndat
          write(*,'(5f10.5)') ctc(i,1),r(i),dctc(i,1),dr(i),r(i) - (p(1) + p(2)*ctc(i,1))
!       s0 = s0 + ( pht(i,k)/cts(i,k) - (p(1) + p(2)*ctc(i,1)) )**2
       end do
       write(*,'(a,5f10.5)') 'a-solution:',p,dp,sig
    end if
    tratab(1,:) = p
    tratab_e(1,:) = dp

!    stop

    ! color index
    do k = 1, npht-1
!       call aline(ctc(:,k),phc(:,k),p,dp)
       call rline2(ctc(:,k),phc(:,k),dctc(:,k),dphc(:,k),p(1),p(2),dp(1),dp(2),sig)
       s0 = 0
       if( verbose ) then
          do i = 1,ndat
             write(*,'(5f10.5)') ctc(i,k),phc(i,k),dctc(i,k),dphc(i,k),phc(i,k) - (p(1) + p(2)*ctc(i,k))
!          s0 = s0 + ( phc(i,k) - (p(1) + p(2)*ctc(i,k)) )**2
          end do
          write(*,'(a,5f10.5)') 'c-solution:',p,dp,sig
       end if
       tratab(k+1,:) = p
       tratab_e(k+1,:) = dp
    end do

!stop


   deallocate(phc,ctc,dphc,dctc,r,dr,pht,dpht,cts,dcts)


  end subroutine stdtra



  subroutine fotra(tra,pairs,ctph,cts,dcts,pht,dpht)

    integer, dimension(:,:), intent(in) :: pairs
    real(dbl), dimension(:,:), intent(in) :: tra
    real(dbl), dimension(:), intent(in) :: ctph,cts,dcts
    real(dbl), dimension(:), intent(out) :: pht,dpht

    real(dbl), dimension(:), allocatable :: ctc
    integer:: i,k,l,npht
    logical :: down 

    npht = size(cts)
    allocate(ctc(npht-1))
    do i = 2,npht
       k = pairs(i,1)
       l = pairs(i,2)
       if( cts(l) > epsilon(cts) .and. cts(k) > epsilon(cts) ) then
          ctc(i-1) = (ctph(k)* cts(k)) / (ctph(l) * cts(l))
       else
          ctc(i-1) = 0
       end if
    end do

    k = pairs(1,1)
    l = pairs(1,2)
    if( cts(k) > epsilon(cts) ) then
       pht(k) = ctph(k) * cts(k) * (tra(1,1) + tra(1,2)*ctc(1))
       dpht(k) = (dcts(k)/cts(k))* pht(k)
    else
       pht = -1
       dpht = -1
       goto 666
    end if
    down = .true.
    do i = 2,npht
       k = pairs(i,1)
       l = pairs(i,2)
       if( k == pairs(1,1) ) down = .false.
       if( down ) then
          if( pht(l) > epsilon(pht) .and. cts(l) > epsilon(cts) ) then
             pht(k)  = pht(l) * (tra(i,1) + tra(i,2)*ctc(i-1))
             dpht(k) = (dcts(l) / cts(l)) * pht(k)
          else
             pht(k) = -1
             dpht(k) = -1
          end if
       else
          if( pht(k) > epsilon(pht) .and. cts(k) > epsilon(cts) .and. &
               abs(tra(i,1) + tra(i,2)*ctc(i-1)) > epsilon(ctc) ) then
             pht(l)  = pht(k) / (tra(i,1) + tra(i,2)*ctc(i-1))
             dpht(l) = (dcts(k) / cts(k)) * pht(l)
          else
             pht(l) = -1
             dpht(l) = -1
          end if
       end if
    end do

    where( pht < 0 ) 
       pht = -1
       dpht = -1
    end where

666 continue
    deallocate(ctc)

  end subroutine fotra




  subroutine ratio(x,y,dx,dy,r,dr)

    real(dbl), dimension(:), intent(in) :: x,y,dx,dy
    real(dbl), dimension(:), intent(out) :: r,dr

    where( dx > epsilon(dx) .and. dy > epsilon(dy) )
       r = x / y
       dr = abs(r)*sqrt((dx/x)**2 + (dy/y)**2)
    elsewhere
       r = 0
       dr = huge(dr)
    end where

  end subroutine ratio

  subroutine tratri(xpht,xdpht,xcts,xdcts,tratab,verb)

    use NelderMead
    use robustmean
    use minpack
    use minpacks
    use rfun
    
    integer :: npar
    real(dbl), dimension(:,:), target, intent(in) :: xpht,xcts,xdpht,xdcts
    real(dbl), dimension(:,:), allocatable, intent(out) :: tratab
    logical, intent(in) :: verb

    real(dbl),dimension(:), allocatable :: p,p0,dp
    real(dbl),dimension(:,:), allocatable :: t,dt,r,relp,fjac,cov
    real(dbl) :: s0,reqmin,sum1,sum2,w,med, rms
    integer :: icount, numres, ifault, info, nprint, it, i, j, n
    character(len=80) :: tfmt,rfmt,resfmt,trfmt


    ! BIG WARNING! 
!    pht => xpht
!    cts => xcts
!    dpht => xdpht
!    dcts => xdcts
    npht = size(pht,2)
    ncts = size(cts,2)
    verbose = verb

    if( size(pht,1) /= size(cts,1) ) stop 'Dimensions pht /= cts.'
    if( size(dpht,1) /= size(pht,1) ) stop 'Dimensions pht /= dpht.'
    if( size(dcts,1) /= size(cts,1) ) stop 'Dimensions cts /= dcts.'

    ndat = size(pht,1)

    if( .not. (ndat > 0) ) stop 'Data missing.'

    if( verbose ) then
       write(*,*) "=> Initial data:"
       write(tfmt,'(a,i0,a,i0,a)') '(i5,',npht,'g12.5,',npht,'g12.5)'
       do j = 1,size(pht,1)
          write(*,tfmt) j,pht(j,:),cts(j,:)
       end do
    end if

    write(tfmt,'(a,i0,a)') '(',npht,'f15.6)'
    write(rfmt,'(a,i0,a,i0,a,i0,a)') '(',2*(npht-1),'g12.5,',npht,'f9.5,',npht,'f10.5)'
!    write(*,*) trim(rfmt)

    npar = npht*ncts
    allocate(t(ncts,npht),dt(ncts,npht),r(ndat,ncts),relp(ndat,ncts))

    r = pht / cts

    if( verbose ) then
       write(*,*) "=> Ratios:"
       write(trfmt,'(a,i0,a)') '(i5,',npht,'g12.5)'
       do j = 1,size(pht,1)
          write(*,trfmt) j,r(j,:)
       end do
    end if

    t = 0
    dt = epsilon(dt)
    dt = 0
    do i = 1,ncts
       call rinit(r(:,i),t(i,i),dt(i,i))
    end do

    if( verbose ) then       
       write(*,*) "=> Initial estimation of diagonal:"
       do i = 1,ncts
          write(*,'(i5,1x,g0.5,a,g0.5)') i,t(i,i)," +- ",dt(i,i)
       end do
    end if

!    do i = 1,ncts
!       do j = 1,npht
!          if( i /= j ) t(i,j) = -(dt(i,i)/t(i,i) + dt(j,j)/t(j,j))*(t(i,i) + t(j,j))/4
!       end do
!    end do
    

    ! bootstrap for robust method
    allocate(p(npar),p0(npar),dp(npar))
    p = pack(t,.true.)

    if( verbose ) then
       write(*,*) "=> Estimation minimizing of absolute deviations: ndat=",ndat
       write(*,*) " # status      mean deviation    lambda   evaluations"
    end if

    lambda = sum(abs(p))/npar
    reqmin = epsilon(reqmin)
    s0 = huge(s0)
    do it = 1,1!00

       n = 0
       do i = 1,ncts
          do j = 1,npht
             n = n + 1
             dp(n) = max(dt(i,i),dt(j,j),epsilon(1.0))*(-1.0)**(i+j)
          end do
       end do

       p0 = p
       call nelmin(medtri,npar,p0,p,rms,reqmin,dp,1,1000000,icount,numres,ifault)
       t = reshape(p,(/ncts,npht/))

       if( verbose ) then
          write(*,'(i3,i5,g15.5,g10.3,i10)') it,ifault,rms,lambda,icount
          write(*,tfmt) (t(i,:),i=1,ncts)
       end if

       if( abs(rms - s0) < 0.001 ) exit

       lambda = 10*lambda
       s0 = rms

    enddo

    allocate(mad(ncts))

    call res(t,r)
    r = abs(r)
    do j = 1,size(r,2)
       call rinit(r(:,j),med,w)
       mad(j) = med!/0.674
    end do

    if( verbose ) then
       write(*,*) "=> Estimation of scale:"
       write(*,tfmt) mad
    end if

    if( verbose ) then
       write(*,*) "   Residuals       .... relative precision  "
       write(resfmt,'(a,i0,a,i0,a)') '(i5,',npht,'g13.5,',npht,'f10.5)'
       call relerr(t,relp)
       do i = 1,size(r,1)
          write(*,resfmt) i,r(i,:),relp(i,:)
       end do
    end if

    allocate(fjac(npar,npar),cov(npar,npar))

    if( verbose ) then
       nprint = 1
    else
       nprint = 0
    end if

    lambda = sum(abs(p))
    do j = 1,7
       call lmdif2(robfcn,p,epsilon(p),fjac,nprint,info)
       lambda = 10*lambda
    end do
    call lmder2(derfcn,p,epsilon(p),fjac,nprint,info)

    t = reshape(p,(/ncts,npht/))

    ! residual sum
    call qrinv(fjac,cov)

    call res(t,r)
    do j = 1,ncts

       sum1 = 0
       sum2 = 0
       do i = 1, ndat
          w = r(i,j)/mad(j)
          sum1 = sum1 + huber(w)**2
          sum2 = sum2 + dhuber(w)
       end do
       s0 = mad(j)**2*sum1/sum2**2*ndat**2
       rms = sqrt(s0 / (ndat - npar))

       n = 0
       do i = 1, npht
          n = n + 1
          dt(j,i) = sqrt(s0*cov(n,n)/(ndat - npar))
       end do

    enddo

    if( verbose ) then
       write(*,*) "   Residuals       .... relative precision  "
       write(resfmt,'(a,i0,a,i0,a)') '(i5,',npht,'g13.5,',npht,'f10.5)'
       call relerr(t,relp)
       do i = 1,size(r,1)
          write(*,resfmt) i,r(i,:),relp(i,:)
       end do
    end if

    if( verbose ) then
       write(*,*) "=> Robust estimation:"
       write(*,*) ' info=',info
       write(*,*) " Laplace's multiplicator=",lambda
       write(*,*) " Jacobian in minimum:"
       write(*,*) " RMS=",rms

       write(*,*) " Matrix of Transformations:"
       write(*,tfmt) (t(i,:),i=1,ncts)

       write(*,*) " Errors of Matrix of Transformation:"
       write(*,tfmt) (dt(i,:),i=1,ncts)

       if( size(pht,2) > 1 ) then
          write(*,*) " Band ratios (catalogue, instrumental)  Ratios  Relative precision:"
          call relerr(t,relp)
          do i = 1,ndat
             write(*,rfmt) (pht(i,j-1)/pht(i,j),j=2,size(pht,2)), &
                  (cts(i,j-1)/cts(i,j),j=2,size(pht,2)),pht(i,:)/cts(i,:),relp(i,:)
          end do
          
          do i = 1,ndat
             write(*,*) (pht(i,j-1)/pht(i,j),j=2,size(pht,2)), &
                  (cts(i,j-1)/cts(i,j),j=2,size(pht,2)),&
                  pht(i,:),cts(i,:),cts(i,:)/pht(i,:)
          end do
       end if

    end if

    allocate(tratab(ncts,npht))
    tratab = t

    deallocate(t,dt,p,p0,dp,r,relp,mad,fjac,cov)

  end subroutine tratri

  subroutine res(t,r)
    
    real(dbl), dimension(:,:), intent(in) :: t
    real(dbl), dimension(:,:), intent(out) :: r
    real(dbl) :: x,d
    integer :: i,n

    do n = 1, ndat
       do i = 1,npht
          x = sum(t(i,:)*cts(n,:))
          d = sqrt(dpht(n,i)**2 + sum(t(i,:)**2 * dcts(n,:)**2))
          r(n,i) = (pht(n,i) - x)/d
       end do
    end do
    
  end subroutine res

  subroutine relerr(t,r)
    
    real(dbl), dimension(:,:), intent(in) :: t
    real(dbl), dimension(:,:), intent(out) :: r
    real(dbl) :: x
    integer :: i,n

    do n = 1, ndat
       do i = 1,npht
          x = sum(t(i,:)*cts(n,:))
          r(n,i) = (pht(n,i) - x)/((pht(n,i) + x)/2)
       end do
    end do
    
  end subroutine relerr

  subroutine robfcn(m,np,p,fvec,iflag)
    
    use rfun

    integer, intent(in) :: m,np
    integer, intent(inout) :: iflag
    real(dbl), dimension(np), intent(in) :: p
    real(dbl), dimension(m), intent(out) :: fvec
    real(dbl), dimension(:,:), allocatable :: t,r,fv
    real(dbl) :: rp,ss,x,d
    integer :: npar,n,i,j

!    if( iflag == 0 .and. verbose ) write(*,*) '#robfcn:',real(p)

    npar = ncts*npht
    allocate(t(ncts,npht),fv(ncts,npht),r(ndat,ncts))
    fv = 0.0_dbl
    t = reshape(p,(/ncts,npht/))

    call res(t,r)

    do n = 1, ndat

       do i = 1,npht
          x = sum(t(i,:)*cts(n,:))
          d = sqrt(sum(t(i,:)**2 * dcts(n,:)**2) + dpht(n,i)**2)
          rp = huber(r(n,i)/mad(i))
          fv(i,:) = fv(i,:) - rp*(cts(n,i)/(d*mad(i)))
       end do
    end do

    ss = 0.0_dbl
    do j = 1,ncts
       do i = 1,npht
          if( abs(i-j) > 1 ) then
             fv(i,j) = fv(i,j) + lambda*t(i,j)**2
          end if
       end do
    end do

    fvec = pack(fv,.true.)

    deallocate(t,fv,r)

  end subroutine robfcn


  subroutine derfcn(m,np,p,fvec,fjac,ldfjac,iflag)
    
    use rfun

    integer, intent(in) :: m,np,ldfjac
    integer, intent(inout) :: iflag
    real(dbl), dimension(np), intent(in) :: p
    real(dbl), dimension(m), intent(out) :: fvec
    real(dbl), dimension(ldfjac,np), intent(out) :: fjac
    real(dbl), dimension(:,:), allocatable :: t,r,fv
    real(dbl) :: rp,x,d,u
    integer :: npar,n,i,j

    npar = ncts*npht
    allocate(t(ncts,npht),r(ndat,ncts))
    t = reshape(p,(/ncts,npht/))   
    call res(t,r)

    if( iflag == 1 ) then

       allocate(fv(ncts,npht))
       fv = 0

       do n = 1, ndat

          do i = 1,npht
             x = sum(t(i,:)*cts(n,:))
             d = sqrt(sum(t(i,:)**2 * dcts(n,:)**2) + dpht(n,i)**2)
             rp = huber(r(n,i)/mad(i))
             fv(i,:) = fv(i,:) - rp*(cts(n,i)/(d*mad(i)))
          end do
       end do
       
       fvec = pack(fv,.true.)
       deallocate(fv)

    else if( iflag == 2 ) then

       fjac = 0
       do n = 1, ndat

          do i = 1,ncts
             x = sum(t(i,:)*cts(n,:))
             d = sqrt(sum(t(i,:)**2 * dcts(n,:)**2) + dpht(n,i)**2)
             rp = dhuber(r(n,i)/mad(i))
             u = rp/(d*mad(i))**2
             do j = 1,npht
                fjac(i,j) = fjac(i,j) + u*cts(n,i)*cts(n,j)
             end do
          end do
       end do
       

    end if

    deallocate(t,r)

  end subroutine derfcn


  function medtri(p) result(s)

    real(dbl), dimension(:), intent(in) :: p
    real(dbl), dimension(:,:), allocatable :: t,r
    real(dbl) :: s,ss
    integer :: npar,i,j

    npar = ncts*npht
    allocate(r(ndat,ncts),t(ncts,npht))
    t = reshape(p,(/ncts,npht/))

    call res(t,r)
    s = sum(abs(r))/size(r)

    ss = 0
    do i = 1,ncts
       do j = 1,npht
          if( abs(i-j) > 1 ) then
             ss = ss + abs(t(i,j))
          end if
       end do
    end do

    s = s + lambda *ss

    deallocate(r,t)
       
  end function medtri

end module fotran
