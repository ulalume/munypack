!
!  FITS I/O for photometric calibration
!
!  Copyright © 2012-5 F.Hroch (hroch@physics.muni.cz)
!
!  This file is part of Munipack.
!
!  Munipack is free software: you can redistribute it and/or modify
!  it under the terms of the GNU General Public License as published by
!  the Free Software Foundation, either version 3 of the License, or
!  (at your option) any later version.
!  
!  Munipack is distributed in the hope that it will be useful,
!  but WITHOUT ANY WARRANTY; without even the implied warranty of
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!  GNU General Public License for more details.
!  
!  You should have received a copy of the GNU General Public License
!  along with Munipack.  If not, see <http://www.gnu.org/licenses/>.

module mfits

  use iso_fortran_env
  use fitsio

  implicit none

  integer, parameter, private :: dbl = selected_real_kind(15)
  real(dbl), parameter, private :: rad = 57.295779513082322865_dbl
  logical, private :: verbose = .true.


contains


  subroutine readcat(cat,labels,label_mag,label_magerr, &
       alpha,delta,mag,magerr,catid,status)

    character(len=*), intent(in) :: cat
    character(len=*), dimension(:), intent(in) :: labels,label_mag,label_magerr
    real(dbl), dimension(:), allocatable, intent(out) :: alpha,delta
    real(dbl), dimension(:,:), allocatable, intent(out) :: mag,magerr
    character(len=*), intent(out) :: catid
    integer, intent(out) :: status

    integer, parameter :: frow = 1, felem = 1
    real(dbl), parameter :: nullval = 99.99999

    integer :: nrows, ncols, i
    integer, dimension(size(labels)) :: cols
    integer, dimension(size(label_mag)) :: col_mag
    integer, dimension(size(label_magerr)) :: col_magerr
    character(len=FLEN_CARD) :: buf
    logical :: anyf


    status = 0

    ! open and move to a table extension
    call fttopn(15,cat,0,status)
    call ftgnrw(15,nrows,status)
    if( status /= 0 ) goto 666

    ! define reference frame and identification of catalogue
    call ftgkys(15,'EXTNAME',catid,buf,status)
    if( status == KEYWORD_NOT_FOUND ) then
       catid = ''
       status = 0
    end if

    ! find columns by labels
    do i = 1, size(labels)
       call ftgcno(15,.false.,labels(i),cols(i),status)
    end do

    do i = 1, size(label_mag)
       call ftgcno(15,.false.,label_mag(i),col_mag(i),status)
    end do
    do i = 1, size(label_magerr)
       call ftgcno(15,.false.,label_magerr(i),col_magerr(i),status)
    end do
    if( status /= 0 ) goto 666

    if( nrows > 0 ) then
       ncols = size(label_mag)
       allocate(alpha(nrows),delta(nrows),mag(nrows,ncols),magerr(nrows,ncols))

       magerr = 9.99999

       call ftgcvd(15,cols(1),frow,felem,nrows,nullval,alpha,anyf,status)
       call ftgcvd(15,cols(2),frow,felem,nrows,nullval,delta,anyf,status)

       do i = 1, size(col_mag)
          call ftgcvd(15,col_mag(i),frow,felem,nrows,nullval,mag(:,i),anyf,status)
       end do
       do i = 1, size(col_magerr)
          call ftgcvd(15,col_magerr(i),frow,felem,nrows,nullval/10,magerr(:,i),anyf,status)
       end do
    end if

    call ftclos(15,status)
    if( status /= 0 ) goto 666

    return 

666 continue

    call ftclos(15,status)
    call ftrprt('STDERR',status)

    if( allocated(mag) ) deallocate(mag,magerr,alpha,delta)

  end subroutine readcat


  subroutine readframe(filename,keys,alpha,delta,cts,dcts,ftol,exptime,area, &
       photosys,filter,aper,annuls,init_area,status,airmass)

    use astrotrafo
    use phio

    character(len=*), intent(in) :: filename
    character(len=*), dimension(:), intent(in) :: keys
    real(dbl), dimension(:), allocatable, intent(out) :: alpha,delta,cts,dcts
    real(dbl), intent(out) :: ftol,exptime, area, aper
    real(dbl), dimension(:), intent(out) :: annuls
    character(len=*), intent(out) :: photosys,filter
    logical, intent(in) :: init_area
    integer, intent(out) :: status
    real(dbl), optional, intent(out) :: airmass
    
    integer, parameter :: frow = 1
    integer, parameter :: felem = 1
    integer, parameter :: DIM = 2
    integer, dimension(DIM) :: naxes
    integer :: naxis, nrows, colnum, saper, i, s1, s2, s3, s4, s5
    character(len=FLEN_COMMENT) :: com
    character(len=FLEN_CARD) :: key, col_q, col_qe
    real(dbl), dimension(:), allocatable :: x,y
    real(dbl) :: nullval
    real :: fwhm
    logical :: anyf
    logical :: in_photons
    type(AstroTrafoProj) :: tproj

 
    in_photons = PhotCalibrated(filename)
   
    ! input FITS file
    status = 0
    call ftiopn(20,filename,0,status)
    call ftgidm(20,naxis,status)
    call ftgisz(20,DIM,naxes,status)
    if( status /= 0 ) goto 666

    ! read astrometric calibration
    call wcsget(20,tproj,status)
    if( status == KEYWORD_NOT_FOUND ) then
       write(error_unit,*) 'Astrometry keywords not found in header.'
       goto 666
    end if
    ftol = 5.0*tproj%err

    s1 = 0
    call ftgkyd(20,keys(1),exptime,com,s1)
    if( s1 == KEYWORD_NOT_FOUND ) then
       write(error_unit,*) "Warning: Exposure time identified by FITS keyword `",&
            trim(keys(1)),"' not found. Default is 1s."
       exptime = 1
       s1 = 0
    end if

    s2 = 0
    if( .not. init_area ) then
       call ftgkyd(20,keys(2),area,com,s2)
       if( s2 == KEYWORD_NOT_FOUND ) then
          write(error_unit,*) "Warning: Area identified by FITS keyword `", &
               trim(keys(2)),"' not found. Default is 1m2."
          area = 1
          s2 = 0
       end if
    end if

    s3 = 0
    if( present(airmass) ) then
       call ftgkyd(20,keys(3),airmass,com,s3)
       if( s3 == KEYWORD_NOT_FOUND ) then
          write(error_unit,*) "Warning: Airmass identified by FITS keyword `", &
               trim(keys(3)),"' not found. Default is 0 (extra-atmospheric)."
          airmass = 0
          s3 = 0
       end if
    end if

    s4 = 0
    call ftgkys(20,keys(4),photosys,com,s4)
    if( s4 == KEYWORD_NOT_FOUND ) then
!       write(error_unit,*) "Photometry system by FITS keyword `", &
!            trim(keys(4)),"' not found, leaving it empty."
       photosys = ''
       s4 = 0
    end if

    s5 = 0
    call ftgkys(20,keys(5),filter,com,s5)
    if( s5 == KEYWORD_NOT_FOUND ) then
       write(error_unit,*) "Filter identified by FITS keyword `", &
            trim(keys(5)),"' not found. Default is `' (empty)."
       filter = ''
       s5 = 0
    end if

    status = s1 + s2 + s3 + s4 + s5
    if( status /= 0 ) goto 666

    ! select extension with photometry
    call ftmnhd(20,BINARY_TBL,APEREXTNAME,0,status)
    if( status == BAD_HDU_NUM ) then
       if( verbose ) write(error_unit,*) "Failed to find a photometry extension: ",&
            trim(filename)
       goto 666
    end if

    call ftgkye(20,FITS_KEY_FWHM,fwhm,com,status)
    if( status == 0 ) then
       ftol = fwhm * tproj%scale
    else if( status == KEYWORD_NOT_FOUND ) then
       status = 0
    end if

    ! determine COUNT or PHOTON columns
    if( in_photons ) then
       col_q = FITS_COL_PHOTON
       col_qe = FITS_COL_PHOTONERR
    else
       col_q = FITS_COL_COUNT
       col_qe = FITS_COL_COUNTERR
    end if

    call ftgkyd(20,FITS_KEY_APER,aper,com,status)
    call ftgkyj(20,FITS_KEY_SAPER,saper,com,status)

    ! sky annuls
    annuls = 0.0_dbl
    do i = 1, 2
       call ftkeyn(FITS_KEY_ANNULUS,i,key,status)
       call ftgkyd(20,key,annuls(i),com,status)
    end do
    if( status == KEYWORD_NOT_FOUND ) status = 0
    if( status /= 0 ) goto 666

    aper = aper / tproj%scale
    annuls = annuls / tproj%scale

    call ftgnrw(20,nrows,status)
    if( status /= 0 ) goto 666

    allocate(x(nrows),y(nrows))

    ! table
    call ftgcno(20,.false.,FITS_COL_X,colnum,status)
    call ftgcvd(20,colnum,frow,felem,nrows,nullval,x,anyf,status)
    call ftgcno(20,.false.,FITS_COL_Y,colnum,status)
    call ftgcvd(20,colnum,frow,felem,nrows,nullval,y,anyf,status)

    if( status == 0 ) then
       allocate(alpha(nrows), delta(nrows))
       call invtrafo(tproj,x,y,alpha,delta)
    end if
    deallocate(x,y)

    allocate(cts(nrows),dcts(nrows))
    write(key,'(a,i0)') FITS_COL_APCOUNT,saper
    call ftgcno(20,.false.,key,colnum,status)
    call ftgcvd(20,colnum,frow,felem,nrows,nullval,cts,anyf,status)

    !    call ftkeyn(FITS_COL_APCOUNTERR,saper,key,status)
    ! use of 'ftkeyn' in not valid for keywords over 8 charactes,
    ! but name of a column is limited by significantly more characters
    write(key,'(a,i0)') FITS_COL_APCOUNTERR,saper
    call ftgcno(20,.false.,key,colnum,status)
    call ftgcvd(20,colnum,frow,felem,nrows,nullval,dcts,anyf,status)

666 continue

    call ftclos(20,status)
    call ftrprt('STDERR',status)

  end subroutine readframe

  subroutine checkframe(filename,keys,filter,nrows,status)

    character(len=*), intent(in) :: filename
    character(len=*), dimension(:), intent(in) :: keys
    character(len=*), intent(out) :: filter
    integer, intent(out) :: nrows
    integer, intent(in out) :: status

    character(len=FLEN_COMMENT) :: com
    
    call ftiopn(20,filename,0,status)
    call ftgkys(20,keys(5),filter,com,status)
    if( status == KEYWORD_NOT_FOUND ) then
       write(error_unit,*) "Filter identified by FITS keyword `", &
            trim(keys(5)),"' not found. Default is `' (empty)."
       filter = ''
       status = 0
    end if

    call ftmnhd(20,BINARY_TBL,APEREXTNAME,0,status)
    if( status == BAD_HDU_NUM ) then
       if( verbose ) write(error_unit,*) "Failed to find a photometry extension: ",&
            trim(filename)
       goto 666
    end if

    call ftgnrw(20,nrows,status)

666 continue

    call ftclos(20,status)
    call ftrprt('STDERR',status)

  end subroutine checkframe


  subroutine readframes(filenames,keys,filters_ref,photosys,ra,dec,cts,dcts,utol,&
            exptime,area,filter,aper,annuls,tol,init_area,status)

    character(len=*), dimension(:), intent(in) :: filenames,keys,filters_ref
    character(len=*), intent(out) :: photosys
    real(dbl), dimension(:,:), allocatable, intent(out) :: ra,dec,cts,dcts,annuls
    real(dbl), dimension(:), allocatable, intent(out) :: exptime,area,tol,aper
    character(len=FLEN_VALUE), dimension(:), allocatable, intent(out) :: filter
    real(dbl), intent(in) :: utol
    logical, intent(in) :: init_area
    integer, intent(out) :: status
    
    real(dbl) :: ftol
    integer :: n, nd, nfiles, i, j
    real(dbl), dimension(:), allocatable :: qra,qdec,dn,ddn
    integer, allocatable, dimension(:) :: ndat, order
    character(len=FLEN_VALUE) :: phsys

    nfiles = size(filenames)
    allocate(filter(nfiles),ndat(nfiles),order(nfiles))

    ! before loading of the (big) data, we are discovering their sizes and filters
    status = 0
    do n = 1, nfiles
       call checkframe(filenames(n),keys,filter(n),ndat(n),status)
       if( status /= 0 ) then
          write(error_unit,*) "File: `",trim(filenames(n)),"'"
          stop 'Failed to read a frame.'
       end if
    end do

    ! array allocation dimension
    nd = maxval(ndat)

    ! and using the info to arrange order by filters
    n = 0
    do i = 1,size(filters_ref)
       do j = 1,size(filter)
          if( filters_ref(i) == filter(j) ) then
             n = n + 1
             order(n) = j
          end if
       end do
    end do
    if( n /= nfiles ) &
         stop 'Filters of frames does not unique corresponds to ones of standard set.'

    ! now, we are prepared to read the data
    allocate(exptime(nfiles),area(nfiles),tol(nfiles), &
         aper(nfiles),annuls(nfiles,2),cts(nd,nfiles),dcts(nd,nfiles), &
         ra(nd,nfiles),dec(nd,nfiles))

    cts = -1
    dcts = -1
    ra = 0
    dec = 0

    status = 0
    do n = 1, nfiles

       call readframe(filenames(order(n)),keys,qra,qdec,dn,ddn,ftol,&
            exptime(n),area(n),phsys,filter(n),aper(n),annuls(n,:),init_area,&
            status)

       if( status /= 0 ) then
          write(error_unit,*) "File: `",trim(filenames(n)),"'"
          stop 'Failed to read a frame.'
       end if

       if( utol > epsilon(utol) ) then
          tol(n) = utol
       else
          tol(n) = ftol
       end if

       if( n == 1 ) then
          photosys = phsys
       else
          if( phsys /= photosys ) write(error_unit,*) &
               "Uncompatible photometry systems: `",trim(phsys),"' and `", &
               trim(photosys),"'."
       end if

       nd = size(dn)

       cts(1:nd,n) = dn
       dcts(1:nd,n) = ddn
       ra(1:nd,n) = qra
       dec(1:nd,n) = qdec

       deallocate(qra,qdec,dn,ddn)

    end do
    deallocate(order)

  end subroutine readframes
  
end module mfits

