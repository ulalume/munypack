!
!  aperture photometry
!
!  Copyright © 2010-14 F.Hroch (hroch@physics.muni.cz)
!
!  This file is part of Munipack.
!
!  Munipack is free software: you can redistribute it and/or modify
!  it under the terms of the GNU General Public License as published by
!  the Free Software Foundation, either version 3 of the License, or
!  (at your option) any later version.
!  
!  Munipack is distributed in the hope that it will be useful,
!  but WITHOUT ANY WARRANTY; without even the implied warranty of
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!  GNU General Public License for more details.
!  
!  You should have received a copy of the GNU General Public License
!  along with Munipack.  If not, see <http://www.gnu.org/licenses/>.


program aphot

  use fitsio
  use ifits
  use fitsaphot
  use mdaosky
  use mdaofotometr

  implicit none

  character(len=4*FLEN_FILENAME) :: record,key,val
  character(len=FLEN_FILENAME) :: outname, file,backup, output = ''
  character(len=FLEN_KEYWORD), dimension(1) :: extnames
  real, dimension(:), allocatable :: apertures
  real, dimension(2) :: ring
  logical :: verbose = .false., plog = .false., remove = .false.
  integer :: i,eq,status,naper,saper

  saper = 0

  allocate(apertures(12))
  forall( i = 1:size(apertures) )
     apertures(i) = 2.0*exp(0.1567271184*((i-1)*1.570796327))
     ! logarithmic spiral, pitch 8.9073 deg
  end forall
!  ring = (/20.0, 30.0/)
  ring = -1

  do
     read(*,'(a)',end=20) record

     eq = index(record,'=')
     if( eq == 0 ) stop 'Improper input.'
     key = record(:eq-1)
     val = record(eq+1:)

     if( key == 'VERBOSE' ) then

        read(val,*) verbose

     else if( key == 'PIPELOG' ) then

        read(val,*) plog

     else if( key == 'REMOVE' ) then
        
        read(val,*) remove

     else if( key == 'SAPER' ) then

        read(val,*) saper

     else if( key == 'NAPERTURES' ) then

        read(val,*) naper
        deallocate(apertures)
        allocate(apertures(naper))

     else if( key == 'APERTURES' ) then

        read(val,*) (apertures(i),i=1,size(apertures))

     else if( key == 'RING' ) then

        read(val,*) ring

     else if( key == 'FILE' ) then

        read(val,*) file, backup, output

        status = 0
        call fitsback(file,backup,output,.false.,outname,status)

        if( remove ) then

           if( verbose ) then
              write(*,*)
              write(*,'(a)') "========  Removing photometry: "//trim(outname)
              write(*,*)
           end if

           extnames(1) = APEREXTNAME
!           extnames(2) = PHOTOEXTNAME
           call fremove(outname,extnames)
           
        else

           if( verbose ) then
              write(*,*)
              write(*,'(a)') "========  Processing file: "//trim(outname)
              write(*,*)
           end if
           
           call faphot(outname,saper,apertures,ring,verbose,plog)

        end if

     end if

  end do

20 continue

  deallocate(apertures)

  stop 0

end program aphot
