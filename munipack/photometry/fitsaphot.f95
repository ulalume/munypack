!
!  fitsaphot
!
!  Copyright © 2013-4 F.Hroch (hroch@physics.muni.cz)
!
!  This file is part of Munipack.
!
!  Munipack is free software: you can redistribute it and/or modify
!  it under the terms of the GNU General Public License as published by
!  the Free Software Foundation, either version 3 of the License, or
!  (at your option) any later version.
!  
!  Munipack is distributed in the hope that it will be useful,
!  but WITHOUT ANY WARRANTY; without even the implied warranty of
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!  GNU General Public License for more details.
!  
!  You should have received a copy of the GNU General Public License
!  along with Munipack.  If not, see <http://www.gnu.org/licenses/>.


module fitsaphot

  use fitsio
  use iso_fortran_env

  implicit none

  integer, parameter, private :: rp = selected_real_kind(15)

contains

subroutine faphot(filename,selaper,raper,xring,verbose,plog)

  use mdaofotometr

  integer, parameter :: DIM = 2

  character(len=*),intent(in) :: filename
  integer, intent(in) :: selaper
  real, dimension(:),intent(in) :: raper,xring
  logical, intent(in) :: verbose, plog

  integer :: status,naxis,bitpix,nrows,xcol,ycol,hcol,hdutype,i,j,n,saper
  integer, parameter :: group = 1, extver = 0, frow = 1, felem = 1, nbegin = 4
  real, parameter :: nullval = 0.0
  integer, dimension(DIM) :: naxes
  logical :: anyf
  real, dimension(:,:), allocatable :: data, stddev
  real, dimension(:), allocatable :: xcens,ycens,hstar,sky,sky_err
  real, dimension(:,:), allocatable :: apcts,apcts_err
  real, dimension(2) :: ring
  character(len=FLEN_VALUE), dimension(:), allocatable :: ttype, tform, tunit
  real :: lobad, hibad, fwhm
  character(len=FLEN_COMMENT) :: com
  character(len=FLEN_VALUE) :: key

  status = 0
  call ftiopn(15,filename,1,status)
  call ftgipr(15,DIM,bitpix,naxis,naxes,status)
  if( status /= 0 ) goto 666

  allocate(data(naxes(1),naxes(2)),stddev(naxes(1),naxes(2)))
  call ftg2de(15,group,nullval,size(data,1),naxes(1),naxes(2),data,anyf,status)
  
  call ftmnhd(15,IMAGE_HDU,EXT_STDDEV,extver,status)
  if( status == 0 ) then
     call ftg2de(15,1,nullval,naxes(1),naxes(1),naxes(2),stddev,anyf,status)
  else if ( status == BAD_HDU_NUM ) then
     ! if the information about standard deviation is not available,
     ! we are continuing with epsilons
     stddev = epsilon(stddev)
     status = 0
  end if

  if( status /= 0 ) then
     write(error_unit,*) trim(filename),": Failed to read data."
     goto 666
  end if

  call ftmnhd(15,BINARY_TBL,FINDEXTNAME,extver,status)
  if( status == BAD_HDU_NUM ) then
     write(error_unit,*) trim(filename),": ",trim(FINDEXTNAME)//' extension not found. Has been stars detected?'
     goto 666
  end if

  call ftgnrw(15,nrows,status)

  call ftgkye(15,FITS_KEY_LOWBAD,lobad,com,status)
  call ftgkye(15,FITS_KEY_HIGHBAD,hibad,com,status)
  call ftgkye(15,FITS_KEY_FWHM,fwhm,com,status)
  if( status /= 0 ) then
     write(error_unit,*) trim(filename),": Required keywords ",trim(FITS_KEY_LOWBAD),&
          ",",trim(FITS_KEY_HIGHBAD)," or ",trim(FITS_KEY_FWHM)," not found."
     goto 666
  end if

  ! update parameters
  if( all(xring > 0) ) then
     ring = xring
  else
     ring(1) = 7*fwhm
     ring(2) = int(sqrt(1000 + ring(1)**2)+1)  ! sqrt(N/pi + ring(1)**2)
  end if


  allocate(xcens(nrows),ycens(nrows),hstar(nrows))
  
  call ftgcno(15,.false.,FITS_COL_X,xcol,status)
  call ftgcno(15,.false.,FITS_COL_Y,ycol,status)
  call ftgcno(15,.false.,FITS_COL_PEAKRATIO,hcol,status)
  call ftgcve(15,xcol,frow,felem,size(xcens),nullval,xcens,anyf,status)
  call ftgcve(15,ycol,frow,felem,size(ycens),nullval,ycens,anyf,status)
  call ftgcve(15,hcol,frow,felem,size(hstar),nullval,hstar,anyf,status)
  if( status /= 0 ) goto 666

  allocate(apcts(nrows,size(raper)),apcts_err(nrows,size(raper)))
  allocate(sky(nrows),sky_err(nrows))

  apcts = -1.0
  apcts_err = -1.0

  call daophotsb(data,stddev,xcens,ycens,hstar,raper,ring,lobad,hibad,1.0, &
       verbose,plog,apcts,apcts_err,sky,sky_err,status)
  if( status /= 0 ) goto 666

  ! store results to next extension
  call ftmnhd(15,BINARY_TBL,APEREXTNAME,extver,status)
  if( status == BAD_HDU_NUM ) then
     status = 0
  else
     ! already presented ? remove it !
     call ftdhdu(15,hdutype,status)
     if( status /= 0 ) goto 666
  end if

  n = nbegin+2*size(raper)
  allocate(ttype(n), tform(n), tunit(n))

  tform = '1D'
  tunit = ''
  ttype(1) = FITS_COL_X
  ttype(2) = FITS_COL_Y
  ttype(3) = FITS_COL_SKY
  ttype(4) = FITS_COL_SKYERR

  do i = 1, size(raper)
     j = nbegin - 1 + 2*i
     write(ttype(j),'(a,i0)') trim(FITS_COL_APCOUNT),i
     write(ttype(j+1),'(a,i0)') trim(FITS_COL_APCOUNTERR),i
  end do


  ! aperture photometry table
  call ftibin(15,0,size(ttype),ttype,tform,tunit,APEREXTNAME,0,status)

  call ftukye(15,FITS_KEY_FWHM,fwhm,-5,'[pix] standard FWHM of objects',status)

  ! aperture play
  saper = selaper
  if( saper == 0 ) then
     saper = 1
     do i = 1,size(raper)
        if( raper(i) > fwhm/2 ) then
           saper = i
           exit
        end if
     end do
  end if
  if( .not. (1 <= saper .and. saper <= size(raper)) ) stop 'Bad aperture selected.'

  call ftpkyj(15,FITS_KEY_NAPER,size(raper),'Count of apertures',status)
  call ftpkyj(15,FITS_KEY_SAPER,saper,'Selected aperture (for processing)',status)
  call ftpkye(15,FITS_KEY_APER,raper(saper),-5,'[pix] radius of selected aperture',status)
  do i = 1, size(raper)
     call ftkeyn(FITS_KEY_APER,i,key,status)
     call ftpkye(15,key,raper(i),-5,'[pix] aperture radius',status)
  end do

  call ftpkye(15,trim(FITS_KEY_ANNULUS)//'1',ring(1),-5,'[pix] inner sky annulus radius',status)
  call ftpkye(15,trim(FITS_KEY_ANNULUS)//'2',ring(2),-5,'[pix] outer sky annulus radius',status)

  call ftpcle(15,1,frow,felem,size(xcens),xcens,status)
  call ftpcle(15,2,frow,felem,size(ycens),ycens,status)
  call ftpcle(15,3,frow,felem,size(sky),sky,status)
  call ftpcle(15,4,frow,felem,size(sky_err),sky_err,status)

  do i = 1,size(apcts,2)
     j = nbegin-1+2*i
     call ftpcle(15,j,frow,felem,size(apcts(:,i)),apcts(:,i),status)
     call ftpcle(15,j+1,frow,felem,size(apcts_err(:,i)),apcts_err(:,i),status)
  end do
  deallocate(ttype,tform,tunit)

666 continue

  if( allocated(data) ) deallocate(data,stddev)
  if( allocated(xcens) ) deallocate(xcens,ycens,hstar)
  if( allocated(apcts) ) deallocate(apcts,apcts_err,sky,sky_err)

  call ftclos(15,status)
  call ftrprt('STDERR',status)

end subroutine faphot

end module fitsaphot

