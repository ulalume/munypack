!
!  photometric calibration
!
!  Copyright © 2012 - 2014 F.Hroch (hroch@physics.muni.cz)
!
!  This file is part of Munipack.
!
!  Munipack is free software: you can redistribute it and/or modify
!  it under the terms of the GNU General Public License as published by
!  the Free Software Foundation, either version 3 of the License, or
!  (at your option) any later version.
!  
!  Munipack is distributed in the hope that it will be useful,
!  but WITHOUT ANY WARRANTY; without even the implied warranty of
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!  GNU General Public License for more details.
!  
!  You should have received a copy of the GNU General Public License
!  along with Munipack.  If not, see <http://www.gnu.org/licenses/>.


module etacalibre

  implicit none

  integer, parameter, private :: dbl = selected_real_kind(15)
  real(dbl), dimension(:), allocatable, private :: cts,dcts,pht,dpht
  real(dbl), private :: funratio_sig
  logical, parameter, private :: debug = .false.
  logical, parameter, private :: analytic = .true.

  private :: funratio, fundif, funder, loglikely, residuals, difjac

  
contains

  subroutine rcal(xpht,xdpht,xcts,xdcts,ctph,dctph,verbose)

    !
    ! this subroutine implements robust estimation of mean ratio
    !
    !     t =  pht / cts
    !
    ! including estimation of scater
    !

    use robustmean
    use rfun
    use fmmfmin
    use minpacks
    use neldermead

    real(dbl), dimension(:), target, intent(in) :: xpht,xdpht,xcts,xdcts
    real(dbl), intent(in out) :: ctph,dctph
    logical, intent(in) :: verbose

    integer :: ndat,n,i,info,nprint,icount,numres,ifault,nwins
    real(dbl) :: d,c,r,w,s,log0,rmin,rmax,sig,wlim,wsig,sum2,sum3,reqmin
    real(dbl),dimension(:), allocatable :: res,p,dp
    real(dbl),dimension(2,2) :: jac, hess, cor, dcor
    real(dbl),dimension(2) :: u,u0,du

    if( size(cts) /= size(pht) ) stop 'size(cts) /= size(pht)'

    if( verbose ) then
       nprint = 1
    else
       nprint = 0
    end if
    
    ndat = size(xpht)

    if( ndat < 1 ) then                ! no data, no fun
       ctph = 1
       dctph = 0
       return
    endif

    if( ndat == 1 ) then               ! single point, no fun
       ctph = xpht(1) / xcts(1)
       dctph = 0
       return
    endif

    allocate(pht(ndat),cts(ndat),dpht(ndat),dcts(ndat),res(ndat))
    pht = xpht
    cts = xcts
    dpht = xdpht
    dcts = xdcts


    ! initial estimate of ratio 
    if( verbose ) write(*,*) 'Initial estimate: pht(i),dpht(i),cts(i),dcts(i), ratio'
    rmin = huge(rmin)
    rmax = 0
    do i = 1,ndat
       ! we're allways positive
       if( pht(i) > 0 .and. cts(i) > 0 ) then
          r = pht(i) / cts(i)
          if( verbose ) write(*,'(i3,1p4g15.5,0pf13.5)') i,pht(i),dpht(i),cts(i),dcts(i),r
          if( r < rmin ) rmin = r
          if( r > rmax ) rmax = r
       end if
    end do

    ! initial estimate ratio and scale by minimizing ihuber((pht - ctph*cts)/d)
    ctph = fmin(rmin,rmax,funratio,epsilon(ctph))
    call residuals(ctph,res)
    sig = funratio_sig
    if( verbose ) write(*,*) "Initial ctph,sig:",ctph,sig

    if( verbose .and. .false. ) then
       write(*,'(a,3g15.5)') 'Ratio: initial estimate, min, max:',ctph, rmin, rmax
       write(*,'(a,3g10.5)') 'ctph,sig: ',ctph,sig
       write(*,*) 'Photons, ctph*counts, relative errors, residuals:'
       do i = 1,ndat
          c = ctph*cts(i)
          write(*,'(1p2g20.5,0pf10.5,0pf10.3)') pht(i),c,(pht(i) - c)/c,res(i)
       end do
    end if

    ! identical data on input or just few points available
    if( abs(sig) < epsilon(sig) .or. ndat <= 3 )then

       ! for ndat < 1, see above
       dctph = sig/sqrt(ndat - 1.0)
       goto 666
    endif

    ! winsorisation at x-sigma
    if( verbose ) write(*,'(a,f3.1,a)') 'Winsorisation at ',winscut, &
         " for pht, ctph*cts, dpht, residual in sigma, replacement: "
    nwins = 0
    do i = 1,ndat
       wlim = winscut*sig
       
       if( abs(res(i)) > wlim ) then
          nwins = nwins + 1
          wsig = - sign(wlim,res(i))
          c = max(pht(i) + wsig*sqrt(dpht(i)**2+ctph**2*dcts(i)**2),0.0)
          if( verbose ) &
               write(*,'(i3,1p2g15.5,g12.2,1pg11.3,g15.5)') &
               i,pht(i),ctph*cts(i),dpht(i),res(i),c
          cts(i) = c / ctph
       end if
    end do


    ! locate proper solution
    ! Approximate location of -log(L), the function has one global minimum
    call residuals(ctph,res)
    du(1) = 0.1*ctph
    du(2) = 0.1*sig
    u = (/ctph, sig/)
    u0 = u
    reqmin = epsilon(reqmin)
    call nelmin(loglikely,size(u),u0,u,log0,reqmin,du,1,10000,icount,numres,ifault)
    if( verbose ) write(*,'(a,2i6,3g15.5)') 'Log-likelihood solution: ',ifault,icount,u
    
    if( ifault /= 0 ) then
       if( verbose ) write(*,*) "Finished prematurely without likelihood convergence."
       goto 666
    endif

    s = u(2)
    ctph = u(1)
    dctph = s/sqrt(real(ndat))

    if( verbose ) write(*,'(a,2g10.5,i6)') &
         'ctph, s (winsorising and robust applied): ',ctph,s,nwins

    ! solution of psi( (pht(i) - ctph*cts(i)) / s*sqrt(..)
    if( verbose ) write(*,'(4(6x,a2,7x))') 't','s','f1','f2'
    if( analytic ) then
       call lmder2(funder,u,epsilon(u),jac,nprint,info)
    else
       call lmdif2(fundif,u,epsilon(u),jac,nprint,info)
    end if

    if( verbose ) then
       write(*,*) 'Analytic jacobian: ',analytic
       write(*,*) 'Minpack info:',info
    end if

    if( info /= 5 ) then

       ctph = u(1)
       s = u(2)
       sig = huber_sigcorr*s

       call qrinv(jac,hess)

       dcor = 0
       forall( i = 1:2 )
          dcor(i,i) = 1/sqrt(abs(hess(i,i)))
       end forall
       cor = matmul(dcor,hess)
       cor = matmul(cor,dcor)
       
       if( verbose ) then
          write(*,*) 'jac:',real(jac(1,:))
          write(*,*) 'jac:',real(jac(2,:))
          write(*,*) 'hess:',real(hess(1,:))
          write(*,*) 'hess:',real(hess(2,:))
          write(*,'(a,2f15.5)') 'cor:',cor(1,:)
          write(*,'(a,2f15.5)') 'cor:',cor(2,:)
       end if
          
       ! estimate errors

       if( abs(hess(1,1)) > epsilon(hess) ) then
          allocate(p(ndat),dp(ndat))
          call residuals(ctph,res)
          call hubers(res/s,p)
          call dhubers(res/s,dp)
          sum2 = sum(dp)
          sum3 = sum(p**2)
          dctph = s*sqrt(abs(hess(1,1)))*sqrt(sum3/sum2*ndat/(ndat-1.0))
          deallocate(p,dp)
       else
          dctph = s/sqrt(ndat - 1.0)
       end if

    else

       if( verbose ) write(*,*) "Finished prematurely without convergence in gradient."

    end if


666 continue

    ! residual sum
    if( verbose ) then
       call residuals(ctph,res)
       write(*,*) '# photons, computed counts, rel.err, expected rel.err, huber, dhuber:'
       n = 0
       do i = 1, ndat
          c = ctph*cts(i)
          d = (pht(i) - c)/((pht(i) + c)/2.0)
          w = sqrt((dpht(i)/ctph)**2 + dcts(i)**2)/((cts(i) + pht(i)/ctph)/2.0)
          r = res(i)/s
          if( abs(d) < w ) n = n + 1
          write(*,'(i3,1p2g13.5,0p4f10.5)') i,pht(i),c,d,w,r,dhuber(r)
       enddo
       write(*,'(a,g0.3)') " sig=",sig
       write(*,'(a,g15.5,a,1pg10.2)') 'ctph =',ctph,'+-',dctph
       write(*,'(a,i0,a,i0,a,f0.1,a)') 'Points within 1-sigma error interval: ', &
            n,'/',ndat,' (',100.*real(n)/real(ndat),'%)'
    end if


    deallocate(res,pht,cts,dpht,dcts)

  end subroutine rcal

  function funratio(t)

    use robustmean
    use rfun

    real(dbl), intent(in) :: t
    real(dbl),dimension(:), allocatable :: res
    real(dbl) :: funratio,x

    allocate(res(size(pht)))

    call residuals(t,res)
    call rinit(res,x,funratio_sig)
    funratio = abs(x)

    deallocate(res)

  end function funratio


  subroutine residuals(t,r)
    
    real(dbl), intent(in) :: t
    real(dbl), dimension(:), intent(out) :: r

    r = (pht - t*cts)/sqrt(t**2*dcts**2 + dpht**2)
    
  end subroutine residuals


  subroutine fundif(m,np,p,fvec,iflag)
    
    use rfun

    integer, intent(in) :: m,np
    integer, intent(inout) :: iflag
    real(dbl), dimension(np), intent(in) :: p
    real(dbl), dimension(m), intent(out) :: fvec
    real(dbl), dimension(:), allocatable :: r,f,ds
    real(dbl), dimension(2,2) :: jac
    real(dbl), dimension(2) :: fv
    integer :: n
    real(dbl) :: t,s

    if( iflag == 0 ) then

       write(*,'(4g15.5)') p,fvec

       if( debug ) then
          n = 2
          call funder(2,2,p,fv,jac,2,n)
          write(*,*) ' jac:',jac(1,:)
          write(*,*) ' jac:',jac(2,:)

       end if
       return

    end if

    n = size(pht)
    allocate(r(n),f(n),ds(n))
    t = p(1)
    s = p(2)

    ds = sqrt(t**2*dcts**2 + dpht**2)
    r = (pht - t*cts)/ds
    r = r / s

    call hubers(r,f)
!    f = r

    fv(1) = sum(f*(cts*dpht**2 + pht*t*dcts**2)/ds**3) - s*t*sum(dcts**2/ds**2)
    fv(2) = sum(f*r) - n

    fvec = fv / s

    deallocate(r,f,ds)

  end subroutine fundif


  subroutine funder(m,np,p,fvec,fjac,ldfjac,iflag)
    
    use rfun

    integer, intent(in) :: m,np,ldfjac
    integer, intent(inout) :: iflag
    real(dbl), dimension(np), intent(in) :: p
    real(dbl), dimension(m), intent(out) :: fvec
    real(dbl), dimension(ldfjac,np), intent(out) :: fjac
    real(dbl), dimension(:), allocatable :: r,f,df,ds,ds2,cs,dd,d1s,d2s
    real(dbl), dimension(2,2) :: dfjac
    integer :: n
    real(dbl) :: t,s

    if( iflag == 0 ) then
       write(*,'(4g15.5)') p,fvec
       
       if( debug ) then
          write(*,*) ' jac:',fjac(1,:)
          write(*,*) ' jac:',fjac(2,:)

          call difjac(p(1),p(2),dfjac)
          write(*,*) 'djac:',dfjac(1,:)
          write(*,*) 'djac:',dfjac(2,:)

       end if

       return
    end if

    n = size(pht)
    allocate(r(n),f(n),ds(n),cs(n),ds2(n),df(n),dd(n),d1s(n),d2s(n))
    t = p(1)
    s = p(2)

    ds2 = t**2*dcts**2 + dpht**2
    ds = sqrt(ds2)
    r = (pht - t*cts)/ds
    r = r / s

    call hubers(r,f)
!    f = r

    dd = pht - t*cts
    d1s = t*dcts**2/ds
    d2s = dcts**2*(ds - t*d1s)/ds2
    cs = cts*ds + dd*d1s

    if( iflag == 1 ) then

       fvec(1) = sum(f*cs/s/ds2) - sum(d1s/ds)
       fvec(2) = sum(f*dd/s**2/ds) - n/s

    else if( iflag == 2 ) then

       call dhubers(r,df)
!       df = 1

       fjac(1,1) = sum(df*(cs/s/ds2)**2) - &
            sum(f*(dd*(d2s*ds-2*d1s**2)-2*cts*ds*d1s)/s/ds**3) + &
            sum((d2s*ds - d1s**2)/ds**2)
       fjac(1,2) = sum(df*cs*dd/s**3/ds**3) + sum(f*cs/s**2/ds2)
       fjac(2,2) = sum(df*(dd/s**2/ds)**2) + sum(f*2*dd/s**3/ds) - n/s**2

       fjac(2,1) = fjac(1,2)
       fjac = - fjac

    end if

    deallocate(r,f,ds,cs,ds2,df,dd,d1s,d2s)

  end subroutine funder


  subroutine difjac(t,s,jac)

    real(dbl), intent(in) :: t,s
    real(dbl), dimension(:,:), intent(out) :: jac
    real(dbl), parameter :: d = 1e-4
    real(dbl), dimension(2) :: fv1,fv2
    integer :: iflag

    iflag = 1

    call fundif(2,2,(/t+d,s/),fv1,iflag)
    call fundif(2,2,(/t-d,s/),fv2,iflag)
    jac(1,:) = (fv1 - fv2)/(2*d)

    call fundif(2,2,(/t,s+d/),fv1,iflag)
    call fundif(2,2,(/t,s-d/),fv2,iflag)
    jac(2,:) = (fv1 - fv2)/(2*d)

  end subroutine difjac



  function loglikely(p)

    use rfun

    real(dbl), dimension(:), intent(in) :: p
    real(dbl) :: loglikely
    real(dbl), dimension(:), allocatable :: r,f,ds
    integer :: n
    real(dbl) :: t,s

    n = size(pht)
    allocate(r(n),f(n),ds(n))

    t = p(1)
    s = p(2)

    ds = s * sqrt(t**2*dcts**2 + dpht**2)
    r = (pht - t*cts)/ds

    call ihubers(r,f)
!    f = r**2/2
    loglikely = sum(f) + sum(log(ds))
    
    deallocate(r,f,ds)

  end function loglikely


  ! an interface for plotting of functions in minimum (not used for calibration)
  subroutine graph(xpht,xdpht,xcts,xdcts,ctph,sig,type,fvec)

    real(dbl), dimension(:), target, intent(in) :: xpht,xdpht,xcts,xdcts
    real(dbl), intent(in) :: ctph,sig
    character(len=*), intent(in) :: type
    real(dbl), dimension(:) :: fvec
    integer :: iflag, ndat

    ndat = size(xpht)
    allocate(pht(ndat),cts(ndat),dpht(ndat),dcts(ndat))
    pht = xpht
    cts = xcts
    dpht = xdpht
    dcts = xdcts

    if( type == "grad" ) then
       iflag = 1
       call fundif(2,2,(/ctph,sig/),fvec,iflag)
    else if( type == "like" ) then
       fvec(1) = loglikely((/ctph,sig/))
    else
       fvec = 0
    end if
    
    deallocate(pht,cts,dpht,dcts)
    
  end subroutine graph


end module etacalibre



module calibre
  
  implicit none

  integer, parameter, private :: dbl = selected_real_kind(15)

  type photores
    
     integer :: ndat
     real(dbl) :: ctph,dctph
     real(dbl), dimension(:), allocatable :: ra,dec,pht,cts,res

  end type photores


contains

  subroutine photores_init(phres,ndat)

    type(photores), intent(out) :: phres
    integer, intent(in) :: ndat
    
    allocate(phres%ra(ndat),phres%dec(ndat),phres%pht(ndat),phres%cts(ndat), &
         phres%res(ndat))
    phres%ndat = ndat
    phres%ctph = 0
    phres%dctph = 0

  end subroutine photores_init

  subroutine photores_destroy(phres)

    type(photores), intent(in out) :: phres
    
    deallocate(phres%ra,phres%dec,phres%pht,phres%cts,phres%res)
    phres%ndat = 0

  end subroutine photores_destroy


subroutine caliber(pairs,filters,tratab,pht,dpht,cts,dcts,ctph,dctph,phres,verbose)

  use etacalibre
  use fotran

  integer, dimension(:,:), intent(in) :: pairs
  character(len=*), dimension(:), intent(in) :: filters
  real(dbl), dimension(:,:), intent(in) :: tratab,pht,dpht,cts,dcts
  real(dbl), dimension(:), intent(in out) :: ctph,dctph
  type(photores), dimension(:), intent(in out) :: phres
  logical, intent(in) :: verbose

  real(dbl),dimension(:,:),allocatable :: ct, dct
  real(dbl),dimension(:,:),allocatable :: relerr
  integer :: i,n,ndat,npht,ncts

  ndat = size(pht,1)
  npht = size(pht,2)
  ncts = size(cts,2)
  allocate(ct(ndat,npht),dct(ndat,npht),relerr(ndat,npht))

  do i = 1, ndat
     call fotra(tratab,pairs,ctph,cts(i,:),dcts(i,:),ct(i,:),dct(i,:))
  end do

  do n = 1, npht
     if( verbose ) write(*,*) 'Filter: ',trim(filters(n))
     call rcal(pht(:,n),dpht(:,n),ct(:,n),dct(:,n),ctph(n),dctph(n),verbose)
     relerr(:,n) = (pht(:,n) - ctph(n)*ct(:,n))/pht(:,n)
  end do

  phres(:)%ctph = ctph
  phres(:)%dctph = dctph

  do i = 1,npht
     phres(i)%pht = pht(:,i)
     phres(i)%cts = ct(:,i)
     phres(i)%res = relerr(:,i)
  end do

  deallocate(ct,dct)

end subroutine caliber


subroutine calibr(pht,dpht,cts,dcts,ctph,dctph,phres,verbose)

  use etacalibre
  
  real(dbl), dimension(:,:), intent(in) :: pht,dpht,cts,dcts
  real(dbl), dimension(:), intent(out) :: ctph,dctph
  type(photores), dimension(:), intent(in out) :: phres
  logical, intent(in) :: verbose

  real(dbl),dimension(:),allocatable :: ct, dct,ph,dph
  real(dbl),dimension(:,:),allocatable :: relerr
  integer :: i,n,ndat,ncol

  if( size(pht,1) /= size(cts,1) ) stop 'Non-conforming data.'

  ndat = size(pht,1)
  ncol = size(pht,2)
  allocate(ct(ndat),dct(ndat),ph(ndat),dph(ndat),relerr(ndat,ncol))

  do n = 1, size(cts,2)

     ct(:) = cts(:,n)
     dct(:) = dcts(:,n)

     ph(:) = pht(:,n)
     dph(:) = dpht(:,n)

     where( dph < epsilon(dph) )
        dph = sqrt(ph)
     end where
     
     where( dct < epsilon(dct) )
        dct = sqrt(ct)
     end where
     
     call rcal(ph,dph,ct,dct,ctph(n),dctph(n),verbose)

     relerr(:,n) = (ph - ctph(n)*ct)/((ph + ctph(n)*ct)/2)

  end do

  phres(:)%ctph = ctph
  phres(:)%dctph = dctph

  do i = 1, size(ctph)
     phres(i)%pht = pht(:,i)
     phres(i)%cts = cts(:,i)
     phres(i)%res = relerr(:,i)
  end do

  deallocate(ct,dct,ph,dph)

end subroutine calibr


end module calibre

