!
!  fitspphot
!
!  Copyright © 2013 F.Hroch (hroch@physics.muni.cz)
!
!  This file is part of Munipack.
!
!  Munipack is free software: you can redistribute it and/or modify
!  it under the terms of the GNU General Public License as published by
!  the Free Software Foundation, either version 3 of the License, or
!  (at your option) any later version.
!  
!  Munipack is distributed in the hope that it will be useful,
!  but WITHOUT ANY WARRANTY; without even the implied warranty of
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!  GNU General Public License for more details.
!  
!  You should have received a copy of the GNU General Public License
!  along with Munipack.  If not, see <http://www.gnu.org/licenses/>.


module fitspphot

  use fitsio
  use iso_fortran_env

  implicit none

  integer, parameter, private :: rp = selected_real_kind(15)

contains

subroutine fpphot(filename,verbose,plog,fkeys)

  use psfengine

  integer, parameter :: DIM = 2

  character(len=*),intent(in) :: filename
  logical, intent(in) :: verbose, plog
  character(len=*),dimension(:),intent(in) :: fkeys

  integer :: status,blocksize,naxis,bitpix,nrows,xcol,ycol,scol,ccol,hdutype,i,j,n
  integer, parameter :: group = 1, extver = 0, frow = 1, felem = 1, nbegin = 4
  real(rp), parameter :: nullval = 0.0_rp
  integer, dimension(DIM) :: naxes
  logical :: anyf
  real(rp), dimension(:,:), allocatable :: data, rframe
  real(rp), dimension(:), allocatable :: xcens,ycens,sky,sky_err,asky,acts,cts,cts_err,q
  logical, dimension(:), allocatable :: id
  character(len=FLEN_VALUE), dimension(:), allocatable :: ttype, tform, tunit
  real(rp) :: lobad, hibad, phpadu, fwhm, ring
  character(len=FLEN_COMMENT) :: com
  character(len=FLEN_VALUE) :: key

  status = 0
  call ftiopn(15,filename,1,status)
  call ftgipr(15,DIM,bitpix,naxis,naxes,status)
  if( status /= 0 ) goto 666

  call ftgkyd(15,fkeys(1),phpadu,com,status)
  if( status == KEYWORD_NOT_FOUND ) then
     write(error_unit,*) trim(filename),": Gain identified by `",trim(fkeys(1)),"' keyword not found."     
     goto 666
  end if

  allocate(data(naxes(1),naxes(2)))
  call ftg2dd(15,group,nullval,size(data,1),naxes(1),naxes(2),data,anyf,status)

  if( status /= 0 ) then
     write(error_unit,*) trim(filename),": Failed to read data."
     goto 666
  end if

  call ftmnhd(15,BINARY_TBL,FINDEXTNAME,extver,status)
  if( status == BAD_HDU_NUM ) then
     write(error_unit,*) trim(filename),": ",trim(FINDEXTNAME)//' extension not found. Has been stars detected?'
     goto 666
  end if

  call ftgkyd(15,FITS_KEY_LOWBAD,lobad,com,status)
  call ftgkyd(15,FITS_KEY_HIGHBAD,hibad,com,status)
  call ftgkyd(15,FITS_KEY_FWHM,fwhm,com,status)
  if( status /= 0 ) then
     write(error_unit,*) trim(filename),": Required keywords ",trim(FITS_KEY_LOWBAD),&
          ",",trim(FITS_KEY_HIGHBAD)," or ",trim(FITS_KEY_FWHM)," not found."
     goto 666
  end if

  call ftmnhd(15,BINARY_TBL,APEREXTNAME,extver,status)
  if( status == BAD_HDU_NUM ) then
     write(error_unit,*) trim(filename),": ",trim(APEREXTNAME)//' extension not found. An aperture photometry is missing.'
     goto 666
  end if

  call ftgnrw(15,nrows,status)

  call ftgkyd(15,trim(FITS_KEY_ANNULUS)//'1',ring,com,status)
  if( status /= 0 ) then
     write(error_unit,*) trim(filename),": Required keywords ",trim(FITS_KEY_ANNULUS),&
          " not found."
     goto 666
  end if
  ring = 10*fwhm

  allocate(xcens(nrows),ycens(nrows),asky(nrows),acts(nrows))
  
  call ftgcno(15,.false.,FITS_COL_X,xcol,status)
  call ftgcno(15,.false.,FITS_COL_Y,ycol,status)
  call ftgcno(15,.false.,FITS_COL_SKY,scol,status)
  call ftgcno(15,.false.,trim(FITS_COL_APCOUNT)//'1',ccol,status)

  call ftgcvd(15,xcol,frow,felem,size(xcens),nullval,xcens,anyf,status)
  call ftgcvd(15,ycol,frow,felem,size(ycens),nullval,ycens,anyf,status)
  call ftgcvd(15,scol,frow,felem,size(asky),nullval,asky,anyf,status)
  call ftgcvd(15,ccol,frow,felem,size(acts),nullval,acts,anyf,status)
  if( status /= 0 ) goto 666

  allocate(cts(nrows),cts_err(nrows),sky(nrows),sky_err(nrows), &
       rframe(size(data,1),size(data,2)))

  call psfmodel(xcens,ycens,asky,acts,data,ring,lobad,hibad,phpadu, &
       verbose,plog,cts,cts_err,sky,sky_err,rframe,status)

  if( status /= 0 ) goto 666

  ! store results to next extension
  call ftmnhd(15,BINARY_TBL,PSFEXTNAME,extver,status)
  if( status == BAD_HDU_NUM ) then
     status = 0
  else
     ! already presented ? remove it !
     call ftdhdu(15,hdutype,status)
     if( status /= 0 ) goto 666
  end if

  n = 6
  allocate(ttype(n), tform(n), tunit(n))

  tform = '1D'
  tunit = ''
  ttype(1) = FITS_COL_X
  ttype(2) = FITS_COL_Y
  ttype(3) = FITS_COL_SKY
  ttype(4) = FITS_COL_SKYERR
  ttype(5) = FITS_COL_PSFCOUNT
  ttype(6) = FITS_COL_PSFCOUNTERR

  ! PSF photometry
  call ftibin(15,0,size(ttype),ttype,tform,tunit,PSFEXTNAME,0,status)

  call ftukyd(15,FITS_KEY_FWHM,fwhm,-5,'[pix] standard FWHM of objects',status)

  call ftpcld(15,1,frow,felem,size(xcens),xcens,status)
  call ftpcld(15,2,frow,felem,size(ycens),ycens,status)
  call ftpcld(15,3,frow,felem,size(sky),sky,status)
  call ftpcld(15,4,frow,felem,size(sky_err),sky_err,status)
  call ftpcld(15,5,frow,felem,size(cts),cts,status)
  call ftpcld(15,6,frow,felem,size(cts_err),cts_err,status)
  deallocate(ttype,tform,tunit)


  ! save residuals
  call ftinit(115,'!r.fits',blocksize,status)
  call ftiimg(115,-32,naxis,naxes,status)
  call ftp2dd(115,1,naxes(1),naxes(1),naxes(2),rframe,status)
  if( status == NUMERICAL_OVERFLOW ) status = 0
  call ftclos(115,status)


  ! photometry table
  call ftmnhd(15,BINARY_TBL,PHOTOEXTNAME,extver,status)
  if( status == BAD_HDU_NUM ) then
     status = 0
  else
     ! already presented ? remove it !
     call ftdhdu(15,hdutype,status)
     if( status /= 0 ) goto 666
  end if

  allocate(ttype(6), tform(6), tunit(6))

  tform = '1D'
  tunit = ''
  ttype(1) = FITS_COL_X
  ttype(2) = FITS_COL_Y
  ttype(3) = FITS_COL_SKY
  ttype(4) = FITS_COL_SKYERR
  ttype(5) = FITS_COL_COUNT
  ttype(6) = FITS_COL_COUNTERR
  call ftibin(15,0,size(ttype),ttype,tform,tunit,PHOTOEXTNAME,0,status)  
  call ftukys(15,FITS_KEY_ORIGHDU,APEREXTNAME,'copied from',status)

  call ftukyd(15,FITS_KEY_FWHM,fwhm,-5,'[pix] standard FWHM of objects',status)

  ! select valid records only
  allocate(id(size(xcens)))
  id = .false.
  n = 0
  do i = 1, size(xcens)
     if( cts(i) >= 0 ) then
        n = n + 1
        id(i) = .true.
     end if
  end do

  allocate(q(n))

  q = pack(xcens,id)
  call ftpcld(15,1,frow,felem,n,q,status)

  q = pack(ycens,id)
  call ftpcld(15,2,frow,felem,n,q,status)

  q = pack(sky,id)
  call ftpcld(15,3,frow,felem,n,q,status)

  q = pack(sky_err,id)
  call ftpcld(15,4,frow,felem,n,q,status)

  q = pack(cts,id)
  call ftpcld(15,5,frow,felem,n,q,status)

  q = pack(cts_err,id)
  call ftpcld(15,6,frow,felem,n,q,status)

  deallocate(id,q)

  deallocate(ttype,tform,tunit)

666 continue

  if( allocated(data) ) deallocate(data)
  if( allocated(xcens) ) deallocate(xcens,ycens,asky,acts)
  if( allocated(cts) ) deallocate(cts,cts_err,sky,sky_err)

  call ftclos(15,status)
  call ftrprt('STDERR',status)

end subroutine fpphot

end module fitspphot

