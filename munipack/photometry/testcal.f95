

! gfortran -Wall --check=all  -I../lib fotran.f95 fmin.f95 calibre.f95 testcal.f95 -L. -L../lib -L../minpack -lrstat -lsort -llmin -lminpacks -lminpack -lm


program testcal

  use etacalibre
  implicit none

  integer, parameter :: dbl = selected_real_kind(15)
  integer, parameter :: nmax = 1000
  real(dbl), dimension(nmax) :: ph,dph,ct,dct
  real(dbl) :: r,dr
  integer :: i

  do i = 1,nmax
     ph(i) = pnoise(200.0_dbl)
     dph(i) = sqrt(ph(i))
     ct(i) = pnoise(100.0_dbl)
     dct(i) = sqrt(ct(i))
  end do

  call rcal(ph,dph,ct,dct,r,dr,.true.)


  do i = 1,nmax
     ph(i) = gdis(20000.0_dbl,141.42_dbl)
     dph(i) = sqrt(ph(i))
     ct(i) = gdis(10000.0_dbl,100.0_dbl)
     dct(i) = sqrt(ct(i))
  end do
  call rcal(ph,dph,ct,dct,r,dr,.true.)


  do i = 1,nmax
     r = 10000 + 100*i
     ph(i) = gdis(2*r,sqrt(2*r))
     dph(i) = sqrt(ph(i))
     ct(i) = gdis(r,sqrt(r))
     dct(i) = sqrt(ct(i))
  end do
  call rcal(ph,dph,ct,dct,r,dr,.true.)


contains

  function pnoise(lam) result(k)

    ! Knuth's algorithm by http://en.wikipedia.org/wiki/Poisson_distribution

    real(dbl), intent(in) :: lam
    real(dbl) :: L,p,u
    integer :: k

    L = exp(-lam)
    k = 0
    p = 1
    do
       k = k + 1
       call random_number(u)
       p = p*u
       if( .not. (p > L) ) exit
    enddo

  end function pnoise

  function gdis(mean, sig)

    ! generate random data with gauss distribution

    real(dbl), intent(in) :: mean, sig
    real(dbl) :: gdis, x

    call random_number(x)
    gdis = mean - 1.618*sqrt(2.0)*sig*ierfc(2*x)   ! Numerical Recipes (6.14.3)

  end function gdis

  function ierfc(x)

    real(dbl), intent(in) :: x
    real(dbl) :: ierfc
    
    ierfc = ierf(x-1)

  end function ierfc

  function ierf(x)

    ! http://en.wikipedia.org/wiki/Error_function
    
    real(dbl), intent(in) :: x
    real(dbl) :: ierf, u, v, a, y, w
    
    a = 0.140012288686666
    u = log(1 - x**2)
    v = 2/3.14159265358979/a
    y = v + u/2
    
    w = sqrt(y**2 - u/a)
    ierf = sign(1.0d0,x)*sqrt(w - v)
    
  end function ierf


end program testcal

