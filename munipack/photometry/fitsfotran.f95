!
!  FITS I/O for photometric calibration
!
!  Copyright © 2014 F.Hroch (hroch@physics.muni.cz)
!
!  This file is part of Munipack.
!
!  Munipack is free software: you can redistribute it and/or modify
!  it under the terms of the GNU General Public License as published by
!  the Free Software Foundation, either version 3 of the License, or
!  (at your option) any later version.
!  
!  Munipack is distributed in the hope that it will be useful,
!  but WITHOUT ANY WARRANTY; without even the implied warranty of
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!  GNU General Public License for more details.
!  
!  You should have received a copy of the GNU General Public License
!  along with Munipack.  If not, see <http://www.gnu.org/licenses/>.

module fits_fotran

  use fitsio

  implicit none

  integer, parameter, private :: dbl = selected_real_kind(15)


contains

  subroutine traload(nametable,photosys_ref,photosys_instr, &
       filters,tratab,status)

    character(len=*), intent(in) :: nametable
    character(len=*), intent(out) :: photosys_ref,photosys_instr
    character(len=FLEN_VALUE), dimension(:,:), allocatable, intent(out) :: &
         filters
    real(dbl), dimension(:,:), allocatable, intent(out) :: tratab
    integer, intent(out) :: status

    character(len=FLEN_KEYWORD) :: key
    character(len=FLEN_VALUE) :: extname
    character(len=FLEN_CARD) :: buf
    integer :: ncols,nrows,i
    integer, parameter :: frow = 1, felem = 1
    real(dbl), parameter :: nullval = 0.0_dbl
    logical :: anyf

    status = 0

    ! open and move to first table extension
    call fttopn(15,nametable,0,status)
    call ftgnrw(15,nrows,status)
    call ftgncl(15,ncols,status)

    if( status /= 0 ) goto 666

    ! check reference frame and identification of this table
    call ftgkys(15,'EXTNAME',extname,buf,status)
    if( extname /= FTHDUNAME ) stop "An extension name identificator doesn't match."

    call ftkeyn(FITS_KEY_PHOTSYS,1,key,status)
    call ftgkys(15,key,photosys_instr,buf,status)
    call ftkeyn(FITS_KEY_PHOTSYS,2,key,status)
    call ftgkys(15,key,photosys_ref,buf,status)

    ! filters
    allocate(filters(nrows,2))
    do i = 1,2
       call ftgcvs(15,i,frow,felem,nrows,'',filters(:,i),anyf,status)
    end do

    allocate(tratab(nrows,ncols-2))
    do i = 1, size(tratab,2)
       call ftgcvd(15,i+2,frow,felem,nrows,nullval,tratab(:,i),anyf,status)
    end do

666 continue

    call ftrprt('STDERR',status)
    call ftclos(15,status)

    if( status /= 0 ) then
       if( allocated(tratab) ) deallocate(tratab)
       if( allocated(filters) ) deallocate(filters)
    end if

  end subroutine traload


  subroutine trawrite(output,backup,filenames,photosys_ref,photosys_instr,filters_ref, &
    filters_instr,ra,dec,airmass,ctph,ph,dph,flux,dflux,cts,dcts,tratab,tratab_e,pairs)

    character(len=*), intent(in) :: output, backup,photosys_ref,photosys_instr
    character(len=*), dimension(:), intent(in) :: filenames,filters_ref, filters_instr
    real(dbl), dimension(:), intent(in) :: ra,dec,airmass,ctph
    real(dbl), dimension(:,:), intent(in) :: tratab,tratab_e,ph,dph,flux,dflux,cts,dcts
    integer, dimension(:,:), intent(in) :: pairs

    integer, parameter :: frow = 1, felem = 1
    character(len=FLEN_VALUE), dimension(:), allocatable :: ttype, tform,tunit, filters
    character(len=FLEN_KEYWORD) :: key
    character(len=FLEN_CARD) :: buf
    real(dbl) :: x
    integer :: n,m,nrows,ncols, nbands,status, blocksize

    status = 0
    blocksize = 0
    call fitsbackup(output,backup,status)
    call ftinit(15,output,blocksize,status)
    call ftiimg(15,8,0,(/0/),status)
    call ftpcom(15,'Photometry system conversion table.',status)
    call ftpkys(15,FITS_KEY_CREATOR,FITS_VALUE_CREATOR,FITS_COM_CREATOR,status)
    call ftpcom(15,MUNIPACK_VERSION,status)
    call ftpcom(15,'Description: http://munipack.physics.muni.cz/dataform_phfotran.html',status)
    
    ! calibration extension
    nrows = size(tratab,1)
    ncols = size(tratab,2) + size(tratab_e,2) + 2
    allocate(ttype(ncols),tform(ncols),tunit(ncols))
    tform(1:2) = 'A'
    tform(3:) = '1D'
    tunit = '' !'an instrumental filter'
    ttype(1:2) = (/'REF  ','INSTR'/)
    m = 2 + size(tratab,2)
    do n = 1, size(tratab,2)
       write(ttype(n+2),'(a,i0)') 'ORDER_',n-1
       write(ttype(n+m),'(a,i0)') 'e_ORDER_',n-1
    end do

    call ftibin(15,nrows,ncols,ttype,tform,tunit,FTHDUNAME,0,status)

    call ftkeyn(FITS_KEY_PHOTSYS,1,key,status)
    call ftpkys(15,key,photosys_instr,'instrumental photometry system',status)

    call ftkeyn(FITS_KEY_PHOTSYS,2,key,status)
    call ftpkys(15,key,photosys_ref,'reference photometry system (standard)',status)

    x = sum(airmass)/size(airmass)
    call ftpkyd(15,FITS_KEY_AIRMASS,x,-6,'average air-mass',status)

    call ftpcom(15,'Photometry transformation has been derived by processing: ',status)
    call ftpcom(15,' filename    filter    airmass    ctph ',status)
    do n = 1,size(filenames)
       write(buf,'(f8.5,1x,1pg12.5)') airmass(n),ctph(n)
       call ftpcom(15,"'"//trim(filenames(n))//"' '"//trim(filters_instr(n)) &
            //"' "//trim(buf),status)
    end do

    allocate(filters(nrows))
    do n = 1,nrows
       filters(n) = filters_ref(pairs(n,1))
    end do
    call ftpcls(15,1,frow,felem,nrows,filters,status)
    do n = 1,nrows
       filters(n) = filters_ref(pairs(n,2))
    end do
    call ftpcls(15,2,frow,felem,nrows,filters,status)
    deallocate(filters)

    do n = 1,size(tratab,2)
       call ftpcld(15,n+2,frow,felem,nrows,tratab(:,n),status)
       call ftpcld(15,n+m,frow,felem,nrows,tratab_e(:,n),status)
    end do
    
    deallocate(ttype,tform,tunit)

    ! data info extension
    nrows = size(ra)
    nbands = size(ph,2)
    ncols = (2*3)*nbands + 2
    allocate(ttype(ncols),tform(ncols),tunit(ncols))
    tform = '1D'
    tunit(1:2) = 'deg'
    tunit(3:2+2*nbands) = 'ph/s/m2'
    tunit(3+2*nbands:2+4*nbands) = 'W/m2'
    tunit(3+4*nbands:) = 'cts/s/m2'
    ttype(1:2) = (/'RA ','DEC'/)
    ttype(3:2+1*nbands) = 'PH_'//filters_ref
    ttype(3+1*nbands:2+2*nbands) = 'ePH_'//filters_ref
    ttype(3+2*nbands:2+3*nbands) = 'FLUX_'//filters_ref
    ttype(3+3*nbands:2+4*nbands) = 'eFLUX_'//filters_ref
    ttype(3+4*nbands:2+5*nbands) = 'CTS_'//filters_instr
    ttype(3+5*nbands:) = 'eCTS_'//filters_instr

    call ftibin(15,nrows,ncols,ttype,tform,tunit,trim(FTHDUNAME)//'data',0,status)
    call ftpcld(15,1,frow,felem,nrows,ra,status)
    call ftpcld(15,2,frow,felem,nrows,dec,status)
    do n = 1,nbands
       call ftpcld(15,2+n,frow,felem,nrows,ph(:,n),status)
    end do
    do n = 1,nbands
       call ftpcld(15,2+nbands+n,frow,felem,nrows,dph(:,n),status)
    end do
    do n = 1,nbands
       call ftpcld(15,2+2*nbands+n,frow,felem,nrows,flux(:,n),status)
    end do
    do n = 1,nbands
       call ftpcld(15,2+3*nbands+n,frow,felem,nrows,dflux(:,n),status)
    end do
    do n = 1,nbands
       call ftpcld(15,2+4*nbands+n,frow,felem,nrows,cts(:,n),status)
    end do
    do n = 1,nbands
       call ftpcld(15,2+5*nbands+n,frow,felem,nrows,dcts(:,n),status)
    end do

    deallocate(ttype,tform,tunit)

    call ftclos(15,status)
    call ftrprt('STDERR',status)

  end subroutine trawrite

end module fits_fotran
