!
!  Flat     Average of the scaled set of scientific images.
!  Copyright (C) 1997 - 2015  Filip Hroch, Masaryk University, Brno, CZ
!
!  This file is part of Munipack.
!
!  Munipack is free software: you can redistribute it and/or modify
!  it under the terms of the GNU General Public License as published by
!  the Free Software Foundation, either version 3 of the License, or
!  (at your option) any later version.
!  
!  Munipack is distributed in the hope that it will be useful,
!  but WITHOUT ANY WARRANTY; without even the implied warranty of
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!  GNU General Public License for more details.
!  
!  You should have received a copy of the GNU General Public License
!  along with Munipack.  If not, see <http://www.gnu.org/licenses/>.

Program aFlat

  use LibList
  use stat
  use robustmean
  use rfun
  use fitsio
  use xfitsio
  use iso_fortran_env

  implicit none

  ! numerical precision of real numbers
  integer, parameter :: dbl = selected_real_kind(15)

  character(len=*), parameter :: afid = 'FLAT'

  ! Default Output image name:
  character(len=FLEN_FILENAME) :: nameflat='flat.fits', backup

  ! No. of image dimensions
  integer, parameter :: DIM = 2

  ! Default output mean level:
  real :: level = -1

  ! saturation
  real :: saturate = -1

  integer :: i,j,eq,nobs, istat, naxis,n, iter
  integer, dimension(DIM) :: naxes
  integer :: bitpix = -32
  logical :: verbose = .false.

  ! image
  real :: avg, sig, t, savg, dt, g, egain, sgain
  real :: darketime = -1
  real :: etime = -1
  real :: xdark = -1.0
  real :: gain = 1
  logical :: gain_set = .false.
  logical :: gain_estimate = .false.
  real, allocatable, dimension(:,:) :: flat,dflat,flsig,res,des
  real, allocatable, dimension(:) :: fbuf, egains
  real, dimension(:,:), pointer :: ccd
  real(dbl) :: r
  logical, allocatable, dimension(:,:) :: mask
  Character(len=4*FLEN_FILENAME) :: sb,key,val,record,buf,biasname='',darkname=''
  Character(len=FLEN_CARD) :: dateobs,f,dateobs1,filter1,imagetyp1,imagetyp
  character(len=FLEN_KEYWORD) :: FILTER=FITS_KEY_FILTER,DATE_OBS=FITS_KEY_DATEOBS, &
       EXPTIME=FITS_KEY_EXPTIME, KEY_IMAGETYP=FITS_KEY_IMAGETYP, KEY_GAIN=FITS_KEY_GAIN,&
       KEY_SATURATE=FITS_KEY_SATURATE
  type(Imagetype), pointer :: list,curr
  type(xFITS) :: fits, bias, dark

  buf = ''
  filter1 = ''
  imagetyp1 = ''

  call xfits_init(bias)
  call xfits_init(dark)

  call InitList(list)
  curr => list

  do
     read(*,'(a)',end=20) record

     eq = index(record,'=')
     if( eq == 0 ) stop 'Improper input.'
     key = record(:eq-1)
     val = record(eq+1:)

     if( key == 'OUTPUT' ) then
        read(val,*) nameflat, backup
     endif

     if( key == 'BITPIX' ) then
        read(val,*) bitpix
     endif

     if( key == 'LEVEL' ) then
        read(val,*) level
     endif

     if( key == 'SATURATE' ) then
        read(val,*) saturate
     endif

     if( key == 'VERBOSE' ) then
        read(val,*) verbose
     endif

     if( key == 'FITS_KEY_FILTER' ) then
        read(val,*) FILTER
     endif

     if( key == 'FITS_KEY_DATEOBS' ) then
        read(val,*) DATE_OBS
     endif

     if( key == 'FITS_KEY_EXPTIME' ) then
        read(val,*) EXPTIME
     endif

     if( key == 'FITS_KEY_IMAGETYP' ) then
        read(val,*) KEY_IMAGETYP
     endif

     if( key == 'FITS_KEY_GAIN' ) then
        read(val,*) KEY_GAIN
     endif

     if( key == 'FITS_KEY_SATURATE' ) then
        read(val,*) KEY_SATURATE
     endif

     if( key == 'GAIN_ESTIMATE' ) then
        read(val,*) gain_estimate
     endif

     if( key == 'BIAS' ) then

        read(val,*) biasname
        if( verbose ) write(error_unit,*) "BIAS=",trim(biasname)
        call xfits_read(biasname,bias)
        if( .not. bias%status ) stop 'Failed to load the bias frame.'

     end if

     if( key == 'XDARK' ) then
        read(val,*) xdark
     end if

     if( key == 'DARK' ) then

        read(val,*) darkname
        if( verbose ) write(error_unit,*) "DARK=",trim(darkname)
        call xfits_read(darkname,dark)
        if( .not. dark%status ) stop 'Failed to load the dark frame.'

        istat = 0
        call xfits_kye(dark,EXPTIME,darketime,istat)
        if( istat /= 0 ) then
           istat = 0
           darketime = -1
           if( verbose ) &
                write(error_unit,*) "An exposure time for dark frame unknown."
        end if

     end if

     if( key == 'GAIN' ) then
        read(val,*) gain
        gain_set = .true.
     endif

     if( key == 'FILE' ) then

        if( verbose .and. getno(curr) == 0 ) &
             write(error_unit,*) "Filename, robust mean, abs. dev.:"

        read(val,*) sb
        if( verbose ) write(error_unit,'(a)',advance="no") trim(sb)//":"

        call xfits_init(fits)
        call xfits_read(sb,fits)
        if( .not. fits%status ) then
           call xfits_deallocate(fits)
           goto 665
        end if
                         
        istat = 0
        call xfits_kys(fits,DATE_OBS,dateobs,istat)
        if( istat /= 0 ) then
           istat = 0
           dateobs = ''
        end if

        call xfits_kys(fits,FILTER,f,istat)
        if( istat /= 0 ) then
           istat = 0
           f = ''
        end if
        if( f == '' .and. verbose ) &
             write(error_unit,'(a)',advance="no") "Warning: empty filter .. "

        call xfits_kys(fits,KEY_IMAGETYP,imagetyp,istat)
        if( istat /= 0 ) then
           istat = 0
           imagetyp = ''
        end if

        call xfits_kye(fits,EXPTIME,etime,istat)
        if( istat /= 0 ) then
           istat = 0
           etime = -1
        end if

        if( .not. gain_set ) then
           call xfits_kye(fits,KEY_GAIN,gain,istat)
           if( istat /= 0 ) then
              write(error_unit,*) &
                   "Warning: Gain keyword not found in FITS header of `", &
                   trim(sb),"' (default is 1)."
              istat = 0
              gain = 1
           end if
        end if

        if( saturate < 0 ) then
           call xfits_kye(fits,KEY_SATURATE,saturate,istat)
           if( istat /= 0 ) then
              istat = 0
              saturate = huge(saturate)
           end if
        end if

        ! first image => initialisation
        if( getno(curr) == 0 )then

           naxis = fits%naxis
           naxes = fits%naxes
              
           dateobs1 = dateobs
           filter1 = f
           imagetyp1 = imagetyp

           if( bias%status ) then
              if( sum(abs(bias%naxes - naxes)) /= 0 ) stop "Bias has different size."
           end if

           if( dark%status ) then
              if( sum(abs(dark%naxes - naxes)) /= 0 ) stop "Dark has different size."
           end if

        else

           if( sum(abs(naxes - fits%naxes)) > 0 ) then
              write(error_unit,*) &
                   "Dimensions of images mutually does not corresponds. Skipping."
              goto 665
           endif

           if( imagetyp1 /= imagetyp ) then
              write(error_unit,*) &
                   "Image-types mutually does not corresponds. Skipping."
              goto 665
           endif


           if( f /= filter1 ) &
                write(error_unit,'(a)',advance="no") "Warning: uncompatible filters .."

        endif

        ! prepare corrected flat
        if( xdark > 0 )then
           t = xdark
        else if( etime > 0 .and. darketime > 0 )then
           t = etime / darketime
        else
           t = 1
        end if

        ! pre-correct input flat
        if( bias%status ) fits%image = fits%image -   bias%image
        if( dark%status ) fits%image = fits%image - t*dark%image
        fits%image = gain * fits%image

        ! determine mean levels
        allocate(fbuf(naxes(1)*naxes(2)))
        fbuf = pack(fits%image,.true.)
        call rmean(fbuf,avg,savg,sig)
        deallocate(fbuf)

        ! check saturation
        t = avg + 5*sig
        if( t > saturate ) then
           write(error_unit,'(a,1pg0.2,a,g0.2,a)') &
                " Warning: High checked level value is over saturation (", &
                t," > ",saturate,"). Skipping."
           goto 665
        end if

        ! add image
        allocate(ccd(naxes(1),naxes(2)))

        ! pre-scale image
        ccd = fits%image

        if( verbose ) write(error_unit,'(2x,1pg0.5,2x,1pg0.3)') avg,sig

        sig = sqrt(avg)

        Call AddItem (curr,array=ccd,filename=sb,skymod=avg,skysig=sig, &
             dateobs=dateobs,filter=f)
           
665     continue
        call xfits_deallocate(fits)

     end if

  enddo

20 continue
  nobs = getno(curr)
  
  if( nobs == 0 ) stop 'No input image(s).'

  if( verbose ) then
     write(error_unit,*)
     write(error_unit,*) 'Number of input images:',nobs
     write(error_unit,*) 'Dimension:',naxes(1),'x',naxes(2)
     write(error_unit,*) 'Filter: ',trim(filter1)
     write(error_unit,*) 'Preparing a flat-field frame ...'
  end if

  ! compute averadged flat-field
  allocate(flat(naxes(1),naxes(2)),dflat(naxes(1),naxes(2)),flsig(naxes(1),naxes(2)),&
       res(naxes(1),naxes(2)),des(naxes(1),naxes(2)),mask(naxes(1),naxes(2)), &
       fbuf(naxes(1)*naxes(2)),egains(nobs))
       

  do iter = 1,2

     ! mean flat
     do j = 1,naxes(2)
        do i = 1,naxes(1)

           curr => list
           do 
              curr => GetNext(curr)
              if( .not. associated(curr) ) exit
              call GetItem(curr,array=ccd,skymod=avg)
              fbuf(getno(curr)) = ccd(i,j) / avg
           enddo

           call rmean(fbuf(1:nobs),flat(i,j),dflat(i,j),flsig(i,j))
        enddo
     enddo

     if( iter == 2 ) exit

     if( verbose ) then
        write(error_unit,*) 'Iteration:',iter
        if( gain_estimate ) then
           write(error_unit,*) "Filename,      mean,    rel. Newton's correction,    std.dev.,  gain:"
        else
           write(error_unit,*) "Filename,      mean,    rel. Newton's correction:"
        end if
     end if


     ! Precising approximations, thanks to very good initial estimate
     ! (and very slow derivations), we are plan to proceed a few (one) iteration(s).
     ! The converegence in Newton's iterations is rapid, under limit of
     ! std. deviation of mean is reached after one cycle.


     ! residuals     
     curr => list
     do 
        curr => GetNext(curr)
        if( .not. associated(curr) ) exit
        call GetItem(curr,filename=sb,array=ccd,skymod=avg,skysig=sig)

        mask = ccd > epsilon(ccd) .and. ccd < saturate
        where( mask )
           des = sqrt(ccd)
           res = (ccd - avg*flat) / des
        end where
        n = count(mask)

        t = 0
        dt = 0
        do i = 1,size(ccd,1)
           do j = 1,size(ccd,2)
              if( mask(i,j) ) then
                 r = res(i,j)
                 t = t + real(huber(r))
                 dt = dt - real(dhuber(r))*flat(i,j)/sqrt(ccd(i,j))
              end if
           end do
        end do

        if( verbose ) then
           write(error_unit,'(a,1p,2x,g13.7,2x,1pg10.1)',advance="no") &
             trim(sb)//": ", avg,(t/dt)/avg

           if( gain_estimate ) then
              fbuf = pack(res, mask)
              call rmean(fbuf(1:n),t,savg,sig)
              g = 1/sig**2
              write(error_unit,'(1x,f11.3,2x,f6.3)') sig,g
              egains(curr%i) = g
           else
              write(error_unit,*)
           end if
        end if

        avg = avg - t/dt
        call SetItem(curr,skymod=avg)

     enddo

  end do


  ! final robust mean over the whole area
  fbuf = pack(flat,.true.)
  call rmean(fbuf,avg,t,sig)

  ! multiply to another mean level
  if( level > 0 ) then
     flat = level * flat
     dflat = level * dflat
     flsig = level * flsig
     avg = level
     sig = level * sig
  end if

  if( gain_estimate ) then
     call rmean(egains,egain,sgain,t)
  end if

  if( verbose ) then
     write(error_unit,*) 'Output image: ',trim(nameflat)
     write(error_unit,'(a,3x,1pg0.7,2(2x,g0.1))') 'Final mean, sig., std.dev.:',avg,t,sig
     if( gain_estimate ) write(error_unit,*) 'Estimated gain, std.dev:',egain,sgain
  end if

  ! Output image
  istat = 0
  call fitsbackup(nameflat,backup,istat)
  call ftinit(26,nameflat,1,istat)
  call ftphps(26,bitpix,naxis,naxes,istat)
  call ftpkye(26,'MEAN',avg,6,'Mean level',istat)
  call ftpkye(26,'SIG',sig,2,'Standard deviation',istat)
  call ftpkye(26,KEY_GAIN,gain,6,'[e-/ADU] gain',istat)
  if( gain_estimate ) then
     call ftpkye(26,'GAIN_AVG',egain,6,'[e-/ADU] estimated gain',istat)
     call ftpkye(26,'GAIN_STD',sgain,2,'[e-/ADU] std.dev of estimated gain',istat)
  end if
  if( filter1 /= '' ) &
       call ftpkys(26,FILTER,filter1,'filter of the first on input',istat)
  if( imagetyp1 /= '' ) &
     call ftpkys(26,KEY_IMAGETYP,imagetyp,'image type',istat)
  call ftpkys(26,DATE_OBS,dateobs1,'UTC of the first on input',istat)
  if( bitpix > 0 ) then
     call ftpkye(26,'BSCALE',1.0,10,'',istat)
     call ftpkye(26,'BZERO',2.0**(bitpix-1),10,'',istat)
  endif

  if( nobs > 0 ) then
     write(buf,'(a,i0,a)') 'Result of robust average and scaling of ',nobs,' exposure(s).'
     call ftpcom(26,buf,istat)
     call ftpcom(26,'File name, time of start, average, abs. dev. for each image used:',istat)
     curr => list
     do
        curr => GetNext(curr)
        if( .not. associated(curr) ) exit
        call GetItem(curr,filename=sb,dateobs=dateobs,skymod=avg,skysig=sig)
        write(f,'(1pg0.3,1x,1pg0.3)') avg,sig
        call ftpcom(26,"'"//trim(sb)//"' '"//trim(dateobs)//"' "//trim(f),istat)
     enddo
     call ftpcom(26,'All data are derived from '//trim(DATE_OBS)//' and '//trim(FILTER)//' keywords.',istat)
  endif

  if( darkname /= '' ) then
     write(buf,'(f0.5)') t
     call ftphis(26,afid//" dark: '"//trim(darkname)//"' *"//trim(buf),istat)
  end if
           
  if( biasname /= '' ) then
     call ftphis(26,afid//" bias: '"//trim(biasname)//"'",istat)
  end if

  call ftukys(26,FITS_KEY_CREATOR,FITS_VALUE_CREATOR,FITS_COM_CREATOR,istat)
  call ftpcom(26,MUNIPACK_VERSION,istat)

  ! flat
  call ftp2de(26,1,naxes(1),naxes(1),naxes(2),flat,istat)
  if( istat == NUMERICAL_OVERFLOW ) istat = 0

  ! standard deviation of mean
  call ftiimg(26,bitpix,naxis,naxes,istat)
  call ftukys(26,'EXTNAME',EXT_STDDEV,'',istat)
  call ftpcom(26,'The estimation of standard deviation of mean of flat-field',istat)
  call ftpcom(26,'(approximatelly SIG extension divided by sqrt(n)).',istat)
  call ftp2de(26,1,naxes(1),naxes(1),naxes(2),dflat,istat)
  if( istat == NUMERICAL_OVERFLOW ) istat = 0

  ! standard deviation
  call ftiimg(26,bitpix,naxis,naxes,istat)
  call ftukys(26,'EXTNAME',EXT_SIG,'',istat)
  call ftpcom(26,'The estimation of standard deviation of the flat-field',istat)
  call ftpcom(26,'(mean quadratical deviation, dispersion).',istat)
  call ftp2de(26,1,naxes(1),naxes(1),naxes(2),flsig,istat)
  if( istat == NUMERICAL_OVERFLOW ) istat = 0

  call ftclos(26,istat)

  if( allocated(flat) ) deallocate(flat,dflat,flsig,res,des,mask,fbuf,egains)
  call DestroyList(list)
  call xfits_deallocate(bias)
  call xfits_deallocate(dark)

  if( istat == 0 ) then
     stop 0
  else
     call ftrprt('STDERR',istat)
  end if


end Program AFlat

