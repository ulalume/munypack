!
! coloring
!
!
! Copyright © 2010-2 F.Hroch (hroch@physics.muni.cz)
!
! This file is part of Munipack.
!
! Munipack is free software: you can redistribute it and/or modify
! it under the terms of the GNU General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
! 
! Munipack is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.
! 
! You should have received a copy of the GNU General Public License
! along with Munipack.  If not, see <http://www.gnu.org/licenses/>.
!

program mcoloring

  use coloring_book

  implicit none

  integer, parameter :: slen = 32768
  character(len=slen) :: record,radek,line,cname,backup,cspace
  character(len=slen),dimension(:),allocatable :: fnames
  character(len=10),dimension(3) :: key
  integer :: n,i,j,istat,nband = 0

  cname = 'color.fits'
  cspace = ''
  key(1) = 'INFO'           ! <argument> print some information
  key(2) = 'OUTPUT'         ! name of output file (default 'color.fits')
  key(3) = 'COLORSPACE'     ! data stored in the colorspace(default 'XYZ')

  ! NBAND key sets number of input bands (3 only?)
  ! line without key a file with single band is specified, sorted 
  ! by increased wavelength (eg. BVR)

  ! read input data
  n = 0
  do
     read(*,'(a)',end=99,iostat=istat) record
     if( istat /= 0 ) then
        write(*,*) 'Unexpected data on input.'
        stop 34
     end if

     j = index(record,'#')
     if( j == 0 ) then
        radek = record
     else
        radek = record(1:j-1)
     end if

     do i = 1, size(key)
        j = index(radek,trim(key(i)))
        if( j > 0 ) line = radek(index(radek,'=')+1:)

        if( j > 0 .and. key(i) == 'INFO' ) then
           write(*,*) 'info [parameters]'
           stop 0
        end if

        if( j > 0 .and. key(i) == 'OUTPUT' ) then
           read(line,*,iostat=istat) cname,backup
           if( istat /= 0 .or. cname == '') then
              write(*,*) 'An error during reading of the output file name.'
              stop 34
           end if
        end if

        if( j > 0 .and. key(i) == 'COLORSPACE' ) then
           read(line,*,iostat=istat) cspace
           if( istat /= 0 .or. cspace == '' ) then
              write(*,*) 'An error during reading of the input colorspace.'
              stop 34
           end if
        end if

     end do

     if( j == 0 ) then

        if( index(radek,'NBAND') > 0 ) then

           read(radek(index(radek,'=')+1:),*,iostat=istat) nband
           if( istat /= 0 .or. nband <= 0 ) then
              write(*,*) 'NBAND parameter read failed.'
              stop 34
           end if
           if( allocated(fnames) ) then
              write(*,*) 'NBAND multiply specified ?'
              stop 35
           end if
           allocate(fnames(nband))

        else if( nband > 0 .and. n < nband .and. radek /= ' ') then

           n = n + 1
           read(radek,*,iostat=istat) fnames(n)
           if( istat /= 0 ) then
              write(*,*) 'Failed to parse of an input record.'
              stop 34
           end if

        end if

     end if

  end do
99 continue

  if( n /= nband ) then
     write(*,*) 'A full set of FITS files not specified.'
     if( allocated(fnames) ) deallocate(fnames)
     stop 33
  end if

  if( cspace == '' ) then
     write(*,*) 'Input colorspace not specified.'
     if( allocated(fnames) ) deallocate(fnames)
     stop 33
  end if

  call coloring(cspace,fnames,cname,backup)

  stop 0

end program mcoloring
