!
! astrometry
!
!
! Copyright © 2011-5 F.Hroch (hroch@physics.muni.cz)
!
! This file is part of Munipack.
!
! Munipack is free software: you can redistribute it and/or modify
! it under the terms of the GNU General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
! 
! Munipack is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.
! 
! You should have received a copy of the GNU General Public License
! along with Munipack.  If not, see <http://www.gnu.org/licenses/>.
!

! TODO:
!
!   * add all WCS related projections
!   * add catalogue/reference matching identifier
!
!  testing:
!   * improve high-precision algorithm to fit all parameters simulatenously
!   * rewrite to use Penalty function to non-fitting parameters (centre)

! TODO for 0.5.7
!   * add mirror projection (done)
!   * add FITS extension with astrometry results
!   * filtering input for double stars (on photography or digital camera)
!   * try to select better triangles firstly
!   * crossmatching by backtracking
!   * provide manual sequences as a reference parameter
!   * estimate dispersion simultaneously
!   * use Rayleigh statistics (done)

program astrometry

  use fitsio
  use astrofits
  use astrofitting
  use astromatcher
  use iso_fortran_env

  implicit none

  integer, parameter :: dbl = selected_real_kind(15)
  real(dbl), parameter :: rad = 57.295779513082322865_dbl

  character(len=4*FLEN_FILENAME) :: record, key, val
  character(len=FLEN_FILENAME) :: ref = '', cat = '', rel = '', &
       output = '', filename = '', backup = '', outname
  character(len=FLEN_VALUE) :: col_ra = 'RA', col_dec = 'DEC', &
       col_pmra = '', col_pmdec = '', col_mag = ''

  character(len=FLEN_VALUE) :: type = 'GNOMONIC', fit = 'ROBUST', aunits = '',&
       match='BACKTRACKING'
  character(len=FLEN_VALUE) :: rframe = '', idcat = '', key_epoch = 'EPOCH', &
       dateobs = 'DATE-OBS'
  real(dbl), allocatable, dimension(:) :: alpha,delta,pmalpha,pmdelta,x,y,ares,dres, &
       flux, rflux
  integer,  allocatable, dimension(:) :: id1,id2
  integer,  allocatable, dimension(:) :: i1,i2
  character(len=FLEN_COMMENT), allocatable, dimension(:) :: com
  real(dbl), dimension(2) :: crpix, crval, dcrval, xcrpix, xcrval, zcrval
  real(dbl) :: tepoch,jd,sc,dsc,pa,dpa,rms,a,da,b,db,s,ds,w,h,sm,refl
  real(dbl) :: sig2 = 1.0, sig1= 1.0/3600.0, xsig = 3.0, fsig = -1
  integer :: eq,n,status
  integer :: minmatch = 5, maxmatch = 50
  logical :: fcrpix = .false., fcrval = .false., &
       initpar = .true., matched = .false., rmscheck = .true., fluxtest = .true., &
       verbose = .false., wcssave = .true., plog = .false., fullmatch = .false.

  n = 0

  ! read input parameters
  do
     read(*,'(a)',end=99) record

     eq = index(record,'=')
     if( eq == 0 ) stop 'Improper input.'
     key = record(:eq-1)
     val = record(eq+1:)

    ! astrometrical parameters

     if( key == 'PROJECTION' ) then

        ! IMPORTANT: the projection must * PRECEDE * both CAT and REF

        read(val,*) type
        
     else if( key == 'FIT' ) then

        read(val,*) fit

     else if( key == 'AUNITS' ) then

        read(val,*) aunits

     else if( key == 'WCSSAVE' ) then

        read(val,*) wcssave

     else if( key == 'VERBOSE' ) then

        read(val,*) verbose

     else if( key == 'PIPELOG' ) then

        read(val,*) plog

     else if( key == 'CRPIX' ) then

        read(val,*) crpix
        fcrpix = .true.

     else if( key == 'CRVAL' ) then

        read(val,*) crval
        fcrval = .true.

     else if( key == 'SCALE' ) then

        read(val,*) sc

     else if( key == 'ANGLE' ) then

        read(val,*) pa

     else if( key == 'SIG' ) then

        read(val,*) sig2

     else if( key == 'SIGCAT' ) then

        read(val,*) sig1

     else if( key == 'FSIG' ) then

        read(val,*) fsig

     else if( key == 'FLUXCHECK' ) then

        read(val,*) fluxtest

     else if( key == 'INITPAR' ) then

        read(val,*) initpar


     ! match parameters

     else if( key == 'MATCH' ) then

        read(val,*) match

     else if( key == 'MINMATCH' ) then

        read(val,*) minmatch

     else if( key == 'MAXMATCH' ) then

        read(val,*) maxmatch

     else if( key == 'FULLMATCH' ) then

        read(val,*) fullmatch

     else if( key == 'RMSCHECK' ) then

        read(val,*) rmscheck

     else if( key == 'COL_RA' ) then

        read(val,*) col_ra

     else if( key == 'COL_DEC' ) then

        read(val,*) col_dec

     else if( key == 'COL_PMRA' ) then

        read(val,*) col_pmra

     else if( key == 'COL_PMDEC' ) then

        read(val,*) col_pmdec

     else if( key == 'COL_MAG' ) then

        read(val,*) col_mag

     else if( key == 'FITS_KEY_EPOCH' ) then
        
        read(val,*) key_epoch

     else if( key == 'FITS_KEY_DATEOBS' ) then
        
        read(val,*) dateobs

     else if( key == 'REL' ) then

        read(val,*) rel

        call readfile(rel,dateobs,w,h,x,y,rflux,xcrpix,tepoch,xcrval,sc,pa,refl,status)
        if( status /= 0 ) then
           write(error_unit,*) "Failed to open reference frame:`"//trim(rel)//"'" 
           stop 1
        end if

        type = ' '
        xcrval = 0.0_dbl
        sc = 1.0_dbl
        pa = 0.0_dbl
        n = min(size(x),maxmatch)
        allocate(alpha(n),delta(n),pmalpha(n),pmdelta(n))
        alpha = x(1:n) - xcrpix(1)
        delta = y(1:n) - xcrpix(2)
        pmalpha = 0.0_dbl
        pmdelta = 0.0_dbl
        deallocate(x,y)
        

     else if( key == 'REF' ) then

        read(val,*) ref

        call reffile(ref,dateobs,alpha,delta,rflux,tepoch,status)
        if( status /= 0 ) then
           write(error_unit,*) "Failed to open reference frame:`"//trim(ref)//"'" 
           stop 1
        end if

        if( initpar ) call init(alpha,delta,xcrval(1),xcrval(2))
        allocate(pmalpha(size(alpha)),pmdelta(size(delta)))
        pmalpha = 0.0_dbl
        pmdelta = 0.0_dbl

     else if( key == 'CAT' ) then

        read(val,*) cat

        call readtable(cat,col_ra,col_dec,col_pmra,col_pmdec,col_mag,&
             key_epoch,alpha,delta,pmalpha,pmdelta,rflux,rframe,idcat,tepoch,status)
        if( status /= 0 ) then
           write(error_unit,*) "Failed to open the catalogue file:`"//trim(cat)//"'" 
           stop 1
        end if
        if( initpar ) call init(alpha,delta,xcrval(1),xcrval(2))

     else if( key == 'FILE' ) then

        if( cat == '' .and. ref == '' .and. rel == '' ) then
           write(error_unit,*) 'A reference file (catalogue, frame) unspecified.'
           stop 1
        end if

        read(val,*) filename, backup, output

        if( verbose ) then
           write(*,*)
           write(*,*) "===== Astrometry calibration for `"//trim(filename)//"' ====="
        end if
        
        if( plog ) then
!           write(*,'(a)') "=> Astrometry calibration for `"//trim(filename)//"'."
!           write(*,'(a)') "=> Processing..."
           write(*,'(a)') "=ASTROMETRY> Start `"//trim(filename)//"'."
        end if

        status = 0

        call readfile(filename,dateobs,w,h,x,y,flux,crpix,jd,zcrval,sc,pa,refl,status)

        if( status /= 0 ) then
           write(error_unit,*) "Failed to open the file: `"//trim(filename)//"'"
           goto 77
        end if

        if( match(1:4) == 'NEAR' .and.  zcrval(1) < -360.0 ) then
           write(error_unit,*) "Matching by 'NEARLY' needs parameters (scale,..) or already calibrated frame."
           goto 77
        end if

        if( match(1:4) == 'NEAR' .and. zcrval(1) > -360.0 ) xcrval = zcrval
        if( .not. fcrval ) crval = xcrval

        if( match(1:4) == 'BACK' ) then
           if( plog ) then
!              write(*,'(a)') "=> Matching ..."
              write(*,'(a)') "=ASTROMATCH> Start"
           end if

           if( plog ) call uvhist(x,y)

           call astromatch(minmatch,maxmatch,type,alpha,delta,pmalpha,pmdelta,rflux, &
                x,y,flux,crval(1),crval(2),crpix(1),crpix(2),id1,id2,sig1,sig2,fsig,&
                fluxtest,tepoch,jd,fullmatch,verbose,plog,matched)
           !write(*,'(a,f10.7)') "=MATCH> End ",like

        else

           call listmatch(type,alpha,delta,pmalpha,pmdelta,x,y,tepoch,jd, &
                crval(1),crval(2),crpix(1),crpix(2),sc,pa,refl,xsig*sig2/sc,id1,id2,matched)

!           write(*,*) size(id1)
        end if

        if( .not. matched ) then
           if(plog) write(*,'(a)') "=ASTROMATCH> Finish Fail"

           write(*,'(a)') "=C> Mutual match for files `"//trim(filename)//"' and `"// &
                trim(cat)//trim(ref)//trim(rel)//"' failed."
!           write(*,'(a,f6.3,a,f6.3,a)') "=C> (probability under limit: probability =", &
!                like,' < validity=',0.5,")"
        else
           if( plog ) write(*,'(a)') "=ASTROMATCH> Finish Success"

           if( plog ) write(*,'(a)') "=ASTROFIT> Start"
!           if( verbose ) write(*,'(a)') "=> Fitting ..."
           call astrofit(fit,type,alpha,delta,pmalpha,pmdelta,x,y,sig2,xsig,&
                tepoch,jd,crpix(1),crpix(2),crval(1),dcrval(1),crval(2),dcrval(2), &
                sc,dsc,refl,rms,pa,dpa,ares,dres,id1,id2,i1,i2,rmscheck,verbose,plog)

           ! use a return flag instead ?
           if( size(i1) > 0 ) then

              if( plog ) then
                 write(*,'(a,e15.7)') "=ASTROFIT> Final ",rms
                 write(*,'(a)') "=ASTROFIT> Finish Success"
              end if
              
              allocate(com(14+size(i1)))
              
              a = cos(pa/rad)
              da = abs((dpa/rad)*sin(pa/rad))
              b = sin(pa/rad)
              db = abs((dpa/rad)*cos(pa/rad))
              s = 3600.0_dbl*sc
              ds = s*dsc/sc

              if( aunits == '' ) then
                 if( rel /= '' ) then
                    aunits = 'pix'
                 else if( log10(3600.0e6*rms) < 0.5 ) then
                    aunits = 'uas'
                 else if( log10(3600.0e3*rms) < 0.5 ) then
                    aunits = 'mas'
                 else if( log10(3600.0*rms) < 0.5 ) then
                    aunits = 'arcsec'
                 else if( log10(60.0*rms) < 0.5 ) then
                    aunits = 'arcmin'
                 else 
                    aunits = 'deg'
                 end if
              end if

              if( rel /= '' ) then
                 write(com(1),'(a)') "Type: relative"
                 write(com(2),'(a)') "Reference frame: "//trim(rel)
              else
                 write(com(1),'(a)') "Type: absolute"
!                 write(com(2),'(a)') "Coordinate reference frame: ",trim(rframe)
                 if( ref /= '' ) then
                    write(com(2),'(a)') "Reference frame: "//trim(ref)
                 else
                    write(com(2),'(a)') "Reference catalogue: "//trim(idcat)
                 end if
              end if
              write(com(3),'(a)') "Projection: "//trim(type)
              write(com(4),'(a,i0)') "Objects used = ",size(i1)
              if( aunits == 'arcsec' .or. aunits == 'arcmin' .or. &
                   aunits == 'deg' .or. aunits == 'mas' .or. aunits == 'uas') then
                 if( aunits == 'uas' ) then
                    sm = 3600.0e6_dbl
                 else if( aunits == 'mas' ) then
                    sm = 3600.0e3_dbl
                 else if( aunits == 'arcsec' ) then
                    sm = 3600.0_dbl
                 else if( aunits == 'arcmin' ) then
                    sm = 60.0_dbl
                 else if( aunits == 'deg' ) then
                    sm = 1.0_dbl
                 end if
                 
                 s = sm/sc
                 ds = s*dsc/sc
                 write(com(5),'(a,en12.1,3a)') "RMS = ",sm*rms,' [',trim(aunits),']'
                 write(com(6),'(a,g20.10,a,es9.1,3a)') "Scale = ",s," +- ",ds,' [',trim(aunits),'/pix]'
              else if( aunits == "pix" ) then
                 write(com(5),'(a,g20.10,a)') "RMS = ",rms,' [pix]'
                 write(com(6),'(a,g20.10,a,es9.1)') "Scale = ",sc," +- ",dsc
              end if
              write(com(7),'(a,f15.10,a,es9.1)') "cos(pa) = ",a," +- ",da
              write(com(8),'(a,f15.10,a,es9.1)') "sin(pa) = ",b," +- ",db
              write(com(9),'(a,f15.10,a,es9.1,a)') "Position Angle (pa)  = ",pa," +- ",dpa," [deg]"
              if( aunits == 'pix' ) then
                 write(com(10), '(a,f15.10,a,es9.1)') "Alpha center projection (CRVAL1) = ", &
                      crval(1)," +- ",dcrval(1)
                 write(com(11),'(a,f15.10,a,es9.1)') "Delta center projection (CRVAL2) = ", &
                      crval(2)," +- ",dcrval(2)
              else
                 write(com(10),'(a,f15.10,a,es9.1,a)') "Alpha center projection (CRVAL1) = ", &
                      crval(1)," +- ",dcrval(1)," [deg]"
                 write(com(11),'(a,f15.10,a,es9.1,a)') "Delta center projection (CRVAL2) = ", &
                      crval(2)," +- ",dcrval(2)," [deg]"
              end if
              write(com(12),'(a,f9.3,a)') "Horizontal center (CRPIX1) = ",crpix(1),' [pix]'
              write(com(13),'(a,f9.3,a)') "Vertical   center (CRPIX2) = ",crpix(2),' [pix]'

              if( aunits == 'uas' .or. aunits == 'mas' .or. aunits == 'arcsec' .or. aunits == 'arcmin' &
                   .or. aunits == 'deg' ) then 
                 write(com(14),'(3a)') 'Catalogue RA,DEC [deg]        Data X,Y [pix]     Residuals [',trim(aunits),']'
              else if( aunits == 'pix' ) then
                 write(com(14),'(a)') 'Reference X,Y [pix]       Data X,Y [pix]       Residuals [pix]'
              end if

              if( rel /= '' ) then
                 do n = 1, size(ares)
                    write(com(14+n),'(2f9.3,6x,2f9.3,1x,2f9.3)') &
                         alpha(i1(n))+xcrpix(1),delta(i1(n))+xcrpix(2),x(i2(n)),y(i2(n)),ares(n),dres(n)
                 end do
              else
                 do n = 1, size(ares)
                    write(com(14+n),'(2f13.8,2f9.3,2en12.1)') &
                         alpha(i1(n)),delta(i1(n)),x(i2(n)),y(i2(n)),sm*ares(n),sm*dres(n)
                 end do
              end if

!              if( plog ) write(*,'("=C> ",a)') (trim(com(n)),n=1,size(com))
              if( verbose ) write(*,'(a)') (trim(com(n)),n=1,size(com))

              
              if( wcssave ) then
                 if( plog ) write(*,'(a)') "=WCSSAVE> Start"

                 call fitsback(filename,backup,output,.false.,outname,status)
                 call wcsupdate(outname,type,crval,sc,pa,dpa,fcrpix,crpix,refl,rms,com,status)
                 
                 if( status == 0 ) then
                    if( plog ) write(*,'(a)') "=WCSSAVE> Finish Success"
                 else
                    if( plog ) write(*,'(a)') "=WCSSAVE> Finish Fail"
                    write(*,*) "Save of WCS failed: `"//trim(filename)//"' to `"//trim(outname)//"'."
                 end if

              end if

              deallocate(com)


           else
              write(error_unit,'(a)') "=> Astrometry of `"//trim(filename)//"' failed."
              if( plog ) write(*,'(a)') "=ASTROFIT> Finish Fail"
           end if

           if( allocated(ares)  ) deallocate(ares,dres,i1,i2)

        end if

77      continue
        if( allocated(x) ) deallocate(x,y,flux)
        if( allocated(id1) ) deallocate(id1,id2)

        if( plog ) write(*,'(a)') "=ASTROMETRY> Finish"

     end if


  end do
99 continue

  if( allocated(alpha) ) deallocate(alpha,delta,pmdelta,pmalpha,rflux)

  stop 0


contains

  subroutine init(a,d,acen,dcen)

    use robustmean

    real(dbl), dimension(:), intent(in) :: a,d
    real(dbl), intent(out) :: acen,dcen
    real(dbl) :: x

    call rmean(a,acen,x)
    call rmean(d,dcen,x)

  end subroutine init


end program astrometry
