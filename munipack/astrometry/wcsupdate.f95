!
! wcsupdate - update well-formated FITS header
!
!
! Copyright © 2011-5 F.Hroch (hroch@physics.muni.cz)
!
! This file is part of Munipack.
!
! Munipack is free software: you can redistribute it and/or modify
! it under the terms of the GNU General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
! 
! Munipack is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.
! 
! You should have received a copy of the GNU General Public License
! along with Munipack.  If not, see <http://www.gnu.org/licenses/>.
!

program wcsupdates

  use fitsio
  use astrofits

  implicit none

  integer, parameter :: ndim = 2
  integer, parameter :: dbl = selected_real_kind(15)
  integer, parameter :: clen = 80
  integer :: ncom, n
  character(len=4*FLEN_FILENAME) :: record, key, val, file='', &
       output = '',backup = '', outname
  real(dbl), dimension(ndim) :: crval = 0.0_dbl, crpix = 0.0_dbl
  real(dbl) :: scale = 1.0_dbl, angle = 0.0_dbl, dangle = epsilon(1.0_dbl), &
       refl = 1.0_dbl, rms = 0.0_dbl
  character(len=FLEN_COMMENT), dimension(:), allocatable :: com
  character(len=FLEN_VALUE) :: type = 'GNOMONIC'
  integer :: istat, eq, status
  logical :: fcrpix = .false.
  logical :: reflex = .false.
  logical :: verbose = .false.

  allocate(com(0))

  ncom = 0
  n = 0
  do
     read(*,'(a)',end=99) record

     eq = index(record,'=')
     if( eq == 0 ) stop 'Improper input.'
     key = record(:eq-1)
     val = record(eq+1:)

     if( key == 'PROJECTION' ) then

        read(val,*) type

     else if( key == 'CRPIX' ) then

        read(val,*) crpix
        fcrpix = .true.

     else if( key == 'CRVAL' ) then

        read(val,*) crval

     else if( key == 'SCALE' ) then

        read(val,*) scale

     else if( key == 'ANGLE' ) then

        read(val,*) angle

     else if( key == 'DANGLE' ) then

        read(val,*) dangle

     else if( key == 'REFLEX' ) then

        read(val,*) reflex
        if( reflex ) refl = -1.0_dbl

     else if( key == 'RMS' ) then

        read(val,*) rms

     else if( key == 'VERBOSE' ) then

        read(val,*) verbose

     else if( key == 'FILE' ) then

        read(val,*) file,backup,output

     else if( key == 'NCOM' ) then

        read(val,*,iostat=istat) ncom
        deallocate(com)
        allocate(com(ncom))
        
     else if( key == 'COM' ) then

        if( size(com) > 0 ) then
           n = n + 1
           if( n < size(com) ) read(val,*) com(n)
        end if

     end if


  end do
99 continue

  status = 0
  call fitsback(file,backup,output,.false.,outname,status)
  call wcsupdate(outname,type,crval,scale,angle,dangle,fcrpix,crpix,refl,rms,com,status)

  deallocate(com)
  stop 0

end program wcsupdates
