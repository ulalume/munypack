!
! wcsremove - remove astrometry calibration
!
!
! Copyright © 2012-3 F.Hroch (hroch@physics.muni.cz)
!
! This file is part of Munipack.
!
! Munipack is free software: you can redistribute it and/or modify
! it under the terms of the GNU General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
! 
! Munipack is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.
! 
! You should have received a copy of the GNU General Public License
! along with Munipack.  If not, see <http://www.gnu.org/licenses/>.
!

program wcsremoves

  use fitsio
  use astrofits

  implicit none

  integer :: eq,status
  character(len=4*FLEN_FILENAME) :: record, key, val,file, output, backup, outname
  logical :: verbose = .false.


  do
     file=''
     output = ''
     backup = ''
     
     read(*,'(a)',end=99) record

     eq = index(record,'=')
     if( eq == 0 ) stop 'Improper input.'
     key = record(:eq-1)
     val = record(eq+1:)

     if( key == 'VERBOSE' ) then

        read(val,*) verbose

     else if( key == 'FILE' ) then

        read(val,*) file,backup,output

        status = 0
        call fitsback(file,backup,output,.false.,outname,status)
        call wcsremove(outname,status)

     end if


  end do
99 continue

  stop 0

end program wcsremoves
