!
! robfit - fitting absolute deviations
!
! Copyright © 2011-5 F.Hroch (hroch@physics.muni.cz)
!
! This file is part of Munipack.
!
! Munipack is free software: you can redistribute it and/or modify
! it under the terms of the GNU General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
! 
! Munipack is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.
! 
! You should have received a copy of the GNU General Public License
! along with Munipack.  If not, see <http://www.gnu.org/licenses/>.
!

module robfit
  
  implicit none

  logical, private :: debug = .true.
  integer, parameter, private :: dbl = selected_real_kind(15)
  real(dbl), parameter, private :: rad = 57.295779513082322865_dbl
  real(dbl), dimension(:), allocatable, private :: x,y,u,v
  real(dbl), private :: mad, refl, xcen, ycen
  integer, private :: ndat

  private :: dres,minfun

contains

  subroutine dres(p,r,d)

    use astrotrafo

    real(dbl), dimension(:), intent(in) :: p
    real(dbl), dimension(:,:), intent(out) :: r
    real(dbl), dimension(:,:), intent(out), optional :: d

    real(dbl) :: xx,yy,du,dv,uu,vv
    integer :: i
    type(AstroTrafoProj) :: t

    call trafo_init(t,xcen=xcen,ycen=ycen, &
         ucen=p(1),vcen=p(2),scale=p(3),rot=rad*p(4),refl=refl)
    
    do i = 1, ndat
       call invaffine(t,x(i),y(i),uu,vv,xx,yy)
       du = u(i) - uu
       dv = v(i) - vv

       r(i,:) = (/du,dv/)
       if( present(d) ) d(i,:) = (/xx,yy/)
!       write(*,'(9f12.5)') u(i),v(i),x(i),y(i),3600*du,3600*dv
    end do
    
  end subroutine dres


  subroutine minfun(m,np,p,fvec,iflag)
    
    use rfun

    integer, intent(in) :: m,np
    integer, intent(inout) :: iflag
    real(dbl), dimension(np), intent(in) :: p
    real(dbl), dimension(m), intent(out) :: fvec
    real(dbl), dimension(:,:), allocatable :: r,d
    real(dbl) :: sf,sc,sx,sy,rx,ry,rpx,rpy,dx,dy
    integer :: i

    if( iflag == 0 .and. debug ) write(*,*) '#robfit t:',real(p)

    sf = 0.0_dbl
    sc = 0.0_dbl
    sx = 0.0_dbl
    sy = 0.0_dbl

    allocate(r(size(x),2),d(size(x),2))
    call dres(p,r,d)
    do i = 1, ndat
       dx = d(i,1)
       dy = d(i,2)
       rx = r(i,1)
       ry = r(i,2)
       rpx = huber(rx/mad)
       rpy = huber(ry/mad)
       sx = sx + rpx
       sy = sy + rpy
       sc = sc + (rpx*dx + rpy*dy)
       sf = sf + (rpx*dy - rpy*dx)
    end do
    fvec = (/-sx,-sy,-sc,sf*p(3)/)/mad
!    write(*,'(4e19.5)') fvec
    deallocate(r,d)

  end subroutine minfun
  

  subroutine robmin(type,a,d,xc,yc,rf,xx,yy,xmad,acen,dacen,dcen,ddcen,sc,dsc,pa,dpa,rms,verbose)

    ! find proper transformation by robust mean squares

    use rfun
    use astrotrafo
    use minpack
    use minpacks

    ! parameters
    character(len=*),intent(in) :: type
    real(dbl),dimension(:),intent(in) :: a,d,xx,yy
    real(dbl),intent(in) :: xmad,xc,yc,rf
    real(dbl),intent(inout) :: acen,dcen,sc,pa
    real(dbl),intent(out) :: dacen,ddcen,dsc,dpa,rms
    logical, intent(in) :: verbose

    real(dbl), parameter :: eps = 1e2*epsilon(1.0_dbl)
    integer, parameter :: npar = 4

    real(dbl),dimension(npar) :: p,dp
    real(dbl),dimension(npar,npar) :: fjac,cov
    real(dbl),dimension(:,:), allocatable :: res
    integer :: iter,i,info,nprint,nra,ndec
    real(dbl) :: s0,sum1,sum2,wx,wy
    type(AstroTrafoProj) :: t,ti

    debug = verbose

    if(debug) write(*,*) '=== Robust fitting ==='

    rms = huge(rms)
    refl = rf
    mad = xmad
    xcen = xc
    ycen = yc
    ndat = size(a)
    
    if( .not. (ndat > npar) ) stop 'Robust fitting needs five stars at least.'

    ! normalization
    allocate(x(ndat),y(ndat),u(ndat),v(ndat),res(ndat,2))
    x = xx
    y = yy

    call trafo_init(t,type,acen,dcen)
    call proj(t,a,d,u,v)
       
    ! setup
    p(1:2) = 0.0_dbl
    p(3) = 1.0_dbl / sc
    p(4) = pa / rad

    if( debug ) write(*,'(a)') &
         '# info   rms      acen         dcen        offset[pix]       [pix/deg] rot[deg]'

    do iter = 1, 13
       nprint = 0 ! or 1

       call lmdif2(minfun,p,eps,fjac,nprint,info)
       if( info == 0 ) stop 'Improper input parameters.'
       ! testing errors:
       ! if( info == 4 ? ) write(error_unit,*) 'No konvergence.'

       ! residual sum
       call dres(p,res)
       sum1 = 0.0_dbl
       sum2 = 0.0_dbl
       do i = 1, ndat
          wx = res(i,1)/mad
          wy = res(i,2)/mad
          sum1 = sum1 + huber(wx)**2 + huber(wy)**2
          sum2 = sum2 + dhuber(wx) + dhuber(wy)
       enddo

       ! robust estimate of sum of squares in minimum
       s0 = mad**2*sum1/sum2**2*ndat**2
       rms = sqrt(s0 / (ndat - npar))

       if( debug ) write(*,'(2i2,es10.2,2f11.5,2g12.3,f10.1,f7.1)') &
            iter,info,rms,acen,dcen,p(1:2)/p(3),1/p(3),rad*p(4)

       ! finish when parameters are appropriate small
       ! and iterations couldn't make any progress
       if( iter > 1 .and. all(abs(p(1:2)) < eps)  ) exit

       ! update center of projection and projected coordinates
       call trafo_init(ti,type,acen=acen,dcen=dcen, &
            xcen=xcen,ycen=ycen,scale=p(3),rot=p(4),refl=refl)
       call invproj(ti,p(1),p(2),acen,dcen)
       call trafo_init(t,type,acen,dcen)
       call proj(t,a,d,u,v)
       p(1:2) = 0.0_dbl

    end do

    ! statistical errors of parameters
    call qrinv(fjac,cov)
    if( debug ) then
       write(*,'(a)') '# Covariance matrix:'
       do i = 1,npar
          write(*,'(a,4g15.5)') '#',cov(i,:)
       end do
    end if

    do i = 1,npar
       dp(i) = sqrt(s0*max(cov(i,i),epsilon(cov))/(ndat - npar))
    end do

    sc = 1.0_dbl/p(3)
    pa = rad*p(4)

    ! deviations of output parameters
    dacen = rms / sqrt(real(ndat))
    ddcen = rms / sqrt(real(ndat))
    dsc = sc*(dp(3)/p(3))
    dpa = rad*dp(4)

    if( debug ) then
       nra = 0
       ndec = 0
       do i = 1, ndat
          if( res(i,1) > 0 ) nra  = nra + 1
          if( res(i,2) > 0 ) ndec = ndec + 1
       end do

       write(*,'(a,4es15.3)') '# solution:  ',p
       write(*,'(a,4es15.2)') '# deviations:',dp
       write(*,'(a,2es10.2,a,g0.3,a)') '# s0,rms: ',s0,rms,' [deg]    ', &
            3600*rms,' [arcsec]'
       write(*,'(a,i7,"/",f0.1,2(i5,"+-",f0.1))') '# sign test (total, RA+, Dec+): ',&
            ndat,ndat/2.0,nra,sqrt(nra*0.25),ndec,sqrt(ndec*0.25)
       ! simple version of sign test, var = n*p*(1-p), where p = 0.5
    end if

    deallocate(u,v,x,y,res)

  end subroutine robmin

end module robfit

