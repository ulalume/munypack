!
! astrofits - write WCS to FITS image & read star table
!
!
! Copyright © 2011-3, 2015 F.Hroch (hroch@physics.muni.cz)
!
! This file is part of Munipack.
!
! Munipack is free software: you can redistribute it and/or modify
! it under the terms of the GNU General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
! 
! Munipack is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.
! 
! You should have received a copy of the GNU General Public License
! along with Munipack.  If not, see <http://www.gnu.org/licenses/>.
!

module astrofits

  use iso_fortran_env
  use fitsio
  use astrotrafo
  use trajd

  implicit none

  integer, parameter, private :: dbl = selected_real_kind(15)

contains

  subroutine readtable(filename, label_alpha, label_delta, label_pmalpha, label_pmdelta,&
       label_mag, key_epoch, alpha, delta, pmalpha, pmdelta, refflux, rframe, catid, &
       epoch,status)

    character(len=*), intent(in) :: filename, label_alpha, label_delta, label_pmalpha, &
         label_pmdelta, label_mag, key_epoch
    real(dbl), allocatable, dimension(:), intent(out) :: alpha, delta, pmalpha, pmdelta, refflux
    character(len=*), intent(out) :: rframe, catid
    real(dbl), intent(out) :: epoch
    integer, intent(out) :: status

    real(dbl), parameter :: nullval = 1e33
    integer, parameter :: ncols = 5, frow = 1, felem = 1
    integer, dimension(ncols) :: col, statuses
    integer :: nrows,i
    logical :: anyf
    character(len=FLEN_CARD) :: buf
    character(len=FLEN_VALUE), dimension(ncols) :: label,colname

    label(1) = label_alpha
    label(2) = label_delta
    label(3) = label_pmalpha
    label(4) = label_pmdelta
    label(5) = label_mag

    status = 0
    buf = ''

    ! open and move to a table extension
    call fttopn(15,filename,0,status)
    call ftgnrw(15,nrows,status)

    if( status /= 0 ) goto 666

    ! define reference frame and identification of catalogue
    rframe = ''
    catid = ''
    call ftgkys(15,'EXTNAME',catid,buf,status)
    call ftgkyd(15,key_epoch,epoch,buf,status)
    if( status == KEYWORD_NOT_FOUND ) then
       status = 0
       epoch = yearjd(2000.0_dbl)   ! 2451545.0
    else
       epoch = yearjd(epoch)
    end if

    ! find columns by labels
    do i = 1, ncols
       colname(i) = ''
       call ftgcnn(15,.false.,label(i),colname(i),col(i),status)
       statuses(i) = status
       if( (i == 1 .or. i == 2) .and. status /= 0 ) goto 666
    end do
    status = 0

    if( nrows > 0 ) then

       if( statuses(1) == 0 .and. statuses(2) == 0) then
          allocate(alpha(nrows),delta(nrows))
          call ftgcvd(15,col(1),frow,felem,nrows,nullval,alpha,anyf,status)
          call ftgcvd(15,col(2),frow,felem,nrows,nullval,delta,anyf,status)
       else
          goto 666
       end if

       allocate(pmalpha(nrows),pmdelta(nrows))
       pmalpha = 0.0_dbl
       pmdelta = 0.0_dbl
       if( statuses(3) == 0 .and. statuses(4) == 0) then
          call ftgcvd(15,col(3),frow,felem,nrows,nullval,pmalpha,anyf,status)
          call ftgcvd(15,col(4),frow,felem,nrows,nullval,pmdelta,anyf,status)
          if( status == 0 ) then
             where( abs(pmalpha) < 0.1*nullval )
                pmalpha = pmalpha / 3.6e6_dbl  ! supposing mas/year
             elsewhere
                pmalpha = 0.0_dbl
             end where
             where( abs(pmdelta) < 0.1*nullval )
                pmdelta = pmdelta / 3.6e6_dbl
             elsewhere
                pmdelta = 0.0_dbl
             end where
          end if
       end if

       allocate(refflux(nrows))
       if( statuses(5) == 0 ) &
            call ftgcvd(15,col(5),frow,felem,nrows,nullval,refflux,anyf,status)

       if( status == 0 ) then
          refflux = 10.0**((25.0 - refflux)/2.5)
       else
          refflux = 1.0
       end if

    end if

    if( status /= 0 ) goto 666

    call ftclos(15,status)
    return

666 continue

    if( allocated(alpha) ) deallocate(alpha,delta)
    if( allocated(pmalpha) ) deallocate(pmalpha,pmdelta)
    if( allocated(refflux) ) deallocate(refflux)

    call ftclos(15,status)
    call ftrprt('STDERR',status)

  end subroutine readtable


  subroutine readfile(filename, key_dateobs, width, height, xcoo, ycoo, flux, crpix, jd, &
       crval, sc, pa, refl, status)

    use phio

    character(len=*), intent(in) :: filename, key_dateobs
    real(dbl), intent(out) :: width, height
    real(dbl), allocatable, dimension(:), intent(out) :: xcoo, ycoo, flux
    real(dbl), dimension(:), intent(out) :: crpix, crval
    real(dbl), intent(out) :: jd, sc, pa, refl
    integer,intent(out) :: status

    integer :: naxis, pcount,gcount, bitpix, nrows, n, ns
    integer, dimension(2) :: naxes
    logical :: simple,extend,anyf
    integer, parameter :: extver = 0, frow = 1, felem = 1
    real(dbl), allocatable, dimension(:) :: x,y,cts
    real(dbl), parameter :: nullval = 0.0_dbl
    real(dbl) :: sec
    integer :: year,m,d,h,min
    character(len=FLEN_CARD) :: colname1, colname2,line,buf
    integer :: col1, col2
    type(AstroTrafoProj) :: t

    status = 0
    call ftiopn(15,filename,0,status)
    if( status /= 0 ) goto 666

    call ftghpr(15,2,simple,bitpix,naxis,naxes,pcount,gcount,extend,status)
    if( status /= 0 .or. naxis /= 2 ) then
       if( naxis /= 2 ) then
            write(error_unit,*) trim(filename), &
                 ": Sorry, just two dimensional images are implemented yet."
            status = -1
         end if
       goto 666
    end if
    width = naxes(1)
    height = naxes(2)
    crpix = naxes/2.0_dbl

    line = ''
    buf = ''
    call ftgkys(15,key_dateobs,line,buf,status)
    call fts2tm(line,year,m,d,h,min,sec,status)
    if( status == 0 ) then
       jd = datjd(real(year,dbl),real(m,dbl),d+(h + (min + sec/60.0_dbl)/60.0_dbl)/24.0_dbl)
    else if( status == KEYWORD_NOT_FOUND ) then
       status = 0
       jd = 2451544.5_dbl
       write(error_unit,*) trim(filename),": Keyword `",trim(key_dateobs),"' not found. Set to J2000.0."
    else
       goto 666
    end if

    ! get calibration
    call wcsget(15,t,status)
    if( status == 0 ) then
       sc = t%scale
       pa = t%rot
       refl = t%refl
       crval = (/t%acen, t%dcen/)
       crpix = (/t%xcen, t%ycen/)
    else
       status = 0
       crval = -999.999
    end if

    ! FIND extension
    call ftmnhd(15,BINARY_TBL,FINDEXTNAME,extver,status)
    if( status == BAD_HDU_NUM ) then
       write(error_unit,*) trim(FINDEXTNAME)//': extension not found. Coordinates of objects are not available yet.'
       goto 666
    end if
    call ftgnrw(15,nrows,status)
    if( status /= 0 ) goto 666
    allocate(x(nrows),y(nrows))

    ! the column labels are specific for MUNIPACK extension only
    colname1 = ''
    colname2 = ''
    call ftgcnn(15,.false.,FITS_COL_X,colname1,col1,status)
    call ftgcnn(15,.false.,FITS_COL_Y,colname2,col2,status)

    call ftgcvd(15,col1,frow,felem,size(x),nullval,x,anyf,status)
    call ftgcvd(15,col2,frow,felem,size(y),nullval,y,anyf,status)

    ! photometry extension
    call ftmnhd(15,BINARY_TBL,APEREXTNAME,extver,status)
    if( status == BAD_HDU_NUM ) then
       status = 0
       ! aperture photometry is not available, no problem, we are continuing moreover
       ns = size(x)
       allocate(xcoo(ns),ycoo(ns),flux(ns))
       xcoo = x
       ycoo = y
       flux = 1.0
    else if( status == 0 ) then
       ! aperture photometry is available, we can use photometry informations
       ! to improve robustness
       call ftgcnn(15,.false.,trim(FITS_COL_APCOUNT)//"1",colname1,col1,status)
       allocate(cts(nrows))
       call ftgcvd(15,col1,frow,felem,size(cts),nullval,cts,anyf,status)
       
       ! we selecting stars with valid photometry only
       ns = count(cts > 0.0)
       allocate(xcoo(ns),ycoo(ns),flux(ns))
       ns = 0
       do n = 1,nrows
          if( cts(n) > 0.0 .and. ns < size(xcoo) ) then
             ns = ns + 1
             xcoo(ns) = x(n)
             ycoo(ns) = y(n)
             flux(ns) = cts(n)
          end if
       end do
       deallocate(cts)

    end if

    call ftclos(15,status)

    deallocate(x,y)

    return

666 continue

    if( allocated(x) ) deallocate(x,y)
    if( allocated(cts) ) deallocate(cts)

    call ftclos(15,status)
    call ftrprt('STDERR',status)

  end subroutine readfile


  subroutine reffile(filename,key_dateobs,alpha,delta,rflux,jd,status)

    character(len=*), intent(in) :: filename, key_dateobs
    real(dbl), allocatable, dimension(:), intent(out) :: alpha, delta, rflux
    real(dbl), intent(out) :: jd
    integer, intent(out) :: status

    real(dbl) :: w, h, sc, pa, refl
    real(dbl), allocatable, dimension(:) :: x,y
    real(dbl), dimension(2) :: crpix,crval
    type(AstroTrafoProj) :: t
        

    call readfile(filename,key_dateobs,w,h,x,y,rflux,crpix,jd,crval,sc,pa,refl,status)

    if( status == 0 .and. crval(1) > -666.0 .and. allocated(x) ) then

       allocate(alpha(size(x)),delta(size(x)))

       call trafo_init(t,"GNOMONIC",crval(1),crval(2),crpix(1),crpix(2),sc,pa,refl)
       call invtrafo(t,x,y,alpha,delta)

       deallocate(x,y)
    end if

  end subroutine reffile



!-----------------------------------------------------------------------------

  subroutine wcsupdate(file,type,crval,sc,pa,dpa,fcrpix,crpixels,refl,rms,com,status)

    use astrotrafo

    integer, parameter :: ndim = 2
    real(dbl), parameter :: rad = 57.295779513082322865_dbl

    character(len=*), intent(in) :: file, type
    real(dbl), dimension(:), intent(in) :: crpixels,crval
    real(dbl), intent(in) :: sc,pa,dpa,rms,refl
    logical, intent(in) :: fcrpix
    character(len=*), dimension(:), intent(in) :: com
    integer, intent(out) :: status

    integer :: naxis, pcount,gcount, bitpix
    integer, dimension(ndim) :: naxes
    real(dbl), dimension(ndim) :: crpix
    character(len=6), dimension(2), parameter :: keys = (/ 'CTYPE1', 'CTYPE2' /)
    logical :: simple,extend
    real(dbl) :: a,b,da,db,s,err
    real(dbl), dimension(2,2) :: mat,rmat
    type(AstroTrafoProj) :: tra
    character(len=5) :: un
    integer :: i

    if( type == " " ) then
       s = 1.0_dbl
       un = '[pix]'
    else
       s = -1.0_dbl
       un = '[deg]'
    end if

    a = cos(pa/rad)
    da = (dpa/rad)*sin(pa/rad)
    b = sin(pa/rad)
    db = (dpa/rad)*cos(pa/rad)
    err = rms/sqrt(2.0)

    ! clear possible previous calibration
    status = 0
    call wcsremove(file,status)

    status = 0
    call ftiopn(15,file,1,status)
    if( status /= 0 ) goto 666

    if( fcrpix ) then
       crpix = crpixels
    else
       call ftghpr(15,2,simple,bitpix,naxis,naxes,pcount,gcount,extend,status)
       if( status /= 0 ) goto 666
       crpix = naxes/2.0_dbl
    end if

    call ftukys(15,FITS_KEY_CREATOR,FITS_VALUE_CREATOR,FITS_COM_CREATOR,status)

    if( type == "GNOMONIC" )then
       call FTUKYS(15,'CTYPE1','RA---TAN','the coordinate type for the first axis',status)
       call FTUKYS(15,'CTYPE2','DEC--TAN','the coordinate type for the second axis',status)
    else if( type == " " ) then
       call ftpmrk
       do i = 1,size(keys)
          call ftdkey(15,keys(i),status)
          if( status == KEYWORD_NOT_FOUND ) status = 0
       end do
       call ftcmrk
    end if

    call FTUKYD(15,'CRVAL1',crval(1),16, &
         un//' first axis value at reference pixel',status)
    call FTUKYD(15,'CRVAL2',crval(2),16, &
         un//' second axis value at reference pixel',status)
    call FTUKYD(15,'CRDER1',err,1, &
         un//' random error in first axis',status)
    call FTUKYD(15,'CRDER2',err,1, &
         un//' random error in second axis',status)

    if( type == " " ) then
       call FTUKYS(15,'CUNIT1','pix','units of first axis',status)
       call FTUKYS(15,'CUNIT2','pix','units of second axis',status)
    else
       call FTUKYS(15,'CUNIT1','deg','units of first axis',status)
       call FTUKYS(15,'CUNIT2','deg','units of second axis',status)
    end if

    call trafo_init(tra,scale=1/sc,rot=pa,refl=refl)

    ! flip x-axis due to conversion rectangular -> spherical coordinates
    rmat(1,:) = (/-1.0_dbl,0.0_dbl/)
    rmat(2,:) = (/ 0.0_dbl,1.0_dbl/)
    mat = matmul(rmat,tra%mat)

    call FTUKYD(15,'CRPIX1',crpix(1),16, &
         'x-coordinate of reference pixel',status)
    call FTUKYD(15,'CRPIX2',crpix(2),16, &
         'y-coordinate of reference pixel',status)
    call FTUKYD(15,'CD1_1',mat(1,1),16, &
         'partial of first axis coordinate w.r.t. x',status)
    call FTUKYD(15,'CD1_2',mat(1,2),16, &
         'partial of first axis coordinate w.r.t. y',status)
    call FTUKYD(15,'CD2_1',mat(2,1),16, &
         'partial of second axis coordinate w.r.t. x',status)
    call FTUKYD(15,'CD2_2',mat(2,2),16, &
         'partial of second axis coordinate w.r.t. y',status)

    call ftpcom(15,BEGIN_ASTROMETRY,status)
    do i = 1, size(com)
       call ftpcom(15,com(i),status)
    end do

    ! no fitting
    if( size(com) < 1 ) call ftpcom(15,"Astrometry calibration provided by user.",status)

    call ftpcom(15,MUNIPACK_VERSION,status)
    call ftpcom(15,END_ASTROMETRY,status)

    call ftclos(15,status)
    return

666 continue

    call ftclos(15,status)
    call ftrprt('STDERR',status)

  end subroutine wcsupdate


!-----------------------------------------------------------------------------


  subroutine wcsremove(file,status)

    character(len=*), intent(in) :: file
    integer, intent(out) :: status
    integer :: i, nbegin, nend
    character(len=FLEN_CARD) :: card
    character(len=6), dimension(14) :: keys = (/ &
         'CTYPE1', 'CTYPE2','CRVAL1','CRVAL2','CRDER1','CRDER2', &
         'CRPIX1','CRPIX2','CUNIT1','CUNIT2','CD1_1 ','CD1_2 ', &
         'CD2_1 ','CD2_2 ' /)
         
    status = 0
    call ftiopn(15,file,1,status)
    if( status /= 0 ) goto 666

    call ftpmrk
    do i = 1, size(keys)
       call ftdkey(15,keys(i),status)
       if( status == KEYWORD_NOT_FOUND ) status = 0
    end do
    call ftcmrk

    i = 0
    nbegin = 0
    nend = 0
    card = ''
    do
       i = i + 1
       call ftgrec(15,i,card,status)
       if( status /= 0) exit
       if( card(9:) == BEGIN_ASTROMETRY ) nbegin = i
       if( card(9:) == END_ASTROMETRY ) nend = i
    end do
    if( status == 203 ) status = 0

    if( nbegin > 0 ) then
       do i = nbegin, nend
          ! looks strangle, but its is correct because records in header
          ! are moved up imediately after remove
          call ftdrec(15,nbegin,status)
       end do
    end if

    call ftclos(15,status)
    return

666 continue

    call ftclos(15,status)
    call ftrprt('STDERR',status)

  end subroutine wcsremove

end module astrofits
