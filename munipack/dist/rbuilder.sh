#!/bin/sh
#
# Creation of RPM binary linux package
#
# Usage:
#
# $ rbuilder.sh distdir
#
#  *  the output will be placed in $RPMBUILD/RPMS as munipack-*.rpm
#
# Description of detailed usage:
#
#     http://integral.physics.muni.cz/rpmbuilder.html
# 
# Please, check these utilities before:
#  * g++ and gfortran compilers
#  * autoconf, automake
#  * cmake
#  * gtk2-devel
#  * imagemagic
#  * rpm-build
#
# Content of distdir:
#  cfitsio*.tar.gz        rawtran-*.tar.gz
#  munipack-*.tar.gz      wxWidgets-*.tar.bz2
#  fitspng-*.tar.gz
#  libpng-*.tar.gz   
#
# optionaly:
#
#  konve-*.tar.gz
#  picko-*.tar.gz
#
#
#  http://www.advenage.com/topics/binary-RPM.php
#

set -x
set -e

# functions

tarname ()
{
  find $1 -name "$2" -type f | sort | tail -1
}

tdirname ()
{
  XC=$(echo $1 | awk '{if(/\.tar\.gz$/){print "z"} else if(/\.tar\.bz2$/){print "j"};}')
  tar tf${XC} $1 | awk '{FS="/"; if(FNR==2) print $1;}'
}

makeinstall ()
{
  cd $1
  make install
  cd $TOPDIR
}

makeexternal()
{
    TARBALL=$1

    if [ "$TARBALL" ]; then
	DIR=$(tar ztf ${TARBALL} | awk '{FS="/"; if(FNR==2) print $1;}')
    fi

    if [ "$TARBALL" -a "$DIR" ]; then
	rm -rf $DIR
	tar zxf $TARBALL
	cd $DIR
	./configure --prefix=$PREFIX
	make install
	cd $TOPDIR
        BINARIES="$BINARIES ${DIR%-*}"
    fi
}

# end of functions


DISTDIR=$1

if [ -z "$DISTDIR" ]; then
    echo "$0 distdir"
    exit 0
fi 

TOPDIR=$PWD
BUNDLEDIR=/opt/munipack
PREFIX=$TOPDIR/install
RPMBUILD=$TOPDIR/rpmbuild
RPMDIR=$RPMBUILD/BUILDROOT/munipack
RPMOPT=$RPMDIR/$BUNDLEDIR
SPEC=munipack.spec

BINARIES="xmunipack munipack"

MUNIPACK_TAR=$(tarname $DISTDIR munipack-*.tar.gz)
FITSPNG_TAR=$(tarname $DISTDIR fitspng-*.tar.gz)
RAWTRAN_TAR=$(tarname $DISTDIR rawtran-*.tar.gz)
KONVE_TAR=$(tarname $DISTDIR konve-*.tar.gz)
PICKO_TAR=$(tarname $DISTDIR picko-*.tar.gz)
CFITSIO_TAR=$(tarname $DISTDIR cfitsio*.tar.gz)
WXWIDGETS_TAR=$(tarname $DISTDIR wxWidgets*.tar.bz2)
LIBPNG_TAR=$(tarname $DISTDIR libpng*.tar.gz)

MUNIPACK_DIR=$(tar ztf ${MUNIPACK_TAR} | awk '{FS="/"; if(FNR==2) print $1;}')
if [ "$FITSPNG_TAR" ]; then
  FITSPNG_DIR=$(tar ztf ${FITSPNG_TAR} | awk '{FS="/"; if(FNR==2) print $1;}')
fi
if [ "$RAWTRAN_TAR" ]; then
  RAWTRAN_DIR=$(tar ztf ${RAWTRAN_TAR} | awk '{FS="/"; if(FNR==2) print $1;}')
fi
if [ "$KONVE_TAR" ]; then
  KONVE_DIR=$(tar ztf ${KONVE_TAR} | awk '{FS="/"; if(FNR==2) print $1;}')
fi
if [ "$PICKO_TAR" ]; then
  PICKO_DIR=$(tar ztf ${PICKO_TAR} | awk '{FS="/"; if(FNR==2) print $1;}')
fi
CFITSIO_DIR=$(tdirname $CFITSIO_TAR)
WXWIDGETS_DIR=$(tdirname $WXWIDGETS_TAR)
LIBPNG_DIR=$(tdirname $LIBPNG_TAR)

# environment setup
KERNEL=$(uname -s)
ARCH=$(uname -m)
#CPPFLAGS="-DNDEBUG -I$PREFIX/include"
CPPFLAGS="-I$PREFIX/include"
CFLAGS=-O2
CXXFLAGS=-O2
FCFLAGS=-O2
STATIC_LDFLAGS="-static-libgfortran -static-libgcc -static-libstdc++"
LDFLAGS="-L$PREFIX/lib -Wl,-rpath,$BUNDLEDIR/lib"
PKG_CONFIG_PATH=$PREFIX/lib/pkgconfig
LD_LIBRARY_PATH=$PREFIX/lib
export CPPFLAGS CFLAGS CXXFLAGS FCFLAGS LDFLAGS PKG_CONFIG_PATH LD_LIBRARY_PATH

# be sure to prevent crash while paralle make
unset MAKEFLAGS

# remove older builds
rm -rf $TOPDIR/rhell/
rm -rf $TOPDIR/install/
rm -rf $MUNIPACK_DIR
rm -rf $FITSPNG_DIR
rm -rf $RAWTRAN_DIR
rm -rf $KONVE_DIR
rm -rf $PICKO_DIR

mkdir -p $RPMDIR
mkdir -p $RPMOPT

# cFITSIO, http://heasarc.gsfc.nasa.gov/fitsio/
if [ ! -d "$CFITSIO_DIR" ]; then
  tar zxf $CFITSIO_TAR
  cd $CFITSIO_DIR
  ./configure --prefix=$PREFIX --enable-reentrant
  make shared
  cd $TOPDIR
fi
makeinstall $CFITSIO_DIR

# wxWidgets, http://www.wxwidgets.org/downloads/
if [ ! -d "$WXWIDGETS_DIR" ]; then
  tar jxf $WXWIDGETS_TAR
  cd $WXWIDGETS_DIR
  ./configure --with-zlib=builtin \
    --with-libpng=builtin --with-libjpeg=builtin --with-libtiff=builtin \
    --with-regex=builtin --with-expat=builtin  \
    --enable-graphics_ctx --enable-stl --enable-baseevtloop \
    --disable-compat28 \
    --without-odbc --without-opengl --without-libmspack --without-hildon \
    --without-gnomeprint --disable-xrc --disable-aui --disable-richtext  \
    --disable-ribbon --disable-stc --disable-propgrid --disable-gl \
    --prefix=$PREFIX
  make
  cd $TOPDIR
fi
makeinstall $WXWIDGETS_DIR

# preset PATH to use wx-config created by us (prevent to use system wide ones)
PATH=$PREFIX/bin:$PATH

# libpng, http://www.libpng.org/pub/png/libpng.html
if [ ! -d "$LIBPNG_DIR" ]; then
   tar zxf $LIBPNG_TAR
   cd $LIBPNG_DIR
   ./configure --prefix=$PREFIX
   make
   cd $TOPDIR
fi
makeinstall $LIBPNG_DIR

# Munipack
LDFLAGS="$STATIC_LDFLAGS $LDFLAGS "
export LDFLAGS
tar zxf $MUNIPACK_TAR
cd $MUNIPACK_DIR
./bootstrap $PREFIX/share/aclocal
#autoreconf -i -f -I $PREFIX/share/aclocal
./configure --prefix=$PREFIX --enable-bundle
make install
cd $TOPDIR

MVERSION=$(cat $MUNIPACK_DIR/configure.ac | tr '[]' ' ' | awk '{if(/AC_INIT/) print $4;}')

# fitspng
makeexternal $FITSPNG_TAR

# rawtran
makeexternal $RAWTRAN_TAR

# konve
makeexternal $KONVE_TAR

# picko
makeexternal $PICKO_TAR

# docpage
DOC=$PREFIX/share/doc/munipack/html
cd $MUNIPACK_DIR/doc && make mini && make install && \
    tar cf - . | (mkdir -p $DOC && cd $DOC && tar xf - )

cd $PREFIX
tar cf - . \
    --exclude '*include*' \
    --exclude '*locale*' --exclude '*aclocal*' --exclude '*bakefile*' \
    --exclude '*pkgconfig*' --exclude 'lib/libcfitsio.a' \
    --exclude 'lib/libpng*.a' --exclude 'lib/libpng*.la' \
    --exclude 'bin/*-config' --exclude 'share/man/man3/libpng*' \
    --exclude 'share/man/man5/png*' --exclude 'lib/wx*' \
    --exclude 'lib/fortran' \
| (cd $RPMOPT && tar xf - )

cp $TOPDIR/$MUNIPACK_DIR/dist/muni-pack.desktop $RPMOPT/share/munipack/

rmdir $RPMOPT/share/man/man3/ $RPMOPT/share/man/man5/

cat > $TOPDIR/$SPEC <<EOF
Summary: Munipack
Name: munipack
Version: $MVERSION
Release: 1
License: GPL3
Group: Science
URL: http://munipack.physics.muni.cz
BuildArch: $ARCH
Requires: dcraw

%description
Munipack is a general astronomical image software package. 
It provides both command line and graphical interfaces for processing 
of huge sets of images. Currently implemented functions includes tools 
for a basic reduction, aperture photometry, astrometry, matching and 
composition of images. All tools are developed in mind of benefits 
offered by robust statistical methods.

%files
%defattr(-,root,root)
EOF

cd $RPMDIR
find . ! -type d | sed 's/^\.//' >> $TOPDIR/$SPEC


cat >> $TOPDIR/$SPEC <<EOF

%post
cd /usr/bin
for B in $BINARIES; do
    ln -sf $BUNDLEDIR/bin/\$B
done

cd /usr/share/man/man1
for B in $BINARIES; do
    ln -sf $BUNDLEDIR/share/man/man1/\${B}.1
done

XDG_DESKTOP_MENU="\`which xdg-desktop-menu 2> /dev/null\`"
if [ -x "\$XDG_DESKTOP_MENU" ]; then
    \$XDG_DESKTOP_MENU install $BUNDLEDIR/share/munipack/muni-pack.desktop
fi

%postun
XDG_DESKTOP_MENU="\`which xdg-desktop-menu 2> /dev/null\`"
if [ -x "\$XDG_DESKTOP_MENU" ]; then
    \$XDG_DESKTOP_MENU uninstall $BUNDLEDIR/share/munipack/muni-pack.desktop
fi

for B in $BINARIES; do
   rm -f /usr/bin/\$B
   rm -f /usr/share/man/man1/\${B}.1
done

cd $BUNDLEDIR && rm -rf *
rmdir $BUNDLEDIR
EOF

cd $TOPDIR
rpmbuild --buildroot $RPMDIR --define "_rpmdir $RPMBUILD/RPMS"  -bb $SPEC
